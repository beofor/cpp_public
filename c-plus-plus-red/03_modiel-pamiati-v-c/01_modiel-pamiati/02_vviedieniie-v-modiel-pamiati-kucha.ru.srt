1
00:00:00,000 --> 00:00:07,930
В предыдущем видео мы с вами узнали,

2
00:00:07,930 --> 00:00:11,923
что локальные переменные функции
хранятся в специальной области памяти,

3
00:00:11,923 --> 00:00:13,370
которая называется стек.

4
00:00:13,370 --> 00:00:16,445
Хорошо, теперь давайте
рассмотрим другой пример.

5
00:00:16,445 --> 00:00:22,352
У нас есть функция main, в которой
объявлены два вектора целых чисел,

6
00:00:22,352 --> 00:00:27,089
ints1 и ints 2,
а также в этой же функции объявлены

7
00:00:27,089 --> 00:00:30,239
два потока ввода из строк, is1 и is2.

8
00:00:30,239 --> 00:00:34,790
В первом потоке хранятся числа 1,
2, 3, 4, во втором — 5 и 7.

9
00:00:34,790 --> 00:00:39,569
Кроме того, у нас есть функция fill,
которая принимает,

10
00:00:39,569 --> 00:00:43,921
собственно, поток ввода и
вектор целых чисел по ссылке.

11
00:00:43,921 --> 00:00:45,958
Делает она простую вещь.

12
00:00:45,958 --> 00:00:51,128
Она читает из потока ввода числа
последовательно и помещает их в вектор.

13
00:00:51,128 --> 00:00:55,666
Собственно, этой функцией mail
мы пользуемся достаточно просто.

14
00:00:55,666 --> 00:01:00,543
Мы в первый вектор помещаем числа,
которые хранятся в первом потоке.

15
00:01:00,543 --> 00:01:04,023
А во второй вектор помещаем
числа из второго потока.

16
00:01:04,023 --> 00:01:09,925
Таким образом, по итогам работы этой
программы в векторе ins1 будут числа 1,

17
00:01:09,925 --> 00:01:14,830
2, 3, 4, в векторе ins2 будут числа 5 и 7.

18
00:01:14,830 --> 00:01:20,019
Хорошо, теперь давайте зададим вопрос.

19
00:01:20,019 --> 00:01:26,370
А где в памяти хранятся эти числа,
которые мы поместили в наши вектора?

20
00:01:26,370 --> 00:01:31,280
Дело здесь в том, что на этапе
компиляции компилятор не знает,

21
00:01:31,280 --> 00:01:35,795
сколько чисел будет помещено
в каждый из векторов.

22
00:01:35,795 --> 00:01:41,440
И поэтому он не может заранее
зарезервировать на стеке память,

23
00:01:41,440 --> 00:01:46,110
необходимую для хранения этих чисел.

24
00:01:46,110 --> 00:01:47,911
Что мы могли бы предположить?

25
00:01:47,911 --> 00:01:49,504
Мы могли бы предположить,

26
00:01:49,504 --> 00:01:55,150
что эта память берётся ниже
стекового фрейма функции fill.

27
00:01:55,150 --> 00:01:57,450
Что имеется ввиду?

28
00:01:57,450 --> 00:02:04,844
Давайте представим,
что сейчас выполняется функция fill.

29
00:02:04,844 --> 00:02:10,238
Вы можете видеть состояние
стека в момент её исполнения.

30
00:02:10,238 --> 00:02:13,555
Кстати, обратите внимание, мы говорили,

31
00:02:13,555 --> 00:02:17,968
что локальные переменные функции
хранятся в стековом фрейме,

32
00:02:17,968 --> 00:02:22,160
так вот для функции fill у нас в
стековом фрейме не только переменная x,

33
00:02:22,160 --> 00:02:27,152
которая у нас здесь объявлена,
но и формальные параметры

34
00:02:27,152 --> 00:02:31,805
этой функции is и ints,
они тоже являются локальными переменными.

35
00:02:31,805 --> 00:02:35,953
И для них также резервируется
место на стеке.

36
00:02:35,953 --> 00:02:42,244
Итак, вот ситуация,
в которой у нас выполняется функция fill.

37
00:02:42,244 --> 00:02:48,955
Мы предполагаем, что числа,
которые она поместит в вектор,

38
00:02:48,955 --> 00:02:53,809
помещаются ниже её стекового фрейма,
то есть после того,

39
00:02:53,809 --> 00:02:58,600
как функция fill завершится, мы ожидаем,
что стек будет выглядеть таким образом.

40
00:02:58,600 --> 00:03:03,124
Вершина стека находится
уже на функции main,

41
00:03:03,124 --> 00:03:07,880
а ниже фрейма функции fill у нас
разместились четыре целых числа.

42
00:03:07,880 --> 00:03:12,066
Но это предположение,
конечно же, неверное,

43
00:03:12,066 --> 00:03:19,953
потому что второй запуск функции
fill просто перезатрёт те числа,

44
00:03:19,953 --> 00:03:26,356
которые поместил в стек
первый запуск функции.

45
00:03:26,356 --> 00:03:31,195
То есть запускаем нашу функцию fill
во второй раз, вы можете видеть,

46
00:03:31,195 --> 00:03:35,169
что её стековый фрейм снова
перестал быть бледным.

47
00:03:35,169 --> 00:03:37,627
То есть сейчас работает функция fill.

48
00:03:37,627 --> 00:03:42,318
И когда она завершит свою работу,
то элементы второго вектора по

49
00:03:42,318 --> 00:03:46,713
нашему предположению будут
помещены под фрейм функции fill.

50
00:03:46,713 --> 00:03:53,480
Соответственно, они перезатрут
элементы первого вектора.

51
00:03:53,480 --> 00:03:55,888
И, естественно, мы понимаем,

52
00:03:55,888 --> 00:04:01,395
что для хранения элементов векторов ints1,
ints2 стек нам не подходит.

53
00:04:01,395 --> 00:04:04,639
Нам нужен какой-то другой источник памяти.

54
00:04:04,639 --> 00:04:09,310
И, собственно, тут важный момент:
почему нам не подошел стек?

55
00:04:09,310 --> 00:04:13,008
Дело в том, что вот эти целые числа,

56
00:04:13,008 --> 00:04:17,310
они как бы создаются внутри функции fill,

57
00:04:17,310 --> 00:04:22,206
когда мы считываем их из входного потока.

58
00:04:22,206 --> 00:04:28,348
Но жить эти объекты, эти целые числа,
должны дольше, чем работает функция fill.

59
00:04:28,348 --> 00:04:32,551
Вот если бы вектор был
объявлен внутри функции fill,

60
00:04:32,551 --> 00:04:37,160
наполнялся бы в ней и
разрушался после её завершения,

61
00:04:37,160 --> 00:04:41,260
то такое можно было бы
реализовать на стеке.

62
00:04:41,260 --> 00:04:44,512
Для этого есть специальные средства,
мы пока их не рассматривали,

63
00:04:44,512 --> 00:04:45,360
но можно бы было.

64
00:04:45,360 --> 00:04:50,270
Здесь же проблема в том, что эти объекты,
создаваемые функцией fill,

65
00:04:50,270 --> 00:04:53,566
они должны жить дольше,
чем живёт функция fill.

66
00:04:53,566 --> 00:04:55,907
Именно поэтому стек нам не подходит,

67
00:04:55,907 --> 00:05:00,722
потому что по мере работы программы
вызываются разные функции и память на

68
00:05:00,722 --> 00:05:06,800
стеке постоянно перезатирается
новыми вызовами других функций.

69
00:05:06,800 --> 00:05:12,485
Поэтому, ещё раз скажем,
для хранения элементов

70
00:05:12,485 --> 00:05:17,077
вектора нам стек не подходит и
нам нужен другой источник памяти.

71
00:05:17,077 --> 00:05:21,580
И этот источник называется
куча или по-английски heap.

72
00:05:21,580 --> 00:05:26,960
Во время работы программы,
во время исполнения программы она может

73
00:05:26,960 --> 00:05:32,336
в любой момент обращаться в кучу
и выделять в ней блок памяти

74
00:05:32,336 --> 00:05:38,705
произвольного размера или же
освобождать ранее выделенные.

75
00:05:38,705 --> 00:05:42,580
И именно в куче стандартный вектор,

76
00:05:42,580 --> 00:05:46,660
которым мы все с вами пользуемся,
хранит свои элементы.

77
00:05:46,660 --> 00:05:47,952
И давайте посмотрим,

78
00:05:47,952 --> 00:05:53,208
как программа из нашего примера использует
кучу для хранения элементов вектора.

79
00:05:53,208 --> 00:06:00,296
Итак, мы снова запускаем функцию fill
и концентрируемся на том моменте,

80
00:06:00,296 --> 00:06:06,236
когда она работает,
она считывает числа и пополняет вектора.

81
00:06:06,236 --> 00:06:12,254
После того, как она закончит свою работу,
в куче будет выделен блок памяти,

82
00:06:12,254 --> 00:06:15,875
достаточный для хранения
четырёх целых чисел.

83
00:06:15,875 --> 00:06:19,900
И в этом блоке будут хранится числа 1,
2, 3, 4.

84
00:06:19,900 --> 00:06:24,871
При этом вектор inst1,
объявленный нашей функцией main,

85
00:06:24,871 --> 00:06:30,820
будет некоторым образом ссылаться
на эту область памяти в куче,

86
00:06:30,820 --> 00:06:34,522
и таким образам он будет иметь
доступ к этим элементам.

87
00:06:34,522 --> 00:06:38,480
Как он ссылается,
мы с вами рассмотрим в ближайшее время.

88
00:06:38,480 --> 00:06:43,731
Соответственно, когда у нас отработает
функция fill во второй раз,

89
00:06:43,731 --> 00:06:48,680
когда она наполнит второй вектор,
то в куче будет выделен другой

90
00:06:48,680 --> 00:06:53,944
отдельный блок памяти,
достаточный для хранения двух целых чисел.

91
00:06:53,944 --> 00:06:59,094
И там будут храниться элементы 5 и 7,
а вектор ints2,

92
00:06:59,094 --> 00:07:03,909
он будет ссылаться на
них на этот блок памяти.

93
00:07:03,909 --> 00:07:06,916
Обратите внимание на важное отличие.

94
00:07:06,916 --> 00:07:11,627
Когда мы выдвигали нашу гипотезу
про хранение элементов в стеке,

95
00:07:11,627 --> 00:07:15,192
то там второй вызов
функции перетирал первый.

96
00:07:15,192 --> 00:07:19,928
Здесь же каждый вызов создаёт
отдельный блок памяти в куче и

97
00:07:19,928 --> 00:07:22,477
там хранит элементы вектора.

98
00:07:22,477 --> 00:07:26,691
Таким образом,
у нас такие данные не перетираются,

99
00:07:26,691 --> 00:07:29,350
когда они этого делать не должны.

100
00:07:29,350 --> 00:07:31,506
Давайте подведём итоги.

101
00:07:31,506 --> 00:07:32,976
Мы с вами узнали,

102
00:07:32,976 --> 00:07:39,010
что в модели памяти языка C++ есть два
источника памяти — это стек и куча.

103
00:07:39,010 --> 00:07:43,080
В стеке размещаются локальные
переменные функции,

104
00:07:43,080 --> 00:07:47,079
а также служебная информация
для возврата из функции.

105
00:07:47,079 --> 00:07:51,625
В куче же размещаются те объекты,
которые должны жить дольше,

106
00:07:51,625 --> 00:07:53,477
чем создавшая их функции.

107
00:07:53,477 --> 00:07:58,238
Мы это увидели на примере функции fill,
которая заполняла вектора.