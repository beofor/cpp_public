1
00:00:00,000 --> 00:00:06,080
Вы уже порешали разные

2
00:00:06,080 --> 00:00:11,292
задачи на move семантику,
научились неплохо ей пользоваться,

3
00:00:11,292 --> 00:00:15,223
но местами код мог получиться не
очень простым, не очень компактным.

4
00:00:15,223 --> 00:00:18,830
Давайте мы разберём одну конструкцию
языка, которая позволяет сделать

5
00:00:18,830 --> 00:00:23,294
использование move семантики более простым
и более понятным читателю вашего кода.

6
00:00:23,294 --> 00:00:27,981
Рассмотрим задачу SplitIntoSentences,
в которой вам нужно было написать функцию,

7
00:00:27,981 --> 00:00:31,360
которая принимает набор токенов
и разбивает их на предложения.

8
00:00:31,360 --> 00:00:34,093
Как выглядело решение,
которое мы опубликовали?

9
00:00:34,093 --> 00:00:37,080
Там есть функция,
которая находит конец предложения,

10
00:00:37,080 --> 00:00:41,228
и в целом она никак с move семантикой не
связана, просто на алгоритмы и λ функции.

11
00:00:41,228 --> 00:00:42,800
Её мы трогать сейчас не будем.

12
00:00:42,800 --> 00:00:46,063
А есть, собственно,
сама функция SplitIntoSentences,

13
00:00:46,063 --> 00:00:49,177
которую вы видите на экране,
в которой что происходит?

14
00:00:49,177 --> 00:00:53,467
В которой мы заводим некоторый итератор
tokens_begin и идём этим итератором по

15
00:00:53,467 --> 00:00:54,040
токенам.

16
00:00:54,040 --> 00:00:57,725
Мы находим конец предложения очередного,

17
00:00:57,725 --> 00:01:03,430
и теперь именно здесь у нас move семантика
используется, в этих пяти строчках.

18
00:01:03,430 --> 00:01:09,237
Мы берём текущий токен,
идём этим итератором от текущего

19
00:01:09,237 --> 00:01:15,311
токена до конца предложения и все эти
токены вставляем в текущее предложение,

20
00:01:15,311 --> 00:01:19,860
а затем это предложение
вставляем в вектор предложений.

21
00:01:19,860 --> 00:01:23,180
Давайте посмотрим,
в первую очередь, на вот этот цикл.

22
00:01:23,180 --> 00:01:27,912
Этот цикл делает следующее: он перебирает
все токены от одного до другого

23
00:01:27,912 --> 00:01:31,510
(просто в диапазоне токенов) и в
каждый из них вставляет вектор.

24
00:01:31,510 --> 00:01:33,265
Зачем мы для этого писали цикл?

25
00:01:33,265 --> 00:01:36,125
Казалось бы,
у нас для этого некоторые алгоритмы.

26
00:01:36,125 --> 00:01:39,549
Вот если бы мы не знали про move
семантику, как бы мы написали этот цикл?

27
00:01:39,549 --> 00:01:41,712
Мы бы, наверное,
написали что-нибудь такое.

28
00:01:41,712 --> 00:01:43,470
Давайте я вот эту часть закомментирую.

29
00:01:43,470 --> 00:01:48,495
Попробую написать что-нибудь попроще,
без оглядки на move семантику.

30
00:01:48,495 --> 00:01:51,742
Наверное, я бы взял и создал предложение.

31
00:01:51,742 --> 00:01:55,880
Я напомню, что это,
на самом деле, просто вектор.

32
00:01:55,880 --> 00:02:02,320
Sentence от токен у вектора (ну вот это,
правда, вектор).

33
00:02:02,320 --> 00:02:04,454
У вектора есть конструктор
по двум итераторам.

34
00:02:04,454 --> 00:02:11,200
Соответственно, я мог бы создать
предложение от диапазона токенов.

35
00:02:11,200 --> 00:02:18,794
Диапазон токенов у нас следующий:
tokens_begin и sentence_end.

36
00:02:18,794 --> 00:02:25,760
Казалось бы, я создал просто
предложение по диапазону токенов.

37
00:02:25,760 --> 00:02:28,724
Но здесь, конечно,
будут происходить копирования.

38
00:02:28,724 --> 00:02:31,950
Копирования токенов из вот
этого диапазона в предложение.

39
00:02:31,950 --> 00:02:36,975
Но вспомните важную мысль нашего блока,
одну из основных: что если

40
00:02:36,975 --> 00:02:42,000
где-то в коде происходит копирование,
то там же можно сделать и перемещение.

41
00:02:42,000 --> 00:02:42,711
Так и здесь.

42
00:02:42,711 --> 00:02:46,725
Вот в этом алгоритме, на самом деле,
в конструкторе вектора происходит

43
00:02:46,725 --> 00:02:49,850
копирование токенов; можно
сделать там перемещение.

44
00:02:49,850 --> 00:02:52,046
Как это сделать?

45
00:02:52,046 --> 00:02:57,291
Мы подключим модуль итератор
и после этого нам станет

46
00:02:57,291 --> 00:03:02,464
доступна специальная функция,
которую мы вызовем от итераторов.

47
00:03:02,464 --> 00:03:06,310
Функция называется make_move_iterator.

48
00:03:06,310 --> 00:03:14,964
Я вызываю её от этого итератора
и от этого итератора.

49
00:03:14,964 --> 00:03:19,098
Я получаю диапазон move итераторов.

50
00:03:19,098 --> 00:03:23,886
Что такое move итератор,
что возвращает функция make_move_iterator?

51
00:03:23,886 --> 00:03:26,695
Она возвращает некоторую
обёртку над итератором,

52
00:03:26,695 --> 00:03:30,610
которая если мы из неё хотим скопировать
объект, на самом деле, его перемещает.

53
00:03:30,610 --> 00:03:35,593
По сути как функция move меняет семантику
переменной, чтобы она вела себя как

54
00:03:35,593 --> 00:03:39,416
временный объект, так и функция
make_move_iterator меняет семантику

55
00:03:39,416 --> 00:03:44,085
итератора так, чтобы при обращении к нему
данные бы перемещались, а не копировались.

56
00:03:44,085 --> 00:03:48,200
Соответственно, вот так можно проще
создать предложения — без цикла.

57
00:03:48,200 --> 00:03:51,729
Конструктором от двух move итераторов.

58
00:03:51,729 --> 00:03:56,193
А дальше я хочу это предложение
положить в вектор предложений.

59
00:03:56,193 --> 00:03:59,991
Я мог бы написать
sentences.push_back(move(sentence)) вот

60
00:03:59,991 --> 00:04:00,520
так вот.

61
00:04:00,520 --> 00:04:07,430
Но зачем я создал переменную
sentence и тут же из неё помувал?

62
00:04:07,430 --> 00:04:13,470
Наверное, я могу просто в
push_back создать это предложение.

63
00:04:13,470 --> 00:04:19,330
Вот как-нибудь

64
00:04:19,330 --> 00:04:23,780
так.

65
00:04:23,780 --> 00:04:27,215
Я непосредственно при вызове
push_back создаю объект типа

66
00:04:27,215 --> 00:04:31,850
sentence от token и внутри в
него передаю move итератора.

67
00:04:31,850 --> 00:04:33,696
Казалось бы, всё.

68
00:04:33,696 --> 00:04:37,610
Давайте попробуем запустить unit тесты.

69
00:04:37,610 --> 00:04:41,590
Вот unit тесты,
которые исходно были даны в той задаче.

70
00:04:41,590 --> 00:04:45,370
Давайте я запущу и посмотрю, что будет.

71
00:04:45,370 --> 00:04:51,179
Код компилируется.

72
00:04:51,179 --> 00:04:55,940
Он скомпилировался и что-то пошло не так.

73
00:04:55,940 --> 00:04:58,620
Что же мы забыли в нашем коде?

74
00:04:58,620 --> 00:05:02,668
Давайте я остановлю и
посмотрю внимательно.

75
00:05:02,668 --> 00:05:07,515
В этом цикле я не просто
складывал токены в предложения,

76
00:05:07,515 --> 00:05:11,784
я ещё и изменял итератор tokens_begin,
который у меня един на все итерации.

77
00:05:11,784 --> 00:05:14,580
А здесь я итератор
tokens_begin изменить забыл.

78
00:05:14,580 --> 00:05:16,862
Поэтому давайте я его изменю.

79
00:05:16,862 --> 00:05:20,660
tokens_begin равно — каким должен стать

80
00:05:20,660 --> 00:05:25,280
этот итератор в самом
конце — sentence_end.

81
00:05:25,280 --> 00:05:29,215
И теперь я снова скомпилирую и запущу код.

82
00:05:29,215 --> 00:05:31,190
Код компилируется.

83
00:05:31,190 --> 00:05:33,412
Он скомпилировался, запустился.

84
00:05:33,412 --> 00:05:35,210
И мы видим, что тест прошёл.

85
00:05:35,210 --> 00:05:38,546
Здесь, правда, есть некоторые
сообщения от статического анализатора.

86
00:05:38,546 --> 00:05:43,590
На самом деле, если их удалить и код
перекомпилировать, то всё будет хорошо.

87
00:05:43,590 --> 00:05:44,292
Итак.

88
00:05:44,292 --> 00:05:48,156
Мы познакомились с move итераторами,

89
00:05:48,156 --> 00:05:51,791
которые получаются с помощью
функции make_move_iterator.

90
00:05:51,791 --> 00:05:54,370
Давайте я вот этот старый
код окончательно удалю.

91
00:05:54,370 --> 00:05:59,130
И рассмотрим ещё один пример,
ещё одну задачу.

92
00:05:59,130 --> 00:06:04,500
Задача «Читалка Иосифа».

93
00:06:04,500 --> 00:06:09,148
В ней нужно было реализовать
довольно замысловатый алгоритм,

94
00:06:09,148 --> 00:06:13,736
но суть его была в том, что на вход
приходит некоторый диапазон элементов,

95
00:06:13,736 --> 00:06:17,080
и эти элементы нужно как-то в
этом диапазоне переставить.

96
00:06:17,080 --> 00:06:20,456
И как мы там использовали move семантику?

97
00:06:20,456 --> 00:06:23,625
Мы вот здесь, в этом коде,

98
00:06:23,625 --> 00:06:29,054
когда мы уже набрали
наш вектор permutation,

99
00:06:29,054 --> 00:06:34,421
мы из этого вектора элементы перемещаем
в диапазон, начиная с range_begin.

100
00:06:34,421 --> 00:06:35,990
То есть в исходный диапазон.

101
00:06:35,990 --> 00:06:40,020
Опять же, как это можно записать
без цикла с помощью алгоритмов?

102
00:06:40,020 --> 00:06:44,057
Можно было бы написать алгоритм copy; я,
по сути,

103
00:06:44,057 --> 00:06:51,450
хочу скопировать из вектора
permutation (begin от permutation,

104
00:06:51,450 --> 00:06:58,120
end от permutation) в range_begin.

105
00:06:58,120 --> 00:07:01,487
Я мог бы написать вот так,

106
00:07:01,487 --> 00:07:05,780
закомментировав исходный код.

107
00:07:05,780 --> 00:07:11,011
Дальше я могу заметить, что я теперь хочу

108
00:07:11,011 --> 00:07:16,060
перемещать оттуда,
поэтому хорошо бы вызвать

109
00:07:16,060 --> 00:07:21,700
make_move_iterator и
получить что-то вот такое.

110
00:07:21,700 --> 00:07:29,720
[ШУМ] Вызвал make_move_iterator от begin,

111
00:07:29,720 --> 00:07:33,763
make_move_iterator от end и range_begin.

112
00:07:33,763 --> 00:07:37,500
От range_begin вызывать
make_move_iterator не нужно,

113
00:07:37,500 --> 00:07:40,640
потому что это диапазон,
в который мы записываем данные.

114
00:07:40,640 --> 00:07:44,275
make_move_iterator имеет смысл
вызывать только для тех итераторов,

115
00:07:44,275 --> 00:07:48,210
из которых данные будут забираться,
то есть копироваться или перемещаться.

116
00:07:48,210 --> 00:07:48,763
Итак.

117
00:07:48,763 --> 00:07:51,900
Я вызвал copy с make_move_iterator.

118
00:07:51,900 --> 00:07:55,320
Давайте я запущу unit
тесты на эту функцию.

119
00:07:55,320 --> 00:08:00,830
Код компилируется.

120
00:08:00,830 --> 00:08:03,950
И всё в порядке.

121
00:08:03,950 --> 00:08:07,385
А теперь давайте ещё один
важный момент обсудим.

122
00:08:07,385 --> 00:08:11,889
Если вы хотите вызвать copy от move
итераторов (у вас есть какие-то итераторы,

123
00:08:11,889 --> 00:08:15,557
вы хотите их обернуть в make_move_iterator
и вызвать от них copy), на самом деле,

124
00:08:15,557 --> 00:08:19,260
можно сделать короче и
вызвать сразу алгоритм move.

125
00:08:19,260 --> 00:08:21,924
Это, по сути,
некоторый такой «брат» алгоритма copy,

126
00:08:21,924 --> 00:08:24,885
который будет данные не копировать,
а перемещать и избавит вас

127
00:08:24,885 --> 00:08:27,987
от необходимости вызывать
функцию make_move_iterator.

128
00:08:27,987 --> 00:08:30,380
Соответственно, здесь можно написать move,

129
00:08:30,380 --> 00:08:34,690
а вот эти вот обёртки
make_move_iterator удалить.

130
00:08:34,690 --> 00:08:39,585
Я просто говорю: «Перемести мне объекты
из диапазона begin от permutation —

131
00:08:39,585 --> 00:08:43,870
end от permutation в range_begin».

132
00:08:43,870 --> 00:08:48,740
Давайте я уберу всё лишнее и
ещё раз запущу unit тесты.

133
00:08:48,740 --> 00:08:52,656
И — да, у нас всё в порядке.

134
00:08:52,656 --> 00:08:53,640
Итак.

135
00:08:53,640 --> 00:08:58,275
В этом видео мы узнали,
что если есть некоторый алгоритм,

136
00:08:58,275 --> 00:09:03,107
который копирует данные из, например,
входного диапазона, то можно заставить

137
00:09:03,107 --> 00:09:06,820
его эти данные перемещать,
обернув итераторы в make_move_iterator.

138
00:09:06,820 --> 00:09:10,895
Получатся некоторые move итераторы,
которые в целом применимы во всех случаях,

139
00:09:10,895 --> 00:09:14,308
когда алгоритм может копировать
данные из входного диапазона.

140
00:09:14,308 --> 00:09:18,460
И, кстати говоря, с помощью этих самых
move итераторов можно, например,

141
00:09:18,460 --> 00:09:24,964
ускорить задачу про сортировку слиянием,
что мы и попросим вас сделать в задаче.