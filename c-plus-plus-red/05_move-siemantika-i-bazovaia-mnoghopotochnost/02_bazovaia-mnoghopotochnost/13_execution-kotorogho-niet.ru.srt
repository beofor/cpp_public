1
00:00:00,000 --> 00:00:07,500
Давайте вернёмся

2
00:00:07,500 --> 00:00:13,931
к примеру с генерацией и с
суммированием элементов матрицы.

3
00:00:13,931 --> 00:00:18,414
При этом у меня вот сейчас в
Эклипсе находится вариант кода,

4
00:00:18,414 --> 00:00:20,163
с которого мы начинали.

5
00:00:20,163 --> 00:00:24,492
То есть вариант, в котором есть
только однопоточная генерация и

6
00:00:24,492 --> 00:00:29,270
однопоточное суммирование; ну никакого
другого суммирования мы не писали.

7
00:00:29,270 --> 00:00:34,840
У меня есть вот эта функция
GenerateSingleThread однопоточная,

8
00:00:34,840 --> 00:00:37,520
но я её немножечко переписал.

9
00:00:37,520 --> 00:00:42,321
У нас здесь был обычный цикл for,
который бежал

10
00:00:42,321 --> 00:00:47,260
вот по переменной result и для каждой

11
00:00:47,260 --> 00:00:52,381
строки матрицы он выполнял,
он её заполнял.

12
00:00:52,381 --> 00:00:56,860
Я этот цикл for
переписал на алгоритм for_each.

13
00:00:56,860 --> 00:01:00,931
Алгоритм for_each, по сути,
делает то же самое.

14
00:01:00,931 --> 00:01:04,146
Он принимает два итератора
— begin и end и λ,

15
00:01:04,146 --> 00:01:07,850
вернее, какую-то функцию
(не обязательно λ).

16
00:01:07,850 --> 00:01:16,493
Эта функция вызывается для каждого
элемента вот этой последовательности.

17
00:01:16,493 --> 00:01:21,882
Вот я написал λ-функцию,
которая в параметре принимает вектор int,

18
00:01:21,882 --> 00:01:28,770
то есть очередную строку нашей матрицы,
и делает то же самое, что и делала раньше.

19
00:01:28,770 --> 00:01:33,931
Она делает ей reserve и
потом бежит слева направо

20
00:01:33,931 --> 00:01:38,475
и заполняет результатом

21
00:01:38,475 --> 00:01:44,080
ксора номера строки и номера столбца.

22
00:01:44,080 --> 00:01:46,259
Вот таким образом я переписал.

23
00:01:46,259 --> 00:01:48,922
Просто заменил цикл for на for_each.

24
00:01:48,922 --> 00:01:53,920
Просто алгоритм for_each существует
в C++ достаточно давно, ещё,

25
00:01:53,920 --> 00:01:57,626
если я не ошибаюсь,
со стандарта 1998 года.

26
00:01:57,626 --> 00:02:05,970
А цикл for (range-based-for) был
добавлен в C++11 в 2011 году.

27
00:02:05,970 --> 00:02:11,707
До цикла range-based-for алгоритм
for_each был достаточно актуален.

28
00:02:11,707 --> 00:02:16,590
Сейчас его область
применения весьма сузилась.

29
00:02:16,590 --> 00:02:20,370
И смотрите: зачем я это сделал?

30
00:02:20,370 --> 00:02:25,907
Я это сделал для того,
чтобы решить другим образом нашу исходную

31
00:02:25,907 --> 00:02:31,805
задачу — — распараллелить
генерацию матрицы.

32
00:02:31,805 --> 00:02:36,432
Дело в том, что в стандарте C++17,

33
00:02:36,432 --> 00:02:40,780
который был утверждён в 2017 году,

34
00:02:40,780 --> 00:02:45,540
были введены параллельные
версии стандартных алгоритмов.

35
00:02:45,540 --> 00:02:50,377
то есть мы берём какой-то стандартный
алгоритм (в данном случае for_each) и

36
00:02:50,377 --> 00:02:53,620
мы можем достаточно просто
его распараллелить.

37
00:02:53,620 --> 00:02:59,985
Вот смотрите: мы открываем документацию
на алгоритм for_each и видим,

38
00:02:59,985 --> 00:03:05,794
что у него есть вот эта вот вторая версия,

39
00:03:05,794 --> 00:03:10,223
в которой в качестве первого

40
00:03:10,223 --> 00:03:15,296
параметра принимается
некий ExecutionPolicy,

41
00:03:15,296 --> 00:03:19,110
то есть дальше он принимает ForwardIt,

42
00:03:19,110 --> 00:03:23,266
итератор first, итератор last
и UnaryFunction — это функция,

43
00:03:23,266 --> 00:03:27,885
которую он выполняет для каждого
элемента последовательности.

44
00:03:27,885 --> 00:03:30,912
Но первый параметр — это некая Policy.

45
00:03:30,912 --> 00:03:35,517
При этом чтобы понять, что такое Policy,

46
00:03:35,517 --> 00:03:39,990
мы переходим в блок Parameters,
и здесь написано,

47
00:03:39,990 --> 00:03:42,960
что see execution policy for details.

48
00:03:42,960 --> 00:03:46,886
Нажимаем.

49
00:03:46,886 --> 00:03:51,600
И здесь описывается, что есть

50
00:03:51,600 --> 00:03:57,149
несколько вот этих политик
исполнения — последовательная,

51
00:03:57,149 --> 00:04:01,354
параллельная и
parallel_unsequenced_policy,

52
00:04:01,354 --> 00:04:05,220
то есть параллельная,
не сохраняющая порядок.

53
00:04:05,220 --> 00:04:11,750
И тут приводятся разные примеры.

54
00:04:11,750 --> 00:04:15,930
И вот есть три константы

55
00:04:15,930 --> 00:04:21,153
глобальные — это seq, par и par_unseq.

56
00:04:21,153 --> 00:04:24,297
Это, собственно,
константы, которые задают,

57
00:04:24,297 --> 00:04:29,270
какую политику исполнения
алгоритма мы хотим использовать.

58
00:04:29,270 --> 00:04:34,119
При этом вот здесь вверху
(тут достаточно мелко,

59
00:04:34,119 --> 00:04:37,560
я ещё увеличу), вот здесь написано,

60
00:04:37,560 --> 00:04:43,664
что эти политики объявлены в
заголовочном файле execution.

61
00:04:43,664 --> 00:04:52,194
Поэтому чтобы нам получить параллельную
версию алгоритма for_each,

62
00:04:52,194 --> 00:04:56,630
нам достаточно сделать
простую вещь: подключить

63
00:04:56,630 --> 00:05:01,630
заголовочный файл execution и вот здесь в

64
00:05:01,630 --> 00:05:07,998
качестве первого параметра
указать execution: par.

65
00:05:07,998 --> 00:05:13,921
Всё, вот этими двумя
строчками мы распараллелили

66
00:05:13,921 --> 00:05:18,132
наш алгоритм for_each.

67
00:05:18,132 --> 00:05:23,655
И по логике генерация матрицы должна
начать выполняться параллельно.

68
00:05:23,655 --> 00:05:29,072
Я хочу сразу отметить, что это
возможность последнего стандарта C++17,

69
00:05:29,072 --> 00:05:31,760
который был принят в 2017 году.

70
00:05:31,760 --> 00:05:36,270
Давайте, наконец, попробуем и посмотрим,
как это работает.

71
00:05:36,270 --> 00:05:38,000
Запускаем компиляцию.

72
00:05:38,000 --> 00:05:41,040
И у нас не компилируется.

73
00:05:41,040 --> 00:05:45,160
Смотрим в ошибку компиляции.

74
00:05:45,160 --> 00:05:51,610
fatal error: execution: No such file or
directiory.

75
00:05:51,610 --> 00:05:56,624
Дело в том, что несмотря на то,

76
00:05:56,624 --> 00:06:01,805
что стандарт C++17 был принят в 2017

77
00:06:01,805 --> 00:06:06,646
году и в него официально добавили
параллельные версии алгоритмов,

78
00:06:06,646 --> 00:06:11,191
на момент записи этого видео (а
это апрель 2018 года) ни один из

79
00:06:11,191 --> 00:06:16,145
ведущих компиляторов C++,
то есть Microsoft,

80
00:06:16,145 --> 00:06:21,000
GCC и Clang, ни один из этих
компиляторов не реализовали

81
00:06:21,000 --> 00:06:24,990
поддержку параллельных версий алгоритмов.

82
00:06:24,990 --> 00:06:29,195
Поэтому сейчас этим
воспользоваться нельзя.

83
00:06:29,195 --> 00:06:33,670
Мы не могли об это вам не рассказать,
потому что это наше ближайшее будущее,

84
00:06:33,670 --> 00:06:41,142
однако продемонстрировать, как это
работает и какую пользу это приносит,

85
00:06:41,142 --> 00:06:46,289
мы сейчас не можем,
потому что реализации ещё нет.

86
00:06:46,289 --> 00:06:50,743
Поэтому нужно следить
за новостями и ждать,

87
00:06:50,743 --> 00:06:54,722
когда в основных компиляторах
C++ появится, наконец,

88
00:06:54,722 --> 00:06:58,493
поддержка параллельных версий
стандартных алгоритмов.