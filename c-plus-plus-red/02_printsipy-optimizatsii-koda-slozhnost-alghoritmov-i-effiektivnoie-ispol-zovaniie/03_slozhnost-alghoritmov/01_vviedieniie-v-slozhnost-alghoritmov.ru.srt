1
00:00:00,000 --> 00:00:07,330
Здравствуйте.

2
00:00:07,330 --> 00:00:11,546
Меня зовут Антон Полднев, и мы продолжаем
разговор о написании эффективного кода.

3
00:00:11,546 --> 00:00:14,660
Вы только что научились измерять,
сколько работает ваш код,

4
00:00:14,660 --> 00:00:16,257
в частности, сравнивать два алгоритма.

5
00:00:16,257 --> 00:00:19,186
Один работает столько миллисекунд,
другой работает столько миллисекунд.

6
00:00:19,186 --> 00:00:21,422
И так понимать,
какой из алгоритмов эффективнее,

7
00:00:21,422 --> 00:00:24,090
какой из алгоритмов предпочесть
для решения данной задачи.

8
00:00:24,090 --> 00:00:28,161
Но у нас уже в предыдущих курсах
возникали ситуации, когда мы точно знали,

9
00:00:28,161 --> 00:00:30,910
что один алгоритм заведомо
эффективнее другого.

10
00:00:30,910 --> 00:00:33,593
Например, мы проходили контейнер deque.

11
00:00:33,593 --> 00:00:35,476
Чем он был лучше, чем вектор?

12
00:00:35,476 --> 00:00:38,750
Итак, вот давайте снова
вспомним тот самый пример.

13
00:00:38,750 --> 00:00:41,556
Во-первых, подключим сразу profile.h,

14
00:00:41,556 --> 00:00:44,370
который вы уже написали
в предыдущих видео.

15
00:00:44,370 --> 00:00:44,909
Вот он.

16
00:00:44,909 --> 00:00:48,530
Тут у нас есть класс LogDuration
и макрос LogDuration,

17
00:00:48,530 --> 00:00:51,160
который мы сейчас будем использовать.

18
00:00:51,160 --> 00:00:53,907
Давайте сравним вектор и дек,
а если точнее,

19
00:00:53,907 --> 00:00:57,288
попробуем повставлять в начало
вектора и в начало дека.

20
00:00:57,288 --> 00:01:01,522
Вспомним, что вставлять в начало
вектора гораздо менее эффективно,

21
00:01:01,522 --> 00:01:03,283
чем вставлять в начало дека.

22
00:01:03,283 --> 00:01:08,830
Итак, я подключаю модули дек,
вектор, iostream, чтобы выводить.

23
00:01:08,830 --> 00:01:10,090
И что я делаю?

24
00:01:10,090 --> 00:01:15,770
Я измеряю сначала эффективность

25
00:01:15,770 --> 00:01:19,870
вектора.

26
00:01:19,870 --> 00:01:25,005
Создаю пустой вектор, скажем, целых чисел

27
00:01:25,005 --> 00:01:30,140
и вставляю в его начало
последовательно 10 в 5-й

28
00:01:30,140 --> 00:01:35,430
чисел с помощью метода insert.

29
00:01:35,430 --> 00:01:40,880
Вставляю в начало вектора очередное число.

30
00:01:40,880 --> 00:01:44,570
И то же самое делаю для дека.

31
00:01:44,570 --> 00:01:52,570
[БЕЗ_ЗВУКА]

32
00:01:52,570 --> 00:01:57,718
[БЕЗ_ЗВУКА] Создаю

33
00:01:57,718 --> 00:02:01,870
пустой дек и вставляю в его
начало с помощью метода insert.

34
00:02:01,870 --> 00:02:06,274
Как мы уже знаем из второго курса,
вставка в начало дека будет эффективнее.

35
00:02:06,274 --> 00:02:07,640
Давайте это проверим.

36
00:02:07,640 --> 00:02:12,920
Запустим этот код и увидим,

37
00:02:12,920 --> 00:02:19,313
что вставка в начало вектора 100 000
элементов заняла больше полутора секунд,

38
00:02:19,313 --> 00:02:23,999
в то время как вставка в начало дека 100
000 элементов заняла всего 9 миллисекунд.

39
00:02:23,999 --> 00:02:27,659
Итак, это первый пример, когда мы
заранее уже знали, каков будет исход,

40
00:02:27,659 --> 00:02:30,170
каков будет результат
сравнения двух алгоритмов.

41
00:02:30,170 --> 00:02:32,460
Хорошо, давайте рассмотрим
еще один пример.

42
00:02:32,460 --> 00:02:36,247
Я рассказывал,

43
00:02:36,247 --> 00:02:40,720
что если у вас есть какой-то алгоритм,
скажем, lower_bound,

44
00:02:40,720 --> 00:02:44,875
есть глобальная функция lower_bound в
модуле алгоритм, а еще бывают контейнеры,

45
00:02:44,875 --> 00:02:48,410
у которых есть метод lower_bound,
например, контейнер множества.

46
00:02:48,410 --> 00:02:52,809
И я говорил, что если у вас есть такая
ситуация, когда у вас есть одноименные

47
00:02:52,809 --> 00:02:56,707
функция и метод, лучше использовать метод,
он будет заведомо эффективней.

48
00:02:56,707 --> 00:02:59,260
И некоторые слушатели уже
с этим успели столкнуться.

49
00:02:59,260 --> 00:03:01,446
Давайте я на примере
множества продемонстрирую,

50
00:03:01,446 --> 00:03:03,786
что действительно метод
lower_bound эффективней,

51
00:03:03,786 --> 00:03:06,960
чем глобальная функция lower_bound,
если ее вызывать для множества.

52
00:03:06,960 --> 00:03:12,370
Я вот здесь функцию main переименую,
чтобы она нам не мешалась.

53
00:03:12,370 --> 00:03:14,371
И перехожу вот сюда.

54
00:03:14,371 --> 00:03:19,280
Подключаю сет, вектор нам здесь не нужен.

55
00:03:19,280 --> 00:03:22,689
Оставляю profile.h и начинаю замерять.

56
00:03:22,689 --> 00:03:27,430
Я создаю одно-единственное
множество целых чисел,

57
00:03:27,430 --> 00:03:34,160
назову его numbers и заполню его,
скажем, тремя миллионами чисел.

58
00:03:34,160 --> 00:03:39,790
Пишу цикл for от нуля до трех миллионов

59
00:03:39,790 --> 00:03:44,050
и добавляю эти числа в множество.

60
00:03:44,050 --> 00:03:46,847
Вот у меня есть множество из
трех миллионов элементов,

61
00:03:46,847 --> 00:03:50,840
давайте я попробую в нем поискать
какое-нибудь число, например, миллион.

62
00:03:50,840 --> 00:03:54,960
Я сохраню сразу это число,
которое я буду искать,

63
00:03:54,960 --> 00:04:00,050
в константную переменную, вот у меня
число миллион, и теперь я буду мерить.

64
00:04:00,050 --> 00:04:05,050
Как эффективнее найти
lower_bound с этим числом?

65
00:04:05,050 --> 00:04:07,228
Макрос LogDuration.

66
00:04:07,228 --> 00:04:13,080
Сначала я попробую использовать
глобальную функцию lower_bound.

67
00:04:13,080 --> 00:04:17,970
Ну и давайте вызовем

68
00:04:17,970 --> 00:04:26,400
lower_bound(begin), end.

69
00:04:26,400 --> 00:04:30,240
И, собственно, найдем это самое число.

70
00:04:30,240 --> 00:04:33,312
И выведем, каким это число получилось.

71
00:04:33,312 --> 00:04:35,732
Это должно быть это самое число x.

72
00:04:35,732 --> 00:04:38,680
Это первый вариант, как поискать число x.

73
00:04:38,680 --> 00:04:43,280
И второй вариант — это использовать

74
00:04:43,280 --> 00:04:48,645
метод lower_bound.

75
00:04:48,645 --> 00:04:54,010
numbers.lowerbound(x).

76
00:04:54,010 --> 00:04:58,260
И это будет уже метод lower_bound.

77
00:04:58,260 --> 00:05:02,479
Посмотрим, что быстрее.

78
00:05:02,479 --> 00:05:04,500
Запускаем.

79
00:05:04,500 --> 00:05:08,945
И что-то не скомпилировалось,

80
00:05:08,945 --> 00:05:12,210
а именно вот здесь,
потому что я вызвал метод lower_bound.

81
00:05:12,210 --> 00:05:14,504
Он вернул мне итератор,
я попытался вывести итератор.

82
00:05:14,504 --> 00:05:20,420
Итератор вывести нельзя, надо его перед
этим разыменовать с помощью звездочки.

83
00:05:20,420 --> 00:05:22,060
Попробую еще раз.

84
00:05:22,060 --> 00:05:26,700
Компилируется.

85
00:05:26,700 --> 00:05:31,790
Запускается.

86
00:05:31,790 --> 00:05:36,831
И мы видим,

87
00:05:36,831 --> 00:05:41,955
что глобальная функция lower_bound
работала почти секунду, в то время

88
00:05:41,955 --> 00:05:46,675
как с помощью метода lower_bound мы нашли
искомое число меньше, чем за миллисекунду.

89
00:05:46,675 --> 00:05:50,040
Ну и действительно вывелось то
самое число миллион дважды.

90
00:05:50,040 --> 00:05:54,060
Итак, метод lower_bound эффективней,
чем функция lower_bound,

91
00:05:54,060 --> 00:05:56,740
и это мы тоже знали
изначально и без измерений.

92
00:05:56,740 --> 00:06:00,772
Вам не надо было попытаться написать вызов
глобальной функции lower_bound и вызов

93
00:06:00,772 --> 00:06:01,963
метода и сравнивать их.

94
00:06:01,963 --> 00:06:04,448
Вы заранее знали,
что вызов метода эффективней.

95
00:06:04,448 --> 00:06:08,062
Хорошо, мы чуть попозже поговорим
подробнее, почему так происходит,

96
00:06:08,062 --> 00:06:09,940
и пока что рассмотрим третий пример.

97
00:06:09,940 --> 00:06:13,950
Третий пример еще более
простой и понятный,

98
00:06:13,950 --> 00:06:19,270
а именно сравнение функций
lower_bound и функции find_if.

99
00:06:19,270 --> 00:06:23,105
Давайте вспомним,
что же делает функция lower_bound?

100
00:06:23,105 --> 00:06:27,488
Она находит в отсортированном наборе
чисел первое число, даже первый элемент,

101
00:06:27,488 --> 00:06:30,550
необязательно число, первый элемент,
который больше либо равен данному.

102
00:06:30,550 --> 00:06:35,090
И в принципе то же самое мы могли бы
сделать с помощью функции find_if.

103
00:06:35,090 --> 00:06:38,252
Но мы уже заранее знаем,
что функция lower_bound умная,

104
00:06:38,252 --> 00:06:42,078
она делает бинарный поиск,
в то время как find_if запускает цикл for.

105
00:06:42,078 --> 00:06:44,518
Поэтому lower_bound заведомо эффективнее.

106
00:06:44,518 --> 00:06:47,507
Давайте все-таки это на всякий
случай еще раз проверим.

107
00:06:47,507 --> 00:06:53,270
А именно давайте объявим вектор,

108
00:06:53,270 --> 00:06:58,810
в котором будет миллион чисел.

109
00:06:58,810 --> 00:07:03,418
Заведем константу NUMBER_COUNT со

110
00:07:03,418 --> 00:07:08,272
значением миллион и заполним

111
00:07:08,272 --> 00:07:13,500
вектор миллионом чисел.

112
00:07:13,500 --> 00:07:19,303
[БЕЗ_ЗВУКА] Давайте,

113
00:07:19,303 --> 00:07:24,632
скажем, добавим вектор, число и

114
00:07:24,632 --> 00:07:30,160
умножить на 10.

115
00:07:30,160 --> 00:07:35,190
Вот мы заполнили такой вектор, давайте
поробуем в нем что-нибудь поискать.

116
00:07:35,190 --> 00:07:40,610
Ну, например, будем искать число,

117
00:07:40,610 --> 00:07:47,880
я занесу его в константу,
ну, скажем, 7654321.

118
00:07:47,880 --> 00:07:49,365
Вот так.

119
00:07:49,365 --> 00:07:52,340
7 654 321.

120
00:07:52,340 --> 00:07:56,800
Ну и давайте я это число поищу
какое-то фиксированное количество раз,

121
00:07:56,800 --> 00:07:59,382
например, 10 для начала.

122
00:07:59,382 --> 00:08:04,180
Итак, я заполнил вектор вот такими
числами, и теперь я пытаюсь в

123
00:08:04,180 --> 00:08:08,180
нем поискать первое число,
которое больше либо равно, чем number.

124
00:08:08,180 --> 00:08:11,870
Снова используем макрос LogDuration.

125
00:08:11,870 --> 00:08:18,520
В первом случае я поищу
с помощью lower_bound.

126
00:08:18,520 --> 00:08:27,290
[БЕЗ_ЗВУКА] Делаю в Query_Count поисков.

127
00:08:27,290 --> 00:08:33,262
lower_bound (begin(v)),

128
00:08:33,262 --> 00:08:37,564
end(v) и ищу число NUMBER.

129
00:08:37,564 --> 00:08:42,760
Вот втором случае я поищу это самое число

130
00:08:42,760 --> 00:08:47,975
с помощью find_if.

131
00:08:47,975 --> 00:08:49,979
Как я это сделаю?

132
00:08:49,979 --> 00:08:53,190
Я вызову функцию find_if.

133
00:08:53,190 --> 00:08:56,803
Но здесь уже надо передать,
конечно, не число, а λ-функцию,

134
00:08:56,803 --> 00:08:59,510
которая будет описывать,
какое число мы ищем.

135
00:08:59,510 --> 00:09:03,140
Как эта λ-функция должна выглядеть?

136
00:09:03,140 --> 00:09:07,233
Эта λ-функция принимает очередное
число и для него должна сказать,

137
00:09:07,233 --> 00:09:08,758
подходит оно нам или нет.

138
00:09:08,758 --> 00:09:11,596
Оно нам подходит,
если оно больше либо равно, чем x.

139
00:09:11,596 --> 00:09:15,230
И поскольку я в теле этой функции
использую еще x, его надо захватить,

140
00:09:15,230 --> 00:09:18,991
то есть упомянуть в
квадратных скобках λ-функции.

141
00:09:18,991 --> 00:09:24,509
Итак, вот у меня два кода,
давайте сравним, какой же из них быстрее.

142
00:09:24,509 --> 00:09:28,831
Правда, здесь надо использовать не x,
а NUMBER, конечно.

143
00:09:28,831 --> 00:09:31,440
Eclipse мне об этом любезно напомнил.

144
00:09:31,440 --> 00:09:33,797
Все, давайте запускать.

145
00:09:33,797 --> 00:09:40,675
Итак, поиск с помощью lower_bound работал
ноль миллисекунд, ну то есть очень мало.

146
00:09:40,675 --> 00:09:43,865
А поиск с помощью find_if
работал 123 миллисекунды.

147
00:09:43,865 --> 00:09:48,290
То есть lower_bound заведомо быстрее, и мы
об этом, конечно же, и так заранее знали.

148
00:09:48,290 --> 00:09:48,795
Итак.

149
00:09:48,795 --> 00:09:51,661
О чем же мы сейчас будем разговаривать?

150
00:09:51,661 --> 00:09:55,891
О том, что необязательно, для того чтобы
понять, какой алгоритм эффективнее,

151
00:09:55,891 --> 00:09:59,100
для того чтобы понять, какой алгоритм
предпочесть для решения вашей задачи,

152
00:09:59,100 --> 00:10:02,292
необязательно для этого эти алгоритмы
программировать и замерять.

153
00:10:02,292 --> 00:10:06,033
Иногда можно заранее знать,
какой алгоритм вам больше подходит.

154
00:10:06,033 --> 00:10:08,200
Ровно об этом мы поговорим далее.