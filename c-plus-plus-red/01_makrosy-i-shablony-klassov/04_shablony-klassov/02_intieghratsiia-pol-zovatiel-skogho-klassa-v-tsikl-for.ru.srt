1
00:00:00,000 --> 00:00:08,780
Давайте рассмотрим вот такой пример.

2
00:00:08,780 --> 00:00:16,190
У нас есть вектор целых чисел, и мы с вами
знаем, что чтобы по нему проитерироваться,

3
00:00:16,190 --> 00:00:21,760
мы можем написать простой и удобный цикл,
который называется range-based-for.

4
00:00:21,760 --> 00:00:27,880
Ну и давайте, например,
просто выведем элементы нашего вектора.

5
00:00:27,880 --> 00:00:32,682
Все, код компилируется,
работает, все нормально.

6
00:00:32,682 --> 00:00:35,235
Мы это с вами кучу раз делали.

7
00:00:35,235 --> 00:00:39,880
Но если мы захотим проитерироваться
не по всему вектору, а, например,

8
00:00:39,880 --> 00:00:44,590
по первым его трем элементам,
то мы уже не можем воспользоваться удобным

9
00:00:44,590 --> 00:00:49,543
циклом range-based-for,
и нам приходится вместо этого

10
00:00:49,543 --> 00:00:54,531
писать обычный цикл со счетчиком.

11
00:00:54,531 --> 00:00:57,250
Вот таким образом.

12
00:00:57,250 --> 00:01:02,671
Он у нас, конечно,
компилируется и работает,

13
00:01:02,671 --> 00:01:07,459
но это не так удобно, как в первом случае.

14
00:01:07,459 --> 00:01:13,498
Кроме того, в данном случае мы можем
допустить ошибку: если в нашем

15
00:01:13,498 --> 00:01:19,380
векторе будет меньше трех элементов, то
наш цикл просто выйдет за границы вектора,

16
00:01:19,380 --> 00:01:24,169
и программа может вести себя совсем
не так, как мы от нее ожидаем.

17
00:01:24,169 --> 00:01:26,618
Ну и вообще тут получается,

18
00:01:26,618 --> 00:01:31,568
что нет какого-то универсализма,
какой-то универсальности нету.

19
00:01:31,568 --> 00:01:37,074
Мы по целому вектору можем удобным циклом
итерироваться, а по части вектора — нет.

20
00:01:37,074 --> 00:01:41,159
И поэтому давайте мы попробуем
исправить этот недостаток.

21
00:01:41,159 --> 00:01:46,193
Давайте поставим перед собой
задачу написать функцию Head,

22
00:01:46,193 --> 00:01:51,107
которой мы будем пользоваться
вот таким образом.

23
00:01:51,107 --> 00:01:56,940
Эта запись означает, что я хочу с
помощью удобного цикла range-based-for

24
00:01:56,940 --> 00:02:02,440
проитерироваться только по первым
трем элементам нашего вектора.

25
00:02:02,440 --> 00:02:06,745
Кроме того,
эта функция должна корректно работать,

26
00:02:06,745 --> 00:02:10,070
и когда в векторе меньше чем три элемента.

27
00:02:10,070 --> 00:02:15,700
Собственно, у вас может возникнуть
вопрос: «Ну а зачем нам эта функция,

28
00:02:15,700 --> 00:02:17,231
потому что удобнее печать?

29
00:02:17,231 --> 00:02:20,161
Но, может быть,
в ней есть еще какой-то смысл?».

30
00:02:20,161 --> 00:02:22,790
Смотрите, у меня есть
такой необычный пример.

31
00:02:22,790 --> 00:02:27,725
Это немножко обработанная

32
00:02:27,725 --> 00:02:32,660
в графическом редакторе выдача Яндекса
по запросу «язык программирования c++».

33
00:02:32,660 --> 00:02:33,793
И что мы видим?

34
00:02:33,793 --> 00:02:38,950
Мы видим, что по этому запросу
нашлось 59 миллионов результатов.

35
00:02:38,950 --> 00:02:44,890
Но на первой странице поисковой выдачи
представлены только десять из них.

36
00:02:44,890 --> 00:02:47,738
И вот мы можем представить,
чисто теоретически,

37
00:02:47,738 --> 00:02:53,506
что у нас есть вектор из 59
миллионов релевантных документов,

38
00:02:53,506 --> 00:03:00,040
и только первые десять из них нам нужно
обработать, нам нужно по ним пройтись,

39
00:03:00,040 --> 00:03:05,720
и сформировать из них поисковую выдачу,
первую страницу поисковой выдачи.

40
00:03:05,720 --> 00:03:10,420
И когда вы доберетесь до
финальной задачи этого курса,

41
00:03:10,420 --> 00:03:15,300
вы поймете,
что я не просто так приводил этот пример.

42
00:03:15,300 --> 00:03:20,373
Поэтому давайте подумаем,
как мы, пользуясь теми знаниями,

43
00:03:20,373 --> 00:03:26,780
которые у нас есть сейчас,
могли бы написать функцию Head.

44
00:03:26,780 --> 00:03:27,953
Что мы можем сделать?

45
00:03:27,953 --> 00:03:30,430
Мы можем, например, самый простой вариант,

46
00:03:30,430 --> 00:03:36,520
возвращать из функции Head копию префикса
вектора, по которому мы можем пройтись.

47
00:03:36,520 --> 00:03:39,039
Как это будет выглядеть?

48
00:03:39,039 --> 00:03:42,939
Мы напишем шаблон функции в данном случае.

49
00:03:42,939 --> 00:03:46,610
У нас Head — это функция, которая будет...

50
00:03:46,610 --> 00:03:53,858
Функция Head, она возвращает vector<T>,

51
00:03:53,858 --> 00:03:59,050
принимает vector<T> по ссылке.

52
00:03:59,050 --> 00:04:04,832
[ЗВУК] И вот такая у нее сигнатура.

53
00:04:04,832 --> 00:04:11,132
То есть она по ссылке принимает вектор,
и второй параметр — top,

54
00:04:11,132 --> 00:04:15,359
он задает, собственно,
какой размер префикса мы хотим,

55
00:04:15,359 --> 00:04:18,347
по какому размеру префикса
мы хотим проитерироваться.

56
00:04:18,347 --> 00:04:20,428
И что мы делаем?

57
00:04:20,428 --> 00:04:25,904
Мы просто возвращаем
вектор из двух итераторов.

58
00:04:25,904 --> 00:04:30,037
Первый итератор — это, собственно, begin,

59
00:04:30,037 --> 00:04:34,140
а второй — это next (v.begin(),

60
00:04:34,140 --> 00:04:38,350
min(top, v.size())).

61
00:04:38,350 --> 00:04:42,907
То есть первый итератор — это начало
вектора, а второй — это begin,

62
00:04:42,907 --> 00:04:47,621
который мы с помощью
функции next продвигаем

63
00:04:47,621 --> 00:04:51,838
на минимум из значения параметра
top и размера вектора.

64
00:04:51,838 --> 00:04:53,440
Это, собственно...

65
00:04:53,440 --> 00:04:56,557
Минимум мы берем для того случая,
когда мы хотим три элемента,

66
00:04:56,557 --> 00:05:02,677
а в векторе у нас их всего два,
чтобы вернуть только два элемента.

67
00:05:02,677 --> 00:05:08,965
И наша функция, наша функция и наш код
весь целиком компилируется, работает.

68
00:05:08,965 --> 00:05:12,437
Действительно, мы вот здесь
попросили три элемента,

69
00:05:12,437 --> 00:05:15,722
и в консоль вывелись элементы 1, 2 и 3.

70
00:05:15,722 --> 00:05:19,277
Вроде бы все хорошо, но, я уверен,

71
00:05:19,277 --> 00:05:25,103
вы уже сами заметили недостатки в
текущей реализации функции Head.

72
00:05:25,103 --> 00:05:29,525
Во-первых, конечно же,
она создает копию вектора.

73
00:05:29,525 --> 00:05:33,469
И с точки зрения производительности,
с точки зрения эффективности кода,

74
00:05:33,469 --> 00:05:37,139
это совершенно непрактично, потому что
мы всего лишь хотим проитерироваться по

75
00:05:37,139 --> 00:05:42,395
вектору, а для этого мы
создаем его полную копию.

76
00:05:42,395 --> 00:05:47,160
Представьте, что мы хотим проитерироваться
по всем элементам, кроме последнего.

77
00:05:47,160 --> 00:05:50,977
И мы могли бы это делать
с помощью функции Head,

78
00:05:50,977 --> 00:05:55,103
но тогда мы создавали бы
копию почти всего вектора.

79
00:05:55,103 --> 00:05:56,128
Поэтому то,

80
00:05:56,128 --> 00:06:01,468
что она создает копию — это очень плохо
с точки зрения производительности.

81
00:06:01,468 --> 00:06:05,660
Кроме того, мы, вообще говоря,
можем захотеть сделать вот что.

82
00:06:05,660 --> 00:06:10,883
Мы можем захотеть пройтись по
префиксу нашего вектора и,

83
00:06:10,883 --> 00:06:16,576
например, увеличить каждый
его элемент на единицу.

84
00:06:16,576 --> 00:06:21,719
Тогда, если мы после этого пройдемся

85
00:06:21,719 --> 00:06:26,710
по самому вектору целиком,
мы будем ожидать,

86
00:06:26,710 --> 00:06:33,085
что первые его три элемента увеличатся на
единицу, а четвертый и пятый не изменятся.

87
00:06:33,085 --> 00:06:38,102
Запускаем наш код и видим,
что сам вектор не изменился.

88
00:06:38,102 --> 00:06:42,973
То есть у нас все компилируется,
действительно у нас здесь x принимается по

89
00:06:42,973 --> 00:06:47,517
ссылке и изменяется но сам этот
исходный вектор не изменяется.

90
00:06:47,517 --> 00:06:53,400
Хотя вот из этой записи логично ожидать,
что мы действительно изменяем сам вектор.

91
00:06:53,400 --> 00:06:54,619
Но это не так.

92
00:06:54,619 --> 00:06:59,702
И это второй недостаток текущей
реализации функции Head.

93
00:06:59,702 --> 00:07:04,461
Поэтому создание копии
вектора нам не подходит.

94
00:07:04,461 --> 00:07:09,699
И нам нужен какой-то другой способ,
который бы позволил нам из функции

95
00:07:09,699 --> 00:07:15,713
Head вернуть диапазон
внутри исходного вектора,

96
00:07:15,713 --> 00:07:21,065
и с помощью этого диапазона обращаться
к элементам самого вектора.

97
00:07:21,065 --> 00:07:23,562
И что бы нам тут могло помочь?

98
00:07:23,562 --> 00:07:28,780
Мы можем возвращать пару
итераторов и итерироваться по ним.

99
00:07:28,780 --> 00:07:33,387
Собственно, из функции Head вместо
копии вектора возвращать лишь

100
00:07:33,387 --> 00:07:37,826
пару итераторов на вектор v и
итерироваться в цикле по ним.

101
00:07:37,826 --> 00:07:39,930
Как мы это можем сделать?

102
00:07:39,930 --> 00:07:42,302
Как раз с помощью шаблонов классов,

103
00:07:42,302 --> 00:07:45,680
с которыми мы с вами
познакомились в предыдущем видео.

104
00:07:45,680 --> 00:07:47,526
Смотрите, что мы делаем.

105
00:07:47,526 --> 00:07:49,775
У нас функция Head — шаблонная.

106
00:07:49,775 --> 00:07:52,200
Она принимает вектор любого типа.

107
00:07:52,200 --> 00:07:58,765
И поэтому должна, собственно, возвращать и
пару итераторов для вектора любого типа.

108
00:07:58,765 --> 00:08:03,390
Поэтому пару итераторов мы тоже
представляем в виде шаблонного в

109
00:08:03,390 --> 00:08:05,330
данном случае уже класса.

110
00:08:05,330 --> 00:08:07,272
Делаем вот что.

111
00:08:07,272 --> 00:08:13,010
Пишем: template <typename Iterator>.

112
00:08:13,010 --> 00:08:17,850
Можно здесь написать что угодно,
просто я более,

113
00:08:17,850 --> 00:08:22,048
более развернутое имя
для параметра выбрал.

114
00:08:22,048 --> 00:08:27,690
struct IteratorRange, диапазон итераторов.

115
00:08:27,690 --> 00:08:32,835
И объявляем поля first и

116
00:08:32,835 --> 00:08:37,980
last типа Iterator — собственно,
этого самого нашего шаблонного типа.

117
00:08:37,980 --> 00:08:45,679
Теперь нам нужно этот шаблон класса
применить внутри функции Head.

118
00:08:45,679 --> 00:08:51,995
Смотрите, вместо вектора мы
будем возвращать IteratorRange

119
00:08:51,995 --> 00:08:57,722
от vector<T>:: iterator.

120
00:08:57,722 --> 00:09:02,526
Так, немножечко переформатируем код,

121
00:09:02,526 --> 00:09:05,393
чтобы все помещалось.

122
00:09:05,393 --> 00:09:09,144
Компилируем, и у нас не компилируется.

123
00:09:09,144 --> 00:09:14,198
На самом деле, здесь задумано было,
что не будет компилироваться,

124
00:09:14,198 --> 00:09:19,040
потому что здесь еще нужно
написать слово typename.

125
00:09:19,040 --> 00:09:22,320
Мы не будем рассматривать,
почему его здесь нужно писать,

126
00:09:22,320 --> 00:09:25,609
потому что это очень
неочевидный аспект языка C++,

127
00:09:25,609 --> 00:09:31,530
на самом деле встречается нечасто,
поэтому мы оставим за скобками,

128
00:09:31,530 --> 00:09:37,105
вы сможете пользоваться шаблонами классов,
и не зная эту особенность.

129
00:09:37,105 --> 00:09:42,517
Давайте еще раз скомпилируем и обратим
внимание, на самом деле, на другую,

130
00:09:42,517 --> 00:09:47,564
более важную вещь, что сейчас у нас
код все равно не компилируется.

131
00:09:47,564 --> 00:09:48,308
Почему?

132
00:09:48,308 --> 00:09:53,286
Потому что мы из функции Head
возвращаем IteratorRange — это

133
00:09:53,286 --> 00:09:57,298
структура из двух итераторов,
и, естественно,

134
00:09:57,298 --> 00:10:03,450
по ней нельзя пройтись
с помощью цикла for.

135
00:10:03,450 --> 00:10:07,850
Давайте посмотрим,
что же нам тут пишет компилятор.

136
00:10:07,850 --> 00:10:13,326
Давайте прямо увеличим окно с ошибками и

137
00:10:13,326 --> 00:10:19,235
посмотрим на сообщение об ошибке.

138
00:10:19,235 --> 00:10:23,250
Смотрите, здесь написано: "has
no member named 'begin'",

139
00:10:23,250 --> 00:10:25,340
"has no member named 'end'".

140
00:10:25,340 --> 00:10:30,336
И если мы его тут пролистаем,
то здесь упоминается

141
00:10:30,336 --> 00:10:35,652
наш шаблон IteratorRange,
но после него идут угловые скобки,

142
00:10:35,652 --> 00:10:41,119
то есть это конкретный класс,
который сгенерировался из этого шаблона.

143
00:10:41,119 --> 00:10:45,880
И компилятор нам жалуется, что у
этого класса нет методов begin и end.

144
00:10:45,880 --> 00:10:49,540
И это на самом деле подсказка,

145
00:10:49,540 --> 00:10:54,390
что нам нужно сделать, чтобы по классу,

146
00:10:54,390 --> 00:10:59,240
созданному из шаблона IteratorRange,
можно было бы итерироваться циклом for.

147
00:10:59,240 --> 00:11:05,310
Собственно, смотрите: цикл for,

148
00:11:05,310 --> 00:11:09,120
range-based-for, он
итерируется по коллекции.

149
00:11:09,120 --> 00:11:13,393
Чтобы по ней проитерироваться,
ему нужно каким-то образом узнать,

150
00:11:13,393 --> 00:11:16,427
где она начинается,
и где она заканчивается.

151
00:11:16,427 --> 00:11:21,634
И цикл range-based-for это делает,
вызывая у переданного вот

152
00:11:21,634 --> 00:11:28,201
сюда вот объекта методы begin и end,
которые должны возвращать итератор.

153
00:11:28,201 --> 00:11:34,311
Поэтому, чтобы разрешить итерацию с
помощью range-based-for по классу,

154
00:11:34,311 --> 00:11:37,470
созданному из шаблона IteratorRange,

155
00:11:37,470 --> 00:11:42,170
мы просто должны добавить
ему методы begin и end.

156
00:11:42,170 --> 00:11:44,336
Давайте сделаем это.

157
00:11:44,336 --> 00:11:51,592
Добавим метод begin,
который будет возвращать поле first,

158
00:11:51,592 --> 00:11:55,820
и добавим метод end,

159
00:11:55,820 --> 00:12:00,340
который будет возвращать поле last.

160
00:12:00,340 --> 00:12:04,263
Мы сделали два этих действия,
добавили два этих метода.

161
00:12:04,263 --> 00:12:07,186
Компилируем — и оно компилируется.

162
00:12:07,186 --> 00:12:09,660
Запускаем — и оно работает.

163
00:12:09,660 --> 00:12:12,710
Давайте еще раз посмотрим.

164
00:12:12,710 --> 00:12:14,362
Что мы делаем?

165
00:12:14,362 --> 00:12:22,300
Мы вызываем функцию Head для первых
трех элементов нашего вектора,

166
00:12:22,300 --> 00:12:26,757
обращаемся к этим элементам по ссылке,
изменяем их, увеличиваем на единицу,

167
00:12:26,757 --> 00:12:30,110
а потом идем по всему нашему
вектору и выводим его элементы.

168
00:12:30,110 --> 00:12:33,431
И у нас в консоли появилось: 2,
3, 4, 4, 5.

169
00:12:33,431 --> 00:12:38,773
То есть действительно первые три элемента
вектора с помощью нашей функции Head

170
00:12:38,773 --> 00:12:45,780
были увеличены на единицу, и потом мы
смогли увидеть результат этого изменения.

171
00:12:45,780 --> 00:12:52,630
Таким образом,
давайте подведем итоги этого видео.

172
00:12:52,630 --> 00:12:58,469
Мы с вами узнали, что, чтобы по объекту
класса можно было итерироваться

173
00:12:58,469 --> 00:13:03,707
с помощью цикла for, этот класс
должен иметь методы begin и end.

174
00:13:03,707 --> 00:13:09,617
И при этом эти методы должны возвращать
итераторы на начало и конец той коллекции,

175
00:13:09,617 --> 00:13:12,961
которую объект этого класса представляет.