1
00:00:00,000 --> 00:00:07,025
В нашем предыдущем курсе,
в Жёлтом поясе, поясе по C++,

2
00:00:07,025 --> 00:00:13,413
мы с вами уже изучили шаблоны
функций и мы говорили,

3
00:00:13,413 --> 00:00:18,800
что шаблоны функции помогают избежать
дублирования кода в тех функциях, которые

4
00:00:18,800 --> 00:00:23,800
отличаются только типами своих аргументов
или типом возвращаемого значения.

5
00:00:23,800 --> 00:00:27,250
В этом модуле мы с вами
изучим шаблоны класса.

6
00:00:27,250 --> 00:00:30,530
И шаблоны класса решают
ту же самую задачу.

7
00:00:30,530 --> 00:00:35,475
Они позволяют избежать дублирования
кода в классах, которые отличаются

8
00:00:35,475 --> 00:00:40,420
только типами своих полей или
же параметров своих методов,

9
00:00:40,420 --> 00:00:45,650
или типами возвращаемых
значений в методах.

10
00:00:45,650 --> 00:00:49,861
Простейший пример шаблона
класса — это пара.

11
00:00:49,861 --> 00:00:53,568
Давайте мы с вами на время забудем о том,

12
00:00:53,568 --> 00:00:59,479
что существует стандартная пара,
и рассмотрим такой пример.

13
00:00:59,479 --> 00:01:05,239
Вот нам по какой-то причине
понадобилось написать структуру,

14
00:01:05,239 --> 00:01:09,600
у которой есть два поля,
например, string и int.

15
00:01:09,600 --> 00:01:12,538
И что мы пишем?

16
00:01:12,538 --> 00:01:20,934
Мы пишем struct PairOfStringAndInt.

17
00:01:20,934 --> 00:01:27,405
И говорим,
что у нас есть string first и int second.

18
00:01:27,405 --> 00:01:32,670
И вот как-то мы этой парой пользуемся.

19
00:01:32,670 --> 00:01:36,660
Мы её объявили.

20
00:01:36,660 --> 00:01:41,950
first присвоили Hello,

21
00:01:41,950 --> 00:01:45,347
и second, например, присвоили 5.

22
00:01:45,347 --> 00:01:48,435
Вот так мы этой парой пользуемся.

23
00:01:48,435 --> 00:01:52,472
Потом у нас как-то проект развивается,
мы пишем код дальше,

24
00:01:52,472 --> 00:01:54,920
мы понимаем, что нам нужна ещё пара.

25
00:01:54,920 --> 00:01:59,162
Нам нужна пара, например,

26
00:01:59,162 --> 00:02:03,948
из логического значения и символа.

27
00:02:03,948 --> 00:02:07,874
Мы пишем PairOfBoolAndChar.

28
00:02:07,874 --> 00:02:12,982
У нас здесь появляется bool first

29
00:02:12,982 --> 00:02:17,280
и char second.

30
00:02:17,280 --> 00:02:22,316
И точно так же мы её объявляем bool

31
00:02:22,316 --> 00:02:26,515
and char.

32
00:02:26,515 --> 00:02:32,580
И работаем с её полями first и second.

33
00:02:32,580 --> 00:02:37,088
Ну и понятно, что когда у нас возникает

34
00:02:37,088 --> 00:02:41,697
необходимость в каких-то
новых сочетаниях типов,

35
00:02:41,697 --> 00:02:46,239
нам приходится каждый раз
объявлять новую структуру,

36
00:02:46,239 --> 00:02:51,074
давать ей какое-то имя и объявлять
в ней поля нужных нам типов.

37
00:02:51,074 --> 00:02:53,575
И, соответственно, что мы видим?

38
00:02:53,575 --> 00:02:58,730
Мы видим, что вот эти два класса
PairOfStringAndInt и PairOfBoolAndChar,

39
00:02:58,730 --> 00:03:03,360
они структурно одинаковые,
но только отличаются типами своих полей.

40
00:03:03,360 --> 00:03:07,982
И поэтому вместо них мы можем
написать шаблон класса.

41
00:03:07,982 --> 00:03:11,720
Шаблон класса так же,
как и шаблон функции,

42
00:03:11,720 --> 00:03:15,045
начинается с ключевого слова template,

43
00:03:15,045 --> 00:03:21,107
дальше в угловых скобках мы
указываем шаблонные параметры,

44
00:03:21,107 --> 00:03:26,790
мы пишем typename T и typename U.

45
00:03:26,790 --> 00:03:30,901
Всё, мы написали эти строчки,

46
00:03:30,901 --> 00:03:35,176
и теперь мы пишем класс struct Pair.

47
00:03:35,176 --> 00:03:40,115
И внутри этого нашего класса
мы можем использовать

48
00:03:40,115 --> 00:03:45,303
парметры, в данном случае T и U, как типы.

49
00:03:45,303 --> 00:03:50,270
Мы пишем T first U second.

50
00:03:50,270 --> 00:03:56,392
И таким образом мы с вами только что
написали простейший шаблон класса пара.

51
00:03:56,392 --> 00:03:58,190
Как им пользоваться?

52
00:03:58,190 --> 00:04:01,484
Пользоваться им очень просто.

53
00:04:01,484 --> 00:04:06,490
Мы берём и в нашей вот программе,
в данном случае в функции main,

54
00:04:06,490 --> 00:04:11,973
пишем в угловых скобках Pair и

55
00:04:11,973 --> 00:04:18,960
здесь через запятую мы перечисляем типы,

56
00:04:18,960 --> 00:04:26,400
которыми мы хотим воспользоваться
в данном шаблоне.

57
00:04:26,400 --> 00:04:33,150
И здесь мы делаем то же самое: bool char.

58
00:04:33,150 --> 00:04:34,371
Компилируем наш код.

59
00:04:34,371 --> 00:04:35,785
И наш код компилируется.

60
00:04:35,785 --> 00:04:39,447
Нам там компилятор жалуется,
что мы не используем переменную bc,

61
00:04:39,447 --> 00:04:41,247
но сейчас это не важно.

62
00:04:41,247 --> 00:04:46,358
Собственно, мы с вами только
что написали шаблон класса

63
00:04:46,358 --> 00:04:51,360
Pair и с помощью него создали два класса:

64
00:04:51,360 --> 00:04:55,327
Pair string int и Pair bool char.

65
00:04:55,327 --> 00:04:58,072
Что тут важно отметить?

66
00:04:58,072 --> 00:05:01,469
Что Pair — это шаблон класса,

67
00:05:01,469 --> 00:05:06,412
а вот эта вещь Pair от string
и int — это уже класс,

68
00:05:06,412 --> 00:05:09,648
то есть это самостоятельный тип.

69
00:05:09,648 --> 00:05:10,877
Таким образом,

70
00:05:10,877 --> 00:05:16,420
мы в этой программе из шаблонов класса
Pair создаём два класса, два типа.

71
00:05:16,420 --> 00:05:20,296
Pair от string и int и
Pair от bool и char.

72
00:05:20,296 --> 00:05:26,920
И это создание типа из шаблона
класса называется инстанцирование.

73
00:05:26,920 --> 00:05:34,158
И мы будем использовать этот термин
дальше в модуле про шаблоны классов.

74
00:05:34,158 --> 00:05:41,770
Таким образом, давайте с вами подведём
итоги этого краткого вводного видео.

75
00:05:41,770 --> 00:05:48,000
Мы с вами узнали, что в C++ есть не только
шаблоны функции, но и шаблоны класса,

76
00:05:48,000 --> 00:05:53,659
которые также решают задачу
борьбы с дублированием кода.

77
00:05:53,659 --> 00:05:58,034
Чтобы объявить шаблон класса,
нужно написать ключевое слово template,

78
00:05:58,034 --> 00:06:03,094
дальше в угловых скобках перечислить
шаблонные параметры и дальше написать

79
00:06:03,094 --> 00:06:09,736
слово class или struct,
имя класса и дальше просто объявить класс.

80
00:06:09,736 --> 00:06:13,377
Шаблонов параметров у нас
может быть несколько.

81
00:06:13,377 --> 00:06:15,975
Вот в примере с парой у нас было два.

82
00:06:15,975 --> 00:06:21,240
Здесь сейчас приведён пример
шаблона Widget с одним параметром.

83
00:06:21,240 --> 00:06:27,220
Чтобы создать класс из шаблона нужно
в угловых скобках указать типы,

84
00:06:27,220 --> 00:06:30,387
с которыми этот шаблон инстанцируется.

85
00:06:30,387 --> 00:06:36,085
Собственно, мы сказали, что создание
класса из шаблона называется инстанция.