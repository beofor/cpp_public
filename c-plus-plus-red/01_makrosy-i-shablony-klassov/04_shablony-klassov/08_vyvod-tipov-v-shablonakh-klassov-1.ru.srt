1
00:00:00,000 --> 00:00:07,337
[БЕЗ_ЗВУКА] Продолжим

2
00:00:07,337 --> 00:00:12,290
изучать шаблоны классов на
примере шаблона IteratorRange.

3
00:00:12,290 --> 00:00:15,090
Давайте сделаем вот что.

4
00:00:15,090 --> 00:00:19,326
Мы пока что шаблон IteratorRange
используем только вот здесь,

5
00:00:19,326 --> 00:00:21,106
внутри нашей функции Head.

6
00:00:21,106 --> 00:00:26,087
Но этот шаблон более мощный,
он позволяет хранить не только

7
00:00:26,087 --> 00:00:30,788
префикс вектора,
но и какой-то произвольный диапазон,

8
00:00:30,788 --> 00:00:34,698
какой-то произвольный
диапазон внутри вектора.

9
00:00:34,698 --> 00:00:39,618
Например, мы можем захотеть по
какой-то причине обратиться

10
00:00:39,618 --> 00:00:43,950
к суффиксу нашего вектора,
ко второй его половине.

11
00:00:43,950 --> 00:00:47,702
И как бы мы могли это сделать?

12
00:00:47,702 --> 00:00:51,310
Мы должны написать вот что.

13
00:00:51,310 --> 00:00:56,245
IteratorRange, шаблон,
теперь мы его инстанцируем,

14
00:00:56,245 --> 00:01:01,270
vector < int >::iterator, собственно,

15
00:01:01,270 --> 00:01:06,300
мы его инстанцируем этим типом,
типом итератора,

16
00:01:06,300 --> 00:01:10,940
second_half, вторая половина вектора.

17
00:01:10,940 --> 00:01:16,034
И передаем параметры, v.begin + +

18
00:01:16,034 --> 00:01:20,810
v.size / 2, v.end.

19
00:01:20,810 --> 00:01:27,341
Компилируется, компилируется,
правда, компилятор ругается,

20
00:01:27,341 --> 00:01:32,441
что мы никак эту переменную не используем,
но код корректный.

21
00:01:32,441 --> 00:01:35,131
На что тут можно обратить внимание?

22
00:01:35,131 --> 00:01:40,094
Что для объявления переменной
second_half нам пришлось написать

23
00:01:40,094 --> 00:01:44,408
вот столько много букв,
потому что нам надо указать шаблон,

24
00:01:44,408 --> 00:01:48,713
надо указать, чем мы его инстанцируем,
тип, который мы инстанцируем,

25
00:01:48,713 --> 00:01:52,300
тоже достаточно большой и громоздкий
— это vector< int >::iterator.

26
00:01:52,300 --> 00:01:58,062
И, естественно,
так писать не очень-то удобно,

27
00:01:58,062 --> 00:02:02,470
потому что код становится громоздким,
и его тяжело читать.

28
00:02:02,470 --> 00:02:07,629
И мы можем подумать,
а как можно решить эту проблему,

29
00:02:07,629 --> 00:02:12,075
можно ли пользоваться нашим шаблоном,

30
00:02:12,075 --> 00:02:16,440
но как-то более компактно,

31
00:02:16,440 --> 00:02:19,721
с необходимостью меньше печатать.

32
00:02:19,721 --> 00:02:23,855
И здесь давайте мы вспомним
с вами такую вещь.

33
00:02:23,855 --> 00:02:27,878
У нас есть шаблоны классов,
есть шаблоны функций.

34
00:02:27,878 --> 00:02:31,954
И вот, например,
у нас есть шаблон функции Head.

35
00:02:31,954 --> 00:02:36,212
Это шаблон, у него есть параметр T,
но при этом,

36
00:02:36,212 --> 00:02:40,552
когда мы вызываем функцию Head вот здесь,
мы не указываем в

37
00:02:40,552 --> 00:02:45,396
угловых скобках тип, которым,

38
00:02:45,396 --> 00:02:51,760
на который будет заменено значение T.

39
00:02:51,760 --> 00:02:54,694
Компилятор этот тип выводит за нас.

40
00:02:54,694 --> 00:02:57,920
То же самое здесь с функцией RangeSize.

41
00:02:57,920 --> 00:03:03,220
Опять же вот RangeSize,
мы просто передаем в нее конкретный тип,

42
00:03:03,220 --> 00:03:08,240
который возвращает функция Head,
и компилятор по этому типу понимает,

43
00:03:08,240 --> 00:03:13,260
с каким типом T нужно
инстанцировать шаблон RangeSize.

44
00:03:13,260 --> 00:03:20,488
То есть для шаблонов функций компилятор
сам выводит шаблонные аргументы,

45
00:03:20,488 --> 00:03:24,990
и мы можем этой способностью
компилятора воспользоваться,

46
00:03:24,990 --> 00:03:30,815
чтобы упростить инстанцирование
нашего шаблона IteratorRange.

47
00:03:30,815 --> 00:03:34,642
Мы можем написать так
называемую порождающую функцию.

48
00:03:34,642 --> 00:03:36,476
Смотрите, как она выглядит.

49
00:03:36,476 --> 00:03:41,500
Мы пишем template <typename Iterator>,

50
00:03:41,500 --> 00:03:47,891
IteratorRange <Iterator>

51
00:03:47,891 --> 00:03:52,529
MakeRange, MakeRange.

52
00:03:52,529 --> 00:03:57,310
И она принимает у нас два

53
00:03:57,310 --> 00:04:03,610
параметра: Iterator begin, Iterator end,

54
00:04:03,610 --> 00:04:10,360
и возвращает IteratorRange<Iterator>

55
00:04:10,360 --> 00:04:15,610
{begin, end}.

56
00:04:15,610 --> 00:04:20,610
Вот так мы, такую функцию,
такой шаблон функции мы написали.

57
00:04:20,610 --> 00:04:25,208
И вот здесь при объявлении переменной
second_half мы можем сделать вот что,

58
00:04:25,208 --> 00:04:30,270
мы можем написать auto second_half

59
00:04:30,270 --> 00:04:38,300
= MakeRange от вот этих вот параметров.

60
00:04:38,300 --> 00:04:42,670
Компилируем, это компилируется.

61
00:04:42,670 --> 00:04:47,644
Это компилируется, и, например,

62
00:04:47,644 --> 00:04:55,780
мы можем пройтись по второй половине
нашего вектора v и вывести,

63
00:04:55,780 --> 00:05:00,220
убедиться, что у нас там правильно
все сформировалось, построилось.

64
00:05:00,220 --> 00:05:01,771
Так, у нас...

65
00:05:01,771 --> 00:05:07,780
Давайте не будем выводить здесь RangeSize,
он нам больше не нужен.

66
00:05:07,780 --> 00:05:09,459
Вот вывелось 4, 4, 5.

67
00:05:09,459 --> 00:05:13,955
Собственно, здесь у нас вывелся весь
вектор, здесь вывелась его вторая

68
00:05:13,955 --> 00:05:18,680
половина, 4, 4, 5, то есть действительно
создается правильный диапазон.

69
00:05:18,680 --> 00:05:23,740
И смотрите, что мы сделали, мы написали
функцию, шаблон функции MakeRange,

70
00:05:23,740 --> 00:05:29,200
с помощью его вызова проинициализировали
переменную second_half,

71
00:05:29,200 --> 00:05:31,360
указав в качестве ее типа auto.

72
00:05:31,360 --> 00:05:32,560
Код стал короче.

73
00:05:32,560 --> 00:05:36,511
При этом компилятор все делает за нас.

74
00:05:36,511 --> 00:05:41,438
Он сам по вызову функции
MakeRange понимаем тип итератора

75
00:05:41,438 --> 00:05:46,092
и с этим типом инстанцирует
шаблон IteratorRange.

76
00:05:46,092 --> 00:05:52,170
И таким образом мы с помощью порождающей
функции смогли сделать наш код короче.

77
00:05:52,170 --> 00:05:55,338
Давайте подведем итоги.

78
00:05:55,338 --> 00:06:00,597
В этом видео мы с вами познакомились
с порождающими функциями,

79
00:06:00,597 --> 00:06:05,518
которые позволяют возложить на
компилятор выведение шаблонных типов

80
00:06:05,518 --> 00:06:08,545
при инстанцировании шаблона класса.

81
00:06:08,545 --> 00:06:11,547
Какие преимущества у порождающих функций?

82
00:06:11,547 --> 00:06:15,694
Собственно, они сокращают
код и избавляют нас от

83
00:06:15,694 --> 00:06:20,009
необходимости больше печатать по клавишам.

84
00:06:20,009 --> 00:06:22,631
Но у порождающих функций
есть некоторые недостатки.

85
00:06:22,631 --> 00:06:27,202
Во-первых, их нужно писать самостоятельно,
то есть для каждого шаблона класса,

86
00:06:27,202 --> 00:06:32,170
который у нас есть, нам приходится рядом
отдельно писать порождающую функцию,

87
00:06:32,170 --> 00:06:37,440
чтобы она выполняла свою работу.

88
00:06:37,440 --> 00:06:42,710
Кроме того, как у меня здесь приведено
на экране, какой приведен пример,

89
00:06:42,710 --> 00:06:47,230
из записи auto, переменная, равно,

90
00:06:47,230 --> 00:06:50,906
MakeRange, что-то там, не очевидно,

91
00:06:50,906 --> 00:06:55,731
какой тип имеет переменная,
в данном случае full.

92
00:06:55,731 --> 00:07:01,172
То есть нам нужно пойти в объявление
функции MakeRange и посмотреть,

93
00:07:01,172 --> 00:07:02,486
что она возвращает.

94
00:07:02,486 --> 00:07:05,552
Это такой, пусть не глобальный,
не фатальный,

95
00:07:05,552 --> 00:07:09,095
но все-таки недостаток
порождающей функции.

96
00:07:09,095 --> 00:07:13,404
В следующем видео мы познакомимся
с еще одним способом

97
00:07:13,404 --> 00:07:18,767
перекладывания выведения
шаблонных типов на компилятор.