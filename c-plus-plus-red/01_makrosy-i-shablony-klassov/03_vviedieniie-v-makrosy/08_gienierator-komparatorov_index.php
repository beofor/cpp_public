<!DOCTYPE html>
<html class="client-nojs" lang="ru" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Цифровая сортировка — Викиконспекты</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Цифровая_сортировка","wgTitle":"Цифровая сортировка","wgCurRevisionId":64844,"wgRevisionId":64844,"wgArticleId":1264,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Дискретная математика и алгоритмы","Сортировки","Другие сортировки"],"wgBreakFrames":false,"wgPageContentLanguage":"ru","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t."," \t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь"],"wgMonthNamesShort":["","янв","фев","мар","апр","май","июн","июл","авг","сен","окт","ноя","дек"],"wgRelevantPageName":"Цифровая_сортировка","wgRelevantArticleId":1264,"wgRequestId":"1441f107f0c56739a46be8cb","wgIsProbablyEditable":true,"wgRelevantPageIsProbablyEditable":true,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgPreferredVariant":"ru","wgMFExpandAllSectionsUserOption":false,"wgMFDisplayWikibaseDescriptions":{"search":false,"nearby":false,"watchlist":false,"tagline":false},"wgSmjSize":110,"wgSmjUseChem":true,"wgSmjInlineMath":[["$","$"],["[math]","[/math]"]]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"loading","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.sectionAnchor":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.options@1dzjbvu",function($,jQuery,require,module){mw.user.options.set({"variant":"ru"});});mw.loader.implement("user.tokens@1sumy7b",function ( $, jQuery, require, module ) {
mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});/*@nomin*/

});mw.loader.load(["ext.SmjLocal","site","mediawiki.page.startup","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"]);});</script>
<link rel="stylesheet" href="/wiki/load.php?debug=false&amp;lang=ru&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/wiki/load.php?debug=false&amp;lang=ru&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<meta name="generator" content="MediaWiki 1.30.0"/>
<link rel="alternate" type="application/x-wiki" title="Править" href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit"/>
<link rel="edit" title="Править" href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/wiki/opensearch_desc.php" title="Викиконспекты (ru)"/>
<link rel="EditURI" type="application/rsd+xml" href="http://neerc.ifmo.ru/wiki/api.php?action=rsd"/>
<link rel="alternate" type="application/atom+xml" title="Викиконспекты — Atom-лента" href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/resources/lib/html5shiv/html5shiv.min.js"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Цифровая_сортировка rootpage-Цифровая_сортировка skin-vector action-view">		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

						<div class="mw-indicators mw-body-content">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="ru">Цифровая сортировка</h1>
									<div id="bodyContent" class="mw-body-content">
									<div id="siteSub" class="noprint">Материал из Викиконспекты</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Перейти к:					<a href="#mw-head">навигация</a>, 					<a href="#p-search">поиск</a>
				</div>
				<div id="mw-content-text" lang="ru" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p><b>Цифровая сортировка</b> (англ. <i>radix sort</i>) — один из алгоритмов сортировки, использующих внутреннюю структуру сортируемых объектов.
</p>
<div id="toc" class="toc"><div class="toctitle"><h2>Содержание</h2></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#.D0.90.D0.BB.D0.B3.D0.BE.D1.80.D0.B8.D1.82.D0.BC"><span class="tocnumber">1</span> <span class="toctext">Алгоритм</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#.D0.9A.D0.BE.D1.80.D1.80.D0.B5.D0.BA.D1.82.D0.BD.D0.BE.D1.81.D1.82.D1.8C_.D0.B0.D0.BB.D0.B3.D0.BE.D1.80.D0.B8.D1.82.D0.BC.D0.B0_LSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B8"><span class="tocnumber">1.1</span> <span class="toctext">Корректность алгоритма LSD-сортировки</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-3"><a href="#.D0.9F.D1.81.D0.B5.D0.B2.D0.B4.D0.BE.D0.BA.D0.BE.D0.B4"><span class="tocnumber">2</span> <span class="toctext">Псевдокод</span></a>
<ul>
<li class="toclevel-2 tocsection-4"><a href="#LSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B0"><span class="tocnumber">2.1</span> <span class="toctext">LSD-сортировка</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#MSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B0"><span class="tocnumber">2.2</span> <span class="toctext">MSD-сортировка</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-6"><a href="#.D0.A1.D0.BB.D0.BE.D0.B6.D0.BD.D0.BE.D1.81.D1.82.D1.8C"><span class="tocnumber">3</span> <span class="toctext">Сложность</span></a>
<ul>
<li class="toclevel-2 tocsection-7"><a href="#.D0.A1.D0.BB.D0.BE.D0.B6.D0.BD.D0.BE.D1.81.D1.82.D1.8C_LSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B8"><span class="tocnumber">3.1</span> <span class="toctext">Сложность LSD-сортировки</span></a></li>
<li class="toclevel-2 tocsection-8"><a href="#.D0.A1.D0.BB.D0.BE.D0.B6.D0.BD.D0.BE.D1.81.D1.82.D1.8C_MSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B8"><span class="tocnumber">3.2</span> <span class="toctext">Сложность MSD-сортировки</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-9"><a href="#.D0.A1.D0.BC._.D1.82.D0.B0.D0.BA.D0.B6.D0.B5"><span class="tocnumber">4</span> <span class="toctext">См. также</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#.D0.98.D1.81.D1.82.D0.BE.D1.87.D0.BD.D0.B8.D0.BA.D0.B8_.D0.B8.D0.BD.D1.84.D0.BE.D1.80.D0.BC.D0.B0.D1.86.D0.B8.D0.B8"><span class="tocnumber">5</span> <span class="toctext">Источники информации</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id=".D0.90.D0.BB.D0.B3.D0.BE.D1.80.D0.B8.D1.82.D0.BC">Алгоритм</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=1" title="Редактировать раздел «Алгоритм»">править</a><span class="mw-editsection-bracket">]</span></span></h2>
<div class="thumb tright"><div class="thumbinner" style="width:452px;"><a href="/wiki/index.php?title=%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0.png" class="image"><img alt="" src="/wiki/images/thumb/8/8a/%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0.png/450px-%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0.png" width="450" height="174" class="thumbimage" srcset="/wiki/images/8/8a/%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0.png 1.5x" /></a>  <div class="thumbcaption"><div class="magnify"><a href="/wiki/index.php?title=%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0.png" class="internal" title="Увеличить"></a></div>Пример цифровой сортировки трехзначных чисел, начиная с младших разрядов</div></div></div>
<div class="thumb tright"><div class="thumbinner" style="width:440px;"><a href="/wiki/index.php?title=%D0%A4%D0%B0%D0%B9%D0%BB:Msd-sort.png" class="image"><img alt="" src="/wiki/images/6/6e/Msd-sort.png" width="438" height="210" class="thumbimage" /></a>  <div class="thumbcaption"><div class="magnify"><a href="/wiki/index.php?title=%D0%A4%D0%B0%D0%B9%D0%BB:Msd-sort.png" class="internal" title="Увеличить"></a></div>Пример цифровой сортировки трехзначных чисел, начиная со старших разрядов</div></div></div>
<p>Имеем множество последовательностей одинаковой длины, состоящих из элементов, на которых задано <a href="/wiki/index.php?title=%D0%9E%D1%82%D0%BD%D0%BE%D1%88%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BF%D0%BE%D1%80%D1%8F%D0%B4%D0%BA%D0%B0" title="Отношение порядка">отношение линейного порядка</a>. Требуется отсортировать эти последовательности в лексикографическом порядке.
</p><p>По аналогии с разрядами чисел будем называть элементы, из которых состоят сортируемые объекты, разрядами. Сам алгоритм состоит в последовательной сортировке объектов какой-либо устойчивой сортировкой по каждому разряду, в порядке от младшего разряда к старшему, после чего последовательности будут расположены в требуемом порядке.
</p><p>Примерами объектов, которые удобно разбивать на разряды и сортировать по ним, являются числа и строки.
</p>
<ul><li>Для чисел уже существует понятие разряда, поэтому будем представлять числа как последовательности разрядов. Конечно, в разных системах счисления разряды одного и того же числа отличаются, поэтому перед сортировкой представим числа в удобной для нас системе счисления.</li></ul>
<ul><li>Строки представляют из себя последовательности символов, поэтому в качестве разрядов в данном случае выступают отдельные символы, сравнение которых обычно происходит по соответствующим им кодам из <a href="/wiki/index.php?title=%D0%9F%D1%80%D0%B5%D0%B4%D1%81%D1%82%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_%D1%81%D0%B8%D0%BC%D0%B2%D0%BE%D0%BB%D0%BE%D0%B2,_%D1%82%D0%B0%D0%B1%D0%BB%D0%B8%D1%86%D1%8B_%D0%BA%D0%BE%D0%B4%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%BA#.D0.A2.D0.B0.D0.B1.D0.BB.D0.B8.D1.86.D1.8B_.D0.BA.D0.BE.D0.B4.D0.B8.D1.80.D0.BE.D0.B2.D0.BE.D0.BA" title="Представление символов, таблицы кодировок">таблицы кодировок</a>. Для такого разбиения самый младший разряд — последний символ строки.</li></ul>
<p>Для вышеперечисленных объектов наиболее часто в качестве устойчивой сортировки применяют <a href="/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D0%BE%D0%B4%D1%81%D1%87%D0%B5%D1%82%D0%BE%D0%BC" class="mw-redirect" title="Сортировка подсчетом">сортировку подсчетом</a>.
</p><p>Такой подход к алгоритму называют <b>LSD-сортировкой</b> (Least Significant Digit radix sort). Существует модификация алгоритма цифровой сортировки, анализирующая значения разрядов, начиная слева, с наиболее значащих разрядов. Данный алгоритм известен, как <b>MSD-сортировка</b> (Most Significant Digit radix sort).
</p>
<h3><span class="mw-headline" id=".D0.9A.D0.BE.D1.80.D1.80.D0.B5.D0.BA.D1.82.D0.BD.D0.BE.D1.81.D1.82.D1.8C_.D0.B0.D0.BB.D0.B3.D0.BE.D1.80.D0.B8.D1.82.D0.BC.D0.B0_LSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B8">Корректность алгоритма LSD-сортировки</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=2" title="Редактировать раздел «Корректность алгоритма LSD-сортировки»">править</a><span class="mw-editsection-bracket">]</span></span></h3>
<p>Докажем, что данный алгоритм работает верно, используя метод математической индукции по номеру разряда. Пусть <span style='display:none'>[math] n [/math]</span> — количество разрядов в сортируемых объектах.
</p><p><b> База</b>: <span style='display:none'>[math] n = 1 [/math]</span>. Очевидно, что алгоритм работает верно, потому что в таком случае мы просто сортируем младшие разряды какой-то заранее выбранной устойчивой сортировкой.
</p><p><b> Переход</b>: Пусть для <span style='display:none'>[math] n = k [/math]</span> алгоритм правильно отсортировал последовательности по <span style='display:none'>[math] k [/math]</span> младшим разрядам. Покажем, что в таком случае, при сортировке по <span style='display:none'>[math] (k + 1) [/math]</span>-му разряду, последовательности также будут отсортированы в правильном порядке. 
</p><p>Вспомогательная сортировка разобьет все объекты на группы, в которых <span style='display:none'>[math] (k + 1) [/math]</span>-й разряд объектов одинаковый. Рассмотрим такие группы. Для сортировки по отдельным разрядам мы используем устойчивую сортировку, следовательно порядок объектов с одинаковым <span style='display:none'>[math] (k + 1) [/math]</span>-м разрядом не изменился. Но по предположению индукции по предыдущим <span style='display:none'>[math] k [/math]</span> разрядам последовательности были отсортированы правильно, и поэтому в каждой такой группе они будут отсортированы верно. Также верно, что сами группы находятся в правильном относительно друг друга порядке, а, следовательно, и все объекты отсортированы правильно по <span style='display:none'>[math] (k + 1) [/math]</span>-м младшим разрядам.
</p>
<h2><span class="mw-headline" id=".D0.9F.D1.81.D0.B5.D0.B2.D0.B4.D0.BE.D0.BA.D0.BE.D0.B4">Псевдокод</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=3" title="Редактировать раздел «Псевдокод»">править</a><span class="mw-editsection-bracket">]</span></span></h2>
<h3><span class="mw-headline" id="LSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B0">LSD-сортировка</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=4" title="Редактировать раздел «LSD-сортировка»">править</a><span class="mw-editsection-bracket">]</span></span></h3>
<p>В качестве примера рассмотрим сортировку чисел. Как говорилось выше, в такой ситуации в качестве устойчивой сортировки применяют сортировку подсчетом, так как обычно количество различных значений разрядов не превосходит количества сортируемых элементов. Ниже приведен псевдокод цифровой сортировки, которой подается массив <span style='display:none'>[math] A [/math]</span> размера <span style='display:none'>[math] n [/math]</span> <span style='display:none'>[math] m [/math]</span>-разрядных чисел . Сам по себе алгоритм представляет собой цикл по номеру разряда, на каждой итерации которого элементы массива <span style='display:none'>[math] A [/math]</span> размещаются в нужном порядке во вспомогательном массиве <span style='display:none'>[math] B [/math]</span>. Для подсчета количества объектов, <span style='display:none'>[math] i [/math]</span>-й разряд которых одинаковый, а затем и для определения положения объектов в массиве <span style='display:none'>[math] B [/math]</span> используется вспомогательный массив <span style='display:none'>[math] C [/math]</span>. Функция <span style='display:none'>[math] \mathrm{digit(x, i)} [/math]</span> возвращает <span style='display:none'>[math] i [/math]</span>-й разряд числа <span style='display:none'>[math] x [/math]</span>. Также считаем, что значения разрядов меньше <span style='display:none'>[math] k [/math]</span>.
</p>
<pre> <b>function</b> radixSort(int[] A):
     <b>for</b> i = 1 <b>to</b> m               
         <b>for</b> j = 0 <b>to</b> k - 1                              
             C[j] = 0                                  
         <b>for</b> j = 0 <b>to</b> n - 1
             d = digit(A[j], i)
             C[d]++
         count = 0
         <b>for</b> j = 0 <b>to</b> k - 1
             tmp = C[j]
             C[j] = count
             count += tmp
         <b>for</b> j = 0 <b>to</b> n - 1
             d = digit(A[j], i)                             
             B[C[d]] = A[j]            
             C[d]++
         A = B
</pre>
<h3><span class="mw-headline" id="MSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B0">MSD-сортировка</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=5" title="Редактировать раздел «MSD-сортировка»">править</a><span class="mw-editsection-bracket">]</span></span></h3>
<p>Будем считать, что у всех элементов одинаковое число разрядов. Если это не так, то положим на более старших разрядах элементы с самым маленьким значением — для чисел это <span style='display:none'>[math]0[/math]</span>. Сначала исходный массив делится на <span style='display:none'>[math]k[/math]</span> частей, где <span style='display:none'>[math]k[/math]</span> — основание, выбранное для представления сортируемых объектов. Эти части принято называть "корзинами" или "карманами". В первую корзину попадают элементы, у которых старший разряд с номером <span style='display:none'>[math]d = 0[/math]</span> имеет значение <span style='display:none'>[math]0[/math]</span>. Во вторую корзину попадают элементы, у которых старший разряд с номером <span style='display:none'>[math]d = 0[/math]</span> имеет значение <span style='display:none'>[math]1[/math]</span> и так далее. Затем элементы, попавшие в разные корзины, подвергаются рекурсивному разделению по следующему разряду с номером <span style='display:none'>[math]d = 1[/math]</span>. Рекурсивный процесс разделения продолжается, пока не будут перебраны все разряды сортируемых объектов и пока размер корзины больше единицы. То есть останавливаемся когда <span style='display:none'>[math]d \gt  m[/math]</span> или <span style='display:none'>[math]l \geqslant r[/math]</span>, где m — максимальное число разрядов в сортируемых объектах, <span style='display:none'>[math]l[/math]</span>, <span style='display:none'>[math]r[/math]</span> — левая и правая границы отрезка массива <span style='display:none'>[math]A[/math]</span>.
</p><p>В основу распределения элементов по корзинам положен метод распределяющего подсчета элементов с одинаковыми значениями в сортируемом разряде. Для этого выполняется просмотр массива и подсчет количества элементов с различными значениями в сортируемом разряде. Эти счетчики фиксируются во вспомогательном массиве счетчиков <span style='display:none'>[math]cnt[/math]</span>. Затем счетчики используются для вычисления размеров корзин и определения границ разделения массива. В соответствии с этими границами сортируемые объекты переносятся во вспомогательный массив <span style='display:none'>[math]c[/math]</span>, в котором размещены корзины.
После того как корзины сформированы, содержимое вспомогательного массива <span style='display:none'>[math]c[/math]</span> переносится обратно в исходный массив <span style='display:none'>[math]A[/math]</span> и выполняется рекурсивное разделение новых частей по следующему разряду в пределах границ корзин, полученных на предыдущем шаге.
</p><p>Изначально запускаем функцию так <span style='display:none'>[math]\mathrm{radixSort(A, 0, A.length - 1, 1)}[/math]</span>
</p>
<pre> <b>function</b> radixSort(int[] A, int l, int r, int d):
     <b>if</b> d &gt; m <b>or</b> l &gt;= r 
         <b>return</b>
     <b>for</b> j = 0 <b>to</b> k + 1 
         cnt[j] = 0
     <b>for</b> i = l <b>to</b> r                              
         j = digit(A[i], d)
         cnt[j + 1]++
     <b>for</b> j = 2 <b>to</b> k
         cnt[j] += cnt[j - 1]
     <b>for</b> i = l <b>to</b> r
         j = digit(A[i], d)
         c[l + cnt[j]] = A[i]
         cnt[j]--
     <b>for</b> i = l <b>to</b> r
         A[i] = c[i]
     radixSort(A, l, l + cnt[0] - 1, d + 1)
     <b>for</b> i = 1 <b>to</b> k
         radixSort(A, l + cnt[i - 1], l + cnt[i] - 1, d + 1)
</pre>
<h2><span class="mw-headline" id=".D0.A1.D0.BB.D0.BE.D0.B6.D0.BD.D0.BE.D1.81.D1.82.D1.8C">Сложность</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=6" title="Редактировать раздел «Сложность»">править</a><span class="mw-editsection-bracket">]</span></span></h2>
<h3><span class="mw-headline" id=".D0.A1.D0.BB.D0.BE.D0.B6.D0.BD.D0.BE.D1.81.D1.82.D1.8C_LSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B8">Сложность LSD-сортировки</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=7" title="Редактировать раздел «Сложность LSD-сортировки»">править</a><span class="mw-editsection-bracket">]</span></span></h3>
<p>Пусть <span style='display:none'>[math] m [/math]</span> — количество разрядов, <span style='display:none'>[math] n [/math]</span> — количество объектов, которые нужно отсортировать, <span style='display:none'>[math] T(n) [/math]</span> — время работы устойчивой сортировки. Цифровая сортировка выполняет <span style='display:none'>[math] k [/math]</span> итераций, на каждой из которой выполняется устойчивая сортировка и не более <span style='display:none'>[math] O(1) [/math]</span> других операций. Следовательно время работы цифровой сортировки — <span style='display:none'>[math] O(k T(n)) [/math]</span>.
</p><p>Рассмотрим отдельно случай сортировки чисел. Пусть в качестве аргумента сортировке передается массив, в котором содержатся <span style='display:none'>[math] n [/math]</span> <span style='display:none'>[math] m [/math]</span>-значных чисел, и каждая цифра может принимать значения от <span style='display:none'>[math] 0 [/math]</span> до <span style='display:none'>[math] k - 1 [/math]</span>. Тогда цифровая сортировка позволяет отсортировать данный массив за время <span style='display:none'>[math] O(m (n + k)) [/math]</span>, если устойчивая сортировка имеет время работы <span style='display:none'>[math] O(n + k) [/math]</span>. Если <span style='display:none'>[math] k [/math]</span> небольшое, то оптимально выбирать в качестве устойчивой сортировки сортировку подсчетом.
</p><p>Если количество разрядов — константа, а <span style='display:none'>[math] k = O(n) [/math]</span>, то сложность цифровой сортировки составляет <span style='display:none'>[math] O(n) [/math]</span>, то есть она линейно зависит от количества сортируемых чисел.
</p>
<h3><span class="mw-headline" id=".D0.A1.D0.BB.D0.BE.D0.B6.D0.BD.D0.BE.D1.81.D1.82.D1.8C_MSD-.D1.81.D0.BE.D1.80.D1.82.D0.B8.D1.80.D0.BE.D0.B2.D0.BA.D0.B8">Сложность MSD-сортировки</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=8" title="Редактировать раздел «Сложность MSD-сортировки»">править</a><span class="mw-editsection-bracket">]</span></span></h3>
<p>Пусть значения разрядов меньше <span style='display:none'>[math]b[/math]</span>, а количество разрядов — <span style='display:none'>[math]k[/math]</span>. При сортировке массива из одинаковых элементов MSD-сортировкой на каждом шаге все элементы будут находится в неубывающей по размеру корзине, а так как цикл идет по всем элементам массива, то получим, что время работы MSD-сортировки оценивается величиной <span style='display:none'>[math]O(nk)[/math]</span>, причем это время нельзя улучшить. Хорошим случаем для данной сортировки будет массив, при котором на каждом шаге каждая корзина будет делиться на <span style='display:none'>[math]b[/math]</span> частей. Как только размер корзины станет равен <span style='display:none'>[math]1[/math]</span>, сортировка перестанет рекурсивно запускаться в этой корзине. Таким образом, асимптотика будет <span style='display:none'>[math]\Omega(n\log_b{n})[/math]</span>. Это хорошо тем, что не зависит от числа разрядов.
</p><p>Существует также модификация MSD-сортировки, при которой рекурсивный процесс останавливается при небольших размерах текущего кармана, и вызывается более быстрая сортировка, основанная на сравнениях (например, сортировка вставками).
</p>
<h2><span class="mw-headline" id=".D0.A1.D0.BC._.D1.82.D0.B0.D0.BA.D0.B6.D0.B5">См. также</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=9" title="Редактировать раздел «См. также»">править</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul><li> <a href="/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D0%BE%D0%B4%D1%81%D1%87%D0%B5%D1%82%D0%BE%D0%BC" class="mw-redirect" title="Сортировка подсчетом">Сортировка подсчетом</a></li>
<li> <a href="/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%B2%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8" title="Сортировка вставками">Сортировка вставками</a></li></ul>
<h2><span class="mw-headline" id=".D0.98.D1.81.D1.82.D0.BE.D1.87.D0.BD.D0.B8.D0.BA.D0.B8_.D0.B8.D0.BD.D1.84.D0.BE.D1.80.D0.BC.D0.B0.D1.86.D0.B8.D0.B8">Источники информации</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit&amp;section=10" title="Редактировать раздел «Источники информации»">править</a><span class="mw-editsection-bracket">]</span></span></h2>
<ul><li> <a href="http://en.wikipedia.org/wiki/ru:%D0%9F%D0%BE%D1%80%D0%B0%D0%B7%D1%80%D1%8F%D0%B4%D0%BD%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" class="extiw" title="wikipedia:ru:Поразрядная сортировка">Википедия — Цифровая сортировка</a></li>
<li> <a rel="nofollow" class="external text" href="http://rain.ifmo.ru/cat/view.php/vis/sorts/linear-2005">Визуализатор 1</a> — Java-аплет.</li>
<li> <a rel="nofollow" class="external text" href="http://rain.ifmo.ru/cat/view.php/vis/sorts/linear-2001">Визуализатор 2</a> — Java-аплет.</li>
<li> Дональд Кнут Искусство программирования, том 3. Сортировка и поиск</li>
<li> Кормен, Т., Лейзерсон, Ч., Ривест, Р., Штайн, К. Алгоритмы: построение и анализ</li></ul>

<!-- 
NewPP limit report
Cached time: 20200923102852
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.020 seconds
Real time usage: 0.025 seconds
Preprocessor visited node count: 608/1000000
Preprocessor generated node count: 1328/1000000
Post‐expand include size: 30/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    4.606      1 -total
 44.29%    2.040     10 Шаблон:---
-->
</div>
<!-- Saved in parser cache with key wikidb:pcache:idhash:1264-0!canonical and timestamp 20200923102852 and revision id 64844
 -->
</div>					<div class="printfooter">
						Источник — «<a dir="ltr" href="http://neerc.ifmo.ru/wiki/index.php?title=Цифровая_сортировка&amp;oldid=64844">http://neerc.ifmo.ru/wiki/index.php?title=Цифровая_сортировка&amp;oldid=64844</a>»					</div>
				<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D0%B8" title="Служебная:Категории">Категории</a>: <ul><li><a href="/wiki/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%94%D0%B8%D1%81%D0%BA%D1%80%D0%B5%D1%82%D0%BD%D0%B0%D1%8F_%D0%BC%D0%B0%D1%82%D0%B5%D0%BC%D0%B0%D1%82%D0%B8%D0%BA%D0%B0_%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D1%8B" title="Категория:Дискретная математика и алгоритмы">Дискретная математика и алгоритмы</a></li><li><a href="/wiki/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8" title="Категория:Сортировки">Сортировки</a></li><li><a href="/wiki/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%94%D1%80%D1%83%D0%B3%D0%B8%D0%B5_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8" title="Категория:Другие сортировки">Другие сортировки</a></li></ul></div></div>				<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Навигация</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Персональные инструменты</h3>
						<ul>
							<li id="pt-anonuserpage">Вы не представились системе</li><li id="pt-anontalk"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%9C%D0%BE%D1%91_%D0%BE%D0%B1%D1%81%D1%83%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5" title="Страница обсуждений для моего IP [n]" accesskey="n">Обсуждение</a></li><li id="pt-anoncontribs"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%9C%D0%BE%D0%B9_%D0%B2%D0%BA%D0%BB%D0%B0%D0%B4" title="Список правок, сделанных с этого IP-адреса [y]" accesskey="y">Вклад</a></li><li id="pt-createaccount"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D1%82%D1%8C_%D1%83%D1%87%D1%91%D1%82%D0%BD%D1%83%D1%8E_%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C&amp;returnto=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F+%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" title="Мы предлагаем вам создать учётную запись и войти в систему, хотя это и не обязательно.">Создать учётную запись</a></li><li id="pt-login"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%92%D1%85%D0%BE%D0%B4&amp;returnto=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F+%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" title="Здесь можно зарегистрироваться в системе, но это необязательно. [o]" accesskey="o">Войти</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Пространства имён</h3>
						<ul>
														<li id="ca-nstab-main" class="selected"><span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" title="Просмотр основной страницы [c]" accesskey="c">Статья</a></span></li>
							<li id="ca-talk"><span><a href="/wiki/index.php?title=%D0%9E%D0%B1%D1%81%D1%83%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5:%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" rel="discussion" title="Обсуждение основной страницы [t]" accesskey="t">Обсуждение</a></span></li>
						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label">
							<span>Варианты</span>
						</h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Просмотры</h3>
						<ul>
														<li id="ca-view" class="selected"><span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0">Читать</a></span></li>
							<li id="ca-edit"><span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=edit" title="Редактировать данную страницу [e]" accesskey="e">Править</a></span></li>
							<li id="ca-history" class="collapsible"><span><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=history" title="Журнал изменений страницы [h]" accesskey="h">История</a></span></li>
						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Ещё</span></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Поиск</label>
						</h3>

						<form action="/wiki/index.php" id="searchform">
							<div id="simpleSearch">
							<input type="search" name="search" placeholder="Искать в Викиконспекты" title="Искать в Викиконспекты [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Служебная:Поиск" name="title"/><input type="submit" name="fulltext" value="Найти" title="Найти страницы, содержащие указанный текст" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Перейти" title="Перейти к странице, имеющей в точности такое название" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/index.php?title=%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0"  title="Перейти на заглавную страницу"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Навигация</h3>

			<div class="body">
									<ul>
						<li id="n-mainpage-description"><a href="/wiki/index.php?title=%D0%97%D0%B0%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0" title="Перейти на заглавную страницу [z]" accesskey="z">Заглавная страница</a></li><li id="n-recentchanges"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D0%B5%D0%B6%D0%B8%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8" title="Список последних изменений [r]" accesskey="r">Свежие правки</a></li><li id="n-randompage"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0" title="Посмотреть случайно выбранную страницу [x]" accesskey="x">Случайная статья</a></li><li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Место, где можно получить справку">Справка</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Инструменты</h3>

			<div class="body">
									<ul>
						<li id="t-whatlinkshere"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8_%D1%81%D1%8E%D0%B4%D0%B0/%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" title="Список всех страниц, ссылающихся на данную [j]" accesskey="j">Ссылки сюда</a></li><li id="t-recentchangeslinked"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%B2%D1%8F%D0%B7%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5_%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B8/%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0" rel="nofollow" title="Последние изменения в страницах, на которые ссылается эта страница [k]" accesskey="k">Связанные правки</a></li><li id="t-specialpages"><a href="/wiki/index.php?title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BF%D0%B5%D1%86%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D1%8B" title="Список служебных страниц [q]" accesskey="q">Спецстраницы</a></li><li id="t-print"><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;printable=yes" rel="alternate" title="Версия этой страницы для печати [p]" accesskey="p">Версия для печати</a></li><li id="t-permalink"><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;oldid=64844" title="Постоянная ссылка на эту версию страницы">Постоянная ссылка</a></li><li id="t-info"><a href="/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;action=info" title="Подробнее об этой странице">Сведения о странице</a></li>					</ul>
							</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Эта страница последний раз была отредактирована 7 апреля 2018 в 03:05.</li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="/wiki/index.php?title=%D0%92%D0%B8%D0%BA%D0%B8%D0%BA%D0%BE%D0%BD%D1%81%D0%BF%D0%B5%D0%BA%D1%82%D1%8B:%D0%9F%D0%BE%D0%BB%D0%B8%D1%82%D0%B8%D0%BA%D0%B0_%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B4%D0%B5%D0%BD%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8" title="Викиконспекты:Политика конфиденциальности">Политика конфиденциальности</a></li>
											<li id="footer-places-about"><a href="/wiki/index.php?title=%D0%92%D0%B8%D0%BA%D0%B8%D0%BA%D0%BE%D0%BD%D1%81%D0%BF%D0%B5%D0%BA%D1%82%D1%8B:%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5" class="mw-redirect" title="Викиконспекты:Описание">О Викиконспекты</a></li>
											<li id="footer-places-disclaimer"><a href="/wiki/index.php?title=%D0%92%D0%B8%D0%BA%D0%B8%D0%BA%D0%BE%D0%BD%D1%81%D0%BF%D0%B5%D0%BA%D1%82%D1%8B:%D0%9E%D1%82%D0%BA%D0%B0%D0%B7_%D0%BE%D1%82_%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8" title="Викиконспекты:Отказ от ответственности">Отказ от ответственности</a></li>
											<li id="footer-places-mobileview"><a href="http://neerc.ifmo.ru/wiki/index.php?title=%D0%A6%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&amp;mobileaction=toggle_view_mobile" class="noprint stopMobileRedirectToggle">Мобильная версия</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-poweredbyico">
							<a href="//www.mediawiki.org/"><img src="/wiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/wiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /wiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>						</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.020","walltime":"0.025","ppvisitednodes":{"value":608,"limit":1000000},"ppgeneratednodes":{"value":1328,"limit":1000000},"postexpandincludesize":{"value":30,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"timingprofile":["100.00%    4.606      1 -total"," 44.29%    2.040     10 Шаблон:---"]},"cachereport":{"timestamp":"20200923102852","ttl":86400,"transientcontent":false}}});});</script><script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":389});});</script>
	</body>
</html>
