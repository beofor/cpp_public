1
00:00:00,000 --> 00:00:06,012
[БЕЗ_ЗВУКА] Итак,

2
00:00:06,012 --> 00:00:11,010
в предыдущем видео мы с помощью
макросов смогли сделать более удобным

3
00:00:11,010 --> 00:00:14,094
запуск юнит-тестов в нашем фреймворке.

4
00:00:14,094 --> 00:00:18,996
Давайте теперь попытаемся
упростить использование

5
00:00:18,996 --> 00:00:22,720
шаблонов ASSERT и ASSERT EQUAL.

6
00:00:22,720 --> 00:00:27,295
Итак, значит, наша проблема состояла
в том, что нам необходимо было

7
00:00:27,295 --> 00:00:32,008
придумывать вот это вот сообщение
для каждого ASSERT уникальное,

8
00:00:32,008 --> 00:00:35,546
чтобы по нему понимать,
какой ASSERT сработал.

9
00:00:35,546 --> 00:00:40,672
На самом деле, какой информации нам
могло бы быть достаточно для того,

10
00:00:40,672 --> 00:00:46,136
чтобы, увидев её в консоли,
понимать, какой ASSERT сработал?

11
00:00:46,136 --> 00:00:51,478
На самом деле нам достаточно имени
файла и строки в этом файле.

12
00:00:51,478 --> 00:00:55,968
И эту информацию мы можем
получить автоматически.

13
00:00:55,968 --> 00:00:59,303
Для этого есть специальные макросы.

14
00:00:59,303 --> 00:01:06,155
Давайте для начала продемонстрируем их,
как они применяются.

15
00:01:06,155 --> 00:01:09,829
Мы можем написать, например,

16
00:01:09,829 --> 00:01:15,515
const string file,

17
00:01:15,515 --> 00:01:19,700
равно FILE,

18
00:01:19,700 --> 00:01:24,280
точка с запятой и

19
00:01:24,280 --> 00:01:30,241
const int line = LINE.

20
00:01:30,241 --> 00:01:33,235
Компилируем.

21
00:01:33,235 --> 00:01:34,661
Компилируется.

22
00:01:34,661 --> 00:01:37,321
И давайте снова посмотрим на то,

23
00:01:37,321 --> 00:01:43,640
как эти макросы раскрываются.

24
00:01:43,640 --> 00:01:46,328
Значит, смотрим.

25
00:01:46,328 --> 00:01:53,910
Вот здесь в нашем файле на 14-й
строке мы объявили переменную line,

26
00:01:53,910 --> 00:01:59,247
присвоив ей значение макроса LINE,

27
00:01:59,247 --> 00:02:03,097
и мы видим, что line стал равен 14.

28
00:02:03,097 --> 00:02:07,201
При этом file наш
называется macro_intro.cpp.

29
00:02:07,201 --> 00:02:11,260
И вот он: const string
file = "macro_intro.cpp".

30
00:02:11,260 --> 00:02:17,592
Таким образом,
у нас есть специальные макросы,

31
00:02:17,592 --> 00:02:22,780
которые препроцессор создаёт
и обрабатывает автоматически,

32
00:02:22,780 --> 00:02:28,998
которые могут нам сказать, что это за
файл и что это за строчка в нашем файле.

33
00:02:28,998 --> 00:02:30,781
Мы можем ими воспользоваться,

34
00:02:30,781 --> 00:02:33,987
чтобы формировать уникальное
сообщение для ASSERT.

35
00:02:33,987 --> 00:02:35,609
И давайте сделаем это.

36
00:02:35,609 --> 00:02:37,881
Поступим мы следующим образом.

37
00:02:37,881 --> 00:02:45,360
Мы добавим

38
00:02:45,360 --> 00:02:50,830
заголовочный файл stream,

39
00:02:50,830 --> 00:02:56,816
откроем фигурную скобку,
создадим новый блок,

40
00:02:56,816 --> 00:03:01,985
в нём создадим ostringstream — выходной

41
00:03:01,985 --> 00:03:07,400
строковый поток — и в этот поток

42
00:03:07,400 --> 00:03:11,997
выведем значение

43
00:03:11,997 --> 00:03:16,899
макроса file, потом двоеточие,

44
00:03:16,899 --> 00:03:22,382
пробел, даже можно без пробела, и LINE.

45
00:03:22,382 --> 00:03:28,836
И теперь в качестве сообщения
нашего шаблона AssertEqual мы

46
00:03:28,836 --> 00:03:35,120
используем os.str и
закроем фигурную скобку.

47
00:03:35,120 --> 00:03:40,290
Вы можете видеть,
что Eclipse тут что-то нам подчёркивает,

48
00:03:40,290 --> 00:03:44,097
ему что-то не нравится,
и он правильно нам сообщает.

49
00:03:44,097 --> 00:03:47,020
Чтобы создавать многострочные макросы,

50
00:03:47,020 --> 00:03:52,020
каждую строчку макроса кроме последней
нужно завершать нисходящим slash.

51
00:03:52,020 --> 00:03:56,185
Поэтому вот я их даже здесь выровняю.

52
00:03:56,185 --> 00:04:00,834
Если мы после каждой
строчки проставим slash,

53
00:04:00,834 --> 00:04:04,860
кроме последней, вот последняя
строчка с закрывающейся скобкой,

54
00:04:04,860 --> 00:04:09,501
то у нас пропадут какие-то
сообщения от Eclipse пугающие,

55
00:04:09,501 --> 00:04:13,700
и у нас получится правильно,
корректно сформированный макрос.

56
00:04:13,700 --> 00:04:16,526
Давайте теперь им воспользуемся.

57
00:04:16,526 --> 00:04:20,573
Вот у нас есть наш уже вызов,
макрос ASSERT EQUAL.

58
00:04:20,573 --> 00:04:24,214
Теперь он у нас стал макросом от
двух параметров, а не от трёх.

59
00:04:24,214 --> 00:04:28,600
Сообщение нам больше не нужно передавать,
оно генерируется автоматически.

60
00:04:28,600 --> 00:04:34,770
Поэтому мы свободно удаляем это сообщение.

61
00:04:34,770 --> 00:04:42,128
[БЕЗ_ЗВУКА] И у нас
получается вот такой вызов.

62
00:04:42,128 --> 00:04:47,253
ASSERT EQUAL, первый параметр,
вот он, и второй параметр.

63
00:04:47,253 --> 00:04:48,730
Компилируем.

64
00:04:48,730 --> 00:04:51,239
Компилируется.

65
00:04:51,239 --> 00:04:53,080
Запускаем.

66
00:04:53,080 --> 00:04:55,285
И смотрим, что вывелось в консоль.

67
00:04:55,285 --> 00:04:59,770
В консоль вывелось: TestDefaultConstructor
fail: Assertion failed,

68
00:04:59,770 --> 00:05:00,990
ноль не равно один.

69
00:05:00,990 --> 00:05:04,553
И теперь самое интересное.

70
00:05:04,553 --> 00:05:05,436
Вот hint.

71
00:05:05,436 --> 00:05:07,983
Это у нас фреймворк эту строчку выводит.

72
00:05:07,983 --> 00:05:13,781
Здесь выведен, выведено имя
файла: src/macro_intro.cpp и 19.

73
00:05:13,781 --> 00:05:17,750
Это строка, к которой этот ASSERT вызван.

74
00:05:17,750 --> 00:05:19,152
Проверяем.

75
00:05:19,152 --> 00:05:23,099
Вот файл macro_intro и
ASSERT в 19-й строке.

76
00:05:23,099 --> 00:05:26,420
Теперь мы видим,
что вот этот ASSERT сработал корректно,

77
00:05:26,420 --> 00:05:30,912
а вот этот выявил, нашёл ошибку,
и сработал именно он.

78
00:05:30,912 --> 00:05:33,691
И мы понимаем, что с нашим методом,

79
00:05:33,691 --> 00:05:38,273
возвращающим в знаменатель что-то не то,
мы можем пойти и понять,

80
00:05:38,273 --> 00:05:42,718
что да, у нас здесь действительно
есть ошибка, и исправить её.

81
00:05:42,718 --> 00:05:45,081
Но пока мы её исправлять не будем.

82
00:05:45,081 --> 00:05:51,633
Потому что мы можем вот это наше
сообщение даже немного усовершенствовать,

83
00:05:51,633 --> 00:05:56,457
сделать его более наглядным
и более подробным.

84
00:05:56,457 --> 00:06:02,496
Мы можем вывести те аргументы,
которые мы сравниваем между собой.

85
00:06:02,496 --> 00:06:03,714
Как мы это сделаем?

86
00:06:03,714 --> 00:06:06,939
С помощью уже знакомого
нам оператора «решётка».

87
00:06:06,939 --> 00:06:13,837
Мы можем вывести в поток x,
потом вывести не равно y,

88
00:06:13,837 --> 00:06:20,704
тоже с решёткой и через

89
00:06:20,704 --> 00:06:25,779
запятую можем

90
00:06:25,779 --> 00:06:30,746
вывести файл и

91
00:06:30,746 --> 00:06:36,113
строчку в этом файле.

92
00:06:36,113 --> 00:06:37,131
Компилируем.

93
00:06:37,131 --> 00:06:38,306
Компилируется.

94
00:06:38,306 --> 00:06:39,168
Запускаем.

95
00:06:39,168 --> 00:06:42,390
Смотрим, какое сообщение
мы в итоге получили.

96
00:06:42,390 --> 00:06:48,690
Мы получили сообщение,

97
00:06:48,690 --> 00:06:53,100
в котором написано, что
defaultConstructed.Denominator не равно 1.

98
00:06:53,100 --> 00:06:58,980
Где?
В файле macro_intro.cpp в строчке 20.

99
00:06:58,980 --> 00:07:03,680
И мы находим вот этот макрос.

100
00:07:03,680 --> 00:07:11,810
И мы сразу понимаем,
какое выражение у нас оказалось ложным.

101
00:07:11,810 --> 00:07:17,675
И остался нам ещё один штрих,

102
00:07:17,675 --> 00:07:22,891
который надо сделать,
чтобы получить удобное средство для

103
00:07:22,891 --> 00:07:28,913
тестирования.Это перенести вот эти
наши макросы в файл test_runner.h.

104
00:07:28,913 --> 00:07:35,520
Уберём макрос, ASSERT EQUAL переносим

105
00:07:35,520 --> 00:07:43,000
в файл test_runner.h,

106
00:07:43,000 --> 00:07:49,890
дублируем этот макрос,

107
00:07:49,890 --> 00:07:54,360
превращая его в ASSERT,
который принимает только одно

108
00:07:54,360 --> 00:08:01,405
логическое значение,

109
00:08:01,405 --> 00:08:08,450
и соответствующим образом
форматируем сообщение.

110
00:08:08,450 --> 00:08:09,734
Готово.

111
00:08:09,734 --> 00:08:13,590
И есть у нас ещё один макрос.

112
00:08:13,590 --> 00:08:17,800
Это RUN_TEST.

113
00:08:17,800 --> 00:08:22,010
Берём его, также переносим TestRunner.

114
00:08:22,010 --> 00:08:28,090
Компилируем.

115
00:08:28,090 --> 00:08:29,165
Компилируется.

116
00:08:29,165 --> 00:08:34,765
Ну и последняя мелочь, которую нам
осталось сделать, — это внедрить макрос

117
00:08:34,765 --> 00:08:40,280
ASSERT EQUAL во всю нашу

118
00:08:40,280 --> 00:08:45,945
программу.

119
00:08:45,945 --> 00:08:47,232
Компилируем.

120
00:08:47,232 --> 00:08:48,717
Компилируется.

121
00:08:48,717 --> 00:08:49,707
Работает.

122
00:08:49,707 --> 00:08:55,180
Теперь идём в класс Rational,
исправляем в нём допущенную ранее ошибку,

123
00:08:55,180 --> 00:08:57,810
компилируем, запускаем — все тесты прошли.

124
00:08:57,810 --> 00:08:59,038
Отлично.

125
00:08:59,038 --> 00:09:02,176
Давайте подводить итоги.

126
00:09:02,176 --> 00:09:07,305
В этом видео мы с вами посмотрели,
как специальные макросы

127
00:09:07,305 --> 00:09:12,215
FILE и LINE автоматизируют формирование
сообщения для нашего ассерта.

128
00:09:12,215 --> 00:09:14,805
То есть мы смогли
автоматизировать ту работу,

129
00:09:14,805 --> 00:09:17,600
которую до этого нам
приходилось делать вручную.

130
00:09:17,600 --> 00:09:22,933
Благодаря использованию макросов
в нашем тестовом фреймворке

131
00:09:22,933 --> 00:09:27,797
мы смогли сделать его удобнее
в использовании и лаконичнее.