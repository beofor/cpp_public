1
00:00:06,190 --> 00:00:12,571
Здравствуйте! Мы с вами начинаем третий курс нашей специализации - красный пояс по С++.

2
00:00:12,571 --> 00:00:15,080
После первых двух курсов вы уже хорошо овладели

3
00:00:15,080 --> 00:00:18,420
возможностями языка и применили их в нескольких десятках задач.

4
00:00:18,420 --> 00:00:21,770
Компании по всему миру разрабатываю на С++: поиск,

5
00:00:21,770 --> 00:00:26,240
браузеры, back-end социальных сетей и многих других сервисов.

6
00:00:26,240 --> 00:00:29,600
На С++ написаны популярные библиотеки машинного обучения,

7
00:00:29,600 --> 00:00:34,115
а также этот язык активно используется в области разработки игр.

8
00:00:34,115 --> 00:00:36,871
В Яндексе С++ можно встретить в поиске,

9
00:00:36,871 --> 00:00:40,390
рекламе, браузере, картах и многих-многих других продуктах.

10
00:00:40,390 --> 00:00:42,890
С++ позволяет писать эффективный код тогда.

11
00:00:42,890 --> 00:00:44,930
Когда производительности других популярных

12
00:00:44,930 --> 00:00:47,794
языков программирования оказывается недостаточно.

13
00:00:47,794 --> 00:00:50,840
Это актуально не только для десктопных и сервисных программ,

14
00:00:50,840 --> 00:00:52,474
но и для мобильных приложений,

15
00:00:52,474 --> 00:00:56,280
где помимо скорости необходимо экономить еще и батарейку,

16
00:00:56,280 --> 00:00:59,750
и в этом курсе мы вам покажем как это делается.

17
00:00:59,750 --> 00:01:01,010
Но начнем мы с двух вещей,

18
00:01:01,010 --> 00:01:05,645
без которых освещение возможности языка было бы неполным - макросов и шаблонов классов.

19
00:01:05,645 --> 00:01:09,050
Макросы в С++ пользуются не самой лучшей славой.

20
00:01:09,050 --> 00:01:14,380
Однако бывают ситуации, когда без них невозможно добиться простоты и читаемости кода.

21
00:01:14,380 --> 00:01:16,730
Мы познакомим вас с такими ситуациями

22
00:01:16,730 --> 00:01:19,940
и применим макросы для улучшения библиотеки тестирования,

23
00:01:19,940 --> 00:01:22,886
которую мы разработали в желтом поясе по С++.

24
00:01:22,886 --> 00:01:25,430
В предыдущем курсе вы научились создавать шаблоны функций,

25
00:01:25,430 --> 00:01:27,665
в этом же курсе мы познакомим вас с шаблонами классов.

26
00:01:27,665 --> 00:01:29,030
Начиная со второй недели,

27
00:01:29,030 --> 00:01:32,940
мы перейдем к основной теме нашего курса: создания быстрых программ,

28
00:01:32,940 --> 00:01:34,370
и начнем мы с того, что поговорим,

29
00:01:34,370 --> 00:01:36,505
а что же такое быстрые программы?

30
00:01:36,505 --> 00:01:39,830
Для этого мы рассмотрим принципы оптимизации кода.

31
00:01:39,830 --> 00:01:43,045
Затем вас ждет очень важная тема - сложность алгоритмов.

32
00:01:43,045 --> 00:01:45,800
Уметь оценивать асимптотику алгоритмов критически важно,

33
00:01:45,800 --> 00:01:48,030
когда речь заходит об оптимизации программ.

34
00:01:48,030 --> 00:01:51,190
Мы расскажем вам, что такое асимптотика и научим ее считать.

35
00:01:51,190 --> 00:01:54,363
Дальше мы перейдем к изучению модели памяти в С++.

36
00:01:54,363 --> 00:01:55,805
Это очень знаковый блок,

37
00:01:55,805 --> 00:01:59,149
потому что в нем впервые прозвучит слово указатель,

38
00:01:59,149 --> 00:02:02,870
хотя обычно о них рассказывают на самой первой лекции по С++.

39
00:02:02,870 --> 00:02:06,050
Освоив модель памяти мы перейдем к важнейшей теме

40
00:02:06,050 --> 00:02:10,410
- изучению внутреннего устройства и эффективного использования линейных контейнеров.

41
00:02:10,410 --> 00:02:12,800
Как говорится, если хочешь на что то влиять,

42
00:02:12,800 --> 00:02:14,690
эффективно использовать, нужно понимать,

43
00:02:14,690 --> 00:02:16,255
как оно работает внутри,

44
00:02:16,255 --> 00:02:18,555
поэтому мы заглянем внутрь вектора дека,

45
00:02:18,555 --> 00:02:20,935
а также изучим пару новых контейнеров.

46
00:02:20,935 --> 00:02:24,979
На пятой неделе курса вас ждет не менее важная тема - move семантика.

47
00:02:24,979 --> 00:02:29,690
Грамотное ее применение позволяет не только ускорять ваши программы,

48
00:02:29,690 --> 00:02:35,690
избегая лишнего копирования, но и делает проще и понятнее функции и методы классов.

49
00:02:35,690 --> 00:02:39,745
Наконец, заключительная тема нашего курса - базовая многопоточность,

50
00:02:39,745 --> 00:02:41,630
современный процессор уже давно выпускается

51
00:02:41,630 --> 00:02:45,155
с несколькими ядрамы и могут выполнять параллельно несколько наборов команд,

52
00:02:45,155 --> 00:02:49,835
поэтому современному разработчику очень важно уметь создавать многопоточные приложения.

53
00:02:49,835 --> 00:02:51,680
Так же как и в предыдущих курсах,

54
00:02:51,680 --> 00:02:56,720
вас ждет очень много задач по программированию и финальный проект в конце курса,

55
00:02:56,720 --> 00:03:00,975
на этот раз вам надо будет написать собственную поисковую систему,

56
00:03:00,975 --> 00:03:04,284
единственный способ освоить любой язык программирования - это практика.

57
00:03:04,284 --> 00:03:05,735
Можно прочитать кучу книг,

58
00:03:05,735 --> 00:03:08,829
посмотреть массу лекций, но если регулярно не программировать,

59
00:03:08,829 --> 00:03:12,800
вам никогда не удастся овладеть ни одним языком программирования,

60
00:03:12,800 --> 00:03:15,200
именно поэтому подготовили так много задач.

61
00:03:15,200 --> 00:03:18,440
Если вы не проходили желтый пояс по С++ и не уверены,

62
00:03:18,440 --> 00:03:20,180
стоит ли тратить на него время,

63
00:03:20,180 --> 00:03:23,240
то в самом начале курса у нас есть специальная задача.

64
00:03:23,240 --> 00:03:24,890
Попробуйте ее решить.

65
00:03:24,890 --> 00:03:29,075
Если вы не поймете ее условия и не сможете решить ее быстро,

66
00:03:29,075 --> 00:03:32,800
то скорее всего вам надо сначала пройти желтый пояс по С++.

67
00:03:32,800 --> 00:03:35,360
Когда вы закончите этот курс, не останавливайтесь.

68
00:03:35,360 --> 00:03:37,942
Впереди вас будет ждать коричневый поясь по С++.

69
00:03:37,942 --> 00:03:41,537
В нем мы научим вас различным идиомам языка С++,

70
00:03:41,537 --> 00:03:47,270
то есть научим создавать и проектировать элегантные эффективные и надежные блоки кода.