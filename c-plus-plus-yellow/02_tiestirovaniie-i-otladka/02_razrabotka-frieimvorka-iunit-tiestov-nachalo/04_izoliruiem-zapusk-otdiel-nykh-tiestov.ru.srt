1
00:00:00,000 --> 00:00:09,704
[БЕЗ_ЗВУКА] Продолжим улучшать наш
фреймворк для юнит-тестирования.

2
00:00:09,704 --> 00:00:12,730
И в этом видео мы поборемся
вот с таким его недостатком,

3
00:00:12,730 --> 00:00:17,237
что если у нас срабатывает assert,
то тесты,

4
00:00:17,237 --> 00:00:21,235
которые идут после упавшего теста,
не выполняются.

5
00:00:21,235 --> 00:00:24,459
Мы с вами уже говорили, чем это плохо.

6
00:00:24,459 --> 00:00:28,081
Потому что когда у нас не
выполнились все тесты,

7
00:00:28,081 --> 00:00:33,229
мы не видим полной картины, не понимаем,
в каких местах у нас баги есть,

8
00:00:33,229 --> 00:00:36,815
а в каких местах наши
тесты эти баги не нашли.

9
00:00:36,815 --> 00:00:39,613
И поэтому давайте с этим поборемся,

10
00:00:39,613 --> 00:00:44,187
давайте попытаемся получить вот что-то,
что представлено на экране.

11
00:00:44,187 --> 00:00:49,190
То есть вот в нашем примере у нас
три юнит-теста: TestAddSynonyms,

12
00:00:49,190 --> 00:00:51,857
TestCount и TestAreSynonyms.

13
00:00:51,857 --> 00:00:56,590
И в примере на экране вы видите,
что TestCount упал.

14
00:00:56,590 --> 00:00:58,331
Он нашел ошибку.

15
00:00:58,331 --> 00:01:04,343
Но при этом после него все равно
выполнился TestAreSynonyms и сообщил,

16
00:01:04,343 --> 00:01:05,365
что ошибки нет.

17
00:01:05,365 --> 00:01:09,792
Сейчас же, и мы уже это многократно
видели, если TestCount упадет,

18
00:01:09,792 --> 00:01:12,340
то третий тест выполняться не будет.

19
00:01:12,340 --> 00:01:17,952
Давайте подумаем,
как мы можем этого достичь,

20
00:01:17,952 --> 00:01:21,150
достичь такого поведения от наших тестов.

21
00:01:21,150 --> 00:01:25,860
Давайте посмотрим на наш AssertEqual,

22
00:01:25,860 --> 00:01:29,847
и мы видим, что мы специально сделали,

23
00:01:29,847 --> 00:01:33,979
что если равенство не выполняется,
то мы бросаем исключение.

24
00:01:33,979 --> 00:01:37,717
Собственно, вылет этого исключения
за пределы функции main и приводит к

25
00:01:37,717 --> 00:01:41,368
аварийному завершению программы.

26
00:01:41,368 --> 00:01:44,853
Как же мы могли бы эту проблему решить?

27
00:01:44,853 --> 00:01:49,585
Как бы мы могли выполнять все тесты,
независимо от того,

28
00:01:49,585 --> 00:01:51,343
упал какой-то из них или нет?

29
00:01:51,343 --> 00:01:56,150
Ну очень просто — давайте это исключение
(исключение runtime_error) ловить.

30
00:01:56,150 --> 00:02:01,035
Пройдем в функцию main,
где у нас вызываются наши тесты,

31
00:02:01,035 --> 00:02:06,340
и напишем try catch

32
00:02:06,340 --> 00:02:10,870
(runtime_error& e)

33
00:02:10,870 --> 00:02:20,737
cout "TestAre Synon...

34
00:02:20,737 --> 00:02:23,520
Тут же не работает...

35
00:02:23,520 --> 00:02:27,700
Synonyms fail

36
00:02:27,700 --> 00:02:36,840
e.what Вот,

37
00:02:36,840 --> 00:02:40,743
теперь что у нас происходит?

38
00:02:40,743 --> 00:02:47,736
Если этот тест выстрелит, то мы поймаем
это исключение и продолжим работу.

39
00:02:47,736 --> 00:02:51,775
Давайте продемонстрируем,
что мы все правильно сделали.

40
00:02:51,775 --> 00:02:54,240
Давайте пойдем в TestAreSynonyms.

41
00:02:54,240 --> 00:02:59,260
Снова допустим в нем намеренную «багу».

42
00:02:59,260 --> 00:03:03,600
Запустим наш код.

43
00:03:03,600 --> 00:03:06,175
Он все так же падает, но это понятно,

44
00:03:06,175 --> 00:03:09,235
потому что мы только в
одном месте внедрили.

45
00:03:09,235 --> 00:03:11,100
Но зато мы в консоли видим,

46
00:03:11,100 --> 00:03:15,650
что при запуске теста
TestAreSynonyms он написал fail.

47
00:03:15,650 --> 00:03:17,545
Написал, что сработал assert,

48
00:03:17,545 --> 00:03:22,160
а дальше следующий тест TestCount все
равно выполнился, все равно отработал.

49
00:03:22,160 --> 00:03:26,141
Поэтому мы действительно, ловя исключения,

50
00:03:26,141 --> 00:03:33,080
можем выполнять последующие тесты,
даже если и предыдущий упал.

51
00:03:33,080 --> 00:03:36,080
Но есть одна проблема.

52
00:03:36,080 --> 00:03:42,290
Вот смотрите: мы вот это написали для
TestAreSynonyms, а у нас же еще два теста.

53
00:03:42,290 --> 00:03:50,520
И получается, нам надо взять и все
то же самое сделать для TestCount.

54
00:03:50,520 --> 00:03:58,050
При этом надо не забыть, что вот
здесь нужно тоже поменять имя теста.

55
00:03:58,050 --> 00:04:03,250
Ну и третий тест,
третий запуск нам тоже надо обернуть.

56
00:04:03,250 --> 00:04:04,670
Как-то это не очень.

57
00:04:04,670 --> 00:04:08,570
Писать каждый раз неудобно,
«копипастить» — тем более.

58
00:04:08,570 --> 00:04:09,710
Это неудобно.

59
00:04:09,710 --> 00:04:11,376
Чего бы нам хотелось?

60
00:04:11,376 --> 00:04:16,360
Нам бы хотелось, чтобы вот этот
код (вот этот try catch) и вывод,

61
00:04:16,360 --> 00:04:20,530
он был написан в одном месте,
а мы туда могли бы передавать

62
00:04:20,530 --> 00:04:25,793
различные тестовые функции,
и они бы там выполнялись,

63
00:04:25,793 --> 00:04:30,232
и исключения бы от них ловились,
все бы работало, а мы только...

64
00:04:30,232 --> 00:04:34,430
Ну и, соответственно, вот этот код с
try catch был бы написан в одном месте.

65
00:04:34,430 --> 00:04:40,290
И есть на самом деле
способ в C++ это сделать.

66
00:04:40,290 --> 00:04:45,369
Смотрите: мы хотим в некую функцию,

67
00:04:45,369 --> 00:04:48,290
которая будет делать вот этот try catch,

68
00:04:48,290 --> 00:04:53,128
передавать другую функцию
и вызывать ее там.

69
00:04:53,128 --> 00:04:58,670
При этом есть такой момент,
что в C++ функции имеют тип.

70
00:04:58,670 --> 00:05:00,488
Такая вот необычная вещь.

71
00:05:00,488 --> 00:05:05,480
И функции действительно можно передавать
в другие функции как аргумент.

72
00:05:05,480 --> 00:05:06,403
Что это за тип?

73
00:05:06,403 --> 00:05:11,236
Нам на самом деле неважно, потому что у
нас есть такое замечательное средство,

74
00:05:11,236 --> 00:05:12,100
как шаблоны.

75
00:05:12,100 --> 00:05:18,060
И мы сейчас воспользуемся шаблонами
и напишем такой шаблон RunTest,

76
00:05:18,060 --> 00:05:23,677
который будет принимать тестовую функцию
и запускать тест, и ловить исключения.

77
00:05:23,677 --> 00:05:29,808
Давайте его напишем, а потом посмотрим,
как это все выглядит, как это работает.

78
00:05:29,808 --> 00:05:30,996
Делаем так.

79
00:05:30,996 --> 00:05:37,267
Мы сказали, пишем шаблон: template <class

80
00:05:37,267 --> 00:05:42,539
TestFunc> void RunTest

81
00:05:42,539 --> 00:05:47,648
(TestFunc func) Вот,

82
00:05:47,648 --> 00:05:55,126
мы сейчас написали шаблон,
который принимает функцию.

83
00:05:55,126 --> 00:06:00,310
А здесь делаем вот так внутри.

84
00:06:00,310 --> 00:06:05,230
Мы делаем try catch,
здесь вызываем нашу функцию func.

85
00:06:05,230 --> 00:06:11,302
И смотрите: вот здесь
мы вводили имя теста,

86
00:06:11,302 --> 00:06:19,785
давайте мы это имя теста
передадим вторым параметром.

87
00:06:19,785 --> 00:06:25,230
Вот.

88
00:06:25,230 --> 00:06:26,878
Как этим пользоваться?

89
00:06:26,878 --> 00:06:27,898
Очень просто.

90
00:06:27,898 --> 00:06:35,040
Пишем: RunTest (TestAreSynonyms
() ) А вот тут важная вещь!

91
00:06:35,040 --> 00:06:39,634
Видите, нам автодополнение Eclipse
подставило вот здесь скобочки для

92
00:06:39,634 --> 00:06:42,379
TestAddSynonyms, вот их надо удалить.

93
00:06:42,379 --> 00:06:45,831
Когда мы передаем функцию
как параметр другой функции,

94
00:06:45,831 --> 00:06:49,968
мы указываем только ее имя без скобочек,
потому что скобочки — это вызов функции,

95
00:06:49,968 --> 00:06:52,700
и в качестве параметра уйдет
результат вызова этой функции.

96
00:06:52,700 --> 00:06:59,020
Ну а дальше делаем TestCount,

97
00:06:59,020 --> 00:07:03,460
TestAddSynonyms.

98
00:07:03,460 --> 00:07:07,470
Так, для начала проверим,
что это компилируется.

99
00:07:07,470 --> 00:07:10,212
А это не компилируется.

100
00:07:10,212 --> 00:07:11,260
Почему?

101
00:07:11,260 --> 00:07:16,220
Потому что мы забыли передать
второй параметр — test_name.

102
00:07:16,220 --> 00:07:22,230
И здесь мы на самом деле просто
скопируем имена функций.

103
00:07:22,230 --> 00:07:29,130
C++ умеет делать так, чтобы копировать
имена функций было не нужно.

104
00:07:29,130 --> 00:07:32,670
Ну и так же, как и с определением,

105
00:07:32,670 --> 00:07:37,260
какой assert сработал,
нам просто пока не хватает знаний.

106
00:07:37,260 --> 00:07:43,080
Поэтому вот здесь приходится
имя теста дублировать.

107
00:07:43,080 --> 00:07:46,477
Отлично, у нас компилируется.

108
00:07:46,477 --> 00:07:49,388
И вот смотрите, я говорил,

109
00:07:49,388 --> 00:07:53,560
что функции в C++ имеют тип,
но нам не важно, какой это тип.

110
00:07:53,560 --> 00:07:57,720
Вот, собственно, потому что шаблоны нам
позволяют ничего не знать об этом типе,

111
00:07:57,720 --> 00:08:01,880
мы просто сказали, что это шаблон,
он принимает какой-то тип,

112
00:08:01,880 --> 00:08:06,190
который мы обозвали TestFunc, и мы
передали функцию, и у нас все работает.

113
00:08:06,190 --> 00:08:07,660
Функцию можно вызывать.

114
00:08:07,660 --> 00:08:08,609
Так, хорошо.

115
00:08:08,609 --> 00:08:12,920
Давайте проверим, что это не только
компилируется, но и работает.

116
00:08:12,920 --> 00:08:14,225
Отлично.

117
00:08:14,225 --> 00:08:19,723
Смотрите, в консоль
вывелось: TestAreSynonyms

118
00:08:19,723 --> 00:08:25,679
fail: Assertion failed: 0 != 1 Hint:
empty a b Дальше выполнился TestCount,

119
00:08:25,679 --> 00:08:28,600
который вернул OK,
то есть он нормально отработал.

120
00:08:28,600 --> 00:08:33,270
И дальше снова выполнился третий тест,

121
00:08:33,270 --> 00:08:37,510
тоже сообщил о том, что нашел ошибку.

122
00:08:37,510 --> 00:08:41,602
То есть у нас первый и
второй тесты падают,

123
00:08:41,602 --> 00:08:45,662
мы их можем сделать первыми, выполнить,

124
00:08:45,662 --> 00:08:51,163
запустить и увидеть,
что тесты упали, но те тесты,

125
00:08:51,163 --> 00:08:56,230
которые идут после них,
все равно выполняются.

126
00:08:56,230 --> 00:09:01,660
Таким образом,
мы с вами применили шаблон функций,

127
00:09:01,660 --> 00:09:07,580
для того чтобы передавать в качестве
параметров функции другие функции,

128
00:09:07,580 --> 00:09:12,579
и смогли за счет этого написать
универсальный такой шаблон,

129
00:09:12,579 --> 00:09:19,075
который для любого юнит-теста ловит
исключения и позволяет нам все юнит-тесты,

130
00:09:19,075 --> 00:09:24,220
которые мы написали, выполнять при
каждом запуске нашей программы.