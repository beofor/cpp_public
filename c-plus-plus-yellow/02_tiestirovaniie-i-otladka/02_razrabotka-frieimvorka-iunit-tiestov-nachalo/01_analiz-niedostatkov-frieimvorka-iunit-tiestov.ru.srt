1
00:00:00,000 --> 00:00:07,427
[БЕЗ_ЗВУКА] В предыдущем видео

2
00:00:07,427 --> 00:00:12,570
мы с вами смогли с помощью юнит-тестов
найти ошибку в решении задачи «Синонимы».

3
00:00:12,570 --> 00:00:17,085
При этом по ходу разработки
юнит-тестов мы смогли написать такой

4
00:00:17,085 --> 00:00:21,388
небольшой юнит-тест фреймворк,
в который, на самом деле,

5
00:00:21,388 --> 00:00:26,080
можно добавлять было бы и другие тесты,
если бы мы не нашли ошибку.

6
00:00:26,080 --> 00:00:32,608
Давайте посмотрим,
что же за фреймворк у нас получился,

7
00:00:32,608 --> 00:00:37,200
вот так вот за одну лекцию написанный
в прямом эфире, можно сказать.

8
00:00:37,200 --> 00:00:43,278
Значит, во-первых, мы этот фреймворк
основали на стандартной функции Assert,

9
00:00:43,278 --> 00:00:45,360
которая у нас использовалась,

10
00:00:45,360 --> 00:00:49,348
чтобы проверять какие-то
ожидания от нашего кода.

11
00:00:49,348 --> 00:00:55,821
Давайте посмотрим, какие у Assert
есть преимущества и недостатки.

12
00:00:55,821 --> 00:00:59,786
Важным преимуществом Assert является то,

13
00:00:59,786 --> 00:01:05,462
что он показывает, какая именно
проверка сработала в юнит-тесте.

14
00:01:05,462 --> 00:01:12,228
Вот у меня на экране приведен пример
сработавшего Assert, и мы видим,

15
00:01:12,228 --> 00:01:18,657
что если в консоли выведено
coursera.cpp Line 32,

16
00:01:18,657 --> 00:01:23,865
мы понимаем, что мы понимаем,
какой именно Assert сработал,

17
00:01:23,865 --> 00:01:29,279
можем пойти в код и понять, что это
вот эта вот проверка, и мы понимаем,

18
00:01:29,279 --> 00:01:34,568
какой именно юнит-тест у нас выстрелил,
нашел ошибку и

19
00:01:34,568 --> 00:01:40,600
конкретно с ним можем разбираться и
изучать, где же, собственно, проблема.

20
00:01:40,600 --> 00:01:45,278
Правда, на самом деле,
на этом преимущества стандартной

21
00:01:45,278 --> 00:01:49,699
функции Assert заканчиваются,
а начинаются недостатки.

22
00:01:49,699 --> 00:01:53,493
Важным недостатком является то,
что в консоль,

23
00:01:53,493 --> 00:01:58,640
при проверке равенства в
консоль не выводятся значения

24
00:01:58,640 --> 00:02:03,270
тех переменных,
которые в Assert сравниваются.

25
00:02:03,270 --> 00:02:05,968
Вот опять же давайте рассмотрим пример.

26
00:02:05,968 --> 00:02:09,662
Вот у нас в консоли выведено,
у нас сработал Assert,

27
00:02:09,662 --> 00:02:12,840
и в консоль выведено empty == expected.

28
00:02:12,840 --> 00:02:18,808
Мы понимаем,
что это вот этот вот кусок кода,

29
00:02:18,808 --> 00:02:25,780
но мы из кода понимаем, чему равна
константа expected — мы можем прямо здесь,

30
00:02:25,780 --> 00:02:30,846
на месте прочитать и понять,
какой именно словарь синонимов мы ожидаем.

31
00:02:30,846 --> 00:02:35,311
А вот чему оказалась равна
переменная empty, мы нигде не видим.

32
00:02:35,311 --> 00:02:40,115
И чтобы узнать, какой она стала,
нам приходится делать какие-то

33
00:02:40,115 --> 00:02:44,210
дополнительные действия: запускать
отладчик, снова выполнять этот юнит-тест,

34
00:02:44,210 --> 00:02:50,578
смотреть в отладчики, чему оказалась
равна переменная, в данном случае empty.

35
00:02:50,578 --> 00:02:53,746
Это, конечно же, неудобно.

36
00:02:53,746 --> 00:02:54,935
Дальше.

37
00:02:54,935 --> 00:03:00,648
Какие еще были недостатки
у нашего фреймворка,

38
00:03:00,648 --> 00:03:03,330
которым мы пользовались?

39
00:03:03,330 --> 00:03:07,410
Если срабатывал хотя бы один
Assert в наших юнит-тестах,

40
00:03:07,410 --> 00:03:13,227
то программа завершалась аварийно,
и другие тесты не срабатывали.

41
00:03:13,227 --> 00:03:16,100
Давайте даже мы с вами
это продемонстрируем.

42
00:03:16,100 --> 00:03:21,308
Вот мы переключимся в Eclipse, и мы с вами

43
00:03:21,308 --> 00:03:26,285
нашли ошибку с помощью
теста TestAddSynomyms,

44
00:03:26,285 --> 00:03:32,630
вот давайте мы его — он у нас третьим
выполнялся, давайте выполним его вторым.

45
00:03:32,630 --> 00:03:37,115
При этом мы убедимся,
что та ошибка, которую мы нашли,

46
00:03:37,115 --> 00:03:39,230
она у нас сейчас присутствует.

47
00:03:39,230 --> 00:03:46,178
Вот давайте мы скомпилируем наш код,
запустим его и увидим,

48
00:03:46,178 --> 00:03:51,259
что у нас, значит, программа отработала,

49
00:03:51,259 --> 00:03:56,600
один из тестов выстрелил — мы это
видим по красному выводу в консоли.

50
00:03:56,600 --> 00:03:58,060
И что мы здесь видим?

51
00:03:58,060 --> 00:04:01,580
Что тест на функцию AreSynonyms отработал.

52
00:04:01,580 --> 00:04:07,036
Дальше мы видим,
что упал наш тест, вот Line 35.

53
00:04:07,036 --> 00:04:15,320
Мы можем перейти в эту строчку и увидеть,
что это наш тест AddSynonyms.

54
00:04:15,320 --> 00:04:18,798
А вот у нас есть еще третий тест,
TestCount — он не был выполнен,

55
00:04:18,798 --> 00:04:24,508
потому что выполнение
программы прекратилось.

56
00:04:24,508 --> 00:04:28,666
А на самом деле, очень хорошо,
когда у вас независимо от того,

57
00:04:28,666 --> 00:04:33,890
сколько тестов выстрелило, сколько
тестов упало, выполняются все тесты,

58
00:04:33,890 --> 00:04:38,840
потому что в этом случае вы видите
более полную картину, вы понимаете.

59
00:04:38,840 --> 00:04:43,145
Допустим, если у вас только
один юнит-тест вывел ошибку,

60
00:04:43,145 --> 00:04:47,450
вы понимаете, что у вас, скорее всего, там
одна ошибка в одной тестируемой сущности,

61
00:04:47,450 --> 00:04:49,567
а в остальных местах все в порядке,

62
00:04:49,567 --> 00:04:54,440
и вы можете спокойно сконцентрироваться
только на одном блоке вашей программы.

63
00:04:54,440 --> 00:04:59,829
Поэтому то,
что при падении одного из тестов другие

64
00:04:59,829 --> 00:05:04,810
не выполняются, те, которые вызываются
после него — это заметный недостаток.

65
00:05:04,810 --> 00:05:10,519
Кроме того, вот, смотрите,
опять же вернемся в Eclipse

66
00:05:10,519 --> 00:05:15,920
и посмотрим вот на этот вывод теста,
который завершился успешно.

67
00:05:15,920 --> 00:05:20,970
Он — давайте мы его даже откроем,
вот он, — он в конце

68
00:05:20,970 --> 00:05:26,318
своего выполнения выводит в стандартный
вывод сообщение OK, чтобы мы знали,

69
00:05:26,318 --> 00:05:31,339
что конкретно этот тест выполнился,
отработал и не нашел никаких ошибок.

70
00:05:31,339 --> 00:05:35,533
Но мы это выводим в стандартный вывод,
в cout.

71
00:05:35,533 --> 00:05:39,041
Я напомню, что мы,
вообще говоря, решаем задачу,

72
00:05:39,041 --> 00:05:42,705
задание по программированию
из нашего курса про синонимы,

73
00:05:42,705 --> 00:05:47,651
и решение этой задачи — вот оно сейчас
тут у нас закомментировано, но оно,

74
00:05:47,651 --> 00:05:53,376
как видите, вот здесь вот тоже должно
выводить в стандартный вывод, в cout.

75
00:05:53,376 --> 00:05:55,146
Что получается?

76
00:05:55,146 --> 00:05:57,034
Что вывод тестов,

77
00:05:57,034 --> 00:06:02,935
он смешивается с выводом основной нашей
программы, решением нашей задачи.

78
00:06:02,935 --> 00:06:07,296
И это плохо, потому что вы,
отправляя решение в тестирующую систему,

79
00:06:07,296 --> 00:06:10,295
вы можете забыть
закомментировать вызов тестов,

80
00:06:10,295 --> 00:06:13,970
и они тоже будут выводить
что-то в стандартный вывод,

81
00:06:13,970 --> 00:06:18,785
тестирующая система будет рассматривать
это как вывод вашего решения,

82
00:06:18,785 --> 00:06:22,347
не будет принимать ваши решения, вы будете
думать, что у вас ошибка, а ошибки,

83
00:06:22,347 --> 00:06:28,270
на самом деле нет — вы просто забыли
закомментировать вызов тестов.

84
00:06:28,270 --> 00:06:33,170
Так что тот факт,
что тесты что-то выводят в

85
00:06:33,170 --> 00:06:36,950
стандартный вывод — это тоже неудобно,
это тоже недостаток.

86
00:06:36,950 --> 00:06:44,170
Конечно же,
в мире C++ существует большое количество

87
00:06:44,170 --> 00:06:50,225
уже готовых фреймворков для создания
юнит-тестов, в которых нету ни одного из

88
00:06:50,225 --> 00:06:55,310
перечисленных недостатков — это
большие широкоприменяемые системы,

89
00:06:55,310 --> 00:07:00,238
которые хорошо оттестированы и
весьма удобны в использовании.

90
00:07:00,238 --> 00:07:03,780
Вот на экране приведены некоторые из них.

91
00:07:03,780 --> 00:07:09,203
И я уверен, что кто-то из тех,

92
00:07:09,203 --> 00:07:14,861
кто смотрит это видео, мог возмутиться:
а зачем мы вот там в предыдущем

93
00:07:14,861 --> 00:07:20,302
видео написали свой фреймворк для
тестирования, какой-то такой простенький,

94
00:07:20,302 --> 00:07:24,415
а сразу не стали учить вас
использовать что-то из готового?

95
00:07:24,415 --> 00:07:27,995
Но на самом деле мы сделали это осознанно.

96
00:07:27,995 --> 00:07:33,385
Мы специально хотим в следующих
видео исправить все указанные

97
00:07:33,385 --> 00:07:38,964
недостатки в нашем простеньком
фреймворке и создать свой,

98
00:07:38,964 --> 00:07:44,300
еще один фреймворк для
написания юнит-тестов,

99
00:07:44,300 --> 00:07:50,410
чтобы вы могли им пользоваться и применять
его для тестирования своих программ,

100
00:07:50,410 --> 00:07:53,553
своих решений заданий нашего курса.

101
00:07:53,553 --> 00:07:59,880
И мы это сделали осознанно, специально,
во-первых, чтобы показать вам,

102
00:07:59,880 --> 00:08:04,696
что текущих знаний вот эти полутора
курсов по C++ уже хватает,

103
00:08:04,696 --> 00:08:09,910
для того чтобы на C++ написать
что-то практически применимое,

104
00:08:09,910 --> 00:08:14,770
что-то полезное, что можно применять
для тестирования своих программ.

105
00:08:14,770 --> 00:08:19,770
Кроме того, так как мы вместе
с вами напишем этот фреймворк

106
00:08:19,770 --> 00:08:24,144
самостоятельно, вы будете понимать,
как он устроен внутри.

107
00:08:24,144 --> 00:08:29,156
Вы будете видеть,
как те или иные конструкции языка

108
00:08:29,156 --> 00:08:36,122
C++ применяются для придания
фреймворку желаемых свойств.

109
00:08:36,122 --> 00:08:42,066
Вы будете видеть,
как C++ применяется на реальной задаче,

110
00:08:42,066 --> 00:08:46,016
для того чтобы получился
удобный фреймворк.

111
00:08:46,016 --> 00:08:50,855
И, наконец, так как вы будете понимать,
как он устроен, у вас будет возможность

112
00:08:50,855 --> 00:08:55,709
самостоятельно вносить в него изменения,
вносить изменения в этот фреймворк,

113
00:08:55,709 --> 00:09:02,455
улучшать его и делать удобнее для вас.

114
00:09:02,455 --> 00:09:05,298
Поэтому, начиная со следующего видео,

115
00:09:05,298 --> 00:09:10,597
мы будем избавляться от перечисленных
недостатков юнит-тест фреймворка,

116
00:09:10,597 --> 00:09:18,420
последовательно продвигаясь к удобному
и функциональному юнит-тест фреймворку.