1
00:00:00,000 --> 00:00:06,257
В данной лекции

2
00:00:06,257 --> 00:00:10,581
мы познакомимся с порядком создания
объектов в языке C++, рассмотрим,

3
00:00:10,581 --> 00:00:12,858
в каком порядке происходит
их конструирование,

4
00:00:12,858 --> 00:00:17,082
а также в подробностях разберем,
как работают списки инициализации.

5
00:00:17,082 --> 00:00:22,435
Я немного упростил весь код, я оставил
только класс «яблоко», унаследованный

6
00:00:22,435 --> 00:00:27,040
от класса fruit, а также завел совсем
простую структуру данных — логер.

7
00:00:27,040 --> 00:00:30,172
Этот логер пишет плюсик и
его идентификатор в момент,

8
00:00:30,172 --> 00:00:33,790
когда он создается,
когда для него вызывается конструктор,

9
00:00:33,790 --> 00:00:37,970
и пишет минус в тот момент,
когда для него вызывается деструктор.

10
00:00:37,970 --> 00:00:41,430
Значит, зачем я все это сделал,
сейчас станет понятно.

11
00:00:41,430 --> 00:00:45,270
Добавим этот логер в наш класс Fruit.

12
00:00:45,270 --> 00:00:49,339
Заводим переменную логера, и,

13
00:00:49,339 --> 00:00:54,850
например, запишем туда Fruit.

14
00:00:54,850 --> 00:01:01,029
Точно так же поступим для класса,
который публичным

15
00:01:01,029 --> 00:01:05,570
наследованием унаследован от класса Fruit,
и положим туда яблоко, Apple.

16
00:01:05,570 --> 00:01:07,696
Значит, соберем наш код.

17
00:01:07,696 --> 00:01:10,750
В функции main я только создаю яблоко.

18
00:01:10,750 --> 00:01:13,821
Посмотрим, что будет выведено на экран.

19
00:01:13,821 --> 00:01:18,180
Видим, что сначала был логер
создан внутри объекта Fruit,

20
00:01:18,180 --> 00:01:21,950
затем внутри объекта «яблоко»,

21
00:01:21,950 --> 00:01:28,268
затем был удален логер внутри яблока,
а затем был удален логер внутри Fruit.

22
00:01:28,268 --> 00:01:30,010
О чем нам это может говорить?

23
00:01:30,010 --> 00:01:35,380
Это может говорить о том, что если у
нас есть некоторая иерархия классов,

24
00:01:35,380 --> 00:01:39,810
то при создании объекта,
принадлежащего данной иерархии,

25
00:01:39,810 --> 00:01:43,885
всегда будет сначала вызываться
конструктор базового класса,

26
00:01:43,885 --> 00:01:46,576
затем класса наследника
и так далее по цепочке.

27
00:01:46,576 --> 00:01:50,072
При этом, когда объекты данных
классов будут удаляться,

28
00:01:50,072 --> 00:01:54,766
сначала всегда вызывается объект самого
последнего класса наследника и так

29
00:01:54,766 --> 00:01:59,630
далее по цепочке вверх, пока мы не дойдем
до деструктора самого базового класса,

30
00:01:59,630 --> 00:02:04,262
то есть что мы тут и видим: сначала
сконструировался Fruit, затем яблоко,

31
00:02:04,262 --> 00:02:06,840
потом удалилось яблоко и удалился Fruit.

32
00:02:06,840 --> 00:02:10,740
Давайте мы немножко перефакторим наш код.

33
00:02:10,740 --> 00:02:15,919
Во-первых, сделаем так, чтобы логеры
были зашиты не внутри наших классов,

34
00:02:15,919 --> 00:02:20,526
а как-то конфигрурировались, то есть
применим те знания, которые мы знаем.

35
00:02:20,526 --> 00:02:24,490
Во-первых, type у нас
может быть константным.

36
00:02:24,490 --> 00:02:29,490
Даже не так, даже мы просто
создадим объект класса Fruit,

37
00:02:29,490 --> 00:02:35,370
напишем у него — нет.

38
00:02:35,370 --> 00:02:39,850
Применим те знания, которые мы знаем:
создадим конструктор класса Fruit,

39
00:02:39,850 --> 00:02:45,410
передаем в него строчку-идентификатор.

40
00:02:45,410 --> 00:02:50,778
[ШУМ] И при

41
00:02:50,778 --> 00:02:55,848
этом инициализируем наш логер l этой

42
00:02:55,848 --> 00:03:01,880
строкой плюс прибавим к ней в скобочках

43
00:03:01,880 --> 00:03:06,760
имя нашего базового класса.

44
00:03:06,760 --> 00:03:11,640
И, кстати, теперь мы можем
наш логер сделать константой.

45
00:03:11,640 --> 00:03:16,943
При этом код перестал собираться,
потому что, во-первых,

46
00:03:16,943 --> 00:03:23,577
пропала переменная type,
а во-вторых, мы видим,

47
00:03:23,577 --> 00:03:29,610
что почему-то не вызывается
умолчательный конструктор для Fruit.

48
00:03:29,610 --> 00:03:32,286
Потому что он отсутствует.

49
00:03:32,286 --> 00:03:36,827
А класс «яблоко» публично унаследован
от класса Fruit, соответственно,

50
00:03:36,827 --> 00:03:42,870
когда «яблоко» конструируется, перед этим
вызывается конструктор класса Fruit.

51
00:03:42,870 --> 00:03:47,380
При этом в этот конструктор мы
обязаны передать некую строчку,

52
00:03:47,380 --> 00:03:51,202
которая как-то идентифицирует объект,
а мы этого не делаем.

53
00:03:51,202 --> 00:03:53,070
Значит, давайте это сделаем.

54
00:03:53,070 --> 00:03:58,840
Вызовем конструктор Fruit с
идентификатором «яблока».

55
00:03:58,840 --> 00:04:07,910
[ШУМ] Соберем наш код еще раз.

56
00:04:07,910 --> 00:04:10,515
И запустим его.

57
00:04:10,515 --> 00:04:13,729
Видим, что наши изменения применились.

58
00:04:13,729 --> 00:04:17,360
Теперь мы видим, что этот
идентификатор вызывается ровно так,

59
00:04:17,360 --> 00:04:20,970
как мы его определили —
добавилось слово Fruit.

60
00:04:20,970 --> 00:04:25,520
И также давайте в классе
«яблоко» мы тоже добавим строчку-

61
00:04:25,520 --> 00:04:29,940
идентификатор.

62
00:04:29,940 --> 00:04:36,235
[ШУМ] В

63
00:04:36,235 --> 00:04:42,310
«яблоке» мы тоже объявим логер
и тоже его проинициализируем.

64
00:04:42,310 --> 00:04:46,100
Отлично.

65
00:04:46,100 --> 00:04:51,430
Теперь при создании объектов
нам надо указать идентификатор.

66
00:04:51,430 --> 00:04:56,550
Пусть это будет «яблоко1»,
а это будет «яблоко2».

67
00:04:56,550 --> 00:05:04,260
[ШУМ]

68
00:05:04,260 --> 00:05:11,970
Поправим ошибку компиляции.

69
00:05:11,970 --> 00:05:16,104
Соберем наш код и запустим.

70
00:05:16,104 --> 00:05:21,459
Видим, что что сначала
у нас создался объект

71
00:05:21,459 --> 00:05:26,909
класса a1 — на самом деле, класс «яблоко»,
но идентификатор у него сейчас a1.

72
00:05:26,909 --> 00:05:29,970
Был вызван конструктор
базового класса для него,

73
00:05:29,970 --> 00:05:33,553
затем был вызван конструктор класса Apple.

74
00:05:33,553 --> 00:05:38,903
После этого начал создаваться объект a2,
при этом удаление произошло ровно

75
00:05:38,903 --> 00:05:45,953
в обратном порядке: сначала был удален
объект a2, потом был удален объект a1.

76
00:05:45,953 --> 00:05:51,031
Значит, сначала создали объект a1,
потом объект a2.

77
00:05:51,031 --> 00:05:53,982
Когда для них начали
вызываться деструкторы,

78
00:05:53,982 --> 00:05:56,576
произошло все ровно в обратном порядке.

79
00:05:56,576 --> 00:05:59,610
Значит, когда мы создаем
переменные обычные,

80
00:05:59,610 --> 00:06:03,500
конструирование и удаление
объектов работает точно так же.

81
00:06:03,500 --> 00:06:08,110
Кто создается раньше, тот удаляется позже.

82
00:06:08,110 --> 00:06:14,510
Давайте создадим более сложный класс,

83
00:06:14,510 --> 00:06:20,530
например «яблочное дерево», которое будет
в себе содержать несколько разных яблок.

84
00:06:20,530 --> 00:06:25,460
[ШУМ]

85
00:06:25,460 --> 00:06:33,460
AppleTree.

86
00:06:33,460 --> 00:06:41,850
[ШУМ] В нем мы создадим два яблока.

87
00:06:41,850 --> 00:06:49,016
[ШУМ] И

88
00:06:49,016 --> 00:06:55,850
проинициализируем их в
списке инициализации.

89
00:06:55,850 --> 00:07:01,500
[ШУМ]

90
00:07:01,500 --> 00:07:10,408
[ШУМ] Класс!

91
00:07:10,408 --> 00:07:14,330
И добавим еще

92
00:07:14,330 --> 00:07:20,485
логер.

93
00:07:20,485 --> 00:07:26,640
Запустим наш код.

94
00:07:26,640 --> 00:07:29,303
Видим, что он собрался.

95
00:07:29,303 --> 00:07:34,150
И теперь здесь создадим «яблочное дерево».

96
00:07:34,150 --> 00:07:41,602
Запустим наш код, и видим,
собственно, уже ожидаемый результат.

97
00:07:41,602 --> 00:07:46,849
Первым было создано «яблочное дерево»,
затем наши «фруты».

98
00:07:46,849 --> 00:07:51,636
Теперь такой серьезный вопрос:
от чего реально зависит порядок,

99
00:07:51,636 --> 00:07:54,810
в котором будут
сконструированы наши объекты?

100
00:07:54,810 --> 00:07:58,871
Ведь, смотрите,
у нас есть список инициализации,

101
00:07:58,871 --> 00:08:02,490
а также есть место,
в котором мы их объявляем.

102
00:08:02,490 --> 00:08:07,807
Вопрос: что будет, если я сейчас
поменяю местами объекты внутри

103
00:08:07,807 --> 00:08:13,190
списка инициализации,
то есть изменю порядок их инициализации?

104
00:08:13,190 --> 00:08:21,050
То есть, смотрите, сначала я создаю объект
a2 вроде бы, потом создаю объект a1.

105
00:08:21,050 --> 00:08:23,284
Попробуем собрать наш код.

106
00:08:23,284 --> 00:08:28,970
Видим, что появились некоторые warning'и,
но код при это собрался.

107
00:08:28,970 --> 00:08:30,934
Запустим его.

108
00:08:30,934 --> 00:08:32,750
Что мы видим?

109
00:08:32,750 --> 00:08:37,723
Мы видим, что тем не менее,
несмотря на то что в списке инициализации

110
00:08:37,723 --> 00:08:42,696
мы сначала инициализируем объект a2,
а затем объект a1,

111
00:08:42,696 --> 00:08:48,908
все равно сначала создается объект a1,
потом создается объект a2.

112
00:08:48,908 --> 00:08:53,303
Так что же, получается, что список
инициализации на самом деле никак не

113
00:08:53,303 --> 00:08:55,490
влияет на порядок создания объектов?

114
00:08:55,490 --> 00:08:57,130
Да, именно так и получается.

115
00:08:57,130 --> 00:09:00,475
Сам по себе список инициализации
просто указывает значения,

116
00:09:00,475 --> 00:09:03,450
которыми нужно проинициализировать
данные объекты.

117
00:09:03,450 --> 00:09:08,418
Реальный же порядок их конструирования
зависит только от того,

118
00:09:08,418 --> 00:09:11,967
в каком порядке они
перечислены внутри класса.

119
00:09:11,967 --> 00:09:15,460
Давайте посмотрим на warning,
который здесь появились.

120
00:09:15,460 --> 00:09:20,710
Развернем их на весь экран и увидим,
что a1, что a2,

121
00:09:20,710 --> 00:09:24,702
что у них будет неправильный

122
00:09:24,702 --> 00:09:29,616
порядок инициализации — именно об
этом гласит это предупреждение.

123
00:09:29,616 --> 00:09:34,616
При этом, если бы мы собирали наш код
с флагом «трактовать предупреждения

124
00:09:34,616 --> 00:09:38,757
как ошибки», мы бы никогда не
наступили на такую проблему.

125
00:09:38,757 --> 00:09:40,890
Почему это может быть проблемой?

126
00:09:40,890 --> 00:09:46,149
Например, потому что мы можем
закладываться в инициализации

127
00:09:46,149 --> 00:09:49,800
наших объектов на уже некоторые
ранее проинициализированные поля.

128
00:09:49,800 --> 00:09:53,687
Значит, во-первых, давайте сейчас
проверим, что порядок инициализации,

129
00:09:53,687 --> 00:09:56,500
действительно, зависит только
от порядка объявления.

130
00:09:56,500 --> 00:10:01,638
Я вот сейчас поменял местами
объекты a1 и a2 внутри

131
00:10:01,638 --> 00:10:06,633
нашего класса, при этом список
инициализации сделал прежним.

132
00:10:06,633 --> 00:10:10,070
То есть сначала я инициализирую a1,
потом a2.

133
00:10:10,070 --> 00:10:11,544
Запускаем наш код.

134
00:10:11,544 --> 00:10:16,410
Видим, что теперь a2 стало
инициализировать раньше, чем a1.

135
00:10:16,410 --> 00:10:19,820
Поменяем их местами.

136
00:10:19,820 --> 00:10:24,144
Видим, что a1 теперь снова
инициализируется раньше,

137
00:10:24,144 --> 00:10:27,900
чем a2, при этом warning'и пропали —
все происходит так, как мы ожидаем.

138
00:10:27,900 --> 00:10:31,960
Значит, давайте разберем
потенциальную проблемы.

139
00:10:31,960 --> 00:10:37,438
Значит, как может «выстрелить»
в коде место, в котором

140
00:10:37,438 --> 00:10:40,820
список инициализации работает не в той
последовательности, в которой вы ожидаете.

141
00:10:40,820 --> 00:10:49,280
Предположим, мы в «яблочное дерево» тоже
передаем какой-нибудь идентификатор.

142
00:10:49,280 --> 00:10:54,740
Давайте удалим логер,
а идентификатор будем

143
00:10:54,740 --> 00:11:00,080
хранить в отдельной переменной type.

144
00:11:00,080 --> 00:11:04,030
И вот, смотрите, в данном случае
она у нас объявлена последней,

145
00:11:04,030 --> 00:11:09,016
но в коде в списке инициализации
мы напишем его так,

146
00:11:09,016 --> 00:11:14,250
чтобы все выглядело как будто мы
проинициализировали ее самой первой.

147
00:11:14,250 --> 00:11:19,450
Здесь мы будем ее использовать активно.

148
00:11:19,450 --> 00:11:26,771
То есть если смотреть на этот
код и не смотреть на то,

149
00:11:26,771 --> 00:11:31,022
как действительно объявлены объекты
внутри нашего класса, поля,

150
00:11:31,022 --> 00:11:35,700
то создается впечатление, что type
проинициализирован первым — все хорошо.

151
00:11:35,700 --> 00:11:40,591
Дальше мы начинаем его как-то использовать
и как-то работает конструктор.

152
00:11:40,591 --> 00:11:49,094
Значит, давайте передадим строчку,
[ШУМ] Соберем наш код.

153
00:11:49,094 --> 00:11:52,540
Появились предупреждения, но он собрался.

154
00:11:52,540 --> 00:11:53,406
И что мы видим?

155
00:11:53,406 --> 00:11:55,412
Вывелась какая-то абракадабра.

156
00:11:55,412 --> 00:11:57,830
То есть что был реально в переменной type,

157
00:11:57,830 --> 00:12:02,910
неизвестно — там лежал какой-то мусор,
и мы его заиспользовали.

158
00:12:02,910 --> 00:12:08,420
Значит, чтобы это починить, давайте
объявим переменную type самой первой,

159
00:12:08,420 --> 00:12:10,699
запустим наш код и видим,

160
00:12:10,699 --> 00:12:14,409
что все чудесным образом начало
работать: список инициализации

161
00:12:14,409 --> 00:12:17,340
вызвался в правильном порядке,
объекты правильно проинициализировались.

162
00:12:17,340 --> 00:12:20,488
В данном видео мы в
подробностях рассмотрели,

163
00:12:20,488 --> 00:12:25,380
как работают списки инициализации и в
каком порядке конструируются объекты.