1
00:00:00,000 --> 00:00:05,842
[БЕЗ_ЗВУКА] Теперь

2
00:00:05,842 --> 00:00:10,750
у нас есть представление о том,
как мы можем представить в нашей программе

3
00:00:10,750 --> 00:00:15,835
заранее заданное выражение

4
00:00:15,835 --> 00:00:20,920
и как мы можем его эффективно вычислить,
вводя переменную с консоли.

5
00:00:20,920 --> 00:00:24,674
Теперь давайте вернёмся к более общему

6
00:00:24,674 --> 00:00:29,128
алгоритму и попробуем решить
нашу исходную задачу.

7
00:00:29,128 --> 00:00:34,510
То есть выражение мы должны, на самом
деле, вводить также с консоли — оно должно

8
00:00:34,510 --> 00:00:39,530
быть произвольное — с точностью до тех
условий, которые мы указали ранее.

9
00:00:39,530 --> 00:00:44,916
И мы должны его уметь преобразовывать

10
00:00:44,916 --> 00:00:49,200
в нужный нам формат не вручную,
а с помощью алгоритма,

11
00:00:49,200 --> 00:00:53,920
который бы эффективно по нему
проходился и сам составлял

12
00:00:53,920 --> 00:00:59,700
необходимый набор объектов,
переменных, операций, и так далее.

13
00:00:59,700 --> 00:01:04,848
Но в рамках лекции мы не будем полностью
писать код для нашего алгоритма,

14
00:01:04,848 --> 00:01:07,430
мы возьмём заранее написанный код.

15
00:01:07,430 --> 00:01:12,388
Его полное содержание вы сможете
посмотреть в материалах к

16
00:01:12,388 --> 00:01:14,080
этой видеолекции.

17
00:01:14,080 --> 00:01:19,800
Давайте немного пробежимся
по этому коду и посмотрим,

18
00:01:19,800 --> 00:01:24,800
чем же он отличается от того,
что мы начали писать в рамках этой лекции.

19
00:01:24,800 --> 00:01:31,574
Во-первых, структура классов
у нас остаётся та же самая.

20
00:01:31,574 --> 00:01:37,205
То есть есть базовый класс Node,
есть чисто виртуальный метод Evaluate.

21
00:01:37,205 --> 00:01:44,650
Есть класс для значения,
в нашем случае он называется value.

22
00:01:44,650 --> 00:01:48,740
Ранее мы его называли digit.

23
00:01:48,740 --> 00:01:55,292
Также остался класс для переменной
x и класс для операции.

24
00:01:55,292 --> 00:02:00,946
Соответственно, классы стали
чуть-чуть более сложные,

25
00:02:00,946 --> 00:02:05,600
чтобы помочь нам реализовать
необходимый нам алгоритм.

26
00:02:05,600 --> 00:02:11,890
Алгоритм называется shunting-yard,
алгоритм сортировочных станций.

27
00:02:11,890 --> 00:02:17,140
Более подробно вы можете с ним
ознакомиться, например, в Википедии.

28
00:02:17,140 --> 00:02:24,053
Давайте посмотрим,
что у нас есть функция main.

29
00:02:24,053 --> 00:02:27,563
Функция main выглядит примерно так,
как мы и хотели.

30
00:02:27,563 --> 00:02:30,970
Мы сначала считываем выражение,
затем мы вызываем функцию Parse,

31
00:02:30,970 --> 00:02:35,947
которая в данном случае
принимает не саму строку,

32
00:02:35,947 --> 00:02:39,450
а итераторы на начало
строки и итератор end.

33
00:02:39,450 --> 00:02:45,608
Также принимает ссылку на переменную x,
которую позднее мы будем считывать с

34
00:02:45,608 --> 00:02:50,810
консоли и для каждого отдельного значения
вычислять значение всего выражения.

35
00:02:50,810 --> 00:02:55,180
Это ровно то,
что мы и хотели сделать изначально.

36
00:02:55,180 --> 00:03:00,555
Из интересных моментов
следует отметить то,

37
00:03:00,555 --> 00:03:06,201
что функция Parse у нас стала шаблонной,
то есть нам теперь не важно,

38
00:03:06,201 --> 00:03:11,647
обрабатываем мы строку, вектор, главное,
чтобы в этом контейнере были символы.

39
00:03:11,647 --> 00:03:14,328
И принимаем мы только итераторы.

40
00:03:14,328 --> 00:03:18,696
Сама функция Parse,
конечно же, усложнилась,

41
00:03:18,696 --> 00:03:22,176
она реализует названный ранее алгоритм.

42
00:03:22,176 --> 00:03:28,724
Также следует отметить небольшое
изменение в интерфейсе класса Operation.

43
00:03:28,724 --> 00:03:34,516
Раньше мы передавали в качестве
левого и правого операндов shared_ptr

44
00:03:34,516 --> 00:03:41,847
прямо в конструктор, теперь же мы
их передаём отдельными методами.

45
00:03:41,847 --> 00:03:43,365
Это более удобно,

46
00:03:43,365 --> 00:03:48,800
потому что при разборе произвольного
выражения мы заранее можем и не знать,

47
00:03:48,800 --> 00:03:55,035
что будет у данной конкретной
операции слева, что будет справа.

48
00:03:55,035 --> 00:04:00,510
Мы можем присваивать эти значения
позже по ходу выполнения алгоритма.

49
00:04:00,510 --> 00:04:04,665
Давайте убедимся,
что наша программа работает.

50
00:04:04,665 --> 00:04:09,734
Запускаем ― и нас просят ввести выражение.

51
00:04:09,734 --> 00:04:14,618
Как мы видим, у нас в коде нигде
нету заранее заданных выражений.

52
00:04:14,618 --> 00:04:16,047
Давайте начнём с того,

53
00:04:16,047 --> 00:04:21,060
на примере которого мы пытались
построить ручное представление.

54
00:04:21,060 --> 00:04:26,645
[БЕЗ_ЗВУКА] Теперь

55
00:04:26,645 --> 00:04:32,388
давайте введём различные значения
переменной x и убедимся в том,

56
00:04:32,388 --> 00:04:34,700
что наша программа работает корректно.

57
00:04:34,700 --> 00:04:38,580
Давайте начнём со значения ноль.

58
00:04:38,580 --> 00:04:41,918
Мы получили, что выражение равно 12.

59
00:04:41,918 --> 00:04:46,490
Действительно, пять плюс семь минус ноль
умножить на ноль плюс ноль будет 12.

60
00:04:46,490 --> 00:04:51,826
Далее введём ещё несколько
значений и можем убедиться в том,

61
00:04:51,826 --> 00:05:00,057
что для каждого из них наша программа
вычисляет правильное значение выражения.

62
00:05:00,057 --> 00:05:01,110
Отлично!

63
00:05:01,110 --> 00:05:08,650
Давайте остановим нашу
программу и запустим её заново.

64
00:05:08,650 --> 00:05:15,710
И введём

65
00:05:15,710 --> 00:05:18,829
любое другое выражение.

66
00:05:18,829 --> 00:05:22,730
Например, x умножить на три

67
00:05:22,730 --> 00:05:27,640
минус один плюс x.

68
00:05:27,640 --> 00:05:32,710
Давайте и для него теперь попробуем
ввести несколько значений.

69
00:05:32,710 --> 00:05:35,047
Например, единица.

70
00:05:35,047 --> 00:05:39,460
Один умножить на три минус
один плюс один будет три.

71
00:05:39,460 --> 00:05:40,235
Всё верно.

72
00:05:40,235 --> 00:05:42,800
Также введём ещё несколько значений.

73
00:05:42,800 --> 00:05:46,380
Например, можем ввести
отрицательное значение.

74
00:05:46,380 --> 00:05:48,250
Получается минус пять.

75
00:05:48,250 --> 00:05:51,811
Минус один умножить на
три это будет минус три.

76
00:05:51,811 --> 00:05:55,281
Минус один и ещё раз минус
один будет минус пять.

77
00:05:55,281 --> 00:05:56,112
Всё верно.

78
00:05:56,112 --> 00:06:01,626
Таким образом, с помощью полученных
нами знаний о типе shared_ptr

79
00:06:01,626 --> 00:06:06,665
и виртуальных методах мы
смогли реализовать довольно

80
00:06:06,665 --> 00:06:11,953
непростой алгоритм и решить
относительно сложную задачу

81
00:06:11,953 --> 00:06:19,020
по разбору выражений и вычислению их
с использованием внешней переменной.