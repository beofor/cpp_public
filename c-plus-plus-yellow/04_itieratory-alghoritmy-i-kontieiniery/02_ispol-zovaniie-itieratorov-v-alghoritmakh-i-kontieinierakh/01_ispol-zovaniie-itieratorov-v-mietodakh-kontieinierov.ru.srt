1
00:00:00,000 --> 00:00:06,028
[БЕЗ_ЗВУКА] Итак, мы уже неплохо

2
00:00:06,028 --> 00:00:10,070
знакомы с итераторами, но давайте все-таки
вспомним, где мы их встречали раньше.

3
00:00:10,070 --> 00:00:14,270
Мы встречали их в алгоритмах,
в алгоритмах: sort, count,

4
00:00:14,270 --> 00:00:18,453
count_if, reverse,
find_if вот уже увидели.

5
00:00:18,453 --> 00:00:21,835
Мы встречали их в конструкторах
контейнеров, в частности,

6
00:00:21,835 --> 00:00:25,343
мы с помощью итераторов создавали
множество по элементам вектора и вектор по

7
00:00:25,343 --> 00:00:26,511
элементам множества.

8
00:00:26,511 --> 00:00:28,620
Просто конструктор принимал begin и end.

9
00:00:28,620 --> 00:00:31,596
Кроме того, мы недавно увидели
метод find у множества из словаря,

10
00:00:31,596 --> 00:00:33,170
который тоже возвращал итератор.

11
00:00:33,170 --> 00:00:35,341
Наверное, логично предположить,

12
00:00:35,341 --> 00:00:39,719
что у нас есть огромное количество разных
методов у контейнеров и алгоритмов,

13
00:00:39,719 --> 00:00:42,186
которые возвращают итераторы и
что-то с ними интересное делают.

14
00:00:42,186 --> 00:00:43,786
Мы не могли это рассмотреть
в первом курсе,

15
00:00:43,786 --> 00:00:45,230
потому что вы пока не знали итераторы,

16
00:00:45,230 --> 00:00:50,030
теперь же мы можем рассматривать
эти самые методы контейнеров.

17
00:00:50,030 --> 00:00:54,150
Давайте начнем с методов контейнеров.

18
00:00:54,150 --> 00:00:58,020
Рассмотрим снова вектор строк,
вектор языков программирования,

19
00:00:58,020 --> 00:01:03,545
найдем там, скажем, C++.

20
00:01:03,545 --> 00:01:09,232
auto it = find( begin(langs),

21
00:01:09,232 --> 00:01:12,534
end(langs), "C++").

22
00:01:12,534 --> 00:01:16,602
Ну, у алгоритма find_if есть его аналог,
более простой аналог — алгоритм find,

23
00:01:16,602 --> 00:01:19,430
который просто принимает элемент,
который надо найти.

24
00:01:19,430 --> 00:01:20,451
Что же с ним можно делать?

25
00:01:20,451 --> 00:01:23,657
Вот у нас есть итератор, который указывает
на конкретную позицию в контейнере.

26
00:01:23,657 --> 00:01:25,731
С этой позицией можно что-то делать.

27
00:01:25,731 --> 00:01:29,580
Например, можно у вектора
langs вызвать метод erase,

28
00:01:29,580 --> 00:01:33,980
взять и удалить конкретный
элемент по итератору.

29
00:01:33,980 --> 00:01:38,290
Мы помним, что у множества и
у словаря был метод erase,

30
00:01:38,290 --> 00:01:40,679
который принимал ключ,
который надо было удалить.

31
00:01:40,679 --> 00:01:45,067
У вектора же нет быстрого поиска по ключу,
но можно удалить элемент по итератору.

32
00:01:45,067 --> 00:01:50,557
Давайте я вызову функцию PrintRange,
ту самую нашу шаблонную функцию от вектора

33
00:01:50,557 --> 00:01:55,210
и убежусь, что у меня C++ удалился.

34
00:01:55,210 --> 00:01:59,565
Компилирую.

35
00:01:59,565 --> 00:02:03,920
Компилирую.

36
00:02:03,920 --> 00:02:08,410
Запускаю.

37
00:02:08,410 --> 00:02:12,530
И вижу, что у меня остались все
элементы вектора, кроме C++.

38
00:02:12,530 --> 00:02:15,901
На самом деле с помощью erase
можно удалить и больше всего.

39
00:02:15,901 --> 00:02:18,810
Можно удалить не один элемент,
а целый диапазон.

40
00:02:18,810 --> 00:02:24,250
Например, все, начиная с C++ и до конца,
до end(langs).

41
00:02:24,250 --> 00:02:28,803
Покажем, что у нас удалится все, начиная с
C++, и кажется, останется только Python.

42
00:02:28,803 --> 00:02:29,940
Сейчас мы это увидим.

43
00:02:29,940 --> 00:02:33,390
Да, действительно,
в векторе остался один элемент.

44
00:02:33,390 --> 00:02:35,053
Прекрасно.

45
00:02:35,053 --> 00:02:40,550
А что, если мы одумались и теперь
хотим вернуть C++ обратно в вектор?

46
00:02:40,550 --> 00:02:44,250
Для этого надо использовать метод insert.

47
00:02:44,250 --> 00:02:47,212
Метод insert принимает не просто элемент,
который надо вставить,

48
00:02:47,212 --> 00:02:49,611
потому что у вектора много мест,
куда можно вставить,

49
00:02:49,611 --> 00:02:52,180
а если мы хотим вставить в конец,
мы используем pushback.

50
00:02:52,180 --> 00:02:54,397
Он принимает итератор,
куда мы хотим вставить.

51
00:02:54,397 --> 00:02:58,595
Соответственно, если я напишу
insert(begin(langs), "C++"),

52
00:02:58,595 --> 00:03:01,930
я вставлю C++ в начало вектора.

53
00:03:01,930 --> 00:03:04,902
Поскольку у меня после удаления
там остался только Python,

54
00:03:04,902 --> 00:03:09,050
у меня должен поулчиться вектор из двух
элементов: сначала C++, потом Python.

55
00:03:09,050 --> 00:03:14,170
Да, действительно, так и получилось.

56
00:03:14,170 --> 00:03:18,562
А что если я передам в insert
какой-то другой итератор?

57
00:03:18,562 --> 00:03:21,608
Давайте разберемся, что будет происходить.

58
00:03:21,608 --> 00:03:25,970
Вызов метода insert от итератора и
конкретного значения вставляет это

59
00:03:25,970 --> 00:03:29,627
значение в вектор прямо
перед этим итератором.

60
00:03:29,627 --> 00:03:33,202
Соответственно, если у
нас итератор — это begin,

61
00:03:33,202 --> 00:03:36,414
то мы вставим прямо перед ним,
то есть в самое начало вектора.

62
00:03:36,414 --> 00:03:39,453
Если будет insert от end,
то мы вставим прямо перед end,

63
00:03:39,453 --> 00:03:42,720
то есть в самый конец вектора,
это будет аналог pushback.

64
00:03:42,720 --> 00:03:46,371
Ну и в произвольном случае я вставлю
перед конкретным итератором.

65
00:03:46,371 --> 00:03:50,815
Заметьте: у меня в векторе пять элементов,
но при этом шесть итераторов (мы это

66
00:03:50,815 --> 00:03:54,711
видели до этого) и с другой стороны у
меня шесть позиций вставки в этот вектор:

67
00:03:54,711 --> 00:03:57,360
в самое начало,
в самый конец или где-то между элементами.

68
00:03:57,360 --> 00:04:01,730
Получается, что удобно было сделать
именно так: вставлять перед итератором.

69
00:04:01,730 --> 00:04:03,794
Что еще может...

70
00:04:03,794 --> 00:04:07,080
как еще можно вызывать insert?

71
00:04:07,080 --> 00:04:10,673
Мы уже видели на самом деле
метод insert в первом курсе,

72
00:04:10,673 --> 00:04:14,580
мы тогда вставляли в конец одного
вектора содержимое другого вектора.

73
00:04:14,580 --> 00:04:17,522
И действительно, в insert можно
передать диапазон элементов.

74
00:04:17,522 --> 00:04:20,740
Сначала итератор, куда вставлять, а потом
диапазон элементов, который мы вставляем.

75
00:04:20,740 --> 00:04:25,714
Или можно передать в insert
количество элементов, сколько

76
00:04:25,714 --> 00:04:29,860
раз мы хотим вставить и тот самый элемент,
который мы хотим вставить столько раз.

77
00:04:29,860 --> 00:04:31,718
Это второй пункт списка.

78
00:04:31,718 --> 00:04:36,503
И наконец, можно передать в insert
в фигурных скобках набор элементов,

79
00:04:36,503 --> 00:04:39,640
который мы хотим вставить в
какое-то конкретное место вектора.