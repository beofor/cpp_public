1
00:00:00,000 --> 00:00:06,394
[БЕЗ_ЗВУКА] На текущий

2
00:00:06,394 --> 00:00:11,510
момент мы с вами уже довольно подробно
познакомились с управлением памятью в C++.

3
00:00:11,510 --> 00:00:15,040
Настало время несколько
структурировать наши знания.

4
00:00:15,040 --> 00:00:16,267
Давайте посмотрим,

5
00:00:16,267 --> 00:00:20,922
какие вообще в C++ бывают виды объектов
с точки зрения времени их жизни.

6
00:00:20,922 --> 00:00:23,910
У нас бывают автоматические объекты,

7
00:00:23,910 --> 00:00:27,090
временем жизни автоматических
объектов управляет компилятор.

8
00:00:27,090 --> 00:00:30,323
К автоматическим объектам
относятся локальные переменные,

9
00:00:30,323 --> 00:00:32,458
глобальные переменные и члены классов.

10
00:00:32,458 --> 00:00:34,660
Также у нас бывают динамические объекты.

11
00:00:34,660 --> 00:00:38,250
Временем жизни динамических
объектов управляет программист.

12
00:00:38,250 --> 00:00:43,810
Мы уже знаем, что динамические объекты
создаются с помощью ключевого слова New,

13
00:00:43,810 --> 00:00:48,408
которое вызывается неявно в недрах
функций make_unique и make_shared.

14
00:00:48,408 --> 00:00:51,510
А удаляются они с помощью
ключевого слова delete,

15
00:00:51,510 --> 00:00:55,110
которое опять же вызывается неявно
в деструкторах умных указателей.

16
00:00:55,110 --> 00:01:00,850
Также в C++ существует понятие владения.

17
00:01:00,850 --> 00:01:04,379
Считается, что некоторая
сущность владеет объектом,

18
00:01:04,379 --> 00:01:06,590
если она отвечает за его удаление.

19
00:01:06,590 --> 00:01:10,390
Давайте посмотрим,
кто владеет разными видами объектов.

20
00:01:10,390 --> 00:01:15,282
Для автоматических объектов: если
говорить о локальных переменных,

21
00:01:15,282 --> 00:01:18,610
то локальными переменами владеет
окружающий их блок кода.

22
00:01:18,610 --> 00:01:23,465
Действительно, когда поток управления
программы выходит из блока кода, мы знаем,

23
00:01:23,465 --> 00:01:27,340
что уничтожаются все локальные переменные,
которые были объявлены в этом блоке.

24
00:01:27,340 --> 00:01:32,243
Глобальные переменные: можно считать,
что ими владеет сама программа.

25
00:01:32,243 --> 00:01:37,125
Мы знаем, что когда программа завершается,
уже после завершения функции main,

26
00:01:37,125 --> 00:01:39,810
происходит удаление всех
глобальных переменных.

27
00:01:39,810 --> 00:01:42,413
А членами класса владеет
сам объект класса.

28
00:01:42,413 --> 00:01:45,505
Действительно, мы никогда
не задумываемся о том,

29
00:01:45,505 --> 00:01:48,150
когда нам нужно удалять члены класса.

30
00:01:48,150 --> 00:01:53,003
Мы знаем, что в любой момент, как бы это
ни произошло, когда будет уничтожен сам

31
00:01:53,003 --> 00:01:58,728
объект класса, он позаботится о том, чтобы
в своем деструкторе удалить свои члены.

32
00:01:58,728 --> 00:02:03,450
И с автоматическим объектами: эти правила,

33
00:02:03,450 --> 00:02:08,325
которые мы здесь перечислили,
они очень четкие, и им следует компилятор.

34
00:02:08,325 --> 00:02:10,350
Они всегда работают ровно так.

35
00:02:10,350 --> 00:02:14,840
С динамическими объектами
ситуация несколько интереснее.

36
00:02:14,840 --> 00:02:18,200
Как вы думаете,
кто владеет динамическими объектами?

37
00:02:18,200 --> 00:02:21,350
Как вы можете догадаться
по названию данного блока,

38
00:02:21,350 --> 00:02:24,984
динамическими объектами
владеют умные указатели.

39
00:02:24,984 --> 00:02:28,375
При этом unique_ptr
обеспечивает уникальное,

40
00:02:28,375 --> 00:02:31,300
эксклюзивное владение объектами.

41
00:02:31,300 --> 00:02:35,424
На один объект может указывать
только один unique_ptr,

42
00:02:35,424 --> 00:02:38,590
и один unique_ptr может
указывать только на один объект.

43
00:02:38,590 --> 00:02:41,940
Shared_ptr — это разделяемое,
или совместное, владение.

44
00:02:41,940 --> 00:02:45,842
На один и тот же объект может
указывать несколько shared_ptr.

45
00:02:45,842 --> 00:02:48,225
А вот сырой указатель не владеет объектом.

46
00:02:48,225 --> 00:02:50,598
Считается, что если у нас
есть сырой указатель,

47
00:02:50,598 --> 00:02:54,681
мы его получили каким-то образом,
то мы никаким образом не отвечаем за время

48
00:02:54,681 --> 00:02:57,410
жизни этого объекта,
на который он указывает.

49
00:02:57,410 --> 00:03:03,444
И вот тут ситуация достаточно интересна
с динамическими объектами в том плане,

50
00:03:03,444 --> 00:03:07,351
что вот это то, что мы сейчас перечислили,
это соглашение.

51
00:03:07,351 --> 00:03:08,833
Это не совсем правила.

52
00:03:08,833 --> 00:03:12,503
И этим соглашениям должен
следовать программист.

53
00:03:12,503 --> 00:03:16,876
При этом программист,
вообще говоря, может их нарушить.

54
00:03:16,876 --> 00:03:21,880
Давайте посмотрим на эти соглашения в
действии на конкретном примере кода.

55
00:03:21,880 --> 00:03:25,946
Например, у нас есть функция CreateAnimal.

56
00:03:25,946 --> 00:03:28,350
Посмотрим на ее прототип.

57
00:03:28,350 --> 00:03:31,545
Она принимает строчку и
возвращает unique_ptr.

58
00:03:31,545 --> 00:03:36,023
Нам даже не нужно заглядывать
внутрь этой функции, чтобы понять,

59
00:03:36,023 --> 00:03:40,934
что эта функция создает объект и
возвращает нам его во владение.

60
00:03:40,934 --> 00:03:43,988
Потому что она возвращает нам unique_ptr.

61
00:03:43,988 --> 00:03:48,344
Теперь мы у себя сохраняем uniqie_ptr,
мы как-то им пользуемся,

62
00:03:48,344 --> 00:03:50,870
и владение находится на нашей стороне.

63
00:03:50,870 --> 00:03:54,276
Эта функция следует данным соглашениям.

64
00:03:54,276 --> 00:03:59,390
Следующий пример: у нас
есть объект «фигура».

65
00:03:59,390 --> 00:04:05,588
В этом объекте у нас есть поле «текстура»,
которое сохранено как shared_ptr.

66
00:04:05,588 --> 00:04:09,528
И этот shared_ptr нам передается в
конструкторе То есть из сигнатуры

67
00:04:09,528 --> 00:04:14,836
конструктора мы можем понять, что новый
объект фигуры, который создан с помощью

68
00:04:14,836 --> 00:04:21,010
этого конструктора, он будет находиться
в совместном владении этой текстурой.

69
00:04:21,010 --> 00:04:26,990
Этот пример также следует
нашим соглашениям.

70
00:04:26,990 --> 00:04:29,662
Следующий пример с
использованием сырого указателя.

71
00:04:29,662 --> 00:04:32,970
Наша функция Print, которая
принимает сырой указатель на объект,

72
00:04:32,970 --> 00:04:36,890
как-то пользуется этим объектом, здесь
вызывает ToString, вызывает Evaluate,

73
00:04:36,890 --> 00:04:42,274
но при этом она, разумеется, не удаляет
этот объект и ничего не делает с ним,

74
00:04:42,274 --> 00:04:44,970
что относилось бы к времени
жизни этого объекта.

75
00:04:44,970 --> 00:04:48,410
То есть указатель,
который она получила, — не владеющий.

76
00:04:48,410 --> 00:04:51,990
Давайте подведем небольшой итог.

77
00:04:51,990 --> 00:04:57,395
Существуют соглашения по
владению динамическими объектами.

78
00:04:57,395 --> 00:05:00,174
Этим соглашениям следует программист.

79
00:05:00,174 --> 00:05:02,800
И эти соглашения могут быть нарушены.

80
00:05:02,800 --> 00:05:07,651
Они могут быть нарушены по ошибке,
например, из-за простого незнания,

81
00:05:07,651 --> 00:05:10,520
или они могут быть
нарушены по необходимости.

82
00:05:10,520 --> 00:05:14,440
Необходимость может возникнуть в
том случае, если нам нужно ручное,

83
00:05:14,440 --> 00:05:17,267
низкоуровневое управление
динамической памятью.

84
00:05:17,267 --> 00:05:19,165
Например, мы пишем свой контейнер.

85
00:05:19,165 --> 00:05:22,424
Один из примеров,
когда нам нужно ручное управление,

86
00:05:22,424 --> 00:05:24,820
мы еще посмотрим в следующих видео.

87
00:05:24,820 --> 00:05:30,418
В случае соблюдения этих соглашений,
вы можете гарантировать,

88
00:05:30,418 --> 00:05:34,710
и это очень важно, и это то, зачем
люди придерживаются данных соглашений,

89
00:05:34,710 --> 00:05:39,207
вы гарантируете, что в вашей программе
не может быть никаких утечек

90
00:05:39,207 --> 00:05:42,550
памяти и не может быть
двойного удаления объекта.

91
00:05:42,550 --> 00:05:45,130
То есть если вы следуете этим соглашениям,

92
00:05:45,130 --> 00:05:48,417
у вас такие ситуации просто
теоретически невозможны.

93
00:05:48,417 --> 00:05:50,091
Нельзя написать такой код.

94
00:05:50,091 --> 00:05:54,672
Однако если вы эти соглашения нарушаете,
то все может сработать корректно,

95
00:05:54,672 --> 00:05:58,248
но при определенных ситуациях
вы можете создать программы,

96
00:05:58,248 --> 00:06:01,384
в которых будут утечки
памяти или двойное удаление.

97
00:06:01,384 --> 00:06:04,762
В следующем видео мы как раз
посмотрим на такие примеры.