<meta charset="utf-8"/>
<co-content>
 <h2 level="2">
  Введение
 </h2>
 <p>
  Из предыдущего материала про изменяемые параметры можно было бы сделать довольно неожиданный вывод: неконстантные методы использовать плохо, потому что они имеют неявный изменяемый параметр *this и могут произвольным образом на него влиять.
 </p>
 <p>
  Но здравый смысл подсказывает, что не нужно уходить в крайности: нельзя же обойтись без неконстантных методов, да и в предыдущем материале рассматривалась идея добавить метод BookMany. С другой стороны, нужно понимать, что все аргументы из предыдущего материала справедливы и для неконстантных методов с их параметром *this.
 </p>
 <p>
  Итого, нужно найти баланс. Вспомним проблемы, которые могли возникать в таких функциях.
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    <em>
     Непрозрачность происходящего.
    </em>
    Действительно, от неконстантного метода можно ожидать чего угодно. Поэтому нужно делать его максимально компактным и понятным.
   </p>
  </li>
  <li>
   <p>
    <em>
     Тестируемость.
    </em>
    Метод, имеющий возможность повлиять на весь объект, тестировать надо с соответствующей внимательностью.
   </p>
  </li>
 </ol>
 <p>
  Рассмотрим несколько способов (адекватных) упростить работу с неконстантными методами. К слову, к ним относятся и конструкторы.
 </p>
 <h2 level="2">
  Идея 1. Компактность
 </h2>
 <p>
  Компактность функций и методов — самое важное. Если их код простой и содержит десяток-другой строк, то он легко кешируется в мозге разработчика и не доставляет проблем.
 </p>
 <h2 level="2">
  Идея 2. Прозрачность обновления полей
 </h2>
 <p>
  Раз уж мы всё равно вынуждены обновлять поля, стоит делать это максимально прозрачно и предпочитать для этого конструкцию SomeField = .... В качестве примера рассмотрим конструктор класса DomainChecker из опубликованного решения задачи «Запрещённые домены»:
 </p>
 <pre language="c_cpp">template &lt;typename InputIt&gt;
DomainChecker::DomainChecker(InputIt domains_begin, InputIt domains_end) {
  sorted_domains_.reserve(distance(domains_begin, domains_end));
  for (const Domain&amp; domain : Range(domains_begin, domains_end)) {
    sorted_domains_.push_back(&amp;domain);
  }
  sort(begin(sorted_domains_), end(sorted_domains_), IsDomainLess);
  sorted_domains_ = AbsorbSubdomains(move(sorted_domains_));
}</pre>
 <p>
  Код заполнения вектора sorted_domains_ и его сортировки довольно прост, поэтому написан непосредственно в конструкторе. Но в последней строке возникает необходимость поглотить поддомены наддоменами, и, благодаря тому, что AbsorbSubdomains принимает параметр по значению, а не по неконстантной ссылке, посыл «я меняю поле класса с помощью вызова AbsorbSubdomains» становится более прозрачным.
 </p>
 <p>
  Наконец, sorted_domains_ — единственное неконстантное поле в классе DomainChecker, поэтому больших неожиданностей от методов этого класса ожидать не приходится.
 </p>
 <h2 level="2">
  Идея 3. Выделение сложной логики в статические методы
 </h2>
 <p>
  Мы уже поднимали вопросы тестируемости неконстантных методов: по-хорошему ведь после вызова такого метода класс может измениться как угодно, и это нужно проверять в юнит-тестах. Чтобы уменьшить уровень паранойи, можно выселять сложную логику в статические методы с понятным входом и выходом и тестировать именно их. Ровно так и сделано с методом AbsorbSubdomains — напомним код DomainChecker целиком:
 </p>
 <pre language="c_cpp">class DomainChecker {
public:
  template &lt;typename InputIt&gt;
  DomainChecker(InputIt domains_begin, InputIt domains_end) {
    sorted_domains_.reserve(distance(domains_begin, domains_end));
    for (const Domain&amp; domain : Range(domains_begin, domains_end)) {
      sorted_domains_.push_back(&amp;domain);
    }
    sort(begin(sorted_domains_), end(sorted_domains_), IsDomainLess);
    sorted_domains_ = AbsorbSubdomains(move(sorted_domains_));
  }

  // Check if candidate is subdomain of some domain
  bool IsSubdomain(const Domain&amp; candidate) const {
    const auto it = upper_bound(
        begin(sorted_domains_), end(sorted_domains_),
        &amp;candidate, IsDomainLess);
    if (it == begin(sorted_domains_)) {
      return false;
    }
    return ::IsSubdomain(candidate, **prev(it));
  }

private:
  vector&lt;const Domain*&gt; sorted_domains_;

  static bool IsDomainLess(const Domain* lhs, const Domain* rhs) {
    const auto lhs_reversed_parts = lhs-&gt;GetReversedParts();
    const auto rhs_reversed_parts = rhs-&gt;GetReversedParts();
    return lexicographical_compare(
      begin(lhs_reversed_parts), end(lhs_reversed_parts),
      begin(rhs_reversed_parts), end(rhs_reversed_parts)
    );
  }

  static vector&lt;const Domain*&gt; AbsorbSubdomains(vector&lt;const Domain*&gt; domains) {
    domains.erase(
        unique(begin(domains), end(domains),
               [](const Domain* lhs, const Domain* rhs) {
                 return IsSubOrSuperDomain(*lhs, *rhs);
               }),
        end(domains)
    );
    return domains;
  }
};</pre>
 <p>
  В этом классе есть два статических метода: IsDomainLess и AbsorbSubdomains. В то время как первый очевидным образом помогает переиспользовать компаратор между сортировкой и бинарным поиском, второй по сути просто содержит часть кода конструктора.
 </p>
 <p>
  Выселив часть логики конструктора в AbsorbSubdomains, мы не только облегчили код конструктора, но и получили возможность целенаправленно протестировать именно эту часть логики. А именно, можно вынести AbsorbSubdomains в protected-секцию, отнаследоваться от DomainChecker классом AbsorbSubdomainsUnitTesting и тем самым получить возможность косвенно вызывать AbsorbSubdomains в юнит-тестах.
 </p>
 <p>
  Более высокий уровень паранойи (но тоже иногда пригождающийся) — выселять тело неконстантного метода целиком в статический. Так можно было бы сделать и с конструктором:
 </p>
 <pre language="c_cpp">template &lt;typename InputIt&gt;
DomainChecker::DomainChecker(InputIt domains_begin, InputIt domains_end) {
  sorted_domains_ = BuildSortedDomains(domains_begin, domains_end);
}</pre>
 <p>
  Но в данном случае это не очень оправдано и помогло бы разве что протестировать сортировку доменов.
 </p>
 <h2 level="2">
  Идея 4. Минимизировать количество полей в классе
 </h2>
 <p>
  Как мы уже говорили, каждое поле класса по сути является параметром любого метода, а если и поле, и метод неконстантные, энтропия увеличивается ещё сильнее. Поэтому стоит следить за разумностью количества полей в классе: если их больше четырёх, сложность понимания класса кратно возрастает. (Это, конечно, не относится к статическим константам.)
 </p>
 <p>
  В качестве примера рассмотрим медленное решение задачи «Электронная книга», в которой класс ReadingManager содержит один неконстантный, но короткий метод Read, чуть менее компактный, но константный метод Cheer и три нестатических неконстантных поля:
 </p>
 <pre language="c_cpp">class ReadingManager {
public:
  ReadingManager()
      : user_page_counts_(MAX_USER_COUNT_ + 1, 0),
        sorted_users_(),
        user_positions_(MAX_USER_COUNT_ + 1, -1) {}

  void Read(int user_id, int page_count) {
    if (user_page_counts_[user_id] == 0) {
      AddUser(user_id);
    }
    user_page_counts_[user_id] = page_count;
    int&amp; position = user_positions_[user_id];
    while (position &gt; 0
           &amp;&amp; page_count &gt; user_page_counts_[sorted_users_[position - 1]]) {
      SwapUsers(position, position - 1);
    }
  }

  double Cheer(int user_id) const {
    if (user_page_counts_[user_id] == 0) {
      return 0;
    }
    const int user_count = GetUserCount();
    if (user_count == 1) {
      return 1;
    }
    const int page_count = user_page_counts_[user_id];
    int position = user_positions_[user_id];
    while (position &lt; user_count &amp;&amp;
           user_page_counts_[sorted_users_[position]] == page_count) {
      ++position;
    }
    if (position == user_count) {
      return 0;
    }
    return (user_count - position) * 1.0 / (user_count - 1);
  }

private:
  static const int MAX_USER_COUNT_ = 100'000;

  vector&lt;int&gt; user_page_counts_;
  vector&lt;int&gt; sorted_users_;   // отсортированы по убыванию количества страниц
  vector&lt;int&gt; user_positions_; // позиции в векторе sorted_users_

  int GetUserCount() const {
    return sorted_users_.size();
  }
  void AddUser(int user_id) {
    sorted_users_.push_back(user_id);
    user_positions_[user_id] = sorted_users_.size() - 1;
  }
  void SwapUsers(int lhs_position, int rhs_position) {
    const int lhs_id = sorted_users_[lhs_position];
    const int rhs_id = sorted_users_[rhs_position];
    swap(sorted_users_[lhs_position], sorted_users_[rhs_position]);
    swap(user_positions_[lhs_id], user_positions_[rhs_id]);
  }
};</pre>
 <p>
  Что бы мы делали с этим классом, если бы у нас возникла необходимость добавить ещё одно поле? Рассмотрим несколько вариантов развития событий.
 </p>
 <h3 level="3">
  Пример 1. Логирование Read-запросов
 </h3>
 <p>
  Представим, что в класс понадобилось добавить логирование Read-запросов. Обычно простое добавление поля с логгером не считается плохим стилем, но в случае большого и громоздкого класса стоит рассмотреть идею из предыдущего материала для чтения: не трогать сам ReadingManager, а логирование добавить с помощью прокси-объекта ReadingManagerWithLogger:
 </p>
 <pre language="c_cpp">class ReadingManagerWithLogger {
public:
  ReadingManagerWithLogger(ReadingManager&amp; manager, Logger&amp; logger)
      : manager_(manager),
        logger_(logger)
  {}
  
  void Read(int user_id, int page_count) {
    manager_.Read(user_id, page_count);
    logger_.LogRead(user_id, page_count);
  }
  
private:
  ReadingManager&amp; manager_;
  Logger&amp; logger_;
};</pre>
 <h3 level="3">
  Пример 2. Новое свойство пользователя
 </h3>
 <p>
  Сейчас пользователи ранжируются по количеству прочитанных страниц, и эти числа благополучно хранятся в поле vector&lt;int&gt; user_page_counts_. Что если мы начнём учитывать время чтения и при равном количестве прочитанных страниц будем сортировать пользователей по времени, проведённому за чтением?
 </p>
 <p>
  В этом случае возникает ожидаемое желание добавить поле vector&lt;int&gt; user_time_spent_. Но это будет уже второй вектор с пользовательской статистикой и третий — индексируемый по user_id. Если объединить информацию о пользователе в структуру UserStats, мы не только уменьшим количество полей класса, но и уменьшим вероятность ошибки, когда одну характеристику пользователя обновили, а про другую забыли; наконец, энтропия уменьшится и благодаря отсутствию необходимости поддерживать инвариант «размер вектора user_page_counts_ равен размеру вектора user_time_spent_».
 </p>
 <p>
  <em>
   Замечание про использование векторов для хранения пользовательской статистики. В исходной задаче максимальное количество пользователей было известно заранее и равнялось 100000. Тем не менее, в решении не был использован контейнер array, потому что к тому моменту он ещё не был рассмотрен в курсе. Однако в реальных задачах было бы слишком расточительно сразу выделять много памяти под всех пользователей, да и само ограничение не было бы известно: от таких систем обычно требуется возможность выдерживать любой поток пользовательских событий, потребляя при необходимости больше памяти. Поэтому будем держать в уме, что, хотя в приведённом решении количество пользователей и фиксировано и можно было бы использовать массивы, в реальной жизни не обошлось бы без векторов, расширяемых с приходом новых пользователей.
  </em>
 </p>
 <h3 level="3">
  Пример 3. Статистика по страницам
 </h3>
 <p>
  Предположим, что к требованию учёта пользовательской статистики добавилось ещё одно — хранить некую статистику по страницам. Если это будет, скажем, количество пользователей, дочитавших до каждой страницы, возникнет желание добавить поле vector&lt;int&gt; page_user_counts_. И если в целом намечается тенденция по расширению всевозможных хранимых статистик, может помочь — вкупе с предыдущим примером — вынести все статистики в отдельный класс, чтобы отделить их от полей sorted_users_ и user_positions_, необходимых для поддержания упорядоченности пользователей. Тем самым по коду сразу будет очевидно, относится ли он к хранению статистики или к поддержанию упорядоченности:
 </p>
 <pre language="c_cpp">class ReadingManagerStats {
public:
  ReadingManagerStats(int max_user_count, int max_page_count)
      : user_page_counts_(max_user_count + 1, 0),
        page_user_counts_(max_page_count, 0)
  {}

  int GetUserPageCount(int user_id) const {
    return user_page_counts_[user_id];
  }
  int&amp; GetUserPageCount(int user_id) {
    return user_page_counts_[user_id];
  }
  
  // Методы про page_user_counts_

private:
  vector&lt;int&gt; user_page_counts_;
  vector&lt;int&gt; page_user_counts_;
};

class ReadingManager {
public:
  ReadingManager()
      : stats(MAX_USER_COUNT_, MAX_PAGE_COUNT_),
        sorted_users_(),
        user_positions_(MAX_USER_COUNT_ + 1, -1) {}

  void Read(int user_id, int page_count) {
    if (stats.GetUserPageCount(user_id) == 0) {
      AddUser(user_id);
    }
    stats.GetUserPageCount(user_id) = page_count;
    int&amp; position = user_positions_[user_id];
    while (
        position &gt; 0
        &amp;&amp; page_count &gt; stats.GetUserPageCount(sorted_users_[position - 1])
    ) {
      SwapUsers(position, position - 1);
    }
  }

  double Cheer(int user_id) const {
    if (stats.GetUserPageCount(user_id) == 0) {
      return 0;
    }
    const int user_count = GetUserCount();
    if (user_count == 1) {
      return 1;
    }
    const int page_count = stats.GetUserPageCount(user_id);
    int position = user_positions_[user_id];
    while (position &lt; user_count &amp;&amp;
           stats.GetUserPageCount(sorted_users_[position]) == page_count) {
      ++position;
    }
    if (position == user_count) {
      return 0;
    }
    return (user_count - position) * 1.0 / (user_count - 1);
  }

private:
  static const int MAX_USER_COUNT_ = 100'000;
  static const int MAX_PAGE_COUNT_ = 1000;

  ReadingManagerStats stats;
  vector&lt;int&gt; sorted_users_;   // отсортированы по убыванию количества страниц
  vector&lt;int&gt; user_positions_; // позиции в векторе sorted_users_

  int GetUserCount() const {
    return sorted_users_.size();
  }
  void AddUser(int user_id) {
    sorted_users_.push_back(user_id);
    user_positions_[user_id] = sorted_users_.size() - 1;
  }
  void SwapUsers(int lhs_position, int rhs_position) {
    const int lhs_id = sorted_users_[lhs_position];
    const int rhs_id = sorted_users_[rhs_position];
    swap(sorted_users_[lhs_position], sorted_users_[rhs_position]);
    swap(user_positions_[lhs_id], user_positions_[rhs_id]);
  }
};</pre>
 <p>
  Наконец, такая инкапсуляция помогла бы:
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    Выделить обновление статистики пользователя из неконстантного Get-метода в Set-метод, повысив читаемость кода.
   </p>
  </li>
  <li>
   <p>
    Изменить способ хранения пользовательской статистики с вектора на словарь, не трогая код ReadingManager.
   </p>
  </li>
 </ol>
 <h2 level="2">
  Итоги
 </h2>
 <p>
  Есть несколько способов повысить прозрачность и тестируемость неконстантных методов:
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    Писать их понятно и компактно.
   </p>
  </li>
  <li>
   <p>
    Выселять сложную логику в статические методы, покрывая их юнит-тестами.
   </p>
  </li>
  <li>
   <p>
    Объединять поля в подклассы.
   </p>
  </li>
 </ol>
 <p>
 </p>
</co-content>
<style>
 body {
    padding: 50px 85px 50px 85px;
}

table th, table td {
    border: 1px solid #e0e0e0;
    padding: 5px 20px;
    text-align: left;
}
input {
    margin: 10px;
}
}
th {
    font-weight: bold;
}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
img {
    height: auto;
    max-width: 100%;
}
pre {
    display: block;
    margin: 20px;
    background: #424242;
    color: #fff;
    font-size: 13px;
    white-space: pre-wrap;
    padding: 9.5px;
    margin: 0 0 10px;
    border: 1px solid #ccc;
}
</style>
<script async="" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$$','$$'], ['$','$'] ],
      displayMath: [ ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
