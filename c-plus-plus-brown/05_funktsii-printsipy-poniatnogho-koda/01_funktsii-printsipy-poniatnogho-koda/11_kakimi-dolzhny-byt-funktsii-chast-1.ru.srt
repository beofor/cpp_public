1
00:00:00,000 --> 00:00:07,272
[БЕЗ_ЗВУКА] Итак,

2
00:00:07,272 --> 00:00:09,839
мы начали говорить про функции,
теперь наконец-то,

3
00:00:09,839 --> 00:00:13,890
мы можем, исходя из этого понять, какими
же важными свойствами они должны обладать,

4
00:00:13,890 --> 00:00:17,861
чтобы считаться хорошими
и чтобы код был понятным.

5
00:00:17,861 --> 00:00:22,365
Первое свойство — это
понятность сигнатуры функции.

6
00:00:22,365 --> 00:00:26,629
Вы должны уметь посмотреть на
сигнатуру функции и из этого понять,

7
00:00:26,629 --> 00:00:27,990
что же функция делает.

8
00:00:27,990 --> 00:00:32,590
По ее названию, по входным параметрам,
по типу выходного параметра.

9
00:00:32,590 --> 00:00:37,150
Во-вторых, эта функция должна
быть понятна в месте вызова.

10
00:00:37,150 --> 00:00:41,371
То есть когда функция вызывается, должно
быть понятно примерно, что каждый параметр

11
00:00:41,371 --> 00:00:45,562
означает и чего ожидать от этой
функции в привязке к этим параметрам.

12
00:00:45,562 --> 00:00:49,674
Затем, мы говорили про тестируемость,
и конечно, функции надо писать такими,

13
00:00:49,674 --> 00:00:51,260
чтобы их было удобно тестировать.

14
00:00:51,260 --> 00:00:53,798
Понятно, что есть функции,
которые вы вообще не протестируете,

15
00:00:53,798 --> 00:00:58,049
но и написать функцию можно настолько
плохо, что тестировать ее будет неудобно.

16
00:00:58,049 --> 00:01:01,110
Далее, функция должна быть поддерживаемой.

17
00:01:01,110 --> 00:01:05,613
То есть если придет какой-то другой
разработчик, или вы, и вы захотите как-то

18
00:01:05,613 --> 00:01:10,370
обновить функциональность этой функции,
расширить ее, это должно быть легко.

19
00:01:10,370 --> 00:01:14,166
И наконец, связанное свойство —
это сфокусированность функции.

20
00:01:14,166 --> 00:01:17,250
То есть функция должна решать
одну конкретную задачу.

21
00:01:17,250 --> 00:01:21,447
Не несколько маленьких как-то
переплетенно, а одну конкретную.

22
00:01:21,447 --> 00:01:25,399
И давайте мы сейчас все эти важные
свойства проиллюстрируем несколько

23
00:01:25,399 --> 00:01:27,349
гипертрофированными, но тем не менее,

24
00:01:27,349 --> 00:01:30,972
понятными примерами на примере
известных вам уже задач.

25
00:01:30,972 --> 00:01:33,660
Начнем с понятности сигнатуры.

26
00:01:33,660 --> 00:01:36,266
Вот пожалуйста.

27
00:01:36,266 --> 00:01:39,920
Задача «Экспрессы».

28
00:01:39,920 --> 00:01:43,765
В ней нужно было сделать что-то примерно
следующее — там были запросы вида

29
00:01:43,765 --> 00:01:47,371
«добавить экспресс от одной станции
до другой и проверить, насколько

30
00:01:47,371 --> 00:01:51,470
близко можно подъехать от одной станции
до другой, пользуясь одним экспрессом».

31
00:01:51,470 --> 00:01:54,692
Вы эту задачу только что
прорешали и наверняка вспомнили.

32
00:01:54,692 --> 00:01:59,410
Представьте, что вы увидите чье-то решение
этой задачи и там есть функция Process.

33
00:01:59,410 --> 00:02:04,775
Как видите, она не принимает
ничего и возвращает ничего.

34
00:02:04,775 --> 00:02:07,163
Какой можно вывод из этого сделать?

35
00:02:07,163 --> 00:02:08,790
Что эта функция делает?

36
00:02:08,790 --> 00:02:12,850
Совершенно непонятно,
что делает эта функция,

37
00:02:12,850 --> 00:02:15,285
если не посмотреть внутрь этой функции.

38
00:02:15,285 --> 00:02:20,423
Ну давайте посмотрим внутрь этой функции,
и мы увидим, что эта функция по сути

39
00:02:20,423 --> 00:02:25,868
просто берет и считывает
все входные данные

40
00:02:25,868 --> 00:02:30,550
и вызывает нужные методы
у переменной routes.

41
00:02:30,550 --> 00:02:35,433
А функция main — просто в цикле
для каждого очередного запроса

42
00:02:35,433 --> 00:02:37,650
вызывает функция Process.

43
00:02:37,650 --> 00:02:43,494
То есть эта функция обрабатывает один
конкретный запрос, а переменная routes

44
00:02:43,494 --> 00:02:47,853
при этом — вообще глобальная переменная,
с которой эта функция взаимодействует.

45
00:02:47,853 --> 00:02:50,920
И из-за этого совершенно непонятно,
что эта функция делает,

46
00:02:50,920 --> 00:02:52,871
если вы эту функцию не прочитаете.

47
00:02:52,871 --> 00:02:54,626
Вот это пример плохой функции.

48
00:02:54,626 --> 00:02:58,080
Она работает с глобальной переменной,
она что-то читает на вход,

49
00:02:58,080 --> 00:03:00,908
что-то выводит и зачем здесь
тогда функция — не очень понятно.

50
00:03:00,908 --> 00:03:04,829
Разве что ради переиспользуемости,
но, пожалуйста, так не надо делать.

51
00:03:04,829 --> 00:03:09,871
Еще один пример функции с плохой,
непонятной сигнатурой — вот такой.

52
00:03:09,871 --> 00:03:14,931
Задача — трекер задач,
в ней нужно было как-то

53
00:03:14,931 --> 00:03:20,197
манипулировать задачами разработчиков,
соответственно там была статистика,

54
00:03:20,197 --> 00:03:22,585
у какого человека сколько задач,
в каком статусе,

55
00:03:22,585 --> 00:03:26,760
и нужно было уметь обновить статусы у
набора задач для конкретного человека.

56
00:03:26,760 --> 00:03:30,799
Опять же, представьте,
что вы видите чье-то решение этой задачи,

57
00:03:30,799 --> 00:03:35,682
и там вот такая функция —
PerformPersonTasks, которая принимает,

58
00:03:35,682 --> 00:03:40,914
во-первых, словарь из строки в TasksInfo,
по неконстатной ссылке,

59
00:03:40,914 --> 00:03:46,460
затем она принимает имя человека по
констатной ссылке, это, допустим, понятно.

60
00:03:46,460 --> 00:03:49,877
Она принимает количество задач,
которые надо выполнить,

61
00:03:49,877 --> 00:03:55,850
и еще две не констатных ссылки first
и second на объекты типа TasksInfo.

62
00:03:55,850 --> 00:03:58,101
Понятно ли, что это за first и second?

63
00:03:58,101 --> 00:04:01,590
Ну мне, например, не понятно,
если я не помню эту задачу.

64
00:04:01,590 --> 00:04:04,472
Еще какая-то не констатная
ссылка на person_tasks.

65
00:04:04,472 --> 00:04:09,890
То есть функция как минимум
меняет данные по трем ссылкам.

66
00:04:09,890 --> 00:04:12,063
Это первый аргумент
функции и два последних.

67
00:04:12,063 --> 00:04:13,910
При этом она еще ничего не возвращает.

68
00:04:13,910 --> 00:04:17,930
То есть она принимает что-то на вход, а на
самом деле, TasksInfo это тоже словари,

69
00:04:17,930 --> 00:04:21,273
и как-то она эти три словаря меняет.

70
00:04:21,273 --> 00:04:22,115
Кошмар.

71
00:04:22,115 --> 00:04:23,507
Непонятно, что происходит.

72
00:04:23,507 --> 00:04:26,300
Хуже того, непонятно,
что такое first, что такое second.

73
00:04:26,300 --> 00:04:30,066
И вот эти TasksInfo,
они есть как в двух последних параметрах,

74
00:04:30,066 --> 00:04:32,159
так и в значениях первого словаря.

75
00:04:32,159 --> 00:04:32,870
Вот здесь.

76
00:04:32,870 --> 00:04:35,667
То есть у вас может
закрасться даже подозрение,

77
00:04:35,667 --> 00:04:39,770
что вот эти два последних аргумента
— это ссылки на значения словаря.

78
00:04:39,770 --> 00:04:40,771
В общем, ужасно.

79
00:04:40,771 --> 00:04:45,044
Функция с пятью аргументами, из них еще
и три неконстатные ссылки, не понятно,

80
00:04:45,044 --> 00:04:46,074
чего от нее ожидать.

81
00:04:46,074 --> 00:04:51,648
На самом деле, изначально решение было
довольно приятным, с приятным интерфейсом.

82
00:04:51,648 --> 00:04:56,585
Там был класс TeamTasks,
у него было приватное поле PersonTasks

83
00:04:56,585 --> 00:05:01,338
с тем самым словарем и метод —
PerformPersonTasks, который принимал имя

84
00:05:01,338 --> 00:05:06,487
человека и количество задач и возвращал
через кортеж два словаря задач

85
00:05:06,487 --> 00:05:11,865
со статистикой — задачи, которые сделаны,
и задачи, которые не затронуты.

86
00:05:11,865 --> 00:05:13,987
Вот такой интерфейс был понятным.

87
00:05:13,987 --> 00:05:17,285
Итак.
Первое свойство хорошей функции — это

88
00:05:17,285 --> 00:05:19,282
понятность ее сигнатуры.

89
00:05:19,282 --> 00:05:21,743
Возможность по ее сигнатуре понять,

90
00:05:21,743 --> 00:05:26,420
что же она делает: какие данные принимает
на вход, какие данные возвращает.

91
00:05:26,420 --> 00:05:31,439
В следующем видео мы подробно рассмотрим
остальные четыре свойства хороших функций.