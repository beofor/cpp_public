<meta charset="utf-8"/>
<co-content>
 <h2 level="2">
  Пример: функция с альтернативным исходом
 </h2>
 <p>
  Рассмотрим пример, похожий на рассмотренный в предыдущем материале про optional. Представьте себе функцию, которая возвращает некоторую величину — например, количество денег, которое необходимо списать за показ рекламного объявления. При этом по разным причинам эти деньги может не получиться списать, и тогда функция возвращает описание причины отказа в виде enum:
 </p>
 <pre language="c_cpp">enum class FailReason {
  ZERO_BALANCE,
  LOW_BID,
  INVALID_CURRENCY,
  // ...
};

/* ??? */ ComputeCost(const Banner&amp; banner) {
  if (banner.balance &lt;= 0) {
    // -&gt; FailReason::ZERO_BALANCE
  } else if (banner.bid &lt; MIN_BID) {
    // -&gt; FailReason::LOW_BID
  } else if (!IsCurrencyValid(banner.currency)) {
    // -&gt; FailReason::INVALID_CURRENCY
  }
  // ...
  
  // -&gt; ConvertBid(min(banner.bid, banner.balance), banner.currency)
}</pre>
 <p>
  Как вернуть из функции либо цену, либо причину отклонения? На ум приходят разные варианты, и все из них примерно так же плохи, как и pair вместо optional в рассказе про optional:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    pair&lt;uint64_t, FailReason&gt; — пара, в которой заполнено либо first, либо second. Недостаток — неочевидность критерия успешности выполнения функции. Должна ли нулевая цена означать, что цена не вычислена и нужно обработать FailReason? Или нулевая цена вполне корректна для специальных бесплатных объявлений? Может быть, нужно ввести специальное «успешное» значение для FailReason? Нет уж, потом будем до скончания веков дописывать в функции вида void LogFailure(FailReason reason) странные проверки на reason == FailReason::OK.
   </p>
  </li>
  <li>
   <p>
    tuple&lt;bool, uint64_t, FailReason&gt; — кортеж, в который мы от безысходности добавили bool, призванный помочь понять, какое из полей — uint64_t или FailReason — содержит истинный результат функции. Но сразу же возникает вопрос: как не ошибиться при трактовке значения bool? Когда там true, надо смотреть на цену или на FailReason?
   </p>
  </li>
 </ul>
 <p>
  Безусловно, проблемы с кортежем решаемы:
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    Заменить bool на enum с двумя значениями: COST_RESULT и FAIL_RESULT.
   </p>
  </li>
  <li>
   <p>
    Заменить кортеж на структуру с понятными названиями полей: has_failed, cost, fail_reason.
   </p>
  </li>
 </ol>
 <p>
  Тем не менее, все эти варианты из-за своей громоздкости проигрывают простому решению: использовать класс, который гарантированно хранит объекты либо одного типа, либо другого.
 </p>
 <h2 level="2">
  variant и возврат его из функции
 </h2>
 <p>
  Такой класс действительно есть в стандартной библиотеке языка, начиная с C++17, — это шаблон
  <a href="https://en.cppreference.com/w/cpp/utility/variant" title="">
   variant&lt;T, U&gt;
  </a>
  из библиотеки &lt;variant&gt;: он гарантированно хранит либо объект типа T, либо объект типа U. (Этот же шаблон позволяет хранить и больше типов, но наиболее распространено применение с двумя типами.)
 </p>
 <p>
  Перепишем функцию с его использованием:
 </p>
 <pre language="c_cpp">variant&lt;uint64_t, FailReason&gt; ComputeCost(const Banner&amp; banner) {
  if (banner.balance &lt;= 0) {
    return FailReason::ZERO_BALANCE;
  } else if (banner.bid &lt; MIN_BID) {
    return FailReason::LOW_BID;
  } else if (!IsCurrencyValid(banner.currency)) {
    return FailReason::INVALID_CURRENCY;
  }
  // ...
  
  return ConvertBid(min(banner.bid, banner.balance), banner.currency);
}</pre>
 <p>
  Отметим, что variant, подобно optional, может быть неявно сконструирован из объекта одного из нужных типов. Поэтому в функции достаточно писать return FailReason::LOW_BID;, не вызывая явно конструктор variant.
 </p>
 <p>
  Разберём сразу типичный сценарий использования результата такой функции: залогировать причину неудачи, в противном случае неким образом ипользовать цену. Здесь помогут две функции:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    <a href="https://en.cppreference.com/w/cpp/utility/variant/holds_alternative" title="">
     holds_alternative&lt;T&gt;(v)
    </a>
    — проверяет, правда ли объект v типа variant сейчас хранит в себе объект типа T.
   </p>
  </li>
  <li>
   <p>
    <a href="https://en.cppreference.com/w/cpp/utility/variant/get" title="">
     get&lt;T&gt;(v)
    </a>
    — возвращает ссылку на хранящийся в v объект типа T и бросает исключение в том случае, если v сейчас хранит объект другого типа.
   </p>
  </li>
 </ul>
 <pre language="c_cpp">optional&lt;uint64_t&gt; cost;

if (const auto cost_or_failure = ComputeCost(banner);
    holds_alternative&lt;FailReason&gt;(cost_or_failure)) {
  LogFailure(banner, get&lt;FailReason&gt;(cost_or_failure));
} else {
  cost = get&lt;uint64_t&gt;(cost_or_failure);
}</pre>
 <p>
  Обратите внимание, как в этом примере помогает optional: обернув в него целочисленную переменную, мы позволили иметь ей явное неинициализированное значение.
 </p>
 <h2 level="2">
  Особенности использования variant
 </h2>
 <p>
  Обратим внимание на несколько важных моментов в использовании variant.
 </p>
 <p>
  Во-первых, variant&lt;T, U&gt; не может не хранить ни T, ни U. Если вам необходим «расширенный optional», то есть variant, который может хранить один из данных типов или ничего, используйте
  <a href="https://en.cppreference.com/w/cpp/utility/variant/monostate" title="">
   monostate
  </a>
  . Он же помогает в случае необходимости хранить в variant типы, не имеющие конструктора по умолчанию.
 </p>
 <p>
  Во-вторых, старайтесь не использовать variant с типами, которые могут неявно приводиться друг к другу. Как, например, будет работать такая функция?
 </p>
 <pre language="c_cpp">variant&lt;int, double&gt; GetSomeNumber() {
  return round(asin(0.5) * 180);
}</pre>
 <p>
  Подвох в том, что round возвращает вещественное число и потому функция вернёт variant с double-альтернативой, хотя подразумевался наверняка int. variant, одновременно хранящий double, int и другие типы, может использоваться, например, для хранения вершины JSON.
 </p>
 <p>
  Однозначно плохой пример — variant&lt;uint32_t, size_t&gt;: на 32-битных системах он и вовсе будет хранить два одинаковых типа, и get&lt;size_t&gt; в этом случае просто не скомпилируется.
 </p>
 <h2 level="2">
  Итоги
 </h2>
 <p>
  Используйте шаблон variant в том случае, если вам необходимо вернуть из функции объект одного из множества типов. Следите за понятностью кода: для пользователя функции должно быть максимально прозрачно, что значит каждая из альтернатив.
 </p>
 <p>
 </p>
 <p>
 </p>
</co-content>
<style>
 body {
    padding: 50px 85px 50px 85px;
}

table th, table td {
    border: 1px solid #e0e0e0;
    padding: 5px 20px;
    text-align: left;
}
input {
    margin: 10px;
}
}
th {
    font-weight: bold;
}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
img {
    height: auto;
    max-width: 100%;
}
pre {
    display: block;
    margin: 20px;
    background: #424242;
    color: #fff;
    font-size: 13px;
    white-space: pre-wrap;
    padding: 9.5px;
    margin: 0 0 10px;
    border: 1px solid #ccc;
}
</style>
<script async="" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$$','$$'], ['$','$'] ],
      displayMath: [ ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
