<meta charset="utf-8"/>
<co-content>
 <p>
  В этом материале мы обсудим функции, которые меняют некоторые свои параметры, принимая их по неконстантной ссылке (или неконстантному указателю). Начнём с примера.
 </p>
 <h2 level="2">
  Пример: функция BookRooms
 </h2>
 <p>
  Пример будет основан на классе HotelManager из задачи «Система бронирования отелей», так что напомним его публичный интерфейс:
 </p>
 <pre language="c_cpp">class HotelManager {
public:
  void Book(int64_t time, const string&amp; hotel_name,
            int client_id, int room_count);
  
  int ComputeClientCount(const string&amp; hotel_name);
  int ComputeRoomCount(const string&amp; hotel_name);
  
private:
  // ...
};</pre>
 <p>
  Представим, что некоторый пользователь этого класса — например, туристическая компания — хочет автоматизировать один из своих наиболее частых сценариев работы с HotelManager — групповое бронирование номеров:
 </p>
 <pre language="c_cpp">for (int client_id = first_client_id;
     client_id &lt; first_client_id + client_count;
     ++client_id) {
  manager.Book(time, hotel_name, client_id, room_count_per_client);
}</pre>
 <p>
  Этот код можно вынести в функцию, но тогда объект HotelManager нужно принять по неконстантной ссылке, чтобы менять его внутри функции:
 </p>
 <pre language="c_cpp">void BookRooms(
    HotelManager&amp; manager, const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id, int room_count_per_client
) {
  for (int client_id = first_client_id;
       client_id &lt; first_client_id + client_count;
       ++client_id) {
    manager.Book(time, hotel_name, client_id, room_count_per_client);
  }
}</pre>
 <p>
  Вызов этой функции будет выглядеть следующим образом:
 </p>
 <pre language="c_cpp">BookRooms(manager, hotel_name, time, 10, 1672945, 2);</pre>
 <p>
  Этот подход порождает несколько проблем и вопросов.
 </p>
 <p>
  Во-первых, в месте вызова функции ничто не предвещает изменения переменной manager: она находится в списке аргументов наравне с остальными. В данном случае ещё можно заподозрить неладное по отсутствию возвращаемого значения, но если функция начнёт что-нибудь возвращать, останется уповать лишь на название функции. Эту проблему мы рассмотрим в первую очередь.
 </p>
 <p>
  Во-вторых, вызов функции теряет в понятности и безопасности из-за трёх чисел в списке аргументов. Рассмотрение этой проблемы мы начнём в видео про громоздкость конструкторов, а закончим — в материале про громоздкие сигнатуры функций.
 </p>
 <p>
  Наконец, позволим высказаться внутреннему параноику. Основные проблемы при использовании интерфейсов (и, в частности, функций) возникают при расхождении декларируемой функциональности (что, как заявлено, функция делает) и технически допустимой (что функция может сделать, исходя из сигнатуры). Разберём этот дисбаланс на примере функции BookRooms:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    <strong>
     Что функция делает (обещает делать)?
    </strong>
    Для объекта manager некоторое количество раз вызывает метод Book.
   </p>
  </li>
  <li>
   <p>
    <strong>
     Что функции позволено делать?
    </strong>
    <em>
     Как угодно
    </em>
    менять объект manager. Например, она может создать новый HotelManager и заменить им существующий: manager = HotelManager();
   </p>
  </li>
 </ul>
 <p>
  Последнюю проблему мы рассмотрим в этом материале. Стоит, однако, заметить, что для небольших функций этот дисбаланс не доставляет больших хлопот, но при этом при попытке его устранить код существенно разбухает. Именно поэтому этот материал вынесен из видео в текст: помните о существовании подобных проблем, возьмите на вооружение способы их решения, но применяйте с умом, чётко представляя себе последствия для компактности кода.
 </p>
 <h2 level="2">
  Как сделать вызов функции более прозрачным
 </h2>
 <h3 level="3">
  Способ 1. Принимать по указателю
 </h3>
 <p>
  Довольно распространённый и часто рекомендуемый способ решить проблему понятности вызова — передавать изменяемые параметры по указателю:
 </p>
 <pre language="c_cpp">void BookRooms(
    HotelManager* manager, const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id, int room_count_per_client
);</pre>
 <p>
  Благодаря оператору получения адреса &amp; изменяемые параметры становятся легко отличимыми при вызове функции:
 </p>
 <pre language="c_cpp">BookRooms(&amp;manager, hotel_name, time, 10, 1672945, 2);</pre>
 <p>
  Недостатки:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    Если аргумент manager принимается по указателю, нужно быть готовым к тому, что этот указатель может оказаться нулевым.
   </p>
  </li>
  <li>
   <p>
    Такая договорённость фактически запрещает принимать параметры по константному указателю, как мы обсуждали ранее: &amp; в месте вызова будет автоматически считываться как признак изменяемого аргумента.
   </p>
  </li>
  <li>
   <p>
    Эта договорённость исключительно стилистическая: «О, давайте заменим ссылку на указатель — тогда при вызове появится амперсанд!» Компилятор никак не соотнесёт факт использования указателя с изменяемостью или неизменяемостью. (С другой стороны, в утилиты проверки стиля вполне можно добавить запрет на неконстантные ссылки в параметрах функций.)
   </p>
  </li>
 </ul>
 <h3 level="3">
  Способ 2. Принимать по значению и возвращать новое состояние
 </h3>
 <p>
  В некоторых проектах используется другой способ решения проблемы изменяемых параметров: передавать их по значению и возвращать новое состояние:
 </p>
 <pre language="c_cpp">HotelManager BookRooms(
    HotelManager manager, const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id, int room_count_per_client
);</pre>
 <p>
  По сути предлагается переформулировать задачу функции из «принять разные входные данные, а ещё manager, который нужно обновить» в «принять разные входные данные, в том числе manager — и вернуть новое состояние manager». Таким образом, функция становится более предсказуемой за счёт уже известной концепции: принять входные данные и вернуть результат.
 </p>
 <p>
  При вызове функции появляется не только явное выделение «изменяемого» параметра, но и выбор: нужно ли его изменять или отдать на растерзание копию.
 </p>
 <pre language="c_cpp">// Изменяем manager, и это очевидно из этой строчки
manager = BookRooms(move(manager), hotel_name, time, 10, 1672945, 2);

// Функция не меняет переданные ей объекты и возвращает новый HotelManager
HotelManager new_manager = BookRooms(manager, hotel_name, time, 10, 1672945, 2);
</pre>
 <p>
  Очевидный недостаток такого подхода — его неэффективность при работе с типами с большим количеством данных на стеке.
 </p>
 <h3 level="3">
  Проблемы
 </h3>
 <p>
  Обозначим проблемы обоих способов:
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    Вседозволенность. Функция BookRooms в любом из рассмотренных вариантов может изменить manager совершенно произвольным образом. Действительно ли это необходимо?
   </p>
  </li>
  <li>
   <p>
    Тестируемость. Раз с объектом manager может произойти всё, что угодно, в юнит-тестах изменённый объект необходимо проверять целиком, убеждаясь, что функция ничего не испортила.
   </p>
  </li>
 </ol>
 <p>
  Как упоминалось выше, часто эти проблемы не являются достаточным обоснованием для серьёзных изменений в коде, но осознавать их наличие всё же необходимо.
 </p>
 <h2 level="2">
  Как избавиться от прямого взаимодействия с HotelManager
 </h2>
 <p>
  Для начала, рассмотрим простой случай — функцию, принимающую по неконстантной ссылке один объект и вызывающую у него один и тот же неконстантный метод. Такую функцию можно переписать так, чтобы её суть была непосредственно отражена в сигнатуре. Для этого выделим отдельный объект «запрос к методу Book» и вернём из функции набор таких объектов:
 </p>
 <pre language="c_cpp">struct BookingQuery {
  int64_t time;
  const string&amp; hotel_name;
  int client_id;
  int room_count;
};

vector&lt;BookingQuery&gt; BookRooms(
    const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id, int room_count_per_client
) {
  vector&lt;BookingQuery&gt; queries;
  queries.reserve(client_count);
  for (int client_id = first_client_id;
       client_id &lt; first_client_id + client_count;
       ++client_id) {
    queries.push_back({time, hotel_name, client_id, room_count_per_client});
  }
  
  return queries;
}</pre>
 <p>
  Чтобы применить набор запросов к объекту HotelManager, нужно:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    Добавить в класс HotelManager метод void BookMany(const vector&lt;BookingQuery&gt;&amp; queries) — если класс в вашей власти.
   </p>
  </li>
  <li>
   <p>
    Написать функцию void BookMany(HotelManager&amp; manager, const vector&lt;BookingQuery&gt;&amp; queries): она всё так же принимает HotelManager по неконстантной ссылке, но максимально проста и вряд ли будет модифицироваться.
   </p>
  </li>
 </ul>
 <p>
  Цена такого рефакторинга — не только раздувание кода, но и накладные расходы на составление и хранение вектора запросов. С другой стороны, тестировать такую функцию одно удовольствие: по сути тесты явно проверят, какие запросы должны быть выполнены.
 </p>
 <p>
  Если функция вызывает у изменяемого объекта не один метод, а несколько, возможны разные ситуации:
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    В одной части функции вызывается один метод, в другой части — другой. В этом случае стоит подумать о разделении функции на несколько.
   </p>
  </li>
  <li>
   <p>
    Функция вызывает несколько методов в произвольном порядке. В этом случае можно прибегнуть к упомянутому выше рефакторингу, выделив несколько классов запросов с общим базовым классом, и возвращать вектор умных указателей на базовые запросы. (И код раздувается ещё сильнее.)
   </p>
  </li>
  <li>
   <p>
    Функция не просто вызывает неконстантные методы, не интересуясь успешностью вызова, а существенно использует их возвращаемое значение и возможно, дополнительно вызывает константные методы. Такой функции нельзя обойтись без настоящего изменяемого объекта. Но, возможно, эта функция должна стать неконстантным методом этого объекта?
   </p>
  </li>
 </ol>
 <h2 level="2">
  Как переписать функцию, чтобы побочные эффекты наступали ровно для одного параметра
 </h2>
 <p>
  Более сложная ситуация — это функция, принимающая по неконстантной ссылке и изменяющая в процессе работы сразу несколько параметров. В этом случае можно рассмотреть варианты объединения этих нескольких объектов в один, а затем перейти к идеям из предыдущего раздела.
 </p>
 <h3 level="3">
  Пример 1: бронирование и логирование
 </h3>
 <p>
  Представим, что функция должна не только забронировать номера, но и сообщить об этом в лог:
 </p>
 <pre language="c_cpp">void BookRooms(
    HotelManager&amp; manager, Logger&amp; logger,
    const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id, int room_count_per_client
) {
  for (int client_id = first_client_id;
       client_id &lt; first_client_id + client_count;
       ++client_id) {
    manager.Book(time, hotel_name, client_id, room_count_per_client);
    logger.LogBooking(time, client_id, room_count_per_client);
  }
}</pre>
 <p>
  Помимо непредсказуемости изменения объекта, переданного в функцию по неконстантной ссылке, в этой функции есть ещё одна проблема. Если предполагается, что manager.Book и logger.LogBooking всегда вызываются вместе, то это должно чем-то гарантироваться — иначе есть риск ошибиться и для одного из Book не вызвать LogBooking (если представить себе более запутанную функцию).
 </p>
 <p>
  Решение — объединить HotelManager и Logger в один прокси-объект:
 </p>
 <pre language="c_cpp">class HotelManagerWithLogger {
public:
  HotelManagerWithLogger(HotelManager&amp; manager, Logger&amp; logger)
      : manager_(manager),
        logger_(logger)
  {}
  
  void Book(int64_t time, const string&amp; hotel_name,
            int client_id, int room_count) {
    manager_.Book(time, hotel_name, client_id, room_count);
    logger_.LogBooking(time, client_id, room_count);
  }
  
private:
  HotelManager&amp; manager_;
  Logger&amp; logger_;
};

void BookRooms(
    HotelManagerWithLogger&amp; manager,
    const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id, int room_count_per_client
) {
  for (int client_id = first_client_id;
       client_id &lt; first_client_id + client_count;
       ++client_id) {
    manager.Book(time, hotel_name, client_id, room_count_per_client);
  }
}</pre>
 <p>
  При вызове этой функции достаточно будет создать этот прокси-объект от переменных HotelManager и Logger:
 </p>
 <pre language="c_cpp">HotelManager manager;
Logger logger;
// ...
HotelManagerWithLogger manager_with_logger(manager, logger);
BookRooms(manager_with_logger, hotel_name, time, 10, 1672945, 2);
</pre>
 <p>
  Обратите внимание, что текущая версия функции BookRooms отличается от исходной лишь типом первого параметра. Это наводит на мысли о шаблонизации по типу HotelManager, что действительно будет иметь смысл, если в разных местах программы необходимо вызывать эту функцию как с логгером, так и без него.
 </p>
 <p>
  Дальнейшее развитие рефакторинга предсказуемо: переделываем функцию на возврат vector&lt;BookingQuery&gt; в точности упомянутым выше способом и добавляем в HotelManagerWithLogger метод BookMany (или шаблонизируем функцию BookMany типом HotelManager, позволяя передать в неё HotelManagerWithLogger).
 </p>
 <h3 level="3">
  Пример 2: бронирование отелей и авиарейсов
 </h3>
 <p>
  Рассмотрим ещё один пример использования прокси-объектов для уменьшения количества изменяемых параметров функции: представим, что вместе с комнатами в отеле необходимо бронировать ещё и билеты на самолёт:
 </p>
 <pre language="c_cpp">void OrganizeTrip(
    HotelManager&amp; hotel_manager, AirlineTicketManager&amp; airline_manager,
    const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id,
    int room_count_per_client, int ticket_count_per_client
) {
  for (int client_id = first_client_id;
       client_id &lt; first_client_id + client_count;
       ++client_id) {
    hotel_manager.Book(time, hotel_name, client_id, room_count_per_client);
    airline_manager.Book(time, client_id, ticket_count_per_client);
  }
}</pre>
 <p>
  Уже известный вариант рефакторинга — завести прокси-объект TripManager, хранящий ссылки на HotelManager и AirlineTicketManager:
 </p>
 <pre language="c_cpp">class TripManager {
public:
  TripManager(HotelManager&amp; hotel_manager,
              AirlineTicketManager&amp; airline_manager)
      : hotel_manager_(hotel_manager),
        airline_manager_(airline_manager)
  {}
  
  void Book(int64_t time, const string&amp; hotel_name,
            int client_id, int room_count, int ticket_count) {
    hotel_manager_.Book(time, hotel_name, client_id, room_count);
    airline_manager_.Book(time, client_id, ticket_count);
  }
  
private:
  HotelManager&amp; hotel_manager_;
  AirlineTicketManager&amp; airline_manager_;
};

void OrganizeTrip(
    TripManager&amp; manager,
    const string&amp; hotel_name, int64_t time,
    int client_count, int first_client_id,
    int room_count_per_client, int ticket_count_per_client
) {
  for (int client_id = first_client_id;
       client_id &lt; first_client_id + client_count;
       ++client_id) {
    manager.Book(time, hotel_name, client_id,
                 room_count_per_client, ticket_count_per_client);
  }
}</pre>
 <p>
  С другой стороны, уже намечаются предпосылки к двукратному раздуванию параметров функции OrganizeTrip и метода TripManager::Book: легко себе представить добавление названия авиакомпании и других параметров перелёта. Поэтому, если логика перебора client_id не слишком уникальна, стоит рассмотреть вариант с разделением функции на две: одна для отелей, вторая для авиабилетов.
 </p>
 <h2 level="2">
  Итоги
 </h2>
 <ol bullettype="numbers">
  <li>
   <p>
    Изменяемые параметры функций тяжело отличить от остальных в месте вызова. Это исправляется передачей по указателю или по значению с возвращением нового состояния изменяемого объекта.
   </p>
  </li>
  <li>
   <p>
    Поддерживайте функции небольшими и компактными. Если функция достаточно велика, изменяемые параметры могут доставлять значительные неудобства. Есть различные способы снизить эти неудобства: разделить функцию на несколько, объединить несколько параметров в один прокси-объект или переписать функцию так, чтобы вместо совершения определённых действий над объектом она возвращала набор описаний этих действий. При этом важно понимать, что эти преобразования раздувают код и поэтому целесообразны только для достаточно больших проектов и функций, которые нельзя легко разбить на несколько. Простым небольшим функциям изменяемые параметры не вредят, но и злоупотреблять ими не стоит.
   </p>
  </li>
 </ol>
 <p>
 </p>
</co-content>
<style>
 body {
    padding: 50px 85px 50px 85px;
}

table th, table td {
    border: 1px solid #e0e0e0;
    padding: 5px 20px;
    text-align: left;
}
input {
    margin: 10px;
}
}
th {
    font-weight: bold;
}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
img {
    height: auto;
    max-width: 100%;
}
pre {
    display: block;
    margin: 20px;
    background: #424242;
    color: #fff;
    font-size: 13px;
    white-space: pre-wrap;
    padding: 9.5px;
    margin: 0 0 10px;
    border: 1px solid #ccc;
}
</style>
<script async="" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$$','$$'], ['$','$'] ],
      displayMath: [ ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
