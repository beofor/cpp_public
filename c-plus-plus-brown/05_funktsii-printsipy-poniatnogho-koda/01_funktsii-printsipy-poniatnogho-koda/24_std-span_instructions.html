<meta charset="utf-8"/>
<co-content>
 <h2 level="2">
  Использование span для работы с C-style интерфейсами
 </h2>
 <p>
  В редких случаях возникает необходимость принять в функции набор объектов, непременно расположенных подряд в памяти (будь то стек или куча). Как правило, это обусловлено необходимостью взаимодействия с кодом на C или другим кодом с аналогичными требованиями.
 </p>
 <p>
  Легко представить себе ситуацию, когда ваш проект постепенно переезжает с C на C++ и в процессе возникают подобные функции:
 </p>
 <pre language="c_cpp">// Legacy comparator
int CompareInts(const void* lhs_ptr, const void* rhs_ptr) {
  const int lhs = *static_cast&lt;const int*&gt;(lhs_ptr);
  const int rhs = *static_cast&lt;const int*&gt;(rhs_ptr);
  if (lhs == rhs) {
    return 0;
  } else if (lhs &lt; rhs) {
    return -1;
  } else {
    return 1;
  }
}
  
// Returns count of needles containing in sorted haystack
template &lt;typename InputIt&gt;
size_t CountNumbers(
    const int* haystacks, size_t count,
    InputIt needles_begin, InputIt needles_end
) {
  return count_if(
      needles_begin, needles_end,
      [haystacks, count](int needle) -&gt; bool {
        // TODO: Rewrite me to C++ (how???)
        return bsearch(&amp;needle, haystacks, count, sizeof(*haystacks), 
                       CompareInts);
      }
  );
}</pre>
 <p>
  Вне зависимости от размера функции, подобной CountNumbers, для читающего её сигнатуру C++-разработчика может быть неочевидным, что параметры haystack и count как-то связаны — из этого следует, что есть пространство для ошибок и неосторожных рефакторингов. Самый безобидный пример может быть таким: не разобравшийся в этой функции разработчик добавил какой-то новый числовой параметр в произвольное место — после haystack; после этого все разработчики, которые читают эту функцию, уже не видят параметры haystack и count рядом и уж точно не сочтут их связанными.
 </p>
 <p>
  Решение этой проблемы —
  <a href="https://en.cppreference.com/w/cpp/container/span">
   шаблонный класс std::span
  </a>
  из библиотеки &lt;span&gt;. По сути это просто класс с двумя полями — указателем и количеством. По состоянию на 1 октября 2018 г. он доступен лишь в компиляторе clang при включении флага --std=c++2a.
 </p>
 <p>
  В нашем примере его можно использовать следующим образом:
 </p>
 <pre language="c_cpp">#include &lt;span&gt;

template &lt;typename InputIt&gt;
size_t CountNumbers(
    span&lt;const int&gt; haystacks,
    InputIt needles_begin, InputIt needles_end
) {
  return count_if(
      needles_begin, needles_end,
      [haystacks](int needle) -&gt; bool {
        return bsearch(&amp;needle, haystacks.data(),
                       haystacks.size(), sizeof(haystacks[0]),
                       CompareInts);
      }
  );
}</pre>
 <p>
 </p>
 <p>
  <em>
   (Впрочем, если есть возможность, функцию CountNumbers нужно переписать, переделав на привычный binary_search из библиотеки &lt;algorithm&gt;. Мы рассматриваем случай, когда нужно быстро сделать сигнатуру функции более понятной, а переписать всё целиком на итераторы возможности нет.)
  </em>
 </p>
 <p>
  Создать объект span можно как с помощью непосредственно указателя и числа или двух указателей, так и напрямую из контейнера array или vector:
 </p>
 <pre language="c_cpp">vector&lt;int&gt; haystacks = ReadHaystacks();
vector&lt;int&gt; needles = ReadNeedles();

cout &lt;&lt; CountNumbers(
    span(haystacks.data(), haystacks.size()),
    begin(needles), end(needles)
) &lt;&lt; endl;  // можно, но громоздко

cout &lt;&lt; CountNumbers(
    haystacks,
    begin(needles), end(needles)
) &lt;&lt; endl;  // так короче
</pre>
 <p>
  Обратите внимание, что span соотносится с vector или array так же, как string_view с string: в то время как vector
  <em>
   владеет
  </em>
  данными, span лишь позволяет сослаться на них.
 </p>
 <p>
  Помимо этого, span&lt;T&gt; позволяет менять элементы:
 </p>
 <pre language="c_cpp">vector&lt;int&gt; numbers = {1, 2, 3, 4};
span&lt;int&gt; segment(numbers.data() + 1, 2);  // {2, 3}
++segment[1];
// теперь вектор содержит {1, 2, 4, 4}
</pre>
 <p>
  В первоначальном примере функция принимала span&lt;const int&gt;, чем гарантировала неприкосновенность исходных данных. Соответственно, span&lt;const char&gt; по внутреннему устройству аналогичен string_view, но второй оснащён удобными методами для работы именно со строками: find, remove_prefix и пр.
 </p>
 <h2 level="2">
  Использование span для работы с сериализованными данными
 </h2>
 <p>
  span может пригодиться и тем, кто не имеет необходимости взаимодействовать с C-style кодом.
 </p>
 <p>
  Разработчикам систем, работающих с большими объёмами данных, часто приходится сталкиваться с сериализованными данными — объектами, некоторым образом упакованными в набор байт. В этом случае удобно иметь возможность сослаться на подотрезок этой памяти:
 </p>
 <pre language="c_cpp">#include &lt;cstddef&gt;
#include &lt;fstream&gt;
#include &lt;iterator&gt;
#include &lt;vector&gt;
#include &lt;span&gt;

using namespace std;

// Объекты, хранящиеся в файле в сериализованном виде
struct Object {
  // ...
};

vector&lt;Object&gt; DeserializeObjects(span&lt;const char&gt; memory);

int main() {
  ifstream bin_file("serialized_objects.bin", ios::binary);
  
  // Считываем всё содержимое файла в vector&lt;char&gt;.
  // char здесь используется исключительно как способ представить 1 байт
  // (можно было бы использовать std::byte из C++17,
  // но он пока не интегрирован с ifstream).
  // Здесь содержимое файла копируется в вектор,
  // но в некоторых ситуациях можно обойтись и без копирований:
  // если файл приехал непосредственно в оперативную память
  // и доступен с помощью вызова mmap, можно работать с ним напрямую
  const vector&lt;char&gt; buffer(
      istreambuf_iterator&lt;char&gt;{bin_file},
      istreambuf_iterator&lt;char&gt;{}
  );
  
  // Предположим, что в первом байте лежит количество объектов
  const int object_count = buffer[0];
  
  // Используем span, чтобы передать в функцию десериализации
  // конкретный блок (регион) памяти
  const vector&lt;Object&gt; objects =
      DeserializeObjects(span(buffer.data() + 1,
                              object_count * sizeof(Object)));
      
  ProcessObjects(objects);
 
  return 0;
}</pre>
 <h2 level="2">
  Итоги
 </h2>
 <p>
  span&lt;T&gt; — это удобный способ сослаться на участок памяти с объектами типа T. Два основных сценария его использования:
 </p>
 <ol bullettype="numbers">
  <li>
   <p>
    Необходимость работать с C-style интерфейсом, требующим расположение объектов подряд в памяти и, соответственно, принимающим набор объектов не в виде двух итераторов, а в виде указателя на начало диапазона и размера диапазона.
   </p>
  </li>
  <li>
   <p>
    Работа с участками памяти, хранящими сериализованные объекты.
   </p>
  </li>
 </ol>
 <p>
  Напомним, что span входит в пока не принятый официально стандарт C++20 и доступен лишь в компиляторе clang при включении флага --std=c++2a.
 </p>
 <p>
 </p>
</co-content>
<style>
 body {
    padding: 50px 85px 50px 85px;
}

table th, table td {
    border: 1px solid #e0e0e0;
    padding: 5px 20px;
    text-align: left;
}
input {
    margin: 10px;
}
}
th {
    font-weight: bold;
}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
img {
    height: auto;
    max-width: 100%;
}
pre {
    display: block;
    margin: 20px;
    background: #424242;
    color: #fff;
    font-size: 13px;
    white-space: pre-wrap;
    padding: 9.5px;
    margin: 0 0 10px;
    border: 1px solid #ccc;
}
</style>
<script async="" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$$','$$'], ['$','$'] ],
      displayMath: [ ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
