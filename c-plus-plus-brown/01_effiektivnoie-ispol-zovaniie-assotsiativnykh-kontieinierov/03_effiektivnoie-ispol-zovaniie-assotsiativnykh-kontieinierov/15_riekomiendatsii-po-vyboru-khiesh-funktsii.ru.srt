1
00:00:06,090 --> 00:00:09,475
Здравствуйте! На прошлом занятии мы узнали,

2
00:00:09,475 --> 00:00:12,280
как правильно писать хеш-функцию для своих типов.

3
00:00:12,280 --> 00:00:16,560
Сейчас мы обсудим некоторые практические рекомендации для их написания.

4
00:00:16,560 --> 00:00:18,790
Давайте вспомним, что мы работали со структурой

5
00:00:18,790 --> 00:00:22,995
для автомобильных номеров, и какую хеш-функцию мы для неё написали.

6
00:00:22,995 --> 00:00:25,012
Посмотрим наш код.

7
00:00:25,012 --> 00:00:31,965
Вот наша структура для автомобильного номера. Такую хеш-функцию мы написали,

8
00:00:31,965 --> 00:00:38,260
и благодаря этому мы могли в unordered_set поместить наши автомобильные номера.

9
00:00:38,260 --> 00:00:42,070
Давайте напишем ещё какую-нибудь структуру собственную.

10
00:00:42,070 --> 00:00:48,320
Поместим в неё поля и попробуем написать хешер для неё.

11
00:00:49,740 --> 00:00:57,570
Что там будет? Ну давайте для начала поместим туда число типа double,

12
00:00:57,570 --> 00:01:02,590
и у нас сразу встает вопрос: как же мы будем писать для хеш-функцию для double?

13
00:01:02,590 --> 00:01:05,285
Ведь значение хеш-функции - это целые числа,

14
00:01:05,285 --> 00:01:09,040
а double - это дробное число и не очень понятно,

15
00:01:09,040 --> 00:01:11,410
как его однозначно перевести.

16
00:01:11,410 --> 00:01:13,300
Но ещё больше было бы непонятно,

17
00:01:13,300 --> 00:01:17,930
если бы у нас было вообще не число, а какая-нибудь строка.

18
00:01:17,940 --> 00:01:23,225
Как нам переводить строку хеш-функцией в число?

19
00:01:23,225 --> 00:01:26,500
Неужели снова писать такую же сложную хеш-функцию,

20
00:01:26,500 --> 00:01:28,600
как для автомобильных номеров?

21
00:01:28,600 --> 00:01:30,520
И это было бы удивительно,

22
00:01:30,520 --> 00:01:33,740
потому что тогда мы бы не смогли положить в unordered_set

23
00:01:33,740 --> 00:01:37,650
по умолчанию ни double, ни string.

24
00:01:37,650 --> 00:01:42,220
Но давайте убедимся, что мы на самом деле можем это делать спокойно,

25
00:01:42,220 --> 00:01:48,275
без написания собственных хеш-функций.

26
00:01:48,275 --> 00:01:55,290
Вот мы создаем unordered_set из переменных типа double и код компилируется прекрасно.

27
00:01:55,290 --> 00:01:57,775
И если мы заменим на string,

28
00:01:57,775 --> 00:02:00,415
он тоже прекрасно компилируется.

29
00:02:00,415 --> 00:02:03,905
От нас не требуют написать свою собственную хеш-функцию для этих типов.

30
00:02:03,905 --> 00:02:08,065
Это значит, что для этих типов стандартные хеш-функции уже написаны,

31
00:02:08,065 --> 00:02:12,895
и мы можем их использовать из стандартной библиотеки.

32
00:02:12,895 --> 00:02:18,365
Они так и называются: это структуры hash, параметризованные каким-нибудь типом.

33
00:02:18,365 --> 00:02:19,600
Давайте объявим эти хешеры,

34
00:02:19,600 --> 00:02:25,540
но только не в нашей структуре, конечно,

35
00:02:25,540 --> 00:02:29,415
нам надо написать хешер для нашей структуры.

36
00:02:29,415 --> 00:02:35,145
Давайте скопируем отсюда заголовок и поработаем над ним.

37
00:02:35,145 --> 00:02:38,140
Значит это будет не PlateHasher, а MyHasher.

38
00:02:38,140 --> 00:02:48,420
Мы хотим использовать стандартные хешеры из библиотеки.

39
00:02:48,420 --> 00:02:58,123
Пусть это у нас будет dhash и заведем хешер для строк,

40
00:02:58,123 --> 00:03:07,455
и тогда при вычислении нашей собственной хеш-функций мы можем использовать их для того,

41
00:03:07,455 --> 00:03:12,605
чтобы посчитать компоненты от полей.

42
00:03:12,605 --> 00:03:14,240
Это делается довольно просто,

43
00:03:14,240 --> 00:03:24,905
вот таким вот образом.

44
00:03:24,905 --> 00:03:32,175
Так мы могли бы посчитать значение хеш-функции,

45
00:03:32,175 --> 00:03:35,970
но теперь давайте усложним нашу задачу. Что было бы,

46
00:03:35,970 --> 00:03:40,860
если бы у нас в полях лежал наш тот самый собственный автомобильный номер,

47
00:03:40,860 --> 00:03:43,170
который мы только что записали,

48
00:03:43,170 --> 00:03:45,461
как нам использовать хешер для него?

49
00:03:45,461 --> 00:03:47,460
Ну это небольшая проблема.

50
00:03:47,460 --> 00:03:49,760
Мы же уже тоже написали для него хешер,

51
00:03:49,760 --> 00:03:59,825
объявим его использование прямо здесь и, точно так же,

52
00:03:59,825 --> 00:04:04,830
для его компонента мы

53
00:04:04,830 --> 00:04:16,535
посчитаем хеш для автомобильного номера. Так, здорово.

54
00:04:16,535 --> 00:04:21,810
Итак, мы посчитали хеши для всех компонентов нашей структуры,

55
00:04:21,810 --> 00:04:25,750
но теперь надо их как-то объединить в одно общее значение.

56
00:04:25,750 --> 00:04:27,330
Можно использовать такой же подход,

57
00:04:27,330 --> 00:04:29,220
как мы делали в автомобильных номерах,

58
00:04:29,220 --> 00:04:32,965
либо можно воспользоваться универсальным советом: это

59
00:04:32,965 --> 00:04:37,770
взять какой-нибудь многочлен от нескольких переменных,

60
00:04:37,770 --> 00:04:42,540
вернее с несколькими коэффициентами и посчитать его значение.

61
00:04:42,540 --> 00:04:46,932
Здесь мы должны выбрать значение для переменной X,

62
00:04:46,932 --> 00:04:52,180
а A, B и C мы уже имеем как компоненты хеш-функции от наших полей.

63
00:04:52,180 --> 00:04:57,590
В литературе принято брать в качестве значения для X какое-нибудь простое число,

64
00:04:57,590 --> 00:04:59,880
ну и мы возьмем какое-то простое число,

65
00:04:59,880 --> 00:05:02,235
пусть это будет 37.

66
00:05:02,235 --> 00:05:08,530
И тогда мы можем вернуть это самое значение.

67
00:05:08,840 --> 00:05:14,920
Посчитаем, R1 * X^2 +

68
00:05:14,920 --> 00:05:21,655
+ R2 * X + R3 .

69
00:05:21,655 --> 00:05:24,840
Вот такую вот простую хеш-функцию мы смогли написать,

70
00:05:24,840 --> 00:05:27,515
и теперь конечно же её надо где-то использовать,

71
00:05:27,515 --> 00:05:29,490
а именно в том месте,

72
00:05:29,490 --> 00:05:32,480
где мы попытаемся свой тип складывать в unordered_set.

73
00:05:32,480 --> 00:05:37,920
Не забудем дописать MyHasher,

74
00:05:38,410 --> 00:05:43,708
проверим, что это всё у нас компилируется.

75
00:05:43,708 --> 00:05:46,210
Да, действительно, у нас всё скомпилировалось,

76
00:05:46,210 --> 00:05:49,285
мы смогли всё правильно сделать.

77
00:05:49,285 --> 00:05:54,480
Итак, что мы с вами сегодня сделали?

78
00:05:54,480 --> 00:05:57,300
Мы с вами сегодня закончили наш разговор о том,

79
00:05:57,300 --> 00:06:00,150
как писать хеш-функции для собственных типов.

80
00:06:00,150 --> 00:06:04,410
В качестве распространённых подходов можно отметить следующие: во-первых,

81
00:06:04,410 --> 00:06:06,855
если ваш класс содержит поля стандартных типов,

82
00:06:06,855 --> 00:06:11,490
то для хеширования этих полей можно использовать стандартные хешеры из библиотеки.

83
00:06:11,490 --> 00:06:15,910
Во-вторых, для некоторых типов могут быть уже написаны хешеры - например, вами.

84
00:06:15,910 --> 00:06:18,311
Для комбинации нескольких хешей

85
00:06:18,311 --> 00:06:23,880
часто используют их как коэффициенты при вычислении некого многочлена.

86
00:06:23,880 --> 00:06:27,360
И главное, помните самое важное - что после применения всех рассмотренных

87
00:06:27,360 --> 00:06:32,635
рекомендаций ваша хеш-функция должна давать равномерное распределение по корзинкам.

88
00:06:32,635 --> 00:06:35,940
О том, как выполнять проверку хорошего распределения хеш-функций,

89
00:06:35,940 --> 00:06:39,200
вы увидите в задаче после этого видео.