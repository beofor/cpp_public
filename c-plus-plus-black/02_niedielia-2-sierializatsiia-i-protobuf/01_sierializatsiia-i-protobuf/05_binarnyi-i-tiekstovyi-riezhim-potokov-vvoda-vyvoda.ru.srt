1
00:00:00,000 --> 00:00:05,500
В предыдущем

2
00:00:05,500 --> 00:00:11,325
видео мы с вами изучили,
как работать со стандартными

3
00:00:11,325 --> 00:00:16,064
потоками ввода-вывода при выполнении
бинарной сериализации и десериализации.

4
00:00:16,064 --> 00:00:19,666
Однако у них есть важная особенность,
которую надо изучить,

5
00:00:19,666 --> 00:00:21,978
чтобы у вас не возникало проблем.

6
00:00:21,978 --> 00:00:26,962
Особенность эта заключается в том,
что программа ваша может достаточно

7
00:00:26,962 --> 00:00:31,330
долго корректно работать,
но в какой-то момент взорваться.

8
00:00:31,330 --> 00:00:35,439
И чтобы этого не случилось,
давайте разберемся, о чем речь.

9
00:00:35,439 --> 00:00:40,290
А дело в том,
что при работе как с файловыми потоками,

10
00:00:40,290 --> 00:00:44,530
так и с потоками ввода-вывода из строки,

11
00:00:44,530 --> 00:00:49,525
их нужно открывать в
специальном бинарном режиме,

12
00:00:49,525 --> 00:00:53,510
если мы хотим выполнять именно бинарную
сериализацию и десериализацию.

13
00:00:53,510 --> 00:00:58,646
Вот у меня на экране представлены
конструкторы файловых

14
00:00:58,646 --> 00:01:04,216
потоков и потоков ostringstream и
istringstream, в которые передается

15
00:01:04,216 --> 00:01:09,218
специальный дополнительный
параметр std::ios_base::binary,

16
00:01:09,218 --> 00:01:15,165
и этот параметр, собственно, открывает
эти потоки в специальном бинарном режиме.

17
00:01:15,165 --> 00:01:17,205
И давайте же мы разберемся,

18
00:01:17,205 --> 00:01:23,162
чем бинарный режим потоков
ввода-вывода отличается от текстового.

19
00:01:23,162 --> 00:01:29,137
Когда мы сериализуем данные,
в частности, сохраняем их на диск,

20
00:01:29,137 --> 00:01:34,080
то весьма вероятна ситуация,
когда мы сохраняем, например,

21
00:01:34,080 --> 00:01:39,613
какой-то документ на флешку на компьютере,
который работает под управлением

22
00:01:39,613 --> 00:01:44,986
операционной системы Windows,
а потом читаем этот документ под Linux.

23
00:01:44,986 --> 00:01:51,793
Такая ситуация вполне себе возможна, и вот
мы сейчас эту ситуацию с вами сэмулируем.

24
00:01:51,793 --> 00:01:59,070
У меня есть две программы, вот они у
меня в текстовом редакторе открыты,

25
00:01:59,070 --> 00:02:05,187
это файл writer.cpp и файл reader.cpp,

26
00:02:05,187 --> 00:02:12,341
writer.cpp устроен очень просто, он
объявляет целочисленную переменную value,

27
00:02:12,341 --> 00:02:16,630
которая равна 305 400 406,

28
00:02:16,630 --> 00:02:22,476
и бинарно сохраняет эту
переменную в файл t.bin,

29
00:02:22,476 --> 00:02:27,879
который у нас открыт в
файловом потоке ofstream,

30
00:02:27,879 --> 00:02:33,260
и есть файл reader.cpp,
который открывает этот самый файл

31
00:02:33,260 --> 00:02:38,244
t.bin и читает из него
целое число типа uint32_t.

32
00:02:38,244 --> 00:02:42,240
Ну и, собственно, обе эти программы,

33
00:02:42,240 --> 00:02:47,168
writer.cpp выводит в консоль то значение,
которое он записал,

34
00:02:47,168 --> 00:02:52,175
а reader.cpp выводит то значение,
которое он считал.

35
00:02:52,175 --> 00:02:58,564
Оба эти файла у меня лежат в одной папке,
и дальше я делаю вот что.

36
00:02:58,564 --> 00:03:02,559
Я открываю самую обычную консоль Windows.

37
00:03:02,559 --> 00:03:06,978
Давайте посмотрим,
что у меня здесь есть в папке,

38
00:03:06,978 --> 00:03:10,626
у меня есть файл reader.cpp и writer.cpp.

39
00:03:10,626 --> 00:03:17,590
Точно так же рядом я открываю консоль WLS,

40
00:03:17,590 --> 00:03:22,801
Windows Linux Subsystem
— это маленький Linux,

41
00:03:22,801 --> 00:03:26,390
который появился внутри Windows 10.

42
00:03:26,390 --> 00:03:30,913
И у меня здесь открыта та же самая папка,
вы можете видеть,

43
00:03:30,913 --> 00:03:35,158
что в ней есть файлы
writer.cpp и reader.cpp.

44
00:03:35,158 --> 00:03:37,610
Дальше мы делаем вот что.

45
00:03:37,610 --> 00:03:42,968
В консоли Windows,
то есть в Windows-машине

46
00:03:42,968 --> 00:03:47,500
мы компилируем writer.cpp,

47
00:03:47,500 --> 00:03:51,362
witer.cpp и

48
00:03:51,362 --> 00:03:56,761
назовем наш файл,
исполняемый файл, writer.

49
00:03:56,761 --> 00:04:02,559
Более того, когда он скомпилируется,
мы его сразу же запустим.

50
00:04:02,559 --> 00:04:08,079
Вот мы writer.cpp скомпилировали,
запустили,

51
00:04:08,079 --> 00:04:14,100
и он вывел 305 400 406, ровно то число,
которое у нас есть в программе.

52
00:04:14,100 --> 00:04:18,590
Дальше мы идем в Linux и

53
00:04:18,590 --> 00:04:23,394
делаем здесь то же самое, мы компилируем,

54
00:04:23,394 --> 00:04:29,130
теперь только не writer.cpp,
а reader.cpp, -std=c++1z,

55
00:04:29,130 --> 00:04:34,131
./reader.cpp -oreader, и точно так же,

56
00:04:34,131 --> 00:04:38,310
после того как скомпилируется,
мы программу нашу запускаем.

57
00:04:38,310 --> 00:04:40,034
Компилируем, запускаем.

58
00:04:40,034 --> 00:04:46,162
У нас все скомпилировалось, программа
отработала, но она вывела другое число.

59
00:04:46,162 --> 00:04:52,654
То есть мы видим, что мы записали
в файл 305 миллионов и так далее,

60
00:04:52,654 --> 00:04:58,330
а считали мы 873 074 006.

61
00:04:58,330 --> 00:05:01,451
То есть что-то у нас идет не так.

62
00:05:01,451 --> 00:05:05,939
Хорошо, идем в наши файлы writer.cpp и

63
00:05:05,939 --> 00:05:11,927
reader.cpp и открываем наши файловые

64
00:05:11,927 --> 00:05:16,600
потоки в бинарном режиме.

65
00:05:16,600 --> 00:05:22,741
Добавляем параметр binary в
оба файла и повторяем наши

66
00:05:22,741 --> 00:05:28,126
манипуляции с компиляцией и запуском
программы под Windows и Linux.

67
00:05:28,126 --> 00:05:31,380
Компилируем и запускаем writer, все,

68
00:05:31,380 --> 00:05:35,890
он отлично отработал,
снова вывел 305 400 406.

69
00:05:35,890 --> 00:05:38,563
и делаем то же самое для reader.

70
00:05:38,563 --> 00:05:42,122
Оп!
Супер, наша программа скомпилировалась,

71
00:05:42,122 --> 00:05:45,790
отработала и вывела то число,
которое мы ожидаем.

72
00:05:45,790 --> 00:05:50,315
То есть добавление бинарного режима в

73
00:05:50,315 --> 00:05:54,680
файловые потоки ofstream и
ifstream починило нашу программу,

74
00:05:54,680 --> 00:06:00,255
так что бинарный режим потоков
действительно играет какую-то роль.

75
00:06:00,255 --> 00:06:04,968
И давайте мы с вами разберемся,
что же происходит.

76
00:06:04,968 --> 00:06:09,422
Для этого мы сделаем вот что.

77
00:06:09,422 --> 00:06:14,536
Мы перейдем в наши файлы writer

78
00:06:14,536 --> 00:06:20,160
и reader и отключим на
время бинарный режим,

79
00:06:20,160 --> 00:06:24,570
а число, то,

80
00:06:24,570 --> 00:06:28,980
которое мы записали, мы будем
выводить в шестнадцатеричном виде.

81
00:06:28,980 --> 00:06:33,376
Ну и то же самое у нас
будет происходить в reader,

82
00:06:33,376 --> 00:06:39,126
тоже убираем бинарный режим
и точно так же считанное

83
00:06:39,126 --> 00:06:43,616
число выводим в шестнадцатеричном виде.

84
00:06:43,616 --> 00:06:49,346
Так, открываем консоль,
компилируем, запускаем,

85
00:06:49,346 --> 00:06:54,920
и вот мы получили шестнадцатеричное
представление нашего числа,

86
00:06:54,920 --> 00:06:58,210
и теперь давайте сделаем
то же самое в Linux.

87
00:06:58,210 --> 00:07:03,793
Ага, мы видим, что, как у нас и было,

88
00:07:03,793 --> 00:07:08,530
наша программа выводит не то число,
которое мы туда записали.

89
00:07:08,530 --> 00:07:13,209
Давайте смотреть,
собственно, что происходит.

90
00:07:13,209 --> 00:07:18,501
Видите, у нас последний
байт числа — 56 и там,

91
00:07:18,501 --> 00:07:25,730
и там, дальше у нас идет байт 0a, а здесь
откуда-то появился странный байт 0d.

92
00:07:25,730 --> 00:07:31,497
И уже после него идет 0a,
34, и вот здесь тоже 0a, 34.

93
00:07:31,497 --> 00:07:33,602
Очень странное дело.

94
00:07:33,602 --> 00:07:38,686
На самом деле объяснение этой странности

95
00:07:38,686 --> 00:07:46,220
есть на cppreference в
статье про ввод-вывод.

96
00:07:46,220 --> 00:07:50,996
Здесь есть раздел,
который объясняет разницу между бинарным и

97
00:07:50,996 --> 00:07:54,010
текстовым режимами файловых потоков.

98
00:07:54,010 --> 00:07:58,570
Разница эта заключается вот в чем.

99
00:07:58,570 --> 00:08:03,466
Как, я думаю, многие из вас знают,
в операционной системе Windows для

100
00:08:03,466 --> 00:08:09,855
обозначения перевода строки
используется пара символов \r\n,

101
00:08:09,855 --> 00:08:16,580
или же в байтах это 0D0A, или,
в десятичной системе, 1310.

102
00:08:16,580 --> 00:08:20,723
При этом когда мы с вами
программируем на C++,

103
00:08:20,723 --> 00:08:24,549
неважно, под какую
платформу мы это делаем,

104
00:08:24,549 --> 00:08:30,140
мы всегда в коде для перевода строки
используем только один символ \n.

105
00:08:30,140 --> 00:08:35,215
Так вот, файловые потоки,
даже не файловые потоки,

106
00:08:35,215 --> 00:08:38,495
а потоки ввода-вывода в
стандартной библиотеке,

107
00:08:38,495 --> 00:08:44,273
открытые в текстовом режиме под
операционной системой Windows,

108
00:08:44,273 --> 00:08:48,260
встречая вывод перевода строки \n,

109
00:08:48,260 --> 00:08:54,980
автоматически добавляют в него,
добавляют к нему еще один символ \r.

110
00:08:54,980 --> 00:08:57,643
Теперь переходим в Linux.

111
00:08:57,643 --> 00:09:05,650
В linux для перевода строки используется
только один символ \n, или 0A, 10.

112
00:09:05,650 --> 00:09:11,510
Соответственно, в Linux нет разницы
между текстовым и бинарным режимами,

113
00:09:11,510 --> 00:09:16,650
потому что в текстовом режиме
нет необходимости отслеживать,

114
00:09:16,650 --> 00:09:20,370
чтобы рядом с символом \n был символ \r.

115
00:09:20,370 --> 00:09:25,420
Что же происходило в нашей
эмуляции Linux и Windows?

116
00:09:25,420 --> 00:09:31,750
Мы под Windows сохраняли число,

117
00:09:31,750 --> 00:09:38,777
внутри которого содержится байт 0a,
или \n, или символ перевода строки.

118
00:09:38,777 --> 00:09:43,840
И так как в файле writer.cpp

119
00:09:43,840 --> 00:09:49,754
файловый поток открыт в текстовом режиме,
то рядом с этим байтом

120
00:09:49,754 --> 00:09:55,358
0a дописывался байт 0d, 13.

121
00:09:55,358 --> 00:10:00,047
В Linux же считывались те байты,
которые к нам пришли.

122
00:10:00,047 --> 00:10:05,837
И здесь, на самом деле, мы можем еще
обратить внимание на интересный факт.

123
00:10:05,837 --> 00:10:10,502
Давайте мы посмотрим содержимое папки,
и мы видим,

124
00:10:10,502 --> 00:10:13,560
что файл t.bin имеет размер пять байт.

125
00:10:13,560 --> 00:10:20,650
Пять, хотя мы сохраняем в него число
типа uint32_t, то есть четыре байта.

126
00:10:20,650 --> 00:10:25,725
Вот как раз этот пятый дополнительный
байт возникает оттого, что мы в текстовом

127
00:10:25,725 --> 00:10:31,323
режиме под операционной системой
Windows добавляем байт 0d.

128
00:10:31,323 --> 00:10:36,791
Когда же мы передаем в конструктор потока

129
00:10:36,791 --> 00:10:41,630
ofstream параметр binary,
мы отключаем эту магию,

130
00:10:41,630 --> 00:10:47,705
связанную с тем,
чтобы добавлять дополнительный байт.

131
00:10:47,705 --> 00:10:52,271
И теперь есть очень интересный момент,
который состоит в том,

132
00:10:52,271 --> 00:10:57,215
что если мы просто посмотрим на
программу в файле writer.cpp,

133
00:10:57,215 --> 00:11:00,245
то по ней вообще ни разу не очевидно,

134
00:11:00,245 --> 00:11:04,330
что она имеет какое-то
отношение к переводам строки.

135
00:11:04,330 --> 00:11:09,270
Таким образом,
давайте с вами подведем итоги.

136
00:11:09,270 --> 00:11:14,210
Если вы выполняете бинарную
сериализацию или десериализацию данных

137
00:11:14,210 --> 00:11:19,159
с помощью потоков ввода-вывода
из стандартной библиотеки C++,

138
00:11:19,159 --> 00:11:24,460
то надо обязательно открывать их
в режиме std::ios_base::binary,

139
00:11:24,460 --> 00:11:30,280
потому что, как мы сейчас с вами видели,
ваша программа может, на первый взгляд,

140
00:11:30,280 --> 00:11:35,372
работать правильно, но в какой-то момент,
когда в нее попадут бинарные данные,

141
00:11:35,372 --> 00:11:40,920
содержащие байт \n, байт 10,

142
00:11:40,920 --> 00:11:44,994
у вас могут возникнуть
очень странные эффекты.

143
00:11:44,994 --> 00:11:50,468
Поэтому не забывайте открывать
свои потоки в бинарном режиме.