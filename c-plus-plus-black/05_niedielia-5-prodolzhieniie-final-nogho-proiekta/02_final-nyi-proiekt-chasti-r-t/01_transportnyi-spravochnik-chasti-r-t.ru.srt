1
00:00:05,630 --> 00:00:11,940
Итак, мы добрались до последнего блока финальной задачи коричневого и черного поясов.

2
00:00:11,940 --> 00:00:15,540
В частях с "R" по "T" вам понадобится добавить

3
00:00:15,540 --> 00:00:20,120
поддержку справочника организаций наряду с транспортным справочником.

4
00:00:20,120 --> 00:00:23,250
В части "R" вам понадобится просто

5
00:00:23,250 --> 00:00:26,580
добавить возможность поиска организации по некоторым условиям,

6
00:00:26,580 --> 00:00:29,455
а именно: вам будут даны организации,

7
00:00:29,455 --> 00:00:31,260
которые есть, скажем, в том же городе,

8
00:00:31,260 --> 00:00:33,600
в котором ходят эти самые автобусы.

9
00:00:33,600 --> 00:00:36,535
Организация задается протобуфным сообщением "Company",

10
00:00:36,535 --> 00:00:38,325
в этом сообщении есть разные поля,

11
00:00:38,325 --> 00:00:40,350
но из них пока будут использоваться только четыре,

12
00:00:40,350 --> 00:00:44,830
а именно: это названия организации, это телефоны организации,

13
00:00:44,830 --> 00:00:48,370
это URL-адреса организации и рубрики,

14
00:00:48,370 --> 00:00:51,260
ну, то есть условно типы какие-то этой организации.

15
00:00:51,260 --> 00:00:55,825
Как вы видите, "Name", "Phone" и "Url" это тоже некоторые протобуфные сообщения.

16
00:00:55,825 --> 00:01:01,620
Это, соответственно, название содержит в себе строковое "value",

17
00:01:01,620 --> 00:01:03,900
строковое значение и какой-то тип этого названия,

18
00:01:03,900 --> 00:01:05,950
который нам сейчас на самом деле не пригодится.

19
00:01:05,950 --> 00:01:09,068
Это главное название, это синоним или это короткое название — сейчас неважно,

20
00:01:09,068 --> 00:01:11,095
главное только строковое значение.

21
00:01:11,095 --> 00:01:13,310
Это "message Name".

22
00:01:13,310 --> 00:01:16,389
А "message Phone" описывает номер телефона,

23
00:01:16,389 --> 00:01:19,630
причем для вашего удобства он будет уже [inaudible] ,

24
00:01:19,630 --> 00:01:21,300
то есть он будет разложен на компоненты.

25
00:01:21,300 --> 00:01:23,420
Ну, вот вы видите, какие там есть компоненты.

26
00:01:23,420 --> 00:01:25,380
Опять же, что вам нужно будет использовать,

27
00:01:25,380 --> 00:01:28,111
— это тип этого номера телефона,

28
00:01:28,111 --> 00:01:29,880
это именно телефон или это факс,

29
00:01:29,880 --> 00:01:31,841
это код страны, код города,

30
00:01:31,841 --> 00:01:33,885
собственно сам номер телефона,

31
00:01:33,885 --> 00:01:37,265
который в России семизначный и добавочная часть.

32
00:01:37,265 --> 00:01:39,495
И это все будут строки.

33
00:01:39,495 --> 00:01:41,340
Это как раз то, что вам понадобится использовать.

34
00:01:41,340 --> 00:01:44,510
И наконец, "Url" — это просто строковое значение.

35
00:01:44,510 --> 00:01:45,970
И вот вам будут даны организации,

36
00:01:45,970 --> 00:01:49,880
и нужно будет искать среди этих организаций по каким-то условиям,

37
00:01:49,880 --> 00:01:53,150
по условиям на эти самые четыре свойства.

38
00:01:53,150 --> 00:01:54,320
Ну, а рубрики, соответственно,

39
00:01:54,320 --> 00:01:56,850
это будут некоторые числовые значения и отдельно будет

40
00:01:56,850 --> 00:02:01,050
дан маппинг преобразование этих чисел в какие-то нормальные названия,

41
00:02:01,050 --> 00:02:04,840
условно, парки, кафе, санатории и так далее.

42
00:02:04,840 --> 00:02:06,365
Ну и, давайте, рассмотрим небольшой пример.

43
00:02:06,365 --> 00:02:08,295
Вот, допустим, у нас есть две организации,

44
00:02:08,295 --> 00:02:10,605
два парка, два сочинских парка.

45
00:02:10,605 --> 00:02:11,690
Первый парк "Дендрарий".

46
00:02:11,690 --> 00:02:15,560
У него есть два каких-то номера телефона и какой-то интернет-адрес.

47
00:02:15,560 --> 00:02:17,160
И есть парк "имени Фрунзе",

48
00:02:17,160 --> 00:02:18,375
у которого нет номера телефона,

49
00:02:18,375 --> 00:02:21,090
нет никакого интернет-адреса, просто приходите и гуляйте.

50
00:02:21,090 --> 00:02:23,610
И нужно как-то уметь искать по этим организациям.

51
00:02:23,610 --> 00:02:28,095
Первый пример запроса — название организации "имени Фрунзе".

52
00:02:28,095 --> 00:02:29,650
Конечно, только одна организация,

53
00:02:29,650 --> 00:02:32,535
вторая — имеет название "имени Фрунзе", соответственно,

54
00:02:32,535 --> 00:02:36,369
нужно сказать, что вот, только вторая организация подходит под этот запрос.

55
00:02:36,369 --> 00:02:41,080
Второй пример запроса: меня интересуют парки и санатории, вообще, во всем Сочи.

56
00:02:41,080 --> 00:02:43,535
И поскольку у меня две организации, обе парки,

57
00:02:43,535 --> 00:02:45,310
они обе подходят, вот соответственно,

58
00:02:45,310 --> 00:02:49,060
запрос ответ, на который это две организации.

59
00:02:49,060 --> 00:02:52,270
Другой пример, чуть более сложный: бывают условия на несколько свойств сразу,

60
00:02:52,270 --> 00:02:54,715
в данном случае меня интересуют организации,

61
00:02:54,715 --> 00:03:00,571
у которых указанные номера телефонов 267-18-42 и 267-18-43,

62
00:03:00,571 --> 00:03:03,055
один из них хотя бы должен быть у данной организации,

63
00:03:03,055 --> 00:03:06,894
и обязательно интернет-адрес dendrarium.ru.

64
00:03:06,894 --> 00:03:09,930
Вот такой запрос в последней колонке означает,

65
00:03:09,930 --> 00:03:11,519
что меня интересуют организации,

66
00:03:11,519 --> 00:03:16,589
у которых один из номеров телефона подходит под один из указанных,

67
00:03:16,589 --> 00:03:19,080
из тех двух указанных 18-42,

68
00:03:19,080 --> 00:03:22,905
18-43 и интернет-адрес такой, как я сказал.

69
00:03:22,905 --> 00:03:24,975
С интернет-адресом все понятно,

70
00:03:24,975 --> 00:03:26,410
просто строково сравниваем и все,

71
00:03:26,410 --> 00:03:28,265
с номерами телефонов придется немножко помучиться,

72
00:03:28,265 --> 00:03:29,888
потому что, когда я говорю,

73
00:03:29,888 --> 00:03:33,515
что меня интересуют номера телефонов 267-18-42 и 18-43,

74
00:03:33,515 --> 00:03:36,315
я говорю, что меня как будто бы не важен код страны и код города,

75
00:03:36,315 --> 00:03:38,160
ну, и это тоже нужно будет учесть.

76
00:03:38,160 --> 00:03:40,680
Но, в данном случае, действительно у "Дендрария" второй телефон

77
00:03:40,680 --> 00:03:43,500
подходит под заданные условия и URL-адрес тоже подходит,

78
00:03:43,500 --> 00:03:48,585
и поэтому "Дендрарий" можно вывести как результат ответа на этот запрос.

79
00:03:48,585 --> 00:03:52,970
Ну и, в общем-то, с частью "R" получается все,

80
00:03:52,970 --> 00:03:56,710
вам просто нужно реализовать запрос поиска по организациям.

81
00:03:56,710 --> 00:03:59,700
В части "S" мы наконец полноценно

82
00:03:59,700 --> 00:04:03,170
совместим транспортный справочник и справочник организаций,

83
00:04:03,170 --> 00:04:06,635
и вам понадобится строить маршрут до организации.

84
00:04:06,635 --> 00:04:09,450
При этом условия на организации будут задаваться в том же формате,

85
00:04:09,450 --> 00:04:10,498
что и в части "R",

86
00:04:10,498 --> 00:04:13,120
в виде условий на свойства организаций.

87
00:04:13,120 --> 00:04:14,775
Ну, вот о чем собственно речь?

88
00:04:14,775 --> 00:04:16,530
Вот у нас есть автобусы,

89
00:04:16,530 --> 00:04:19,630
плюс у нас есть организации,

90
00:04:19,630 --> 00:04:24,170
вот в данном случае парк "имени Фрунзе" и парк "Дендрарий".

91
00:04:24,170 --> 00:04:26,150
Теперь для организаций мы будем знать,

92
00:04:26,150 --> 00:04:28,430
какие ближайшие автобусные остановки,

93
00:04:28,430 --> 00:04:33,715
от которых можно до них дойти и расстояние пешком от этих остановок до этих организаций.

94
00:04:33,715 --> 00:04:35,195
Ну вот, например, до парка

95
00:04:35,195 --> 00:04:39,060
"Дендрарий" имеет смысл идти только от остановки "Цирк" и это 180 метров,

96
00:04:39,060 --> 00:04:42,845
а до парка "имени Фрунзе" можно идти от трех остановок: от "Театральной",

97
00:04:42,845 --> 00:04:44,260
от "Пансионата Светлана" и от "Цирка".

98
00:04:44,260 --> 00:04:46,390
Ну, и есть, соответственно, какие-то расстояния.

99
00:04:46,390 --> 00:04:48,679
И получается, наш граф увеличивается,

100
00:04:48,679 --> 00:04:53,435
становится более интересным и по нему можно строить маршрут.

101
00:04:53,435 --> 00:04:56,260
Сам запрос, маршрут до организации,

102
00:04:56,260 --> 00:04:58,870
"RouteToCompany", имеет простую структуру,

103
00:04:58,870 --> 00:05:00,625
на вход подается остановка,

104
00:05:00,625 --> 00:05:04,030
от которой я хочу доехать и фильтр на организации,

105
00:05:04,030 --> 00:05:05,155
до которых я хочу доехать.

106
00:05:05,155 --> 00:05:08,530
Например, я хочу доехать от "Мацесты" до какого-нибудь парка.

107
00:05:08,530 --> 00:05:10,600
И вот такую задачу пользователя нашей

108
00:05:10,600 --> 00:05:13,165
большой красивой системы мы должны научиться решать.

109
00:05:13,165 --> 00:05:15,220
На выходе просто маршрут,

110
00:05:15,220 --> 00:05:16,540
примерно в том же формате,

111
00:05:16,540 --> 00:05:20,015
что и раньше, но добавятся некоторые дополнительные истории.

112
00:05:20,015 --> 00:05:21,700
Как отвечать на этот запрос?

113
00:05:21,700 --> 00:05:23,980
Для начала нужно будет с помощью того кода,

114
00:05:23,980 --> 00:05:27,500
который вы напишете в части "R", найти организации,

115
00:05:27,500 --> 00:05:29,126
которые подходят под данный запрос,

116
00:05:29,126 --> 00:05:31,405
затем выбрать ближайшую из этих организаций,

117
00:05:31,405 --> 00:05:34,720
построить маршрут и нарисовать этот маршрут,

118
00:05:34,720 --> 00:05:38,770
от остановки до организации.

119
00:05:38,770 --> 00:05:41,660
Что делать с самими организациями и как их рисовать?

120
00:05:41,660 --> 00:05:43,840
Вот та карта, которая была до этого, вам ее рисовать не придется,

121
00:05:43,840 --> 00:05:46,790
вам нигде не придется рисовать сразу все организации на карте.

122
00:05:46,790 --> 00:05:51,970
Вам понадобится рисовать только маршрут от остановки до организации.

123
00:05:51,970 --> 00:05:54,885
Момент первый: вам нужно будет точно так же,

124
00:05:54,885 --> 00:05:58,450
как вы для остановок выбирали координаты на схеме,

125
00:05:58,450 --> 00:06:01,825
точно так же для организации нужно будет выбрать координаты на схеме.

126
00:06:01,825 --> 00:06:03,880
Это нужно будет сделать в самом начале для всех

127
00:06:03,880 --> 00:06:07,600
организаций и поэтому итоговая схема будет не совсем такая,

128
00:06:07,600 --> 00:06:09,175
какой она была в части "N",

129
00:06:09,175 --> 00:06:13,570
например, потому что для организации тоже выбираются координаты,

130
00:06:13,570 --> 00:06:15,430
точно теми же сжатиями координат,

131
00:06:15,430 --> 00:06:17,885
с кликами и всем прочим.

132
00:06:17,885 --> 00:06:20,635
Соответственно, схема получается вот такой с учетом организаций,

133
00:06:20,635 --> 00:06:22,934
она же будет подложкой для схемы маршрута.

134
00:06:22,934 --> 00:06:25,735
И здесь видно, например, что от "Театральной" до "Пансионата Светлана",

135
00:06:25,735 --> 00:06:29,020
между двумя крайне левыми остановками расстояние больше,

136
00:06:29,020 --> 00:06:30,700
чем между другими, больше, чем оно было раньше.

137
00:06:30,700 --> 00:06:32,620
Почему? Потому что, на самом деле,

138
00:06:32,620 --> 00:06:33,880
там еще есть парк "имени Фрунзе",

139
00:06:33,880 --> 00:06:35,495
для которого мы тем же алгоритмом,

140
00:06:35,495 --> 00:06:37,835
что и раньше навычисляли координату.

141
00:06:37,835 --> 00:06:40,475
И парк "Дендрарий" где-то вот там расположен, у него такой же "X",

142
00:06:40,475 --> 00:06:41,880
как у остановки стадиона, такой же "Y",

143
00:06:41,880 --> 00:06:44,120
как у "Пансионата Светлана".

144
00:06:44,120 --> 00:06:46,915
Но, опять же, это мы все повычисляли, мы учли,

145
00:06:46,915 --> 00:06:50,050
что парк "имени Фрунзе" соседний с тремя остановками,

146
00:06:50,050 --> 00:06:52,005
что парк "Дендрарий" соседний с одной остановкой,

147
00:06:52,005 --> 00:06:53,320
это мы учли при склейках всевозможных,

148
00:06:53,320 --> 00:06:55,120
которые вы уже написали.

149
00:06:55,120 --> 00:06:57,940
Но сами организации по умолчанию рисоваться не будут,

150
00:06:57,940 --> 00:07:00,195
изначально будет вот такая схема.

151
00:07:00,195 --> 00:07:02,170
И собственно, как будет выглядеть маршрут?

152
00:07:02,170 --> 00:07:05,455
Вы рисуете подложку с учетом всего расположения остановок,

153
00:07:05,455 --> 00:07:06,580
но по алгоритму из части "М" еще.

154
00:07:06,580 --> 00:07:11,495
Дальше вы хотите нарисовать маршрут.

155
00:07:11,495 --> 00:07:14,980
Как это происходит? Вы рисуете

156
00:07:14,980 --> 00:07:18,340
большой полупрозрачный белый прямоугольник и на нем рисуете маршрут,

157
00:07:18,340 --> 00:07:21,080
сначала вы рисуете, собственно, в зависимости от того,

158
00:07:21,080 --> 00:07:22,950
каким задан порядок слоев,

159
00:07:22,950 --> 00:07:25,029
вы рисуете линии автобусов,

160
00:07:25,029 --> 00:07:26,590
через которые проходит маршрут,

161
00:07:26,590 --> 00:07:31,730
вы рисуете черную линию от конечной остановки маршрута до организации,

162
00:07:31,730 --> 00:07:33,865
в данном случае до "Дендрария" маршрут получился,

163
00:07:33,865 --> 00:07:37,360
вы рисуете названия автобусов на конечных,

164
00:07:37,360 --> 00:07:40,103
через которые вы пересаживались,

165
00:07:40,103 --> 00:07:41,290
вы рисуете промежуточные остановки,

166
00:07:41,290 --> 00:07:42,475
эти самые белые круги,

167
00:07:42,475 --> 00:07:46,150
вы рисуете черный круг для организации,

168
00:07:46,150 --> 00:07:47,780
до которой вы доезжаете,

169
00:07:47,780 --> 00:07:50,630
рисуете названия остановок и названия организаций.

170
00:07:50,630 --> 00:07:56,010
И вот именно эта картинка будет выхлопом запроса "RouteToCompany" в части "S".

171
00:07:56,240 --> 00:08:00,930
Наконец в части "Т" в виде кульминации вам

172
00:08:00,930 --> 00:08:04,950
понадобится учесть время работы этих самых организаций,

173
00:08:04,950 --> 00:08:07,080
то есть не имеет смысла приезжать в парк "Дендрарий" ночью,

174
00:08:07,080 --> 00:08:09,505
потому что он закрыт и вот это нужно учесть.

175
00:08:09,505 --> 00:08:11,470
Соответственно, для всех организаций, в данном случае,

176
00:08:11,470 --> 00:08:15,345
для парков, добавится время работы.

177
00:08:15,345 --> 00:08:18,335
Вот "Дендрарий" работает ежедневно с 9:00 до 17:00 зимой,

178
00:08:18,335 --> 00:08:21,785
а парк "имени Фрунзе" работает ежедневно и круглосуточно.

179
00:08:21,785 --> 00:08:25,160
Бывают еще разные способы создать времена работы.

180
00:08:25,160 --> 00:08:26,440
Это вы все подробно прочтете в условии,

181
00:08:26,440 --> 00:08:28,200
ну, например, можно сказать,

182
00:08:28,200 --> 00:08:34,090
что время работы ежедневно с 8:00 до 15:45 и с 17:00 до 23:00,

183
00:08:34,090 --> 00:08:35,302
то есть бывает перерыв на обед,

184
00:08:35,302 --> 00:08:37,424
это тоже допустимо, это тоже описывается.

185
00:08:37,424 --> 00:08:39,960
Бывает, что организация работает ночью,

186
00:08:39,960 --> 00:08:43,080
с десяти вечера до пяти утра.

187
00:08:43,080 --> 00:08:46,908
Но это задается, как промежуток с 0:00 до 5:00 и с 22:00 до 24:00.

188
00:08:46,908 --> 00:08:50,250
Тоже нормально. Можно задавать ограничения на дни недели, например,

189
00:08:50,250 --> 00:08:54,325
работать по вторникам и средам с 10:00 до 17:00, такое тоже можно.

190
00:08:54,325 --> 00:08:57,765
И вот, последний, четвертый пример справа, что можно работать,

191
00:08:57,765 --> 00:08:59,970
иметь разный режим работы в будни и в выходные, например,

192
00:08:59,970 --> 00:09:01,050
в будни с 10:00 до 20:00,

193
00:09:01,050 --> 00:09:02,870
а выходные с 11:00 до 16:00.

194
00:09:02,870 --> 00:09:07,520
Это все будет задаваться и это все вам нужно уметь обрабатывать.

195
00:09:07,520 --> 00:09:11,435
И давайте рассмотрим пару небольших примеров.

196
00:09:11,435 --> 00:09:14,420
Пусть вам нужно построить маршрут от остановки "Мацеста",

197
00:09:14,420 --> 00:09:15,675
вот там справа внизу,

198
00:09:15,675 --> 00:09:19,710
до любого парка и начать в 8:30.

199
00:09:19,710 --> 00:09:24,705
Понятно, что в данном случае все две организации подходят под условие "Парк".

200
00:09:24,705 --> 00:09:28,170
Нам нужно найти ближайшую из них с условием на то,

201
00:09:28,170 --> 00:09:31,430
что не все работает всегда.

202
00:09:31,430 --> 00:09:36,135
В данном случае наиболее оптимальный маршрут — это маршрут до парка "Дендрарий",

203
00:09:36,135 --> 00:09:38,228
он займет, по сути,

204
00:09:38,228 --> 00:09:40,020
25 минут 49 секунд.

205
00:09:40,020 --> 00:09:42,840
Вы приедете туда за четыре минуты 11 секунд до

206
00:09:42,840 --> 00:09:46,935
открытия и вам ничего не стоит дождаться открытия и пойти гулять в "Дендрарий".

207
00:09:46,935 --> 00:09:51,505
Суммарный маршрут займет 30 минут с учетом ожидания открытия организации.

208
00:09:51,505 --> 00:09:54,877
А, если вы поедете от той же остановки, от "Мацесты",

209
00:09:54,877 --> 00:09:57,905
но в восемь утра, тогда вам придется слишком долго ждать

210
00:09:57,905 --> 00:10:01,580
открытия "Дендрария" и проще доехать до парка "имени Фрунзе" и погулять там.

211
00:10:01,580 --> 00:10:04,955
Вы приедете туда в 8:33:05 секунд,

212
00:10:04,955 --> 00:10:08,565
ждать открытия не нужно и такой маршрут займет 33 минуты и пять секунд.

213
00:10:08,565 --> 00:10:12,485
Ну и, соответственно, в части "Т" вам нужно будет учесть время работы организации,

214
00:10:12,485 --> 00:10:14,435
построить маршрут с учетом этого.

215
00:10:14,435 --> 00:10:19,860
Итоговая структура маршрута получается следующей: вы ждете автобуса,

216
00:10:19,860 --> 00:10:21,080
это то самое ожидание автобуса,

217
00:10:21,080 --> 00:10:22,530
которое дается в конфиге,

218
00:10:22,530 --> 00:10:24,470
вы едете на автобусе, с учетом того,

219
00:10:24,470 --> 00:10:27,315
сколько остановок ехать, с учетом расстояния между остановками.

220
00:10:27,315 --> 00:10:31,105
Вы, возможно, пересаживаетесь, возможно снова ждете автобуса, снова едете.

221
00:10:31,105 --> 00:10:32,810
Когда вы доехали до конечной,

222
00:10:32,810 --> 00:10:34,565
до конца вашего маршрута,

223
00:10:34,565 --> 00:10:36,490
вы идете пешком до организации.

224
00:10:36,490 --> 00:10:39,170
Это тоже занимает какое-то время с учетом

225
00:10:39,170 --> 00:10:42,290
скорости пешехода и расстояния от остановки до организации.

226
00:10:42,290 --> 00:10:45,580
И, наконец, возможно вы ждете открытия этой организации.

227
00:10:45,580 --> 00:10:48,410
Вот такой структуры маршрут вам и нужно будет вывести в части "Т".

228
00:10:48,410 --> 00:10:51,950
И на этом финальная задача закончится.