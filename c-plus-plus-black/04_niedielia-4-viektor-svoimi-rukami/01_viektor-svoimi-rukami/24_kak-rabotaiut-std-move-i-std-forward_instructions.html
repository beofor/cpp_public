<meta charset="utf-8"/>
<co-content>
 <p>
  В прошлых лекциях вы узнали о том, что такое forwarding reference, и научились использовать std::move и std::forward для написания эффективного кода на современном C++. В этом материале мы подробнее расскажем о том, как они устроены. Для этого мы сначала обсудим два очень технических момента в устройстве C++: категории значений и схлопывание ссылок.
 </p>
 <p>
 </p>
 <p>
  <strong>
   Категории значений
  </strong>
 </p>
 <p>
  У каждого значения в C++, будь то x, xs[i], 1, 2 + Foo(3) или что-то другое, есть тип, мы подробно обсуждали типы в этой специализации. Однако помимо типа у каждого значения есть категория, определяющая его "эфемерность", и эта категория не зависит непосредственно от его типа (равно как и наоборот).

Стандарт C++ определяет три категории значений. Их точное определение можно найти на
  <a href="https://en.cppreference.com/w/cpp/language/value_category" title="cppreference.com">
   cppreference.com
  </a>
  , мы условно определим их так:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    lvalue — значения, адрес которых можно взять, и которые могут быть слева от оператора присваивания (если имеют неконстантный тип), например имена переменных, результаты выполнения функций и операторов (если они имеют тип lvalue-ссылки) и т. п.;
   </p>
  </li>
  <li>
   <p>
    prvalue — непосредственные результаты вычислений или литералы, например 1, a &gt; b, SomeStruct{}, результат вызова функции (если он имеет не ссылочный тип) и т. п.;
   </p>
  </li>
  <li>
   <p>
    xvalue — "временные, но материальные" значения, например SomeStruct{}.some_field, результаты выполнения функций (если они имеют тип rvalue-ссылки) и т. п.
   </p>
  </li>
 </ul>
 <p>
  Объединение последних двух категорий также называют просто rvalue. Для нас в основном важно только различие между lvalue и rvalue.

Как мы увидели в предыдущих лекциях, категория значения определяет, к какому виду ссылки может привязываться значение при передачи параметров: lvalue будет привязываться к lvalue-ссылке, rvalue может привязаться либо к rvalue-ссылке, либо к lvalue-ссылке на константу. Для примера попробуйте закомментировать различные перегрузки в следующей программе и посмотрите, что произойдет:
 </p>
 <pre language="c_cpp">#include &lt;iostream&gt;

void CheckRef(const int&amp;) {
    std::cout &lt;&lt; "const int&amp;" &lt;&lt; std::endl;
}

void CheckRef(int&amp;) {
    std::cout &lt;&lt; "int&amp;" &lt;&lt; std::endl;
}

void CheckRef(int&amp;&amp;) {
    std::cout &lt;&lt; "int&amp;&amp;" &lt;&lt; std::endl;
}

int main() {
    CheckRef(1);
    return 0;
}</pre>
 <p>
  То, что константная lvalue-ссылка и rvalue-ссылка продляют время жизни временного объекта, обсусловленно именно тем, что они должны в том числе ссылаться на prvalue-значения, которые в обычных условиях живут лишь до конца выражения, в котором встречаются. Указатели и обычные lvalue-ссылки таким свойством не обладают, и они не могут ссылаться на rvalue.
 </p>
 <p>
 </p>
 <p>
  <strong>
   Устройство std::move
  </strong>
 </p>
 <p>
  Из лекций мы узнали, что знание категории значения помогает нам более эффективно его обрабатывать, например использоваь перемещающие конструкторы вместо копирующих в нашей реализации вектора. Однако категория значения теряется при передаче его в качестве аргумента: формальный параметр функции всегда имеет категорию lvalue, и понять, какую категорию имел аргумент, можно только по его типу. Это выглядит контринтуитивно, но на самом деле это логично: rvalue-ссылка — это ссылка
  <em>
   на
  </em>
  rvalue, сама же она как значение имеет независимую категорию. Чтобы при дальнейшей передаче параметра тип ссылки сохранился, нужно воспользоваться функцией std::move:
 </p>
 <pre language="c_cpp">void Foo(int&amp;&amp; x) {
    std::cout &lt;&lt; x &lt;&lt; std::endl;
}

void Bar(int&amp;&amp; x) {
    foo(std::move(x));
}</pre>
 <p>
  Функция std::move в этом фрагменте возвращает int&amp;&amp;, и весь смысл ее использования по сути сводится к тому, чтобы взять lvalue и превратить его в xvalue.

Наверное, прочитав все написанное выше, вы думаете, что функцию move можно было бы определить так:
 </p>
 <pre language="c_cpp">template &lt;typename T&gt;
T&amp;&amp; move(T&amp; value) {
    return static_cast&lt;T&amp;&amp;&gt;(value);
}</pre>
 <p>
  И это будет работать, но только с lvalue-аргументами. По стандарту std::move должен также принимать и rvalue-аргументы, и в прототипе из
  <a href="https://en.cppreference.com/w/cpp/utility/move" title="документации">
   документации
  </a>
  используется forwarding reference T&amp;&amp;:
 </p>
 <pre language="c_cpp">template &lt;typename T&gt;
constexpr typename std::remove_reference&lt;T&gt;::type&amp;&amp; move(T&amp;&amp; t) noexcept;</pre>
 <p>
  Для таких ссылок результат static_cast&lt;T&amp;&amp;&gt; не всегда будет rvalue-ссылкой (мы к этому еще вернемся), поэтому настоящая реализация std::move выглядит вот так:
 </p>
 <pre language="c_cpp">// взято из libc++ (родной стандартной библиотеки clang++)
template &lt;class _Tp&gt;
inline _LIBCPP_INLINE_VISIBILITY _LIBCPP_CONSTEXPR
typename remove_reference&lt;_Tp&gt;::type&amp;&amp;
move(_Tp&amp;&amp; __t) _NOEXCEPT
{
    typedef typename remove_reference&lt;_Tp&gt;::type _Up;
    return static_cast&lt;_Up&amp;&amp;&gt;(__t);
}</pre>
 <p>
  Разница (с точностью до кучи нужных для совместимости макросов) — в том, что мы явно удаляем из типа _Tp ссылки с помощью std::remove_reference перед тем, как приклеить к нему &amp;&amp;. Чтобы понять, зачем это нужно, в следующем разделе мы разберем, чем на самом деле являются forwarding reference.
 </p>
 <p>
 </p>
 <p>
  <strong>
   Схлопывание ссылок
  </strong>
 </p>
 <p>
  В отличие от указателей, не бывает такой вещи, как ссылка на ссылку. Например, если мы определим using IntRef = int&amp;;, то типы IntRef и IntRef&amp; будут на самом деле одним и тем же типом. Это называется схлопыванием ссылок (reference collapsing), и эта вариация правила достаточно очевидна. Столь же очевидно, что схлопываться должны и rvalue-ссылки: если определить using IntRef = int&amp;&amp;, то типы IntRef и IntRef&amp;&amp; будут одинаковыми.

В случае же схлопывания смешанных ссылок действует менее очевидное правило: &amp; + &amp;&amp; = &amp;&amp; + &amp; = &amp;. Благодаря нему работают forwarding references, о которых мы говорили в лекциях. Такие ссылки появляются в двух местах с более-менее одинаковыми правилами дедукции типов:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    параметры шаблонной функции: template &lt;typename T&gt; Foo(T&amp;&amp; x), причем тип T должен быть непосредственным шаблонным параметром функции Foo, и его нельзя использовать в других параметрах функции (например в Foo(T&amp;&amp; x, T&amp;&amp; y) тип T&amp;&amp; будет уже не forwarding reference);
   </p>
  </li>
  <li>
   <p>
    определения переменных вида auto&amp;&amp; x = Foo().
   </p>
  </li>
 </ul>
 <p>
  Пусть определена функция template &lt;typename T&gt; foo(T&amp;&amp; x). Посмотрим, как будет работать дедукция типа для rvalue и lvalue:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    foo(1): T&amp;&amp; = int&amp;&amp;, следовательно T = int (тогда T&amp;&amp; = int&amp;&amp;);
   </p>
  </li>
  <li>
   <p>
    foo(x): T&amp;&amp; = int&amp;, следовательно T = int&amp; (тогда T&amp;&amp; = (int&amp;)&amp;&amp; = int&amp;).
   </p>
  </li>
 </ul>
 <p>
  Важно отметить, что если бы тип T уже был фиксирован в момент вызова foo, дедукции типа бы не произошло, и T&amp;&amp; не мог бы быть либо lvalue-ссылкой, либо rvalue-ссылкой в зависимости от категории значения агумента. По этой же причине если у нас есть два параметра типа T&amp;&amp;, и в качестве аргументов передаются значения с разными категориями, то код просто не скомпилируется:
 </p>
 <pre language="c_cpp">template &lt;typename T&gt;
void Foo(T&amp;&amp; x, T&amp;&amp; y) {
    (void)x;
    (void)y;
}

int main() {
    int i = 0;
    Foo(i, 0);
    return 0;
}</pre>
 <pre language="sh">$ clang++ -std=c++17 test.cpp -o test
test.cpp:9:5: error: no matching function for call to 'Foo'
    Foo(i, 0);
    ^~~
test.cpp:2:6: note: candidate template ignored: deduced conflicting types for parameter 'T'
      ('int &amp;' vs. 'int')
void Foo(T&amp;&amp; x, T&amp;&amp; y) {
     ^
1 error generated.</pre>
 <p>
  И эта же дедукция объясняет, почему внутри std::move нельзя просто сказать static_cast&lt;T&amp;&amp;&gt;: в случае, если T определится дедукцией как lvalue-ссылка, T&amp;&amp; тоже будет lvalue-ссылкой!

Таким образом, forwarding reference — это не специальная разновидность ссылки в языке, а очень специфичный трюк, позволяющий параметру шаблонной функции привязываться к значениям любой категории по ссылке с помощью дедукции точного типа ссылки.
 </p>
 <p>
 </p>
 <p>
  <strong>
   Устройство std::forward
  </strong>
 </p>
 <p>
  Что же будет, если мы напишем функцию:
 </p>
 <pre language="c_cpp">template &lt;typename T, typename U&gt;
T&amp;&amp; MisteryOp(U&amp;&amp; x) {
    return static_cast&lt;T&amp;&amp;&gt;(x);
}</pre>
 <p>
  Она окажется очень похожей на функцию std::forward:
 </p>
 <pre language="c_cpp">#include &lt;iostream&gt;

template &lt;typename T, typename U&gt;
T&amp;&amp; MisteryOp(U&amp;&amp; x) {
    return static_cast&lt;T&amp;&amp;&gt;(x);
}

void CheckRef(const int&amp;) {
    std::cout &lt;&lt; "const int&amp;" &lt;&lt; std::endl;
}

void CheckRef(int&amp;) {
    std::cout &lt;&lt; "int&amp;" &lt;&lt; std::endl;
}

void CheckRef(int&amp;&amp;) {
    std::cout &lt;&lt; "int&amp;&amp;" &lt;&lt; std::endl;
}

template &lt;typename T&gt;
void Foo(T&amp;&amp; x) {
    CheckRef(MisteryOp&lt;T&gt;(x));
}

int main() {
    int i = 0;
    Foo(i);
    Foo(1);
    return 0;
}</pre>
 <p>
  Действительно, в зависимости от того, каким типом оказался T в специализации void Foo(T&amp;&amp; x), MisteryOp&lt;T&gt; будет возвращать либо int&amp;&amp; (xvalue), либо int&amp; (lvalue), таким образом восстанавливая категорию изначального значения с точностью до rvalue/lvalue.

Если мы посмотрим на настоящее определение std::forward, то увидим, что оно очень похоже на нашу функцию:
 </p>
 <pre language="c_cpp">// взято из libc++ (родной стандартной библиотеки clang++)
template &lt;class _Tp&gt;
inline _LIBCPP_INLINE_VISIBILITY _LIBCPP_CONSTEXPR
_Tp&amp;&amp;
forward(typename remove_reference&lt;_Tp&gt;::type&amp; __t) _NOEXCEPT
{
    return static_cast&lt;_Tp&amp;&amp;&gt;(__t);
}

template &lt;class _Tp&gt;
inline _LIBCPP_INLINE_VISIBILITY _LIBCPP_CONSTEXPR
_Tp&amp;&amp;
forward(typename remove_reference&lt;_Tp&gt;::type&amp;&amp; __t) _NOEXCEPT
{
    static_assert(!is_lvalue_reference&lt;_Tp&gt;::value,
                  "can not forward an rvalue as an lvalue");
    return static_cast&lt;_Tp&amp;&amp;&gt;(__t);
}</pre>
 <p>
  Разница лишь в том, что вместо второго шаблонного параметра здесь используется перегрузка, а также в наличии ассерта, предохраняющего от неправильного использования вроде std::forward&lt;int&amp;&gt;(123):
 </p>
 <pre language="c_cpp">#include &lt;iostream&gt;

template &lt;typename T, typename U&gt;
T&amp;&amp; UnsafeForward(U&amp;&amp; x) {
    return static_cast&lt;T&amp;&amp;&gt;(x);
}

int main() {
    int&amp; y = UnsafeForward&lt;int&amp;&gt;(123);  // UB: висящая ссылка
    std::cout &lt;&lt; y &lt;&lt; std::endl;
    return 0;
}</pre>
 <p>
  clang++ не заметит ошибку и выдаст 123, а вот g++ при определенных настройках выдаст 0 с очень загадочным предупреждением:
 </p>
 <pre language="sh">$ docker run --rm -it ubuntu:19.04
root@ed5b69253d43:/# apt-get update -yqq &amp;&amp; apt-get install -yqq --no-install-recommends --no-install-suggests g++ nano &gt;/dev/null 2&gt;&amp;1
root@ed5b69253d43:/# nano test.cpp
root@ed5b69253d43:/# g++ --version
g++ (Ubuntu 8.3.0-6ubuntu1) 8.3.0
Copyright (C) 2018 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
root@ed5b69253d43:/# g++ -std=c++17 -Wall -O3 test.cpp -o test
test.cpp: In function 'int main()':
test.cpp:10:18: warning: '&lt;anonymous&gt;' is used uninitialized in this function [-Wuninitialized]
    std::cout &lt;&lt; y &lt;&lt; std::endl;
                 ^
root@ed5b69253d43:/# ./test
0</pre>
 <p>
 </p>
 <p>
  <strong>
   Заключение
  </strong>
 </p>
 <p>
  В этой статье мы подробно разобрали, что такое на самом деле forwarding reference, а также изучили предназначение и устройство стандартных функций std::move и std::forward.
 </p>
</co-content>
<style>
 body {
    padding: 50px 85px 50px 85px;
}

table th, table td {
    border: 1px solid #e0e0e0;
    padding: 5px 20px;
    text-align: left;
}
input {
    margin: 10px;
}
}
th {
    font-weight: bold;
}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
img {
    height: auto;
    max-width: 100%;
}
pre {
    display: block;
    margin: 20px;
    background: #424242;
    color: #fff;
    font-size: 13px;
    white-space: pre-wrap;
    padding: 9.5px;
    margin: 0 0 10px;
    border: 1px solid #ccc;
}
</style>
<script async="" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$$','$$'], ['$','$'] ],
      displayMath: [ ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
