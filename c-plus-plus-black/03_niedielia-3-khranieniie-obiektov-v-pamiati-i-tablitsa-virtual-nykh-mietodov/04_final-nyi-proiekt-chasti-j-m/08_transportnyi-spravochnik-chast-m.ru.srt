1
00:00:00,000 --> 00:00:06,856
В части

2
00:00:06,856 --> 00:00:11,358
М в заключении этого блока мы
еще лучше модифицируем нашу

3
00:00:11,358 --> 00:00:15,309
склейку координат и покажем это на
примере другого фрагмента из схемы

4
00:00:15,309 --> 00:00:19,233
замечательного города
Сочи ─ 36 и 23 автобуса.

5
00:00:19,233 --> 00:00:22,630
Вот у нас есть изначальная схема,

6
00:00:22,630 --> 00:00:26,709
нарисованная в соответствии с частями с G
по I, и она соответствует географическому

7
00:00:26,709 --> 00:00:31,444
расположению всех остановок,
и она выглядит довольно тоскливо.

8
00:00:31,444 --> 00:00:36,736
Если эту схему нарисовать с помощью
кода из части L, то мы получим,

9
00:00:36,736 --> 00:00:41,890
конечно, более аккуратную историю ─
остановки размещены красиво и равномерно,

10
00:00:41,890 --> 00:00:46,115
но склейка такова,
что все остановки зеленого автобуса,

11
00:00:46,115 --> 00:00:50,340
почти все, расположены левее
остановок синего автобуса.

12
00:00:50,340 --> 00:00:55,020
И с этим хочется что-то сделать: для
этого нужно модифицировать склейку.

13
00:00:55,020 --> 00:00:59,208
Давайте вернемся к исходной схеме,
расположим ее все-таки более равномерно

14
00:00:59,208 --> 00:01:02,010
по экрану, чтобы вам было понятнее,
что происходит.

15
00:01:02,010 --> 00:01:06,666
Затем нам все-таки все
еще нужна интерполяция

16
00:01:06,666 --> 00:01:09,313
между пересадочными и
конечными остановками ─ вот,

17
00:01:09,313 --> 00:01:12,490
мы ее сделали ─ стало красивее,
а теперь нужно посклеивать координаты.

18
00:01:12,490 --> 00:01:18,729
Итак, мы берем самый левый
X ─ он получает номер один.

19
00:01:18,729 --> 00:01:24,882
Берем следующий X ─ остановка «Санаторная
улица» ─ какой X она должна получить?

20
00:01:24,882 --> 00:01:29,404
Мы смотрим на всех ее соседей
по маршрутам и на их номера.

21
00:01:29,404 --> 00:01:35,360
У этой остановки один сосед ─ остановка
«Санаторий «Салют», она имеет номер один,

22
00:01:35,360 --> 00:01:39,040
поэтому наша остановка получает
номер на единицу больше ─ номер два.

23
00:01:39,040 --> 00:01:43,558
Ну, и так далее: для остановок три,
четыре, пять и шесть до «Парка «Ривьера»,

24
00:01:43,558 --> 00:01:45,780
они получают номера с первого по шестой.

25
00:01:45,780 --> 00:01:51,243
Следующий X ─ со остановки «Целинная
улица», ни один ее сосед пока не

26
00:01:51,243 --> 00:01:56,400
имеет номера, мы пока его не рассмотрели,
и поэтому она получает номер один.

27
00:01:56,400 --> 00:02:00,830
Следующая остановка ─ «Целинная улица,
дом 57»,

28
00:02:00,830 --> 00:02:06,132
ее сосед ─ это наша предыдущая остановка
«Целинная улица», она имеет номер один,

29
00:02:06,132 --> 00:02:08,620
поэтому наша остановка получает
номер на единицу больше.

30
00:02:08,620 --> 00:02:13,250
Там мы доходим до остановки «Сбербанк»
и получаем номера с первого по восьмой.

31
00:02:13,250 --> 00:02:18,963
И самое интересное на остановке «Морской
вокзал»: мы смотрим на ее соседей,

32
00:02:18,963 --> 00:02:23,257
они имеют номера шесть и восемь ─ это
остановки «Парк «Ривьера» и «Сбербанк» мы

33
00:02:23,257 --> 00:02:27,706
берем максимальный из этих номеров и нашей
остановке присваиваем номер на единицу

34
00:02:27,706 --> 00:02:30,890
больше, не остановке,
а X на самом деле, это ─ девятка.

35
00:02:30,890 --> 00:02:34,440
Соответственно, X остановки «Морской
вокзал» получил номер девять.

36
00:02:34,440 --> 00:02:39,102
Ну, и наконец, остановка «Театральная»
имеет одного соседа, это «Морской вокзал»

37
00:02:39,102 --> 00:02:43,922
с X с номером девять, и поэтому получает
номер десять у своей X-координаты.

38
00:02:43,922 --> 00:02:47,880
И теперь мы склеиваем
остановки с координатами один,

39
00:02:47,880 --> 00:02:52,200
остановки с координатами два и получаем
вот такую красивую равномерную схему.

40
00:02:52,200 --> 00:02:53,468
С иксами ─ все.

41
00:02:53,468 --> 00:02:58,920
То же самое мы делаем с игреками,
получается чуть-чуть аккуратнее.

42
00:02:58,920 --> 00:03:05,242
Ну, и все, у нас готова красивая схема
маршрутов, и часть темы закончена.