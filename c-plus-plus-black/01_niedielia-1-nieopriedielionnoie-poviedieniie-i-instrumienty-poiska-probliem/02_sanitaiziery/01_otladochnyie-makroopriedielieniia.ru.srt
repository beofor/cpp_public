1
00:00:00,000 --> 00:00:07,168
[БЕЗ_ЗВУКА] Всем привет.

2
00:00:07,168 --> 00:00:10,858
Я Антон Полднев,
и мы начинаем «Черный пояс по C++».

3
00:00:10,858 --> 00:00:14,313
Вас ждет много практики,
я надеюсь, не только в этом курсе,

4
00:00:14,313 --> 00:00:15,561
но и в дальнейшей жизни.

5
00:00:15,561 --> 00:00:19,426
И поэтому мы начинаем с важной и полезной
темы — с инструментов поиска ошибок

6
00:00:19,426 --> 00:00:20,510
в ваших программах.

7
00:00:20,510 --> 00:00:26,590
Как мы уже не раз обсуждали,
C++ декларирует принцип нулевого оверхеда.

8
00:00:26,590 --> 00:00:29,357
То есть если вам нужно
решить какую-то задачу,

9
00:00:29,357 --> 00:00:32,670
C++ будет пытаться решить
ее максимально эффективно.

10
00:00:32,670 --> 00:00:36,400
Например, если вы обращаетесь
к какому-то элементу массива,

11
00:00:36,400 --> 00:00:40,945
C++ просто прибавит это число, номер
индекса к указателю, и не будет проверять,

12
00:00:40,945 --> 00:00:42,730
не вышли ли вы за границу массива.

13
00:00:42,730 --> 00:00:47,642
Как же попросить C++ меньше
доверять разработчику и

14
00:00:47,642 --> 00:00:50,830
перепроверять за ним,
все ли у него в программе хорошо?

15
00:00:50,830 --> 00:00:56,563
Давайте рассмотрим для начала самый
простой пример, в котором мы выйдем

16
00:00:56,563 --> 00:01:00,600
за границы вектора, и подумаем, что с этим
делать, как эту ошибку можно обнаруживать.

17
00:01:00,600 --> 00:01:05,475
Итак, вот у нас программа с пустым main.

18
00:01:05,475 --> 00:01:10,350
Давайте создадим в ней вектор целых чисел,
какой-нибудь самый простой

19
00:01:10,350 --> 00:01:14,200
из одной единицы или даже восьмерки,
чтобы было понятнее.

20
00:01:14,200 --> 00:01:20,268
Зарезервируем две ячейки памяти и
обратимся к элементу номер один,

21
00:01:20,268 --> 00:01:24,257
то есть к последнему элементу,
которого на самом деле нет,

22
00:01:24,257 --> 00:01:26,395
потому что вектор из одного элемента.

23
00:01:26,395 --> 00:01:29,910
Давайте мы заведем переменную с
индексом и обратимся к v [i].

24
00:01:29,910 --> 00:01:31,833
Соберем программу.

25
00:01:31,833 --> 00:01:36,832
Она, конечно же, скомпилируется,
потому что здесь все абсолютно

26
00:01:36,832 --> 00:01:40,600
корректно: я обращаюсь, я вызываю
оператор квадратные скобки от int.

27
00:01:40,600 --> 00:01:43,546
Это вполне допустимая операция,
поэтому все компилируется,

28
00:01:43,546 --> 00:01:45,790
но при запуске у меня
выводится какой-то ноль.

29
00:01:45,790 --> 00:01:49,502
По идее у меня в векторе только восьмерка,
откуда взялся ноль?

30
00:01:49,502 --> 00:01:53,037
Потому что у меня зарезервирована
память еще на одну ячейку,

31
00:01:53,037 --> 00:01:54,666
и в ней сейчас оказался ноль.

32
00:01:54,666 --> 00:01:58,460
При этом программа не упала,
потому что это память нашего вектора.

33
00:01:58,460 --> 00:02:00,811
Как такие ошибки можно находить?

34
00:02:00,811 --> 00:02:02,352
Особенно неприятно,

35
00:02:02,352 --> 00:02:06,713
если такое обращение выход за границы
вектора закопано где-то внутри программы.

36
00:02:06,713 --> 00:02:08,578
Вы просто видите по косвенным признакам,

37
00:02:08,578 --> 00:02:11,943
что программа работает неверно и
вам нужно как-то эту ошибку найти.

38
00:02:11,943 --> 00:02:15,309
Вы, конечно, можете расчехлить
отладчик и в отладчике найти,

39
00:02:15,309 --> 00:02:18,430
что там где-то внутри происходит
не то обращение к памяти.

40
00:02:18,430 --> 00:02:22,700
Но нельзя ли попросить компилятор такие
ошибки отлавливать и явно вам писать

41
00:02:22,700 --> 00:02:26,087
«Ты обратился за границы вектора»?

42
00:02:26,087 --> 00:02:30,291
Для начала что можно сделать,
если вы ничего дополнительного не знаете,

43
00:02:30,291 --> 00:02:32,699
не знаете материала грядущих лекций?

44
00:02:32,699 --> 00:02:38,440
Вы просто можете найти все обращения к
элементам вектора и обернуть их в if'ы.

45
00:02:38,440 --> 00:02:43,155
То есть вывести v[i],
только если i корректно.

46
00:02:43,155 --> 00:02:45,038
То есть i < v.size.

47
00:02:45,038 --> 00:02:47,870
Если i < v.size, то выводим,

48
00:02:47,870 --> 00:02:53,120
иначе говорим «Ой» (Oops).

49
00:02:53,120 --> 00:02:57,920
Давайте соберем эту программу,
там есть небольшой warning про то,

50
00:02:57,920 --> 00:02:59,593
что мы сравниваем знаковые и беззнаковые.

51
00:02:59,593 --> 00:03:02,090
Ну давайте сделаем компилятору
хорошо и сделаем i беззнаковым.

52
00:03:02,090 --> 00:03:04,844
Это как бы не проблема.

53
00:03:04,844 --> 00:03:06,084
Собираю.

54
00:03:06,084 --> 00:03:07,470
Запускаю.

55
00:03:07,470 --> 00:03:08,571
И вижу «Ой».

56
00:03:08,571 --> 00:03:12,975
Хорошо, мы, казалось бы,
ошибку нашли, но не будем же мы,

57
00:03:12,975 --> 00:03:16,104
разработчики, если у нас в
программе что-то идет не так,

58
00:03:16,104 --> 00:03:19,000
все обращения к элементам вектора
заворачивать в такие if'ы.

59
00:03:19,000 --> 00:03:19,944
Конечно, нет.

60
00:03:19,944 --> 00:03:21,968
Хочется чего-то более простого.

61
00:03:21,968 --> 00:03:27,240
Есть у вектора метод add,
который упрощает эту задачу

62
00:03:27,240 --> 00:03:33,545
и сам проверяет, что индекс корректен.

63
00:03:33,545 --> 00:03:36,329
Если некорректен,
то выбрасывает исключения.

64
00:03:36,329 --> 00:03:42,197
Вот я заменил оператор квадратные скобки
на вызов метода add, компилирую программу,

65
00:03:42,197 --> 00:03:49,276
запускаю, и у меня в метод add выкинул
исключения — исключения типа out_of_range,

66
00:03:49,276 --> 00:03:54,390
и говорит, что индекс, который 1 ≥ size,
который тоже 1, а поэтому все плохо.

67
00:03:54,390 --> 00:03:58,036
Казалось бы, мы решили проблему,
но все-таки нет.

68
00:03:58,036 --> 00:04:02,683
Мы же не будем, опять же, для поиска
ошибки заменять все операторы квадратные

69
00:04:02,683 --> 00:04:05,453
скобки на метод add везде,
во всей программе.

70
00:04:05,453 --> 00:04:07,080
Это совершенно неудобно.

71
00:04:07,080 --> 00:04:11,520
Нужно как-то,
оставив оператор квадратные скобки,

72
00:04:11,520 --> 00:04:14,780
вот здесь у меня был
оператор квадратные скобки,

73
00:04:14,780 --> 00:04:20,335
попросить компилятор все равно
проверять выход за границы вектора.

74
00:04:20,335 --> 00:04:22,487
Причем это должно делаться как-то просто,

75
00:04:22,487 --> 00:04:25,890
а не поиском по тексту и какой-то
текстовой заменой в коде программы.

76
00:04:25,890 --> 00:04:30,070
Оказывается, это можно сделать
на этапе препроцессора.

77
00:04:30,070 --> 00:04:33,274
Нужно включить специальные
макроопределения в препроцессоре —

78
00:04:33,274 --> 00:04:34,250
дебажные дефайны.

79
00:04:34,250 --> 00:04:35,584
Как мы это сделаем?

80
00:04:35,584 --> 00:04:42,193
Мы пойдем в настройки проекта Project,
Properties, C/C++ Build,

81
00:04:42,193 --> 00:04:47,470
Settings, вот сюда мы идем и
здесь заходим в препроцессор.

82
00:04:47,470 --> 00:04:52,475
В препроцессоре у нас
здесь Defined Symbols,

83
00:04:52,475 --> 00:04:55,626
−D, и мы добавляем два символа.

84
00:04:55,626 --> 00:05:02,480
Первый символ — это подчеркивание
_GLIBCXX_DEBUG — вот так он выглядит.

85
00:05:02,480 --> 00:05:06,810
И его почти всегда достаточно,
но мы еще добавим

86
00:05:06,810 --> 00:05:11,620
похожий символ _GLIBCXX_DEBUG_PEDANTIC.

87
00:05:11,620 --> 00:05:13,982
Еще более жесткие проверки.

88
00:05:13,982 --> 00:05:16,438
Вот так выглядит этот дефайн.

89
00:05:16,438 --> 00:05:18,240
Его мы тоже включаем.

90
00:05:18,240 --> 00:05:20,567
И у нас включено два дефайна.

91
00:05:20,567 --> 00:05:24,110
Мы их применяем, пересобираем программу.

92
00:05:24,110 --> 00:05:29,020
Собираем.

93
00:05:29,020 --> 00:05:31,705
Все компилируется.

94
00:05:31,705 --> 00:05:33,346
Запускаем.

95
00:05:33,346 --> 00:05:37,749
И при запуске он ругается,
то есть он нашел какую-то проблему,

96
00:05:37,749 --> 00:05:39,010
давайте ее прочитаем.

97
00:05:39,010 --> 00:05:44,328
Мы попытались обратиться к элементу
контейнера и вышли за границы.

98
00:05:44,328 --> 00:05:47,710
Индекс 1, но в векторе всего один элемент.

99
00:05:47,710 --> 00:05:51,920
И тут какое-то сообщение, из которого не
очень понятно, никакой дополнительной

100
00:05:51,920 --> 00:05:55,007
информации оно, кажется, не несет,
кроме адреса этого объекта.

101
00:05:55,007 --> 00:05:57,916
Итак, мы знаем,
что мы обратились к первому элементу.

102
00:05:57,916 --> 00:06:00,090
Мы знаем, что контейнер имел размер 1.

103
00:06:00,090 --> 00:06:02,749
И мы это сделали без
изменения кода программы.

104
00:06:02,749 --> 00:06:06,080
Мы буквально добавили всего
лишь одну опцию в компиляцию.

105
00:06:06,080 --> 00:06:10,856
Но как же нам найти конкретную строчку в
программе, в которой произошла проблема?

106
00:06:10,856 --> 00:06:13,830
В этом сообщении нет никакого
указания на строчку.

107
00:06:13,830 --> 00:06:18,605
Есть указание на строчку внутри
файла с описание вектора,

108
00:06:18,605 --> 00:06:22,076
это строчка 417,
но это не про наш main.cpp.

109
00:06:22,076 --> 00:06:25,300
Поэтому нужно как-то
строчку эту обнаружить.

110
00:06:25,300 --> 00:06:27,783
Как мы это сделаем?

111
00:06:27,783 --> 00:06:33,830
Мы запустим отладчик,
самый обычный отладчик,

112
00:06:33,830 --> 00:06:37,231
но опять же по санитайзеру.

113
00:06:37,231 --> 00:06:40,981
Давайте запустим программу.

114
00:06:40,981 --> 00:06:48,170
Программа запустилась,
но как-то странно упала.

115
00:06:48,170 --> 00:06:52,717
Тут написано,
что у нас вызвался abort в операторе

116
00:06:52,717 --> 00:06:57,942
квадратные скобки у вектора в строчке 417,

117
00:06:57,942 --> 00:07:02,965
а этот оператор вызвался
в main.cpp в строчке 10.

118
00:07:02,965 --> 00:07:05,117
Вот main.cpp, строчка 10,

119
00:07:05,117 --> 00:07:09,670
и вот здесь у меня вызвался этот
самый оператор квадратные скобки.

120
00:07:09,670 --> 00:07:12,597
Мы нашли проблему.

121
00:07:12,597 --> 00:07:17,940
Мы нашли то место в программе,
в котором мы выходим за границы вектора.

122
00:07:17,940 --> 00:07:19,301
Отлично.

123
00:07:19,301 --> 00:07:26,102
Давайте с этим простым
примером мы закончим.

124
00:07:26,102 --> 00:07:31,052
И перейдем к чуть более интересному
примеру — как раз та самая большая

125
00:07:31,052 --> 00:07:33,340
программа, как найти ошибки
в большой программе.

126
00:07:33,340 --> 00:07:39,203
Итак, мы этот DEBUG завершаем
и открываем решение задачи

127
00:07:39,203 --> 00:07:45,350
«Демографические показатели», которые нас
преследуют, кажется, с «Желтого пояса».

128
00:07:45,350 --> 00:07:51,950
Вот у нас решение, которое мы сделали
более красивым в «Коричневом поясе».

129
00:07:51,950 --> 00:07:57,584
Я напомню немного: у нас здесь есть люди,
у людей есть возраст,

130
00:07:57,584 --> 00:08:03,330
пол и статус — ходит он на работу или не
ходит на работу, устроен или не устроен.

131
00:08:03,330 --> 00:08:06,193
Про этих людей есть какие-то операторы.

132
00:08:06,193 --> 00:08:11,330
И мы хотим собрать статистику по
медианному возрасту разных групп людей.

133
00:08:11,330 --> 00:08:13,552
Она собирается в такой структуре.

134
00:08:13,552 --> 00:08:17,620
Есть какие-то операторы, операторы для
дебага, операторы для чего-то еще.

135
00:08:17,620 --> 00:08:22,036
Есть функция вычисления медианного
возраста, есть функция чтения людей, есть

136
00:08:22,036 --> 00:08:27,640
функция вычисления этой самой статистики
и функция вывода этой статистики.

137
00:08:27,640 --> 00:08:32,071
Казалось бы, решение похоже на то,
что было в «Коричневом поясе»,

138
00:08:32,071 --> 00:08:35,110
но представьте, что где-то ошибка,
и мы пытаемся ее найти.

139
00:08:35,110 --> 00:08:41,310
Давайте я запущу эту программу,
я ее собрал.

140
00:08:41,310 --> 00:08:42,096
Я ее запускаю.

141
00:08:42,096 --> 00:08:43,909
Мне нужно ввести каких-то людей.

142
00:08:43,909 --> 00:08:47,965
Давайте для простоты будет один человек,
у которого возраст 0, пол 0,

143
00:08:47,965 --> 00:08:51,388
и is employed тоже 0, ничего страшного.

144
00:08:51,388 --> 00:08:53,865
Мы запускаем и видим, что что-то упало.

145
00:08:53,865 --> 00:08:57,580
Нас, опять же, спасает дебажный дефайн,
который включен в этом проекте.

146
00:08:57,580 --> 00:09:01,072
И он выглядит как-то по-другому,
это явно не про вектор,

147
00:09:01,072 --> 00:09:03,065
у нас что-то другое пошло не так.

148
00:09:03,065 --> 00:09:05,825
То есть дебажный дефайн позволяет
проверять не только выход за границы

149
00:09:05,825 --> 00:09:06,929
вектора, но и что-то еще.

150
00:09:06,929 --> 00:09:08,550
Давайте почитаем, что здесь написано.

151
00:09:08,550 --> 00:09:16,120
Где-то в stl_algo.h в строке
4797 в каком-то алгоритме C++,

152
00:09:16,120 --> 00:09:20,806
который мы вызвали,
какой-то невалидный итератор range.

153
00:09:20,806 --> 00:09:24,110
Диапазон итераторов какой-то неправильный.

154
00:09:24,110 --> 00:09:29,960
Где это происходит,
что-то про вектор написано.

155
00:09:29,960 --> 00:09:33,050
А про какой алгоритм речь, непонятно.

156
00:09:33,050 --> 00:09:35,486
Давайте как эту проблему найдем.

157
00:09:35,486 --> 00:09:37,502
Как?
С помощью отладчика.

158
00:09:37,502 --> 00:09:39,100
Запускаем отладчик.

159
00:09:39,100 --> 00:09:41,838
Запустили отладчик.

160
00:09:41,838 --> 00:09:45,300
Запускаем в нем программу.

161
00:09:45,300 --> 00:09:47,582
Программа радостно падает.

162
00:09:47,582 --> 00:09:52,700
Мы смотрим на так называемый back trace,
на стек вызова функций.

163
00:09:52,700 --> 00:09:56,864
Видим, что функцию main
он почему-то не показал,

164
00:09:56,864 --> 00:09:59,900
но он упал в алгоритме nth_element.

165
00:09:59,900 --> 00:10:01,847
И этого нам достаточно,

166
00:10:01,847 --> 00:10:07,590
чтобы посмотреть обратно на код программы
и найти этот самый nth_element.

167
00:10:07,590 --> 00:10:11,320
Этот алгоритм у нас вызывается
в ComputeMedianAge.

168
00:10:11,320 --> 00:10:13,785
Мы смотрим на вызов nth_element.

169
00:10:13,785 --> 00:10:19,950
Видим, что мы передаем итератор начала,
итератор конца и итератор середины.

170
00:10:19,950 --> 00:10:26,180
А если посмотреть на код nth_element,
где у нас все упало,

171
00:10:26,180 --> 00:10:31,688
то мы увидим,
что здесь есть проверка того,

172
00:10:31,688 --> 00:10:36,150
что диапазон от n-того итератора
до последнего валидный.

173
00:10:36,150 --> 00:10:38,843
Он проверяет, что первого до
середины нормальный диапазон,

174
00:10:38,843 --> 00:10:41,045
и от середины до последнего
нормальный диапазон.

175
00:10:41,045 --> 00:10:45,156
И если мы посмотрим в порядок
аргументов функции, то увидим,

176
00:10:45,156 --> 00:10:50,748
что сначала принимается первый итератор,
потом средний, а потом последний.

177
00:10:50,748 --> 00:10:56,994
А мы в main.cpp сделали не так,
перепутав последний со средним.

178
00:10:56,994 --> 00:11:01,499
И вот нам помог дебажный
дефайн найти проблему при

179
00:11:01,499 --> 00:11:06,250
вызове алгоритма nth_element —
стандартного алгоритма в C++.

180
00:11:06,250 --> 00:11:09,860
Итак, подведем итоги этого видео.

181
00:11:09,860 --> 00:11:16,437
Мы изучили отладочные макроопределения

182
00:11:16,437 --> 00:11:20,870
— это специальные директивы препроцессора,
которые включают специальные проверки

183
00:11:20,870 --> 00:11:24,516
различных инвариантов стандартной
библиотеки: выход за границы вектора,

184
00:11:24,516 --> 00:11:30,330
валидность диапазонов итератора, которые
передаются в стандартные алгоритмы.

185
00:11:30,330 --> 00:11:33,656
При этом при отсутствии этих
макроопределений программа работает

186
00:11:33,656 --> 00:11:35,363
как обычно и с той же скоростью.

187
00:11:35,363 --> 00:11:38,015
Когда же вы включаете
эти дебажные дефайны,

188
00:11:38,015 --> 00:11:43,032
у вас начинают срабатывать эти проверки,
и программа работает дольше за счет того,

189
00:11:43,032 --> 00:11:46,415
что компилятор не до конца
доверяет разработчику.

190
00:11:46,415 --> 00:11:51,437
Обращаю ваше внимание еще раз на то,
что программы все стабильно и хорошо

191
00:11:51,437 --> 00:11:56,140
успешно компилировались, а ошибки
находились при выполнении программы.

192
00:11:56,140 --> 00:12:00,580
Важно понимать,
что отладочные макроопределения,

193
00:12:00,580 --> 00:12:03,775
как и следующий инструмент, который мы
рассмотрим в следующем видео, работают в

194
00:12:03,775 --> 00:12:07,823
процессе выполнения программы и тем самым
замедляют выполнение этой программы.

195
00:12:07,823 --> 00:12:11,865
Больше подробностей про то,
какие ошибки помогают находить отладочные

196
00:12:11,865 --> 00:12:14,715
макроопределения и на что
еще нужно обращать внимание,

197
00:12:14,715 --> 00:12:16,398
вы найдете в текстовом материале.

198
00:12:16,398 --> 00:12:19,420
А в следующем видео мы поговорим
про еще один инструмент

199
00:12:19,420 --> 00:12:21,914
поиска ошибок при выполнении программы.