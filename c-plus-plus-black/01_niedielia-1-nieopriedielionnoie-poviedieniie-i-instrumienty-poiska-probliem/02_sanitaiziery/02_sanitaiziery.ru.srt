1
00:00:05,840 --> 00:00:09,000
В предыдущем видео мы узнали про,

2
00:00:09,000 --> 00:00:11,350
так называемое, отладочное макроопределение, дебажные дефайны,

3
00:00:11,350 --> 00:00:16,755
которые на этапе препроцессинга включали специальные проверки в стандартные библиотеки,

4
00:00:16,755 --> 00:00:18,760
которые не давали вам выйти за границы массива,

5
00:00:18,760 --> 00:00:22,565
передать в алгоритм сеть диапазонов итераторов и так далее, и так далее.

6
00:00:22,565 --> 00:00:25,245
Но здесь это можно считать некоторым везением.

7
00:00:25,245 --> 00:00:30,450
Разработчики стандартной библиотеки о вас позаботились и написали специальный код,

8
00:00:30,450 --> 00:00:33,390
который можно включить препроцессором и который найдет вам

9
00:00:33,390 --> 00:00:37,200
ошибки в процессе выполнения программы.

10
00:00:37,200 --> 00:00:39,705
Но что делать, если вы сами пишете свою библиотеку?

11
00:00:39,705 --> 00:00:42,480
Или, если вы непосредственно работаете с памятью,

12
00:00:42,480 --> 00:00:47,010
вызываете "New", вызываете "Delete" и все еще нуждаетесь в помощи.

13
00:00:47,010 --> 00:00:52,215
Например, вы забываете очистить память и у вас происходит утечка,

14
00:00:52,215 --> 00:00:54,750
или вы дважды удаляете одну и ту же память,

15
00:00:54,750 --> 00:00:56,885
или вы используете инвалидированные ссылки.

16
00:00:56,885 --> 00:01:01,730
Кто вам поможет, если это код не привязаный к стандартной библиотеке?

17
00:01:01,730 --> 00:01:04,045
Вам понадобятся санитайзеры.

18
00:01:04,045 --> 00:01:07,530
Санитайзеры — это некоторый способ собрать

19
00:01:07,530 --> 00:01:12,562
программу еще на уровень ниже, который позволяет найти,

20
00:01:12,562 --> 00:01:16,185
обнаружить использование чужой памяти и использование освобожденной памяти,

21
00:01:16,185 --> 00:01:19,295
обнаружить повторное освобождение памяти,

22
00:01:19,295 --> 00:01:22,965
найти утечки памяти и другие подобные проблемы.

23
00:01:22,965 --> 00:01:24,810
Давайте, рассмотрим простой пример,

24
00:01:24,810 --> 00:01:27,275
но рассмотрим мы его на Linux-машине,

25
00:01:27,275 --> 00:01:30,225
потому что под Linux-ом с санитайзерами все в порядке,

26
00:01:30,225 --> 00:01:32,825
а под Windows они реализованы не до конца.

27
00:01:32,825 --> 00:01:36,598
Про Windows будет отдельная статья, а показывать я буду все на Linux-машине.

28
00:01:36,598 --> 00:01:43,630
Итак, я беру консоль и покажу вам простой пример.

29
00:01:43,630 --> 00:01:46,750
В этом простом примере я напишу функцию,

30
00:01:46,750 --> 00:01:53,065
в которой я верну ссылку на строчку, которая будет невалидной.

31
00:01:53,065 --> 00:01:57,220
Функция "MakeString", здесь внутри, допустим,

32
00:01:57,220 --> 00:02:00,320
у меня будет вектор строк,

33
00:02:02,430 --> 00:02:08,450
состоящий из двух строк в С++ и Python.

34
00:02:09,500 --> 00:02:17,015
И верну я нулевой элемент этого вектора.

35
00:02:17,015 --> 00:02:21,530
А функция "Main" я напишу const auto ссылка ref равно

36
00:02:21,530 --> 00:02:27,135
"MakeString"

37
00:02:27,135 --> 00:02:34,305
и попробую вывести содержимое этой ссылки.

38
00:02:34,305 --> 00:02:38,815
Давайте, я скомпилирую эту программу с помощью

39
00:02:38,815 --> 00:02:45,129
компилятора Clang ++ 6.0. main.cpp минус о main.

40
00:02:45,129 --> 00:02:49,810
Компилирую, все компилируется.

41
00:02:49,810 --> 00:02:54,252
Я ее запускаю и получаю Segmentation fault.

42
00:02:54,252 --> 00:02:55,945
Мне никто, как в предыдущем видео, не сказал,

43
00:02:55,945 --> 00:02:57,730
где у меня проблема.

44
00:02:57,730 --> 00:03:00,625
И это меня огорчает и поэтому я включаю санитайзер,

45
00:03:00,625 --> 00:03:08,365
Address санитайзер с помощью флага минус f sanitize равно adrress.

46
00:03:08,365 --> 00:03:10,555
Я компилирую с этим флагом.

47
00:03:10,555 --> 00:03:13,448
Опять же, у меня программа теперь будет работать медленнее,

48
00:03:13,448 --> 00:03:15,415
но она будет обо мне больше заботиться,

49
00:03:15,415 --> 00:03:17,883
я ее запускаю и вижу,

50
00:03:17,883 --> 00:03:19,162
что что-то пошло не так.

51
00:03:19,162 --> 00:03:23,040
Adrress санитайзер ругается на использование неизвестного адреса 0х060600с000001.

52
00:03:23,040 --> 00:03:27,550
Это мне ни о чем не говорит,

53
00:03:27,550 --> 00:03:30,040
как и то, где конкретно произошла проблема.

54
00:03:30,040 --> 00:03:33,855
Тут опять же какие-то адреса и что-то не так, а что непонятно,

55
00:03:33,855 --> 00:03:36,700
тут даже есть ссылка на мой файл, но непонятно,

56
00:03:36,700 --> 00:03:37,900
где конкретно в нем проблема,

57
00:03:37,900 --> 00:03:39,820
а хотелось бы увидеть конкретную строчку,

58
00:03:39,820 --> 00:03:41,575
в которой я что-то сделал не так.

59
00:03:41,575 --> 00:03:45,070
И здесь не поможет символайзер — программа,

60
00:03:45,070 --> 00:03:50,692
которая конвертирует адреса инструкции моей программы в конкретные строчки кода.

61
00:03:50,692 --> 00:03:52,330
Итак, что я должен сделать,

62
00:03:52,330 --> 00:03:55,480
чтобы сообщение об ошибке от санитайзера было более понятным?

63
00:03:55,480 --> 00:03:59,350
Во-первых, я должен компилировать со специальными флажками,

64
00:03:59,350 --> 00:04:03,900
а именно с флажком минус g, с флажком минус fno-omit-frame-pointer.

65
00:04:03,900 --> 00:04:09,030
Это уже флажки, которые скорее в

66
00:04:09,030 --> 00:04:15,190
некоторых ситуациях покажут вам более понятный вывод санитайзера.

67
00:04:15,190 --> 00:04:20,580
Fno-omit-frame-pointer с одним словом frame и минус fno-optimize-sibling-calls.

68
00:04:20,580 --> 00:04:23,980
Вот так.

69
00:04:26,350 --> 00:04:28,675
Компилирую.

70
00:04:28,675 --> 00:04:31,125
Пробую запустить, у меня все еще какие-то адреса,

71
00:04:31,125 --> 00:04:36,755
мне еще нужно указать специальную переменную окружения с путем к символайзеру.

72
00:04:36,755 --> 00:04:43,810
Как этот путь найти? Я обращаюсь к файлам пакета llvm

73
00:04:43,810 --> 00:04:52,995
6.0 с утилитами llvm и грепаю там, ищу строчку symbolize.

74
00:04:52,995 --> 00:05:01,010
Вижу, что у меня есть путь в usr/lib/llvm-6.0/bin/llvm/symbolizer и именно его я указываю

75
00:05:01,010 --> 00:05:09,105
в переменной ASAN.SYMBOLIZER.PATH при запуске программы,

76
00:05:09,105 --> 00:05:11,400
не при компиляции, при запуске.

77
00:05:11,400 --> 00:05:15,895
Вот этот путь, я его указал и с этой переменной запускаю программу.

78
00:05:15,895 --> 00:05:18,870
И теперь я вижу ту же ошибку,

79
00:05:18,870 --> 00:05:24,200
но наряду с адресами инструкций у меня есть и пояснение на тему того,

80
00:05:24,200 --> 00:05:26,840
какие это функции, в частности я вижу,

81
00:05:26,840 --> 00:05:33,795
что в main.сpp, в строчке 17 у меня вызвало что-то про поток и дальше все пошло не так.

82
00:05:33,795 --> 00:05:36,200
Что у меня в main.cpp в строчке 17?

83
00:05:36,200 --> 00:05:39,075
У меня здесь обращение к этой самой ссылке.

84
00:05:39,075 --> 00:05:42,425
Значит нужно обратить внимание на эту конкретную ссылку.

85
00:05:42,425 --> 00:05:43,930
Что не так со ссылкой?

86
00:05:43,930 --> 00:05:45,080
Да то, что то,

87
00:05:45,080 --> 00:05:49,185
на что она ссылалась освободилось при выходе из функции "MakeString".

88
00:05:49,185 --> 00:05:54,035
И вот мне санитайзер на конкретную строчку вот здесь указал.

89
00:05:54,035 --> 00:06:00,470
Давайте попробуем с помощью санитайзера найти проблему в коде большего объема.

90
00:06:00,470 --> 00:06:04,700
Опять же, это будет задача "Демографические показатели" с глубоко запрятанной ошибкой.

91
00:06:04,700 --> 00:06:05,750
Давайте, я посмотрю на код,

92
00:06:05,750 --> 00:06:07,645
увижу, что он примерно тот же.

93
00:06:07,645 --> 00:06:09,485
Вот тут есть пол, есть человек,

94
00:06:09,485 --> 00:06:11,930
есть какие-то операторы, есть статистика и так далее,

95
00:06:11,930 --> 00:06:13,765
и так далее, и так далее.

96
00:06:13,765 --> 00:06:22,540
Давайте, я скомпелирую той же строчкой компиляции. Все компилируется.

97
00:06:22,540 --> 00:06:25,690
Теперь я запускаю программу,

98
00:06:25,690 --> 00:06:28,505
опять же, с переменной про Symbolizer Path.

99
00:06:28,505 --> 00:06:31,419
Я должен ввести информацию про людей, я, опять же,

100
00:06:31,419 --> 00:06:37,060
поленюсь и введу одного человека с параметрами 0 0 0 и у меня что-то упало.

101
00:06:37,060 --> 00:06:38,690
Давайте, почитаем текст ошибки.

102
00:06:38,690 --> 00:06:45,355
Adrress санитайзер говорит, что мы обратились по неизвестному адресу 0 0 0 0 0 0 0 0 0.

103
00:06:45,355 --> 00:06:49,660
Говоря человеческим языком, мы обратились по нулевому указателю.

104
00:06:49,660 --> 00:06:53,345
Так делать нельзя. Тут даже написано что "Аdrress points to the zero page",

105
00:06:53,345 --> 00:06:54,985
то есть это нулевая страница.

106
00:06:54,985 --> 00:06:57,630
Там, конечно, ничего искать не нужно.

107
00:06:57,630 --> 00:06:59,416
И это случилось в функции "ComputeMedianAge",

108
00:06:59,416 --> 00:07:03,760
в файле main.cpp, в строчке 78.

109
00:07:04,060 --> 00:07:09,663
Давайте, я посмотрю строчку 78, middle стрелочка age,

110
00:07:09,663 --> 00:07:16,970
то middle, итератор middle оказался нулевым указателем.

111
00:07:16,970 --> 00:07:18,920
И тут сходу не очень понятно,

112
00:07:18,920 --> 00:07:22,200
что происходит и в чем конкретно проблема,

113
00:07:22,200 --> 00:07:27,770
поэтому пойдем в отладчик, классический отладчик gdb.

114
00:07:27,770 --> 00:07:34,115
Gdb.main запускаем,

115
00:07:34,115 --> 00:07:38,730
вводим 1 0 0 0 и сразу видно,

116
00:07:38,730 --> 00:07:40,569
где у меня случился Segmentation fault — в return middle age.

117
00:07:40,569 --> 00:07:46,465
Переключаемся в код — и на что мы здесь можем посмотреть?

118
00:07:46,465 --> 00:07:49,085
Мы можем посмотреть на middle,

119
00:07:49,085 --> 00:07:52,555
наверное, и он говорит, что "Cannot acsess memory at adrress 0x0".

120
00:07:52,555 --> 00:07:55,030
Ну, да. Middle нулевой указатель.

121
00:07:55,030 --> 00:07:57,980
Почему? Откуда взялся middle?

122
00:07:57,980 --> 00:07:59,424
Middle — это begin от range_copy плюс range_copy.size пополам.

123
00:07:59,424 --> 00:08:05,118
Давайте посмотрим на range_copy и увидим,

124
00:08:05,118 --> 00:08:07,415
что range_copy это пустой вектор,

125
00:08:07,415 --> 00:08:10,055
вектор длины ноль и капесити ноль.

126
00:08:10,055 --> 00:08:12,100
Понятно, что обращаться по итератору,

127
00:08:12,100 --> 00:08:14,480
который получен с пустого вектора, бесполезно,

128
00:08:14,480 --> 00:08:17,430
ну и он оказался нулевым указателем.

129
00:08:17,430 --> 00:08:20,310
Проблема, собственно, вот в этом закомментированном коде,

130
00:08:20,310 --> 00:08:22,340
который проверял пустой ли диапазон.

131
00:08:22,340 --> 00:08:24,590
Если его раскомментировать, все будет хорошо.

132
00:08:24,590 --> 00:08:28,230
Давайте для порядка его раскомментируем.

133
00:08:30,210 --> 00:08:36,185
Раскомментировали, нужно перекомпилировать программу,

134
00:08:36,185 --> 00:08:37,700
давайте, под санитайзером, чтобы убедиться,

135
00:08:37,700 --> 00:08:39,445
что у нас правда всё-всё-всё хорошо.

136
00:08:39,445 --> 00:08:44,330
Запускаем программу, вводим 1 0 0 0 и всё в порядке,

137
00:08:44,330 --> 00:08:47,235
все медианы и возраста ноль, что довольно логично.

138
00:08:47,235 --> 00:08:52,420
Отлично. Давайте обсудим ещё такой интересный вопрос,

139
00:08:52,420 --> 00:08:57,245
который возникает у опытных пользователей С++.

140
00:08:57,245 --> 00:08:59,540
Почему нельзя было просто,

141
00:08:59,540 --> 00:09:01,405
когда у меня программа упала,

142
00:09:01,405 --> 00:09:04,415
пойти в отладчик и там точно также увидеть строчку,

143
00:09:04,415 --> 00:09:07,600
к которой мы обратились по нулевому указателю?

144
00:09:07,600 --> 00:09:09,535
Дело в том, что, во-первых,

145
00:09:09,535 --> 00:09:12,720
санитайзер позволяет искать утечки памяти,

146
00:09:12,720 --> 00:09:14,955
а отладчик вам такого не найдет,

147
00:09:14,955 --> 00:09:17,460
потому что он к этому не приспособлен.

148
00:09:17,460 --> 00:09:19,175
Это первое. А второе,

149
00:09:19,175 --> 00:09:21,590
санитайзер разработан таким образом, что, конечно же,

150
00:09:21,590 --> 00:09:26,610
он замедляет программу из-за дополнительных проверок в недрах компилятора,

151
00:09:26,610 --> 00:09:29,610
но замедляет он ее не так сильно,

152
00:09:29,610 --> 00:09:31,020
как отладчик, то есть,

153
00:09:31,020 --> 00:09:33,480
если вы даже зайдете в документацию по санитайзеру, то там будет написано,

154
00:09:33,480 --> 00:09:35,060
что ваша программа замедлится раза в два,

155
00:09:35,060 --> 00:09:37,200
но если вы запустите под отладчиком вашу программу,

156
00:09:37,200 --> 00:09:38,400
она будет работать еще дольше,

157
00:09:38,400 --> 00:09:41,890
потому что ответчик отслеживает вообще все-все-все инструкции.

158
00:09:41,890 --> 00:09:44,945
Ну и, наконец, программа собраная под санитайзером,

159
00:09:44,945 --> 00:09:48,810
это та же самая программа и запускаемая точно таким же образом,

160
00:09:48,810 --> 00:09:53,388
что довольно удобно встраивается в любую инфраструктуру запуска каких-либо бинарников.

161
00:09:53,388 --> 00:09:56,662
Запустить же программу под отладчиком это все-таки некоторое приключение.

162
00:09:56,662 --> 00:10:00,420
Давайте, покажу что санитайзер умеет ловить утечки.

163
00:10:00,420 --> 00:10:06,960
Я вернусь в консоль, вернусь в мой проект "Example",

164
00:10:06,980 --> 00:10:14,600
открою там main.cpp и потеряю там какую-нибудь память.

165
00:10:14,600 --> 00:10:18,445
Это довольно легко сделать в С++.

166
00:10:18,445 --> 00:10:25,860
Int*P равно new_Int, ну и давайте попробуем больше ничего не делать,

167
00:10:25,860 --> 00:10:32,155
прямо так скомпилировать, запустить.

168
00:10:32,155 --> 00:10:35,835
Мне Leak санитайзер говорит: "Detected memory leaks", память утекла.

169
00:10:35,835 --> 00:10:40,310
Давайте, я запущу с символайзером и увижу,

170
00:10:40,310 --> 00:10:45,525
что утекли четыре байта из одного объекта,

171
00:10:45,525 --> 00:10:47,239
ну там Int был, логично,

172
00:10:47,239 --> 00:10:49,375
которые выделились в операторе New,

173
00:10:49,375 --> 00:10:52,530
которые вызвали в восьмой строчке файла main.cpp.

174
00:10:52,530 --> 00:10:56,340
Правда, вот в восьмой строчке я правда вызвал New и Delete для него не вызвал.

175
00:10:56,340 --> 00:10:58,842
Санитайзер нашел утечку и сказал,

176
00:10:58,842 --> 00:11:02,175
где конкретно была выделена эта память.

177
00:11:02,175 --> 00:11:07,330
Итак, подведем итоги.

178
00:11:07,330 --> 00:11:10,753
Мы изучили санитайзеры и дебажные дефайны.

179
00:11:10,753 --> 00:11:12,430
Это инструменты,

180
00:11:12,430 --> 00:11:15,790
которые позволяют легко скомпилировать вашу программу

181
00:11:15,790 --> 00:11:19,860
в режиме усиленного поиска ошибок и недоверия разработчику.

182
00:11:19,860 --> 00:11:21,640
Программа из-за этого замедляется,

183
00:11:21,640 --> 00:11:23,615
но зато в процессе выполнения программы,

184
00:11:23,615 --> 00:11:27,520
не при компиляции, находятся всевозможные ошибки.

185
00:11:27,520 --> 00:11:29,650
При этом, все-таки не гарантируется,

186
00:11:29,650 --> 00:11:32,650
что эти инструменты найдут все возможные ошибки,

187
00:11:32,650 --> 00:11:36,160
в некоторых экспериментальных режимах работы санитайзеры, еще написано,

188
00:11:36,160 --> 00:11:40,610
что случаются ложно-положительные срабатывания и об этом нужно куда-то писать.

189
00:11:40,610 --> 00:11:42,760
Хотя на практике, конечно, это случается редко благодаря тому,

190
00:11:42,760 --> 00:11:46,175
что санитайзерами пользуются многие и многие разработчики.

191
00:11:46,175 --> 00:11:50,470
К этому блоку вам будут даны некоторые задачи,

192
00:11:50,470 --> 00:11:52,120
во многом вам известные,

193
00:11:52,120 --> 00:11:53,515
в которых вам нужно будет найти

194
00:11:53,515 --> 00:11:56,880
ошибки с помощью санитайзеров и отладочных макроопределений.

195
00:11:56,880 --> 00:12:01,630
Более того, во всем этом курсе,

196
00:12:01,630 --> 00:12:06,880
во многих задачах в тестирующей системе на некоторых тестах

197
00:12:06,880 --> 00:12:08,679
ваша программа будет запускаться

198
00:12:08,679 --> 00:12:12,665
с включенными санитайзерами и, возможно, отладочными макроопределениями.

199
00:12:12,665 --> 00:12:18,370
И поэтому вам нужно будет усиленно следить за утечками памяти и прочими

200
00:12:18,370 --> 00:12:21,220
нехорошими вещами и у себя также проверять

201
00:12:21,220 --> 00:12:24,340
ваши программы с использованием тех самых инструментов,

202
00:12:24,340 --> 00:12:26,570
которые мы рассмотрели в этих видео.