1
00:00:00,000 --> 00:00:04,716
[БЕЗ_ЗВУКА] Прежде чем

2
00:00:04,716 --> 00:00:09,641
перейти к изучению
неопределенного поведения,

3
00:00:09,641 --> 00:00:13,245
давайте познакомимся с некоторыми
онлайн-сервисами для C++.

4
00:00:13,245 --> 00:00:17,673
Мы рассмотрим онлайн-сервисы,
которые позволяют выполнять запуск

5
00:00:17,673 --> 00:00:21,802
программы; сервис, который позволяет
измерять производительность отдельных

6
00:00:21,802 --> 00:00:27,030
частей программы; и сервис, который
позволяет анализировать ассемблерный код,

7
00:00:27,030 --> 00:00:30,738
который компилятор генерирует
для вашей программы.

8
00:00:30,738 --> 00:00:33,058
Зачем нам нужны эти онлайн-сервисы?

9
00:00:33,058 --> 00:00:36,550
Дело в том, что когда речь заходит
о неопределенном поведении,

10
00:00:36,550 --> 00:00:39,413
то очень часто бывает
необходимо посмотреть,

11
00:00:39,413 --> 00:00:43,246
как программа ведет себя с
разными настройками компилятора,

12
00:00:43,246 --> 00:00:48,010
с разными компиляторами и с разными
версиями одного и того же компилятора.

13
00:00:48,010 --> 00:00:52,828
Соответственно держать весь этот зоопарк
на локальной машине бывает не очень

14
00:00:52,828 --> 00:00:53,334
удобно.

15
00:00:53,334 --> 00:00:55,677
В этом плане нам помогут онлайн-сервисы,

16
00:00:55,677 --> 00:00:59,290
где мы можем выбрать конкретный
компилятор и его версию.

17
00:00:59,290 --> 00:01:00,159
Кроме того,

18
00:01:00,159 --> 00:01:06,027
онлайн-сервисы очень часто применяются
на практике в повседневной разработке.

19
00:01:06,027 --> 00:01:09,766
Поэтому вам будет полезно
с ними познакомиться.

20
00:01:09,766 --> 00:01:14,416
И кроме того, в онлайн-сервисах
удобно делиться своей работой.

21
00:01:14,416 --> 00:01:17,986
Например, вы можете создать
какую-то сессию, посмотреть,

22
00:01:17,986 --> 00:01:20,003
как выполняется определенная программа.

23
00:01:20,003 --> 00:01:23,882
И дальше послать эту ссылку вашему
коллеге либо разместить ее в вопросе на

24
00:01:23,882 --> 00:01:27,270
Stack Overflow или на
каком-нибудь другом ресурсе.

25
00:01:27,270 --> 00:01:31,400
Давайте начнем с сервиса
под названием Wandbox,

26
00:01:31,400 --> 00:01:37,913
этот сервис позволяет писать и запускать
программы на различных языках, кстати.

27
00:01:37,913 --> 00:01:42,910
Не только на C++,
но для C++ он поддерживает компиляторы gcc

28
00:01:42,910 --> 00:01:45,977
и clang различных версий.

29
00:01:45,977 --> 00:01:50,626
Здесь можно выполнить некоторые
настройки компилятора,

30
00:01:50,626 --> 00:01:53,783
указать, хотим ли мы
смотреть на Warning'и,

31
00:01:53,783 --> 00:01:57,521
которые нам дает компилятор,
хотим ли мы включить оптимизацию.

32
00:01:57,521 --> 00:02:01,357
Он также позволяет подключить несколько
известных библиотек, таких как,

33
00:02:01,357 --> 00:02:02,370
например, Boost.

34
00:02:02,370 --> 00:02:03,859
Boost нам сейчас не нужен.

35
00:02:03,859 --> 00:02:07,425
Можно выбрать стандарт,
давайте выберем последний стандарт,

36
00:02:07,425 --> 00:02:08,990
активный на данный момент.

37
00:02:08,990 --> 00:02:10,022
Это C++ 17.

38
00:02:10,022 --> 00:02:12,825
И кроме того, если нам этого недостаточно,

39
00:02:12,825 --> 00:02:16,610
мы можем в явном виде задать
конкретные опции компилятора.

40
00:02:16,610 --> 00:02:21,560
Давайте напишем какую-нибудь
простую программу.

41
00:02:21,560 --> 00:02:27,730
Выведем в выходной поток Hello,

42
00:02:27,730 --> 00:02:31,480
black belt!

43
00:02:31,480 --> 00:02:35,820
Вот такую программу мы написали,
и давайте ее запустим.

44
00:02:35,820 --> 00:02:37,687
Кнопочка запуска.

45
00:02:37,687 --> 00:02:42,358
То есть наша программа компилируется,
выполняется.

46
00:02:42,358 --> 00:02:47,030
В этом окошке мы можем видеть то,
что она вывела в стандартный поток вывода.

47
00:02:47,030 --> 00:02:51,065
И после этого у нас пишется
код возврата нашей программы.

48
00:02:51,065 --> 00:02:54,653
Ноль в данном случае — это значит,
что все хорошо.

49
00:02:54,653 --> 00:02:58,971
После того как мы запустили программу,
у нас есть кнопка Share,

50
00:02:58,971 --> 00:03:03,331
мы можем на нее нажать, и сервис
сгенерирует нам уникальную ссылку,

51
00:03:03,331 --> 00:03:08,330
которую мы соответственно можем отправить
коллеге или где-то ее опубликовать.

52
00:03:08,330 --> 00:03:10,388
Здесь все достаточно просто.

53
00:03:10,388 --> 00:03:13,640
Давайте теперь посмотрим
на следующий сервис.

54
00:03:13,640 --> 00:03:15,791
quickbench.com — это сервис,

55
00:03:15,791 --> 00:03:21,260
который позволяет измерять быстродействие
определенных фрагментов программы.

56
00:03:21,260 --> 00:03:26,272
У себя под капотом он использует фреймворк
Google Benchmark — достаточно известный

57
00:03:26,272 --> 00:03:32,140
фреймворк, который позволяет запускать
профилировку определенных фрагментов кода.

58
00:03:32,140 --> 00:03:36,331
Этот сервис позволяет зарегистрировать

59
00:03:36,331 --> 00:03:40,835
некоторые функции как бенчмарки,
которые он будет проверять.

60
00:03:40,835 --> 00:03:44,360
Здесь мы видим две функции:
StringCreation and StringCopy.

61
00:03:44,360 --> 00:03:50,600
И обе эти функции регистрируются во
фреймворке с помощью макроса бенчмарк.

62
00:03:50,600 --> 00:03:54,645
Внутрь каждой функции передается
объект под названием benchmark State,

63
00:03:54,645 --> 00:03:58,958
с помощью этого объекта можно выполнять
некоторую конфигурацию фреймворка.

64
00:03:58,958 --> 00:04:03,703
Но самое главное, для чего нужен этот
объект, по нему можно итерироваться.

65
00:04:03,703 --> 00:04:08,943
Дело в том, что в бенчмарках, как правило,
проверяется некоторая операция,

66
00:04:08,943 --> 00:04:12,240
которая сама по себе занимает
достаточно мало времени.

67
00:04:12,240 --> 00:04:16,224
И для того чтобы замерить
реальное время ее выполнения,

68
00:04:16,224 --> 00:04:19,880
бывает полезно запустить
ее множество раз подряд.

69
00:04:19,880 --> 00:04:23,455
То есть в цикле повторить по
сути одну и ту же операцию.

70
00:04:23,455 --> 00:04:28,974
Сколько конкретно должно быть итераций,
этих повторений, решать не очень удобно.

71
00:04:28,974 --> 00:04:31,191
И фреймфорк в принципе решает за вас.

72
00:04:31,191 --> 00:04:36,102
Он предоставляет вам этот объект,
этот объект внутри себя реализует методы

73
00:04:36,102 --> 00:04:40,220
begin и end, поэтому его можно
засунуть в ranged based for.

74
00:04:40,220 --> 00:04:44,408
Но поскольку здесь нам этот
цикл нужен только для того,

75
00:04:44,408 --> 00:04:48,865
чтобы провести некоторое количество
итераций, нам совсем не важно,

76
00:04:48,865 --> 00:04:53,913
какое именно значение у нас получается
после доступа к этим методам begin и end,

77
00:04:53,913 --> 00:04:57,910
которые возвращают нам какой-то
там утилитарный итератор.

78
00:04:57,910 --> 00:05:01,355
Для того чтобы подчеркнуть,
что значение нас не интересует,

79
00:05:01,355 --> 00:05:04,540
мы используем переменную под
названием подчеркивание.

80
00:05:04,540 --> 00:05:08,860
Это достаточно распространенная
практика и не только в C++.

81
00:05:08,860 --> 00:05:13,950
Дальше у нас идет тело цикла — это то,
что мы хотим измерить.

82
00:05:13,950 --> 00:05:16,196
В первой функции, как мы видим,

83
00:05:16,196 --> 00:05:20,540
мы создаем объект типа std : :
string из строкового литерала.

84
00:05:20,540 --> 00:05:22,630
И после того как мы его создаем,

85
00:05:22,630 --> 00:05:27,130
мы делаем такой интересный вызов:
benchmark : : DoNotOptimize.

86
00:05:27,130 --> 00:05:31,951
Дело в том,
что само по себе создание объекта std : :

87
00:05:31,951 --> 00:05:36,738
string может не иметь некоторых
наблюдаемых эффектов на программу,

88
00:05:36,738 --> 00:05:40,607
потому что эта переменная у нас
нигде больше не используется.

89
00:05:40,607 --> 00:05:41,983
И поэтому, в принципе,

90
00:05:41,983 --> 00:05:45,730
компилятор может ее оптимизировать
и этот цикл вырезать целиком.

91
00:05:45,730 --> 00:05:50,353
Поэтому нужно убедиться, что компилятор
не попытается эту переменную вырезать.

92
00:05:50,353 --> 00:05:53,890
Для этого и используется такая
конструкция benchmark : : DoNotOptimize.

93
00:05:53,890 --> 00:05:58,863
Внутри у нее реализация специфичная
для каждого конкретного компилятора,

94
00:05:58,863 --> 00:06:03,391
и будем считать, что это просто некоторая
магия, которая делается для нас

95
00:06:03,391 --> 00:06:07,980
фреймворком и которая позволяет убедиться,
что компилятор не уберет лишнего.

96
00:06:07,980 --> 00:06:11,958
Соответственно вот сюда мы
передаем наш created_string.

97
00:06:11,958 --> 00:06:13,830
И у нас есть вторая функция.

98
00:06:13,830 --> 00:06:16,804
Во второй функции мы
делаем немного другое.

99
00:06:16,804 --> 00:06:21,670
Мы сначала создаем объект std : :
string из этого строкового литерала,

100
00:06:21,670 --> 00:06:26,860
а потом в цикле мы выполняем копирование.

101
00:06:26,860 --> 00:06:32,377
Это стандартный пример,
который предоставляет нам quickbench.com,

102
00:06:32,377 --> 00:06:36,115
для того чтобы познакомиться
с профилированием.

103
00:06:36,115 --> 00:06:40,496
Соответственно здесь мы видим, что у
нас в одном случае строчка создается из

104
00:06:40,496 --> 00:06:44,504
литерала, а во втором случае строчка
создается из std : : string.

105
00:06:44,504 --> 00:06:47,590
И действительно это может
занимать разное время.

106
00:06:47,590 --> 00:06:48,901
Давайте посмотрим.

107
00:06:48,901 --> 00:06:52,252
Дальше сервис предлагает
нам выбрать компиляторов,

108
00:06:52,252 --> 00:06:57,170
которых мы хотим проверить,
давайте выберем, допустим, gcc-8.2.

109
00:06:57,170 --> 00:07:02,253
Здесь представлены не самые последние
версии компиляторов, к сожалению,

110
00:07:02,253 --> 00:07:07,718
в этом сервисе, но для большинства
случаев нам этого должно быть достаточно.

111
00:07:07,718 --> 00:07:13,873
Выберем тоже C++ 17 и поставим оптимизацию
O2, поскольку она используется чаще всего.

112
00:07:13,873 --> 00:07:15,620
И теперь запустим бенчмарк.

113
00:07:15,620 --> 00:07:16,790
Окей.

114
00:07:16,790 --> 00:07:23,620
Собственно теперь мы видим результаты.

115
00:07:23,620 --> 00:07:26,963
У нас есть два столбца,
которые соответствуют нашим функциям,

116
00:07:26,963 --> 00:07:30,075
которые мы зарегистрировали
с помощью макроса Benchmark.

117
00:07:30,075 --> 00:07:33,137
И размер каждого столбца показывает время,

118
00:07:33,137 --> 00:07:36,530
в течение которого
выполнялась данная функция.

119
00:07:36,530 --> 00:07:38,462
Соответственно, мы можем видеть,

120
00:07:38,462 --> 00:07:43,090
что функция StringCreation выполнялась
несколько дольше, чем функция StringCopy.

121
00:07:43,090 --> 00:07:46,495
Таким образом, мы можем сделать вывод,

122
00:07:46,495 --> 00:07:50,981
что создание строки из литерала
занимает больше времени,

123
00:07:50,981 --> 00:07:55,170
чем создание строки с помощью
копирования из другой строки.

124
00:07:55,170 --> 00:07:59,320
По крайней мере на данном
компиляторе для данных настроек.

125
00:07:59,320 --> 00:08:06,215
Кроме того, следует обратить внимание
на то, в чем измеряется вот это время.

126
00:08:06,215 --> 00:08:08,339
Оно измеряется в некоторых единицах.

127
00:08:08,339 --> 00:08:13,325
Вы можете видеть по оси ординат,
в данном случае от 0 до 20.

128
00:08:13,325 --> 00:08:19,277
Одна единица соответствует
некоторым накладным расходам,

129
00:08:19,277 --> 00:08:22,358
которые предоставляет сам бенчмарк.

130
00:08:22,358 --> 00:08:26,320
Потому что если мы напишем цикл,
который не делает ничего,

131
00:08:26,320 --> 00:08:30,435
а просто крутится, это тоже займет
некоторое время, правильно?

132
00:08:30,435 --> 00:08:33,315
И вот здесь мы можем посмотреть,
сколько занимает такой цикл,

133
00:08:33,315 --> 00:08:34,550
который не делает ничего.

134
00:08:34,550 --> 00:08:38,044
Нажимаем вот эту галочку,
и это называется Noop.

135
00:08:38,044 --> 00:08:41,710
Noop значит no operation,
то есть не делаем ничего.

136
00:08:41,710 --> 00:08:47,224
Мы видим, что вот столько бы у нас работал
наш бенчмарк, если бы он не делал ничего.

137
00:08:47,224 --> 00:08:51,849
А наши функции, которые все-таки что-то
делали по сравнению с вот этим циклом,

138
00:08:51,849 --> 00:08:55,924
который не делает ничего,
занимают столько-то времени.

139
00:08:55,924 --> 00:08:59,168
Соответственно, 15 и 18 единиц.

140
00:08:59,168 --> 00:09:04,051
После того как мы запустили бенчмарк,
у нас адрес

141
00:09:04,051 --> 00:09:09,629
текущей страницы сгенерировался
в некоторую уникальную ссылку.

142
00:09:09,629 --> 00:09:14,482
Вот здесь мы ее можем посмотреть, вот
эту ссылку можем расшарить и кому-нибудь

143
00:09:14,482 --> 00:09:18,910
передать, чтобы они тоже посмотрели на
результаты работы нашего бенчмарка.

144
00:09:18,910 --> 00:09:19,616
Окей.

145
00:09:19,616 --> 00:09:24,329
И теперь последний сервис
Compiler Explorer.

146
00:09:24,329 --> 00:09:28,702
Этот сервис в отличие от предыдущих
двух ничего не запускает.

147
00:09:28,702 --> 00:09:32,120
Этот сервис позволяет
посмотреть ассемблерный код,

148
00:09:32,120 --> 00:09:36,885
который был сгенерирован
компилятором для нашей программы.

149
00:09:36,885 --> 00:09:42,154
Даже не для программы на самом деле,
здесь можно писать любой C++ код, потому

150
00:09:42,154 --> 00:09:47,782
что этот сервис ничего не будет запускать,
ему нужно просто скомпилировать.

151
00:09:47,782 --> 00:09:51,994
Давайте для примера напишем
вот такую простую функцию,

152
00:09:51,994 --> 00:09:57,271
которая принимает некоторое число
и возвращает это же самое число.

153
00:09:57,271 --> 00:10:00,454
Вот такая простая функция, и мы видим,

154
00:10:00,454 --> 00:10:05,922
что здесь компилятор сгенерировал
для нее некоторый ассемблерный код.

155
00:10:05,922 --> 00:10:08,199
Код состоит из одной инструкции.

156
00:10:08,199 --> 00:10:12,620
Не считая инструкцию ret, которая
означает вернуться из текущей функции.

157
00:10:12,620 --> 00:10:15,770
Эта инструкция mov, eax и edi.

158
00:10:15,770 --> 00:10:18,881
mov — это то же самое,
что оператор присваивания.

159
00:10:18,881 --> 00:10:20,437
Вот если навести мышкой,

160
00:10:20,437 --> 00:10:24,685
мы видим описание: копировать
второй оператор в первый оператор.

161
00:10:24,685 --> 00:10:27,950
То здесь эффективно
написано: eax присвоить edi.

162
00:10:27,950 --> 00:10:34,250
edi — это регистр, в котором
помещаются входные параметры функции.

163
00:10:34,250 --> 00:10:39,010
eax — это регистр, в котором
помещается входное значение функции.

164
00:10:39,010 --> 00:10:43,332
Соответственно здесь мы выходное
значение присваиваем входному значению,

165
00:10:43,332 --> 00:10:46,814
что вполне логично и соответствует тому,
что мы описали.

166
00:10:46,814 --> 00:10:52,471
Но у вас, наверное, в голове есть модель,
что входные параметры и выходные

167
00:10:52,471 --> 00:10:57,863
значения помещаются на стек,
и эта модель все еще верна, она работает.

168
00:10:57,863 --> 00:10:59,013
Но это модель.

169
00:10:59,013 --> 00:11:03,887
Компилятор, естественно, если он
может обойтись без обращения к стеку,

170
00:11:03,887 --> 00:11:07,780
ведь стек лежит в памяти,
обращение к памяти достаточно дорогое,

171
00:11:07,780 --> 00:11:10,191
он пробует все делать на регистрах.

172
00:11:10,191 --> 00:11:15,420
И поэтому на практике, после того как
компилятор отработал и оптимизировал,

173
00:11:15,420 --> 00:11:20,409
он оставляет только обращение к регистрам.

174
00:11:20,409 --> 00:11:21,200
Окей.

175
00:11:21,200 --> 00:11:24,187
Сейчас мы с вами
познакомились с сервисами,

176
00:11:24,187 --> 00:11:28,440
которые помогут нам разобраться
в неопределенном поведении.

177
00:11:28,440 --> 00:11:32,996
А в следующих видео мы
перейдем к его изучению.