1
00:00:00,000 --> 00:00:05,405
[БЕЗ_ЗВУКА] В предыдущем

2
00:00:05,405 --> 00:00:10,382
видео мы с вами посмотрели,
как неопределенное приведение

3
00:00:10,382 --> 00:00:14,705
в программе может привести к очень
неожиданным результатам ее работы.

4
00:00:14,705 --> 00:00:17,660
Однако код, который мы рассмотрели,

5
00:00:17,660 --> 00:00:22,977
вряд ли можно считать хорошим
примером современного C++.

6
00:00:22,977 --> 00:00:27,248
Поэтому сейчас подумайте, как можно
было бы решить проблему с неопределенным

7
00:00:27,248 --> 00:00:29,740
поведением, которая у нас
была в той программе,

8
00:00:29,740 --> 00:00:33,640
с использованием более
правильного идиоматического C++.

9
00:00:33,640 --> 00:00:36,467
Как вы, наверное, могли догадаться,

10
00:00:36,467 --> 00:00:40,884
основная проблема в предыдущей
программе заключается в том,

11
00:00:40,884 --> 00:00:45,300
что у нас размер массива хранился
отдельно от самого массива.

12
00:00:45,300 --> 00:00:52,177
То есть у нас сущность одна — массив, а
переменных две — сам массив и его размер.

13
00:00:52,177 --> 00:00:56,700
Но это на самом деле характерно для C,
но не для C++,

14
00:00:56,700 --> 00:01:01,450
где мы для массива и контейнера можем
непосредственно спросить его размер.

15
00:01:01,450 --> 00:01:05,996
Соответственно если бы в той
программе мы использовали

16
00:01:05,996 --> 00:01:11,755
идиоматически правильный C++, то проблема
бы у нас, в принципе, не возникла.

17
00:01:11,755 --> 00:01:16,220
И действительно, во многих случаях
использование наиболее правильного

18
00:01:16,220 --> 00:01:20,981
современного C++ позволяет избегать
неопределенного поведения, но, конечно,

19
00:01:20,981 --> 00:01:21,652
не всегда.

20
00:01:21,652 --> 00:01:26,542
Давайте сейчас рассмотрим пример,
когда неопределенное поведение возникнет,

21
00:01:26,542 --> 00:01:30,140
даже несмотря на то,
что мы вроде бы все делаем правильно.

22
00:01:30,140 --> 00:01:33,370
Давайте напишем небольшую программу,

23
00:01:33,370 --> 00:01:39,270
которую мы будем использовать для
нашей системы домашней сигнализации.

24
00:01:39,270 --> 00:01:44,411
Предположим, что наша система может
по-разному реагировать на внешний

25
00:01:44,411 --> 00:01:51,859
раздражитель, и ее реакция определяется
некоторым указателем на функцию.

26
00:01:51,859 --> 00:01:56,560
Заведем тип указатель на функцию.

27
00:01:56,560 --> 00:02:00,211
Вот так в C++ записывается
тип указатель на функцию,

28
00:02:00,211 --> 00:02:02,570
который ничего не принимает
и возвращает void.

29
00:02:02,570 --> 00:02:07,408
И соответственно определим
некоторый текущий обработчик,

30
00:02:07,408 --> 00:02:10,038
то есть функция,
которую мы будем вызывать,

31
00:02:10,038 --> 00:02:13,370
если у нас в данный момент
возникает какой-то раздражитель.

32
00:02:13,370 --> 00:02:18,010
Объявим ее для простоты просто
глобальной переменной и инициализируем

33
00:02:18,010 --> 00:02:19,460
нулевым указателем.

34
00:02:19,460 --> 00:02:20,158
Хорошо.

35
00:02:20,158 --> 00:02:24,440
Теперь давайте напишем один
из возможных обработчиков.

36
00:02:24,440 --> 00:02:28,422
Скажем, некоторый агрессивный обработчик.

37
00:02:28,422 --> 00:02:31,490
Назовем его HostileAlarm.

38
00:02:31,490 --> 00:02:37,260
И он будет заниматься тем,
что будет спускать

39
00:02:37,260 --> 00:02:42,585
на атакующего велосираптора.

40
00:02:42,585 --> 00:02:47,910
Окей.

41
00:02:47,910 --> 00:02:52,150
Пока это у нас будет единственный
обработчик, который мы используем.

42
00:02:52,150 --> 00:02:55,741
Дальше мы, пожалуй, не хотим,

43
00:02:55,741 --> 00:03:01,550
чтобы кто угодно из любой части нашей
программы мог выставлять этот обработчик.

44
00:03:01,550 --> 00:03:04,611
Вместо этого мы заведем некоторую функцию,

45
00:03:04,611 --> 00:03:09,081
которую нужно будет вызывать и которая
сама выставит обработчик и, возможно,

46
00:03:09,081 --> 00:03:11,710
произведет некоторое
дополнительное логирование.

47
00:03:11,710 --> 00:03:17,690
Давайте заведем такую
функцию SetHostileAlarm,

48
00:03:17,690 --> 00:03:24,268
которая присвоит нашему
обработчику HostileAlarm и сделает

49
00:03:24,268 --> 00:03:30,043
дополнительное логирование: Hostile

50
00:03:30,043 --> 00:03:34,505
alarm set.

51
00:03:34,505 --> 00:03:35,253
Хорошо.

52
00:03:35,253 --> 00:03:40,399
И теперь, поскольку мы собираемся
для выставления обработчика

53
00:03:40,399 --> 00:03:45,394
использовать только такого рода функции,
сделаем так,

54
00:03:45,394 --> 00:03:49,863
чтобы из других мест к обработчику
обратиться было нельзя.

55
00:03:49,863 --> 00:03:56,710
Для этого мы поместим его в так
называемое анонимное пространство имен.

56
00:03:56,710 --> 00:04:02,722
Анонимное пространство имен — это важный
частный случай пространства имен.

57
00:04:02,722 --> 00:04:07,568
Сущности, которые в нем лежат, для того
чтобы к ним обратиться из внешнего

58
00:04:07,568 --> 00:04:12,592
пространства имен, не нужно никакой
дополнительной квалификации, как бы мы

59
00:04:12,592 --> 00:04:18,145
писали в случае с обычным пространством
имен — его имя и четвероточие.

60
00:04:18,145 --> 00:04:22,783
Но при этом к этим сущностям можно
обращаться только из текущего модуля

61
00:04:22,783 --> 00:04:23,680
трансляции.

62
00:04:23,680 --> 00:04:26,833
В других модулях трансляции
они просто не доступны.

63
00:04:26,833 --> 00:04:30,503
И это ровно то, что нам нужно, ведь мы
собираемся использовать специальные

64
00:04:30,503 --> 00:04:33,340
функции, для того чтобы
обращаться к этому обработчику.

65
00:04:33,340 --> 00:04:38,480
И давайте теперь заведем
некоторую функцию Test,

66
00:04:38,480 --> 00:04:43,104
которая будет заниматься тем,

67
00:04:43,104 --> 00:04:48,650
что вызывать текущий обработчик

68
00:04:48,650 --> 00:04:52,665
и выводить сообщение о том,
что тест прошел успешно.

69
00:04:52,665 --> 00:04:56,680
Test succeeded.

70
00:04:56,680 --> 00:05:01,094
Здесь, поскольку у нас обработчик
может быть нулевым, нам, конечно,

71
00:05:01,094 --> 00:05:05,599
следует проверить, не является ли
обработчик в данный момент нулевым.

72
00:05:05,599 --> 00:05:08,090
Но, допустим, мы забыли это сделать.

73
00:05:08,090 --> 00:05:13,175
И у нашей функции мы только вызовем тест.

74
00:05:13,175 --> 00:05:14,217
Вот так.

75
00:05:14,217 --> 00:05:19,188
Мы написали такую программу и сейчас хотим

76
00:05:19,188 --> 00:05:24,451
залить ее на прошивку нашей сигнализации,

77
00:05:24,451 --> 00:05:27,989
просто для того чтобы убедиться,
что она компилируется,

78
00:05:27,989 --> 00:05:32,602
запускается и выводит сообщение о том,
что тест пройден успешно.

79
00:05:32,602 --> 00:05:37,724
Мы не вызываем функции для того,
чтобы установить какой-то обработчик.

80
00:05:37,724 --> 00:05:40,520
Поэтому мы не ожидаем,
что что-то вообще произойдет.

81
00:05:40,520 --> 00:05:43,829
Мы просто хотим увидеть,
что тест выполнен успешно.

82
00:05:43,829 --> 00:05:46,070
Давайте запустим эту программу.

83
00:05:46,070 --> 00:05:51,253
[БЕЗ_ЗВУКА] Так,

84
00:05:51,253 --> 00:05:54,650
здесь лишние скобки.

85
00:05:54,650 --> 00:06:02,040
Окей.

86
00:06:02,040 --> 00:06:05,417
Смотрите, тест у нас,
конечно, прошел успешно.

87
00:06:05,417 --> 00:06:09,430
Но вместе с тем оказалось,
что мы выпустили велосираптора.

88
00:06:09,430 --> 00:06:10,768
И он нас атаковал.

89
00:06:10,768 --> 00:06:14,460
И в принципе,
не самое хорошее завершение вечера.

90
00:06:14,460 --> 00:06:17,283
Помните, я вас предупреждал,
что такое может произойти?

91
00:06:17,283 --> 00:06:19,840
Нужно быть аккуратнее с
неопределенным поведением.

92
00:06:19,840 --> 00:06:25,968
Но вообще это достаточно неожиданное
поведение, потому что мы

93
00:06:25,968 --> 00:06:30,600
же нигде не устанавливали тот обработчик,
который выпускает велосираптора.

94
00:06:30,600 --> 00:06:34,114
Кроме того, вот у нас есть функция,
которая его устанавливает,

95
00:06:34,114 --> 00:06:36,125
она выводит некоторое логирование.

96
00:06:36,125 --> 00:06:38,590
Этого логирования мы в
выводе не обнаружили.

97
00:06:38,590 --> 00:06:40,102
Что же произошло?

98
00:06:40,102 --> 00:06:46,555
Давайте снова притворимся компилятором
и попробуем понять, как он рассуждал.

99
00:06:46,555 --> 00:06:48,589
Вот он видит функцию Test.

100
00:06:48,589 --> 00:06:52,220
Функцией Test вызывается
текущий обработчик.

101
00:06:52,220 --> 00:06:57,047
Поскольку этот обработчик
не проверяется на ноль,

102
00:06:57,047 --> 00:07:00,669
то компилятор предполагает,
что этот обработчик не является нулевым.

103
00:07:00,669 --> 00:07:04,898
Потому что если бы он был нулевым,
то это было бы неопределенное поведение.

104
00:07:04,898 --> 00:07:09,240
Но ведь программист же обещал, что
неопределенного поведения не произойдет.

105
00:07:09,240 --> 00:07:09,874
Правильно?

106
00:07:09,874 --> 00:07:10,508
Правильно.

107
00:07:10,508 --> 00:07:15,220
Ну, компилятор нам верит и поэтому
считает, что здесь указатель не нулевой.

108
00:07:15,220 --> 00:07:19,130
Тогда он пытается понять,
хорошо, а где вообще в программе

109
00:07:19,130 --> 00:07:23,540
этому указателю присваивается
какое-либо ненулевое значение?

110
00:07:23,540 --> 00:07:26,889
А такое место у нас только одно — вот оно.

111
00:07:26,889 --> 00:07:31,971
Здесь обработчику присваивается
функция HostileAlarm.

112
00:07:31,971 --> 00:07:34,810
И компилятор думает: ладно,

113
00:07:34,810 --> 00:07:38,427
если здесь обработчик ненулевой и
единственное ненулевое значение,

114
00:07:38,427 --> 00:07:43,699
которое ему присваивается, оно находится
вот здесь, ему присваивается HostileAlarm.

115
00:07:43,699 --> 00:07:47,680
Значит, и там он равен HostileAlarm
и собственно его вызывает.

116
00:07:47,680 --> 00:07:50,830
А то,
что это присваивание происходит в функции,

117
00:07:50,830 --> 00:07:56,003
которая на самом деле ни разу не
вызывается, извините, так уж получилось.

118
00:07:56,003 --> 00:07:58,570
Не нужно обманывать компилятор.

119
00:07:58,570 --> 00:08:00,849
Смотрите, что получилось.

120
00:08:00,849 --> 00:08:05,325
Что у нас здесь в этой программе
мы на самом деле используем

121
00:08:05,325 --> 00:08:07,787
только идиоматический C++.

122
00:08:07,787 --> 00:08:12,080
Но несмотря на это,
у нас все равно возникла ситуация,

123
00:08:12,080 --> 00:08:14,988
в которой неопределенное поведение.

124
00:08:14,988 --> 00:08:19,396
В следующем видео мы посмотрим
на еще один похожий пример.