1
00:00:05,350 --> 00:00:08,380
В предыдущем видео мы с вами увидели,

2
00:00:08,380 --> 00:00:12,640
как неопределенное поведение позволило убрать накладные расходы, и,

3
00:00:12,640 --> 00:00:14,650
кроме того, убедились, что без

4
00:00:14,650 --> 00:00:19,940
неопределенного поведения убрать эти накладные расходы было бы вообще невозможно.

5
00:00:19,940 --> 00:00:21,430
В этом видео мы посмотрим,

6
00:00:21,430 --> 00:00:25,900
как неопределенное поведение помогает делать некоторые оптимизации.

7
00:00:25,900 --> 00:00:29,225
Для этого давайте вспомним пару фактов из С++.

8
00:00:29,225 --> 00:00:34,720
Во-первых, переполнение знакового целого приводит к неопределенному поведению.

9
00:00:34,720 --> 00:00:36,030
Это значит, что если мы, скажем,

10
00:00:36,030 --> 00:00:40,070
возьмем максимальное значение int и прибавим к нему единичку,

11
00:00:40,070 --> 00:00:44,350
то поведение программы станет не определено.

12
00:00:44,350 --> 00:00:49,330
Второе: переполнение беззнакового целого строго определено.

13
00:00:49,330 --> 00:00:51,970
Если у нас выражение,

14
00:00:51,970 --> 00:00:54,700
в котором беззнаковое целое переполняется,

15
00:00:54,700 --> 00:01:00,070
то результат этого выражения всегда берется по модулю 2 в степени N,

16
00:01:00,070 --> 00:01:03,070
где N — это количество бит в данном типе.

17
00:01:03,070 --> 00:01:04,345
То есть, если мы, скажем,

18
00:01:04,345 --> 00:01:08,740
к максимальному значению типа unsigned int прибавим единичку,

19
00:01:08,740 --> 00:01:13,665
то нам гарантируется, что результат будет равен нулю.

20
00:01:13,665 --> 00:01:20,050
Теперь давайте сформулируем один важный принцип.

21
00:01:20,050 --> 00:01:22,960
Чисто формально при возникновении

22
00:01:22,960 --> 00:01:27,070
неопределенного поведения компилятор может делать все что угодно.

23
00:01:27,070 --> 00:01:29,800
Наверное, все-таки компилятор не будет заниматься тем,

24
00:01:29,800 --> 00:01:32,785
что делать какую-то совсем уж полную ерунду.

25
00:01:32,785 --> 00:01:34,660
У компиляторов, в принципе,

26
00:01:34,660 --> 00:01:39,470
ровно одна цель — сгенерировать как можно более эффективный код.

27
00:01:39,470 --> 00:01:47,005
Поэтому на практике компилятор придерживается следующей идеи: компилятор предполагает,

28
00:01:47,005 --> 00:01:51,860
что неопределенного поведения вообще не может произойти,

29
00:01:51,860 --> 00:01:56,619
то есть это своего рода договоренность между программистом и компилятором.

30
00:01:56,619 --> 00:02:00,235
Программист обещает компилятору, что он будет писать код,

31
00:02:00,235 --> 00:02:03,170
который не содержит неопределенного поведения.

32
00:02:03,170 --> 00:02:07,420
Это примерно как у нас было в случае с доступом

33
00:02:07,420 --> 00:02:09,580
к элементу массива с помощью квадратных

34
00:02:09,580 --> 00:02:13,075
скобочек: компилятор не проверяет выход за границы,

35
00:02:13,075 --> 00:02:17,620
компилятор доверяет программисту, что он уже сам проверил,

36
00:02:17,620 --> 00:02:21,080
что этот элемент лежит внутри массива.

37
00:02:21,080 --> 00:02:24,090
Но если не проверил, все очень плохо.

38
00:02:24,090 --> 00:02:27,100
ОК, это тоже запомнили.

39
00:02:27,100 --> 00:02:33,180
Теперь давайте посмотрим на следующую пару достаточно странных функций.

40
00:02:33,180 --> 00:02:36,955
Вообще, когда происходит обсуждение неопределенного поведения,

41
00:02:36,955 --> 00:02:40,830
довольно часто можно встретить странные фрагменты кода.

42
00:02:40,830 --> 00:02:42,370
Вы не переживайте, впереди,

43
00:02:42,370 --> 00:02:44,320
в следующих видео нас, в принципе,

44
00:02:44,320 --> 00:02:47,495
ждут более жизненные примеры.

45
00:02:47,495 --> 00:02:49,969
Кроме того, подобного рода выражения

46
00:02:49,969 --> 00:02:55,240
действительно могут встретиться в реальных программах и часто встречаются,

47
00:02:55,240 --> 00:02:58,420
после того как над программами поработает оптимизатор.

48
00:02:58,420 --> 00:03:00,220
Что у нас здесь происходит?

49
00:03:00,220 --> 00:03:02,230
Мы берем n и спрашиваем,

50
00:03:02,230 --> 00:03:04,500
меньше ли n, чем n плюс 1.

51
00:03:04,500 --> 00:03:08,140
Странно, вроде бы должен быть всегда меньше,

52
00:03:08,140 --> 00:03:13,840
то есть функции вроде бы всегда должны возвращать true. Вроде бы.

53
00:03:13,840 --> 00:03:15,765
Давайте посмотрим на эти функции.

54
00:03:15,765 --> 00:03:18,360
Давайте вспомним, что мы только что узнали, а именно,

55
00:03:18,360 --> 00:03:21,550
что переполнение знакового целого — это неопределенное поведение,

56
00:03:21,550 --> 00:03:24,750
переполнение беззнакового целого берет результат по модулю 2

57
00:03:24,750 --> 00:03:28,205
в степени N и что компилятор предполагает,

58
00:03:28,205 --> 00:03:33,060
что неопределенного поведения вообще не должно происходить.

59
00:03:33,060 --> 00:03:35,590
И запомнив все это,

60
00:03:35,590 --> 00:03:40,645
давайте подумаем, какой будет наиболее вероятный вывод следующей программы?

61
00:03:40,645 --> 00:03:44,010
Хорошо, давайте посмотрим, что эта программа выводит.

62
00:03:44,010 --> 00:03:48,500
Для этого мы ее запустим и воспользуемся снова сервисом OneBox.

63
00:03:48,500 --> 00:03:52,400
Вот мы составили программу и запускаем ее.

64
00:03:53,480 --> 00:03:56,315
Видим результат true и false.

65
00:03:56,315 --> 00:03:59,120
Обратите внимание, что, в принципе,

66
00:03:59,120 --> 00:04:04,090
какой-то другой компилятор или какие-то другие настройки могли дать другой результат,

67
00:04:04,090 --> 00:04:05,930
потому что, как вы могли заметить,

68
00:04:05,930 --> 00:04:08,560
в этой программе есть неопределенное поведение,

69
00:04:08,560 --> 00:04:12,530
но мы вас спрашивали именно про наиболее вероятный результат,

70
00:04:12,530 --> 00:04:16,390
и этот результат действительно наиболее вероятен.

71
00:04:16,390 --> 00:04:18,820
Вскоре мы поймем, почему.

72
00:04:18,820 --> 00:04:21,320
Итак, что у нас здесь происходит? True и false.

73
00:04:21,320 --> 00:04:25,670
Сначала у нас выводится значение для знакового инта,

74
00:04:25,670 --> 00:04:28,295
и оно равно true, то есть, действительно,

75
00:04:28,295 --> 00:04:30,260
в случае знакового у нас получается,

76
00:04:30,260 --> 00:04:32,190
что n меньше n плюс 1.

77
00:04:32,190 --> 00:04:35,990
Все логично. А вот в случае беззнакового у нас получается,

78
00:04:35,990 --> 00:04:38,510
что n не меньше n плюс 1,

79
00:04:38,510 --> 00:04:42,305
что может быть немного неожиданно.

80
00:04:42,305 --> 00:04:45,035
Здесь будет полезно посмотреть,

81
00:04:45,035 --> 00:04:49,715
что именно компилятор генерирует для этих двух функций.

82
00:04:49,715 --> 00:04:54,745
Для этого давайте воспользуемся еще одним замечательным сервисом Compiler Explorer.

83
00:04:54,745 --> 00:04:58,520
Ставим в него две эти наши функции,

84
00:04:58,520 --> 00:05:00,785
а он покажет нам ассемблерный код,

85
00:05:00,785 --> 00:05:03,825
который он для них сгенерировал.

86
00:05:03,825 --> 00:05:08,645
Вот функция TestSigned, которая работает со знаковым целым.

87
00:05:08,645 --> 00:05:13,335
Она состоит всего из одной инструкции: mov eax, 1.

88
00:05:13,335 --> 00:05:15,440
Mov — это то же самое, что операция присваивания,

89
00:05:15,440 --> 00:05:17,430
eax — это регистр,

90
00:05:17,430 --> 00:05:20,685
в котором у нас возвращается значение из функции,

91
00:05:20,685 --> 00:05:23,005
и мы присваиваем ему единичку.

92
00:05:23,005 --> 00:05:24,560
Поскольку функция возвращает bool,

93
00:05:24,560 --> 00:05:27,980
эта единичка — это эффективно true, то есть мы видим,

94
00:05:27,980 --> 00:05:32,150
что функция TestSigned всегда возвращает true, независимо от того,

95
00:05:32,150 --> 00:05:34,230
что ей подали на вход,

96
00:05:34,230 --> 00:05:37,250
что, в принципе, наверное, достаточно логично.

97
00:05:37,250 --> 00:05:40,650
Она просто считает, что n всегда меньше n плюс 1.

98
00:05:40,650 --> 00:05:44,240
А если мы посмотрим на функцию TestUnsigned, то мы видим,

99
00:05:44,240 --> 00:05:48,980
что в ней происходит уже какое-то сравнение (cmp — это операция

100
00:05:48,980 --> 00:05:55,040
сравнения) и после этого это сравнение каким-то образом влияет на результат.

101
00:05:55,040 --> 00:05:58,130
То есть она как минимум зависит от параметра (вот,

102
00:05:58,130 --> 00:05:59,850
edi — это регистр,

103
00:05:59,850 --> 00:06:04,084
в котором передается входной параметр) и, вероятно,

104
00:06:04,084 --> 00:06:06,510
может возвращать не только true,

105
00:06:06,510 --> 00:06:09,720
что мы, собственно, и видели при запуске.

106
00:06:09,720 --> 00:06:14,405
Понятно, почему функции отрабатывают так, как они отрабатывают.

107
00:06:14,405 --> 00:06:18,725
Но почему компилятор сгенерировал именно такой код?

108
00:06:18,725 --> 00:06:21,155
Давайте посмотрим.

109
00:06:21,155 --> 00:06:23,410
Допустим, я компилятор,

110
00:06:23,410 --> 00:06:26,265
я смотрю на функцию TestSigned,

111
00:06:26,265 --> 00:06:30,200
которая принимает на вход некоторое число.

112
00:06:30,200 --> 00:06:32,455
Я рассуждаю так: в принципе,

113
00:06:32,455 --> 00:06:35,320
может быть только две возможные ситуации.

114
00:06:35,320 --> 00:06:37,860
Первая ситуация: если n,

115
00:06:37,860 --> 00:06:39,895
которое нам передали, меньше,

116
00:06:39,895 --> 00:06:42,025
чем максимальное возможное значение,

117
00:06:42,025 --> 00:06:45,840
в этом случае я считаю n плюс 1, и оно влезает в int.

118
00:06:45,840 --> 00:06:48,580
Хорошо, тогда я могу предположить,

119
00:06:48,580 --> 00:06:50,965
что n всегда меньше n плюс 1,

120
00:06:50,965 --> 00:06:52,389
это вроде бы логично,

121
00:06:52,389 --> 00:06:54,680
и значит, я спокойно могу вернуть true.

122
00:06:54,680 --> 00:07:00,970
Хорошо. Вторая ситуация: если n равно максимальному значению.

123
00:07:00,970 --> 00:07:08,810
В этом случае вычисление n плюс 1 приведет к переполнению знакового целого,

124
00:07:08,810 --> 00:07:10,530
а это — неопределенное поведение,

125
00:07:10,530 --> 00:07:14,875
то есть в этом случае я как компилятор могу делать все, что захочу.

126
00:07:14,875 --> 00:07:17,590
Захочу — могу вывести что-то в выходной поток,

127
00:07:17,590 --> 00:07:19,415
захочу — отформатирую диск,

128
00:07:19,415 --> 00:07:21,900
захочу — выпущу велоцираптора.

129
00:07:21,900 --> 00:07:23,900
Но что бы сделать?

130
00:07:23,900 --> 00:07:27,590
Давайте я, допустим, верну true. Я же могу вернуть true.

131
00:07:27,590 --> 00:07:32,545
И получается, что во всех ситуациях, которые я рассмотрел,

132
00:07:32,545 --> 00:07:36,160
я возвращаю true, то есть я просто могу сгенерировать функцию,

133
00:07:36,160 --> 00:07:40,505
которая всегда возвращает true, и буду прав.

134
00:07:40,505 --> 00:07:43,420
Рассуждать можно по-другому.

135
00:07:43,420 --> 00:07:46,840
Смотрите, если n меньше максимального значения,

136
00:07:46,840 --> 00:07:50,185
все то же самое; если n равно максимальному значению,

137
00:07:50,185 --> 00:07:53,815
n плюс 1 приводит к переполнению, поведение неопределено.

138
00:07:53,815 --> 00:07:56,780
Но ведь мы же договорились с программистом.

139
00:07:56,780 --> 00:07:58,780
Программист же мне обещал,

140
00:07:58,780 --> 00:08:00,235
что он пишет программы,

141
00:08:00,235 --> 00:08:03,670
в которых неопределенного поведения не возникает,

142
00:08:03,670 --> 00:08:05,485
поэтому я могу быть уверен,

143
00:08:05,485 --> 00:08:10,120
что такой ситуации на самом деле никогда не произойдет.

144
00:08:10,120 --> 00:08:13,305
Поэтому эту ситуацию я просто не рассматриваю.

145
00:08:13,305 --> 00:08:15,670
Единственная ситуация, которая у меня остается,

146
00:08:15,670 --> 00:08:18,800
это та, в которой я возвращаю true.

147
00:08:18,800 --> 00:08:20,950
С этим вроде бы разобрались.

148
00:08:20,950 --> 00:08:26,185
Давайте теперь посмотрим, как компилятор себя ведет для беззнакового числа.

149
00:08:26,185 --> 00:08:28,695
Функция TestUnsigned.

150
00:08:28,695 --> 00:08:34,160
Рассуждения начинаются точно так же: возможно всего две ситуации.

151
00:08:34,160 --> 00:08:38,770
Первая: переданное число n меньше максимального значения,

152
00:08:38,770 --> 00:08:41,955
тогда n плюс 1 влезает в unsigned int.

153
00:08:41,955 --> 00:08:43,985
Я могу провести вычисления,

154
00:08:43,985 --> 00:08:45,580
сравню и, конечно же,

155
00:08:45,580 --> 00:08:48,130
я увижу, что n меньше, чем n плюс 1.

156
00:08:48,130 --> 00:08:51,405
В этом случае я, как честный компилятор, верну true.

157
00:08:51,405 --> 00:08:55,545
И вторая ситуация: n равно максимальному значению.

158
00:08:55,545 --> 00:09:00,250
Но в этом случае n плюс 1 по определению будет равно нулю,

159
00:09:00,250 --> 00:09:03,095
потому что у нас произойдет переполнение беззнакового,

160
00:09:03,095 --> 00:09:07,715
и в этом случае у нас будет уже сравнение максимального значения с нулем.

161
00:09:07,715 --> 00:09:10,180
Максимальное значение не меньше нуля,

162
00:09:10,180 --> 00:09:13,955
поэтому здесь мне придется вернуть false.

163
00:09:13,955 --> 00:09:16,690
Хорошо. Теперь мы разобрались,

164
00:09:16,690 --> 00:09:20,010
как компилятор рассуждает в первом и во втором случае.

165
00:09:20,010 --> 00:09:23,979
И давайте, поскольку мы изначально говорили об оптимизациях,

166
00:09:23,979 --> 00:09:26,140
а оптимизации влияют на время работы,

167
00:09:26,140 --> 00:09:29,140
убедимся, что функция, которая работает со знаковым

168
00:09:29,140 --> 00:09:32,838
числом, действительно отрабатывает быстрее.

169
00:09:32,838 --> 00:09:38,750
Для этого снова откроем наш сервис Benchmark,

170
00:09:38,750 --> 00:09:42,200
вставим в него наши функции TestSigned,

171
00:09:42,200 --> 00:09:48,604
TestUnsigned и подготовим для них две функции Benchmark.

172
00:09:48,604 --> 00:09:52,185
Скажем, CheckSigned будет проверять.

173
00:09:52,185 --> 00:09:56,610
Это нам уже не нужно.

174
00:09:56,610 --> 00:10:00,770
Давайте будем вызывать TestSigned и в

175
00:10:00,770 --> 00:10:05,245
качестве входа будем передавать ему номер текущей итерации.

176
00:10:05,245 --> 00:10:07,820
Чтобы получить номер текущей итерации,

177
00:10:07,820 --> 00:10:10,310
можно спросить у state method iterations.

178
00:10:10,900 --> 00:10:14,590
Сохраним результат и скажем Benchmark,

179
00:10:14,590 --> 00:10:18,410
чтобы этот результат он нам не оптимизировал.

180
00:10:19,060 --> 00:10:30,500
Зарегистрируем эту функцию и дальше сделаем то же самое для Unsigned.

181
00:10:30,500 --> 00:10:34,663
CheckUnsigned здесь, здесь TestUnsigned и CheckUnsigned.

182
00:10:34,663 --> 00:10:40,940
Хорошо, давайте запустим наш Benchmark.

183
00:10:40,940 --> 00:10:43,580
Итак, как мы можем видеть, функция,

184
00:10:43,580 --> 00:10:45,580
которая работает со знаковыми целыми,

185
00:10:45,580 --> 00:10:47,450
действительно, отработала быстрее, чем та,

186
00:10:47,450 --> 00:10:49,410
которая работает с беззнаковыми,

187
00:10:49,410 --> 00:10:50,840
что логично, ведь функция,

188
00:10:50,840 --> 00:10:52,330
которая работает со знаковыми,

189
00:10:52,330 --> 00:10:55,465
вообще, по сути, ничего не делает.

190
00:10:55,465 --> 00:10:58,790
Итак, в этом видео мы с вами посмотрели,

191
00:10:58,790 --> 00:11:03,205
как неопределенное поведение позволило сгенерировать более эффективный код,

192
00:11:03,205 --> 00:11:08,955
причем так получилось, что оно даже привело к более ожидаемому результату.

193
00:11:08,955 --> 00:11:12,590
Такая ситуация, на самом деле, встречается достаточно часто.

194
00:11:12,590 --> 00:11:17,015
Компилятор, действительно, старается эксплуатировать неопределенное поведение,

195
00:11:17,015 --> 00:11:20,945
в основном предполагая, что неопределенного поведения не происходит,

196
00:11:20,945 --> 00:11:25,005
для того чтобы генерировать более эффективный код.

197
00:11:25,005 --> 00:11:26,870
На этом хорошие новости,

198
00:11:26,870 --> 00:11:30,345
которые у нас связаны с неопределенным поведением, заканчиваются,

199
00:11:30,345 --> 00:11:36,870
и следующих видео мы посмотрим на неожиданные последствия неопределенного поведения.