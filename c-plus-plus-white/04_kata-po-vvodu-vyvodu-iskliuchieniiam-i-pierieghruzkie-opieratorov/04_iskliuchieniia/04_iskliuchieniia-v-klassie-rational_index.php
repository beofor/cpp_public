<!DOCTYPE html>
<html lang="en" dir="ltr" class="client-nojs">
<head>
<title>std::domain_error - cppreference.com</title>
<meta charset="UTF-8" />
<meta name="generator" content="MediaWiki 1.21.2" />
<link rel="alternate" type="application/x-wiki" title="Edit" href="/mwiki/index.php?title=cpp/error/domain_error&amp;action=edit" />
<link rel="edit" title="Edit" href="/mwiki/index.php?title=cpp/error/domain_error&amp;action=edit" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/mwiki/opensearch_desc.php" title="cppreference.com (en)" />
<link rel="EditURI" type="application/rsd+xml" href="https://en.cppreference.com/mwiki/api.php?action=rsd" />
<link rel="alternate" type="application/atom+xml" title="cppreference.com Atom feed" href="/mwiki/index.php?title=Special:RecentChanges&amp;feed=atom" />
<link rel="stylesheet" href="https://en.cppreference.com/mwiki/load.php?debug=false&amp;lang=en&amp;modules=ext.gadget.ColiruCompiler%2CMathJax%7Cext.rtlcite%7Cmediawiki.legacy.commonPrint%2Cshared%7Cskins.cppreference2&amp;only=styles&amp;skin=cppreference2&amp;*" />
<meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="https://en.cppreference.com/mwiki/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=styles&amp;skin=cppreference2&amp;*" />
<style>a:lang(ar),a:lang(ckb),a:lang(fa),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}#toc{display:none}.editsection{display:none}
/* cache key: mwiki1-mwiki_en_:resourceloader:filter:minify-css:7:472787eddcf4605d11de8c7ef047234f */</style>

<script src="https://en.cppreference.com/mwiki/load.php?debug=false&amp;lang=en&amp;modules=startup&amp;only=scripts&amp;skin=cppreference2&amp;*"></script>
<script>if(window.mw){
mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"cpp/error/domain_error","wgTitle":"cpp/error/domain error","wgCurRevisionId":118294,"wgArticleId":2086,"wgIsArticle":true,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"cpp/error/domain_error","wgRestrictionEdit":[],"wgRestrictionMove":[]});
}</script><script>if(window.mw){
mw.loader.implement("user.options",function(){mw.user.options.set({"ccmeonemails":0,"cols":80,"date":"default","diffonly":0,"disablemail":0,"disablesuggest":0,"editfont":"default","editondblclick":0,"editsection":0,"editsectiononrightclick":0,"enotifminoredits":0,"enotifrevealaddr":0,"enotifusertalkpages":1,"enotifwatchlistpages":0,"extendwatchlist":0,"externaldiff":0,"externaleditor":0,"fancysig":0,"forceeditsummary":0,"gender":"unknown","hideminor":0,"hidepatrolled":0,"imagesize":2,"justify":0,"math":1,"minordefault":0,"newpageshidepatrolled":0,"nocache":0,"noconvertlink":0,"norollbackdiff":0,"numberheadings":0,"previewonfirst":0,"previewontop":1,"quickbar":5,"rcdays":7,"rclimit":50,"rememberpassword":0,"rows":25,"searchlimit":20,"showhiddencats":0,"showjumplinks":1,"shownumberswatching":1,"showtoc":0,"showtoolbar":1,"skin":"cppreference2","stubthreshold":0,"thumbsize":2,"underline":2,"uselivepreview":0,"usenewrc":0,"watchcreations":0,"watchdefault":0,"watchdeletion":0,
"watchlistdays":3,"watchlisthideanons":0,"watchlisthidebots":0,"watchlisthideliu":0,"watchlisthideminor":0,"watchlisthideown":0,"watchlisthidepatrolled":0,"watchmoves":0,"wllimit":250,"variant":"en","language":"en","searchNs0":true,"searchNs1":false,"searchNs2":false,"searchNs3":false,"searchNs4":false,"searchNs5":false,"searchNs6":false,"searchNs7":false,"searchNs8":false,"searchNs9":false,"searchNs10":false,"searchNs11":false,"searchNs12":false,"searchNs13":false,"searchNs14":false,"searchNs15":false,"gadget-ColiruCompiler":1,"gadget-MathJax":1});;},{},{});mw.loader.implement("user.tokens",function(){mw.user.tokens.set({"editToken":"+\\","patrolToken":false,"watchToken":false});;},{},{});
/* cache key: mwiki1-mwiki_en_:resourceloader:filter:minify-js:7:9f05c6caceb9bb1a482b6cebd4c5a330 */
}</script>
<script>if(window.mw){
mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax"]);
}</script>
<style type="text/css">/*<![CDATA[*/
.source-cpp {line-height: normal;}
.source-cpp li, .source-cpp pre {
	line-height: normal; border: 0px none white;
}
/**
 * GeSHi Dynamically Generated Stylesheet
 * --------------------------------------
 * Dynamically generated stylesheet for cpp
 * CSS class: source-cpp, CSS id: 
 * GeSHi (C) 2004 - 2007 Nigel McNie, 2007 - 2008 Benny Baumann
 * (http://qbnz.com/highlighter/ and http://geshi.org/)
 * --------------------------------------
 */
.cpp.source-cpp .de1, .cpp.source-cpp .de2 {font: normal normal 1em/1.2em monospace; margin:0; padding:0; background:none; vertical-align:top;}
.cpp.source-cpp  {font-family:monospace;}
.cpp.source-cpp .imp {font-weight: bold; color: red;}
.cpp.source-cpp li, .cpp.source-cpp .li1 {font-weight: normal; vertical-align:top;}
.cpp.source-cpp .ln {width:1px;text-align:right;margin:0;padding:0 2px;vertical-align:top;}
.cpp.source-cpp .li2 {font-weight: bold; vertical-align:top;}
.cpp.source-cpp .kw1 {color: #0000dd;}
.cpp.source-cpp .kw2 {color: #0000ff;}
.cpp.source-cpp .kw3 {color: #0000dd;}
.cpp.source-cpp .kw4 {color: #0000ff;}
.cpp.source-cpp .co1 {color: #909090;}
.cpp.source-cpp .co2 {color: #339900;}
.cpp.source-cpp .coMULTI {color: #ff0000; font-style: italic;}
.cpp.source-cpp .es0 {color: #008000; font-weight: bold;}
.cpp.source-cpp .es1 {color: #008000; font-weight: bold;}
.cpp.source-cpp .es2 {color: #008000; font-weight: bold;}
.cpp.source-cpp .es3 {color: #008000; font-weight: bold;}
.cpp.source-cpp .es4 {color: #008000; font-weight: bold;}
.cpp.source-cpp .es5 {color: #008000; font-weight: bold;}
.cpp.source-cpp .br0 {color: #008000;}
.cpp.source-cpp .sy0 {color: #008000;}
.cpp.source-cpp .sy1 {color: #000080;}
.cpp.source-cpp .sy2 {color: #000040;}
.cpp.source-cpp .sy3 {color: #000040;}
.cpp.source-cpp .sy4 {color: #008080;}
.cpp.source-cpp .st0 {color: #008000;}
.cpp.source-cpp .nu0 {color: #000080;}
.cpp.source-cpp .nu6 {color: #000080;}
.cpp.source-cpp .nu8 {color: #000080;}
.cpp.source-cpp .nu12 {color: #000080;}
.cpp.source-cpp .nu16 {color:#000080;}
.cpp.source-cpp .nu17 {color:#000080;}
.cpp.source-cpp .nu18 {color:#000080;}
.cpp.source-cpp .nu19 {color:#000080;}
.cpp.source-cpp .ln-xtra, .cpp.source-cpp li.ln-xtra, .cpp.source-cpp div.ln-xtra {background-color: #ffc;}
.cpp.source-cpp span.xtra { display:block; }

/*]]>*/
</style><!--[if lt IE 7]><style type="text/css">body{behavior:url("/mwiki/skins/cppreference2/csshover.min.htc")}</style><![endif]--></head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-cpp_error_domain_error skin-cppreference2 action-view cpp-navbar">
        <!-- header -->
        <div id="mw-head" class="noprint">
            <div id="cpp-head-first-base">
                <div id="cpp-head-first">
                    <h5><a href="/">
                        cppreference.com                        </a></h5>
                    <div id="cpp-head-search">
                        
<!-- 0 -->
<div id="p-search">
	<h5><label for="searchInput">Search</label></h5>
	<form action="/mwiki/index.php" id="searchform">
		<input type='hidden' name="title" value="Special:Search"/>
				<div id="simpleSearch">
						<input name="search" title="Search cppreference.com [f]" accesskey="f" id="searchInput" />						<button type="submit" name="button" title="Search the pages for this text" id="searchButton"><img src="/mwiki/skins/cppreference2/images/search-ltr.png?303" alt="Search" /></button>					</div>
			</form>
</div>

<!-- /0 -->
                    </div>
                    <div id="cpp-head-personal">
                        
<!-- 0 -->
<div id="p-personal" class="">
<span id="pt-createaccount"><a href="/mwiki/index.php?title=Special:UserLogin&amp;returnto=cpp%2Ferror%2Fdomain+error&amp;type=signup">Create account</a></span>	<div class="menu">
        <ul>
<li id="pt-login"><a href="/mwiki/index.php?title=Special:UserLogin&amp;returnto=cpp%2Ferror%2Fdomain+error" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li>        </ul>
    </div>
</div>

<!-- /0 -->
                    </div>

                </div>
            </div>
            <div id="cpp-head-second-base">
                <div id="cpp-head-second">
                    <div id="cpp-head-tools-left">
                        
<!-- 0 -->
<div id="p-namespaces" class="vectorTabs">
	<h5>Namespaces</h5>
	<ul>
					<li  id="ca-nstab-main" class="selected"><span><a href="/w/cpp/error/domain_error"  title="View the content page [c]" accesskey="c">Page</a></span></li>
					<li  id="ca-talk" class="new"><span><a href="/mwiki/index.php?title=Talk:cpp/error/domain_error&amp;action=edit&amp;redlink=1"  title="Discussion about the content page [t]" accesskey="t">Discussion</a></span></li>
			</ul>
</div>

<!-- /0 -->

<!-- 1 -->
<div id="p-variants" class="vectorMenu emptyPortlet">
		<h5><span>Variants</span><a href="#"></a></h5>
	<div class="menu">
		<ul>
					</ul>
	</div>
</div>

<!-- /1 -->
                    </div>
                    <div id="cpp-head-tools-right">
                        
<!-- 0 -->
<div id="p-views" class="vectorTabs">
	<h5>Views</h5>
	<ul>
					<li id="ca-view" class="selected"><span><a href="/w/cpp/error/domain_error" >View</a></span></li>
					<li id="ca-edit"><span><a href="/mwiki/index.php?title=cpp/error/domain_error&amp;action=edit"  title="You can edit this page. Please use the preview button before saving [e]" accesskey="e">Edit</a></span></li>
					<li id="ca-history" class="collapsible"><span><a href="/mwiki/index.php?title=cpp/error/domain_error&amp;action=history"  title="Past revisions of this page [h]" accesskey="h">History</a></span></li>
			</ul>
</div>

<!-- /0 -->

<!-- 1 -->
<div id="p-cactions" class="vectorMenu emptyPortlet">
	<h5><span>Actions</span><a href="#"></a></h5>
	<div class="menu">
		<ul>
					</ul>
	</div>
</div>

<!-- /1 -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /header -->
        <!-- content -->
<script type="text/javascript">
(function(){
  var bsa = document.createElement('script');
     bsa.type = 'text/javascript';
     bsa.async = true;
     bsa.src = '//s3.buysellads.com/ac/bsa.js';
  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);
})();
</script>
<style>
.bsap{position:absolute;left:-150px;margin-top:27px;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen-Sans,Ubuntu,Cantarell,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;line-height:1.5;overflow:hidden;max-width:130px;text-align:center;border:solid 1px hsla(0,0%,0%,.1);border-radius:4px;background-color:#f9f9f9}
.bsap a{text-decoration:none;color:inherit}
.bsap a:hover{color:inherit}
.bsa_it_i img{max-width:130px;height:auto}
.bsa_it_ad>a:nth-child(2){display:block;padding:0 8px}
.bsa_it_t{font-weight:600}
.bsa_it_t:after{content:' — '}
.bsa_it_p{display:block;padding:8px 12px;font-size:9px;font-weight:600;line-height:1;letter-spacing:.5px;text-transform:uppercase;background:repeating-linear-gradient(-45deg,transparent,transparent 5px,hsla(0,0%,0%,.025) 5px,hsla(0,0%,0%,.025) 10px) hsla(203,11%,95%,.4)}
#carbonads{display:block;overflow:hidden}
#carbonads span{display:block;position:relative;overflow:hidden}
.carbon-img{display:block;margin-bottom:8px;line-height:1;max-width:130px}
.carbon-img img{display:block;margin:0 auto;width:130px;max-width:130px!important;height:auto}
.carbon-text{display:block;padding:0 1em 8px}
.carbon-poweredby{display:none}
</style>
        <div id="cpp-content-base">
            <div id="content">
                <a id="top"></a>
                <div id="mw-js-message" style="display:none;"></div>
                                <!-- firstHeading -->
<div id="bsap_1308987" class="bsarocks bsap_806b7512f50f84c15e2a20e379480845"></div>
                <h1 id="firstHeading" class="firstHeading"><span style="font-size:0.7em; line-height:130%">std::</span>domain_error</h1>
                <!-- /firstHeading -->
                <!-- bodyContent -->
                <div id="bodyContent">
                                        <!-- tagline -->
                    <div id="siteSub">From cppreference.com</div>
                    <!-- /tagline -->
                                        <!-- subtitle -->
                    <div id="contentSub"><span class="subpages">&lt; <a href="/w/cpp" title="cpp">cpp</a>&lrm; | <a href="/w/cpp/error" title="cpp/error">error</a></span></div>
                    <!-- /subtitle -->
                                                            <!-- bodycontent -->
                    <div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="t-navbar" style=""><div class="t-navbar-sep">&#160;</div><div class="t-navbar-head"><a href="/w/cpp" title="cpp"> C++</a><div class="t-navbar-menu"><div><div><table class="t-nv-begin" cellpadding="0" style="line-height:1.1em;">
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/language" title="cpp/language"> Language</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/header" title="cpp/header"> Standard Library Headers</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/freestanding" title="cpp/freestanding"> Freestanding and hosted implementations</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/named_req" title="cpp/named req"> Named requirements </a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility#Language_support" title="cpp/utility"> Language support library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/concepts" title="cpp/concepts"> Concepts library</a> <span class="t-mark-rev t-since-cxx20">(C++20)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/error" title="cpp/error"> Diagnostics library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility" title="cpp/utility"> Utilities library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/string" title="cpp/string"> Strings library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/container" title="cpp/container"> Containers library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/iterator" title="cpp/iterator"> Iterators library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/ranges" title="cpp/ranges"> Ranges library</a> <span class="t-mark-rev t-since-cxx20">(C++20)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/algorithm" title="cpp/algorithm"> Algorithms library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/numeric" title="cpp/numeric"> Numerics library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/locale" title="cpp/locale"> Localizations library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/io" title="cpp/io"> Input/output library</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/filesystem" title="cpp/filesystem"> Filesystem library</a> <span class="t-mark-rev t-since-cxx17">(C++17)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/regex" title="cpp/regex"> Regular expressions library</a> <span class="t-mark-rev t-since-cxx11">(C++11)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/atomic" title="cpp/atomic"> Atomic operations library</a> <span class="t-mark-rev t-since-cxx11">(C++11)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/thread" title="cpp/thread"> Thread support library</a> <span class="t-mark-rev t-since-cxx11">(C++11)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/experimental" title="cpp/experimental"> Technical Specifications</a> </td></tr>
</table></div><div><span class="editsection noprint plainlinks" title="Edit this template"><a rel="nofollow" class="external text" href="https://en.cppreference.com/mwiki/index.php?title=Template:cpp/navbar_content&amp;action=edit">&#91;edit&#93;</a></span></div></div></div></div><div class="t-navbar-sep">&#160;</div><div class="t-navbar-head"><a href="/w/cpp/utility" title="cpp/utility"> Utilities library</a><div class="t-navbar-menu"><div><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv-col-table"><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv-h1"><td colspan="5"> Language support</td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/types" title="cpp/types"> Type support</a> (basic types, RTTI, type traits) </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility/feature_test" title="cpp/utility/feature test"> Library feature-test macros </a> <span class="t-mark-rev t-since-cxx20">(C++20)</span> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/memory" title="cpp/memory"> Dynamic memory management</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility/program" title="cpp/utility/program"> Program utilities</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/error" title="cpp/error"> Error handling</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/coroutine" title="cpp/coroutine"> Coroutine support</a> <span class="t-mark-rev t-since-cxx20">(C++20)</span></td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility/variadic" title="cpp/utility/variadic"> Variadic functions</a> </td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/launder" title="cpp/utility/launder"><span class="t-lines"><span>launder</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/initializer_list" title="cpp/utility/initializer list"><span class="t-lines"><span>initializer_list</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/source_location" title="cpp/utility/source location"><span class="t-lines"><span>source_location</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Three-way comparison <span class="t-mark-rev t-since-cxx20">(C++20)</span></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/three_way_comparable" title="cpp/utility/compare/three way comparable"><span class="t-lines"><span>three_way_comparable</span><span>three_way_comparable_with</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/strong_ordering" title="cpp/utility/compare/strong ordering"><span class="t-lines"><span>strong_ordering</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/weak_ordering" title="cpp/utility/compare/weak ordering"><span class="t-lines"><span>weak_ordering</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/partial_ordering" title="cpp/utility/compare/partial ordering"><span class="t-lines"><span>partial_ordering</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/common_comparison_category" title="cpp/utility/compare/common comparison category"><span class="t-lines"><span>common_comparison_category</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/compare_three_way_result" title="cpp/utility/compare/compare three way result"><span class="t-lines"><span>compare_three_way_result</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/compare_three_way" title="cpp/utility/compare/compare three way"><span class="t-lines"><span>compare_three_way</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/strong_order" title="cpp/utility/compare/strong order"><span class="t-lines"><span>strong_order</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/weak_order" title="cpp/utility/compare/weak order"><span class="t-lines"><span>weak_order</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/partial_order" title="cpp/utility/compare/partial order"><span class="t-lines"><span>partial_order</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/compare_strong_order_fallback" title="cpp/utility/compare/compare strong order fallback"><span class="t-lines"><span>compare_strong_order_fallback</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/compare_weak_order_fallback" title="cpp/utility/compare/compare weak order fallback"><span class="t-lines"><span>compare_weak_order_fallback</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/compare_partial_order_fallback" title="cpp/utility/compare/compare partial order fallback"><span class="t-lines"><span>compare_partial_order_fallback</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/compare/named_comparison_functions" title="cpp/utility/compare/named comparison functions"><span class="t-lines"><span>is_eq</span><span>is_neq</span><span>is_lt</span><span>is_lteq</span><span>is_gt</span><span>is_gteq</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
</table></div></td><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv-h1"><td colspan="5"> General utilities</td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/chrono" title="cpp/chrono"> Date and time</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility/functional" title="cpp/utility/functional"> Function objects</a> </td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/utility/format" title="cpp/utility/format"> Formatting library</a> <span class="t-mark-rev t-since-cxx20">(C++20)</span> </td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/bitset" title="cpp/utility/bitset"><span class="t-lines"><span>bitset</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/hash" title="cpp/utility/hash"><span class="t-lines"><span>hash</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/integer_sequence" title="cpp/utility/integer sequence"><span class="t-lines"><span>integer_sequence</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx14">(C++14)</span></span></span></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Relational operators <span class="t-mark-rev t-deprecated-cxx20">(deprecated in C++20)</span> </td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/rel_ops/operator_cmp" title="cpp/utility/rel ops/operator cmp"><span class="t-lines"><span>rel_ops::operator!=</span><span>rel_ops::operator&gt;</span><span>rel_ops::operator&lt;=</span><span>rel_ops::operator&gt;=</span></span></a></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Integer comparison functions</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/intcmp" title="cpp/utility/intcmp"><span class="t-lines"><span>cmp_equal</span><span>cmp_not_equal</span><span>cmp_less</span><span>cmp_greater</span><span>cmp_less_than</span><span>cmp_greater_than</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/in_range" title="cpp/utility/in range"><span class="t-lines"><span>in_range</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Swap and type operations</td></tr>
<tr class="t-nv-col-table"><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/algorithm/swap" title="cpp/algorithm/swap"><span class="t-lines"><span>swap</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/ranges/swap" title="cpp/utility/ranges/swap"><span class="t-lines"><span>ranges::swap</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx20">(C++20)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/exchange" title="cpp/utility/exchange"><span class="t-lines"><span>exchange</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx14">(C++14)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/declval" title="cpp/utility/declval"><span class="t-lines"><span>declval</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
</table></div></td><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/forward" title="cpp/utility/forward"><span class="t-lines"><span>forward</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/move" title="cpp/utility/move"><span class="t-lines"><span>move</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/move_if_noexcept" title="cpp/utility/move if noexcept"><span class="t-lines"><span>move_if_noexcept</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/as_const" title="cpp/utility/as const"><span class="t-lines"><span>as_const</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
</table></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Common vocabulary types </td></tr>
<tr class="t-nv-col-table"><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/pair" title="cpp/utility/pair"><span class="t-lines"><span>pair</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/tuple" title="cpp/utility/tuple"><span class="t-lines"><span>tuple</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/apply" title="cpp/utility/apply"><span class="t-lines"><span>apply</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/make_from_tuple" title="cpp/utility/make from tuple"><span class="t-lines"><span>make_from_tuple</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
</table></div></td><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/optional" title="cpp/utility/optional"><span class="t-lines"><span>optional</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/any" title="cpp/utility/any"><span class="t-lines"><span>any</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/variant" title="cpp/utility/variant"><span class="t-lines"><span>variant</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"> <br />
</td></tr>
</table></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Elementary string conversions</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/to_chars" title="cpp/utility/to chars"><span class="t-lines"><span>to_chars</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/from_chars" title="cpp/utility/from chars"><span class="t-lines"><span>from_chars</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/utility/chars_format" title="cpp/utility/chars format"><span class="t-lines"><span>chars_format</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
</table></div></td></tr>
</table></div><div><span class="editsection noprint plainlinks" title="Edit this template"><a rel="nofollow" class="external text" href="https://en.cppreference.com/mwiki/index.php?title=Template:cpp/utility/navbar_content&amp;action=edit">&#91;edit&#93;</a></span></div></div></div></div><div class="t-navbar-sep">&#160;</div><div class="t-navbar-head"><a href="/w/cpp/error" title="cpp/error"> Error handling</a><div class="t-navbar-menu"><div><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv-col-table"><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv-h2"><td colspan="5"> Exception handling</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/exception" title="cpp/error/exception"><span class="t-lines"><span>exception</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/uncaught_exception" title="cpp/error/uncaught exception"><span class="t-lines"><span>uncaught_exception</span><span>uncaught_exceptions</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-until-cxx20">(until C++20)</span></span><span><span class="t-mark-rev t-since-cxx17">(C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/exception_ptr" title="cpp/error/exception ptr"><span class="t-lines"><span>exception_ptr</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/make_exception_ptr" title="cpp/error/make exception ptr"><span class="t-lines"><span>make_exception_ptr</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/current_exception" title="cpp/error/current exception"><span class="t-lines"><span>current_exception</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/rethrow_exception" title="cpp/error/rethrow exception"><span class="t-lines"><span>rethrow_exception</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/nested_exception" title="cpp/error/nested exception"><span class="t-lines"><span>nested_exception</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/throw_with_nested" title="cpp/error/throw with nested"><span class="t-lines"><span>throw_with_nested</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/rethrow_if_nested" title="cpp/error/rethrow if nested"><span class="t-lines"><span>rethrow_if_nested</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Exception handling failures</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/terminate" title="cpp/error/terminate"><span class="t-lines"><span>terminate</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/terminate_handler" title="cpp/error/terminate handler"><span class="t-lines"><span>terminate_handler</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/get_terminate" title="cpp/error/get terminate"><span class="t-lines"><span>get_terminate</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/set_terminate" title="cpp/error/set terminate"><span class="t-lines"><span>set_terminate</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/bad_exception" title="cpp/error/bad exception"><span class="t-lines"><span>bad_exception</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/unexpected" title="cpp/error/unexpected"><span class="t-lines"><span>unexpected</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-until-cxx17">(until C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/unexpected_handler" title="cpp/error/unexpected handler"><span class="t-lines"><span>unexpected_handler</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-until-cxx17">(until C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/get_unexpected" title="cpp/error/get unexpected"><span class="t-lines"><span>get_unexpected</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span><span class="t-mark-rev t-until-cxx17">(until C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/set_unexpected" title="cpp/error/set unexpected"><span class="t-lines"><span>set_unexpected</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-until-cxx17">(until C++17)</span></span></span></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Error codes</td></tr>
<tr class="t-nv"><td colspan="5"> <a href="/w/cpp/error/errno_macros" title="cpp/error/errno macros"> Error codes</a> </td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/errno" title="cpp/error/errno"><span class="t-lines"><span>errno</span></span></a></div></div></td></tr>
</table></div></td><td><div><table class="t-nv-begin" cellpadding="0" style="">
<tr class="t-nv-h2"><td colspan="5"> Exception categories</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/logic_error" title="cpp/error/logic error"><span class="t-lines"><span>logic_error</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/invalid_argument" title="cpp/error/invalid argument"><span class="t-lines"><span>invalid_argument</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><strong class="selflink"><span class="t-lines"><span>domain_error</span></span></strong></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/length_error" title="cpp/error/length error"><span class="t-lines"><span>length_error</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/out_of_range" title="cpp/error/out of range"><span class="t-lines"><span>out_of_range</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/runtime_error" title="cpp/error/runtime error"><span class="t-lines"><span>runtime_error</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/range_error" title="cpp/error/range error"><span class="t-lines"><span>range_error</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/overflow_error" title="cpp/error/overflow error"><span class="t-lines"><span>overflow_error</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/underflow_error" title="cpp/error/underflow error"><span class="t-lines"><span>underflow_error</span></span></a></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/tx_exception" title="cpp/error/tx exception"><span class="t-lines"><span>tx_exception</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-tm-ts t-mark-ts">(TM TS)</span></span></span></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> Assertions</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/assert" title="cpp/error/assert"><span class="t-lines"><span>assert</span></span></a></div></div></td></tr>
<tr class="t-nv-h2"><td colspan="5"> system_error facility</td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/error_category" title="cpp/error/error category"><span class="t-lines"><span>error_category</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/generic_category" title="cpp/error/generic category"><span class="t-lines"><span>generic_category</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/system_category" title="cpp/error/system category"><span class="t-lines"><span>system_category</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/error_condition" title="cpp/error/error condition"><span class="t-lines"><span>error_condition</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/errc" title="cpp/error/errc"><span class="t-lines"><span>errc</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/error_code" title="cpp/error/error code"><span class="t-lines"><span>error_code</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
<tr class="t-nv"><td colspan="5"><div class="t-nv-ln-table"><div><a href="/w/cpp/error/system_error" title="cpp/error/system error"><span class="t-lines"><span>system_error</span></span></a></div><div><span class="t-lines"><span><span class="t-mark-rev t-since-cxx11">(C++11)</span></span></span></div></div></td></tr>
</table></div></td></tr>
</table></div><div><span class="editsection noprint plainlinks" title="Edit this template"><a rel="nofollow" class="external text" href="https://en.cppreference.com/mwiki/index.php?title=Template:cpp/error/navbar_content&amp;action=edit">&#91;edit&#93;</a></span></div></div></div></div><div class="t-navbar-sep">&#160;</div></div>
<table class="t-dcl-begin"><tbody>
<tr class="t-dsc-header">
<td> <div>Defined in header <code><a href="/w/cpp/header/stdexcept" title="cpp/header/stdexcept">&lt;stdexcept&gt;</a></code>
 </div></td>
<td></td>
<td></td>
</tr>
<tr class="t-dcl">
<td class="t-dcl-nopad"> <div><span class="mw-geshi cpp source-cpp"><span class="kw1">class</span> domain_error<span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td class="t-dcl-nopad">  </td>
</tr>
<tr class="t-dcl-sep"><td></td><td></td><td></td></tr>
</tbody></table>
<p>Defines a type of object to be thrown as exception. It may be used by the implementation to report domain errors, that is, situations where the inputs are outside of the domain on which an operation is defined.
</p><p>The standard library components do not throw this exception (mathematical functions report domain errors as specified in <span class="t-lc"><a href="/w/cpp/numeric/math/math_errhandling" title="cpp/numeric/math/math errhandling">math_errhandling</a></span>). Third-party libraries, however, use this. For example, <a rel="nofollow" class="external text" href="http://www.boost.org/doc/libs/1_55_0/libs/math/doc/html/math_toolkit/error_handling.html">boost.math</a> throws <code>std::domain_error</code> if <code>boost::math::policies::throw_on_error</code> is enabled (the default setting).
</p>
<div class="t-inheritance-diagram">
<div class="center"><div class="noresize" style="height: 40px; width: 374px; "><map name="ImageMap_1_2068080089"><area href="/w/cpp/error/exception" shape="rect" coords="5,4,107,36" alt="cpp/error/exception" title="cpp/error/exception"/><area href="/w/cpp/error/logic_error" shape="rect" coords="135,4,238,36" alt="cpp/error/logic error" title="cpp/error/logic error"/></map><img alt="std-domain error-inheritance.svg" src="http://upload.cppreference.com/mwiki/images/2/26/std-domain_error-inheritance.svg" width="374" height="40" usemap="#ImageMap_1_2068080089"/><div style="margin-left: 354px; margin-top: -20px; text-align: left;"><a href="/w/File:std-domain_error-inheritance.svg" title="About this image"><img alt="About this image" src="/mwiki/extensions/ImageMap/desc-20.png" style="border: none;"/></a></div></div></div>
<div style="text-align:center;">
<p><span style="font-size:0.7em; line-height:130%">Inheritance diagram</span>
</p>
</div>
</div>
<table id="toc" class="toc"><tr><td><div id="toctitle"><h2>Contents</h2></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Member_functions"><span class="tocnumber">1</span> <span class="toctext">Member functions</span></a></li>
<li class="toclevel-1"><a href="#std::domain_error::domain_error"><span class="tocnumber">2</span> <span class="toctext"><span>std::domain_error::</span>domain_error</span></a>
<ul>
<li class="toclevel-2"><a href="#Parameters"><span class="tocnumber">2.1</span> <span class="toctext">Parameters</span></a></li>
<li class="toclevel-2"><a href="#Exceptions"><span class="tocnumber">2.2</span> <span class="toctext">Exceptions</span></a></li>
<li class="toclevel-2"><a href="#Notes"><span class="tocnumber">2.3</span> <span class="toctext">Notes</span></a></li>
</ul>
</li>
<li class="toclevel-1"><a href="#std::domain_error::operator.3D"><span class="tocnumber">3</span> <span class="toctext"><span>std::domain_error::</span>operator=</span></a>
<ul>
<li class="toclevel-2"><a href="#Parameters_2"><span class="tocnumber">3.1</span> <span class="toctext">Parameters</span></a></li>
<li class="toclevel-2"><a href="#Return_value"><span class="tocnumber">3.2</span> <span class="toctext">Return value</span></a></li>
</ul>
</li>
<li class="toclevel-1"><a href="#std::domain_error::what"><span class="tocnumber">4</span> <span class="toctext"><span>std::domain_error::</span>what</span></a>
<ul>
<li class="toclevel-2"><a href="#Parameters_3"><span class="tocnumber">4.1</span> <span class="toctext">Parameters</span></a></li>
<li class="toclevel-2"><a href="#Return_value_2"><span class="tocnumber">4.2</span> <span class="toctext">Return value</span></a></li>
<li class="toclevel-2"><a href="#Notes_2"><span class="tocnumber">4.3</span> <span class="toctext">Notes</span></a></li>
</ul>
</li>
<li class="toclevel-1"><a href="#Inherited_from_std::logic_error"><span class="tocnumber">5</span> <span class="toctext">Inherited from  <span>std::</span>logic_error</span></a></li>
<li class="toclevel-1"><a href="#Inherited_from_std::exception"><span class="tocnumber">6</span> <span class="toctext">Inherited from   <span>std::</span>exception</span></a>
<ul>
<li class="toclevel-2"><a href="#Member_functions_2"><span class="tocnumber">6.1</span> <span class="toctext">Member functions</span></a></li>
</ul>
</li>
</ul>
</td></tr></table>
<h3><span class="editsection">[<a href="/mwiki/index.php?title=cpp/error/domain_error&amp;action=edit&amp;section=1" title="Edit section: Member functions">edit</a>]</span> <span class="mw-headline" id="Member_functions">Member functions</span></h3>
<table class="t-dsc-begin">

<tr class="t-dsc">
<td>  <div class="t-dsc-member-div t-dsc-member-nobold-div"><div><span class="t-lines"><span>(constructor)</span></span></div></div>
</td>
<td>  constructs a new <code>domain_error</code> object with the given message <br /> <span class="t-mark">(public member function)</span>
</td></tr>


<tr class="t-dsc">
<td>  <div class="t-dsc-member-div"><div><span class="t-lines"><span>operator=</span></span></div></div>
</td>
<td>   replaces the <code>domain_error</code> object <br /> <span class="t-mark">(public member function)</span>
</td></tr>


<tr class="t-dsc">
<td>  <div class="t-dsc-member-div"><div><span class="t-lines"><span>what</span></span></div></div>
</td>
<td>   returns the explanatory string <br /> <span class="t-mark">(public member function)</span>
</td></tr>

</table>
<div class="t-member">
<h2> <span class="mw-headline" id="std::domain_error::domain_error"> <span style="font-size:0.7em; line-height:130%">std::domain_error::</span>domain_error </span></h2>
<table class="t-dcl-begin"><tbody>
<tr class="t-dcl">
<td> <div><span class="mw-geshi cpp source-cpp">domain_error<span class="br0">&#40;</span> <span class="kw4">const</span> <a href="http://en.cppreference.com/w/cpp/string/basic_string"><span class="kw1227">std::<span class="me2">string</span></span></a><span class="sy3">&amp;</span> what_arg <span class="br0">&#41;</span><span class="sy4">;</span></span></div></td>
<td> (1) </td>
<td class="t-dcl-nopad">  </td>
</tr>
<tr class="t-dcl t-since-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp">domain_error<span class="br0">&#40;</span> <span class="kw4">const</span> <span class="kw4">char</span><span class="sy2">*</span> what_arg <span class="br0">&#41;</span><span class="sy4">;</span></span></div></td>
<td> (2) </td>
<td> <span class="t-mark-rev t-since-cxx11">(since C++11)</span> </td>
</tr>
</tbody>
<tbody class="t-dcl-rev t-dcl-rev-num"><tr class="t-dcl-rev-aux">
<td></td>
<td rowspan="142">(3)</td>
<td></td>
</tr>
<tr class="t-dcl t-until-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp">domain_error<span class="br0">&#40;</span> <span class="kw4">const</span> domain_error<span class="sy3">&amp;</span> other <span class="br0">&#41;</span><span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td> <span class="t-mark-rev t-until-cxx11">(until C++11)</span> </td>
</tr>
<tr class="t-dcl t-since-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp">domain_error<span class="br0">&#40;</span> <span class="kw4">const</span> domain_error<span class="sy3">&amp;</span> other <span class="br0">&#41;</span> <span class="kw1">noexcept</span><span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td> <span class="t-mark-rev t-since-cxx11">(since C++11)</span> </td>
</tr>
</tbody><tbody>
<tr class="t-dcl-sep"><td></td><td></td><td></td></tr>
</tbody></table>
<div class="t-li1"><span class="t-li">1-2)</span> Constructs the exception object with <code>what_arg</code> as explanatory string that can be accessed through <a href="/w/cpp/error/exception/what" title="cpp/error/exception/what"><tt>what()</tt></a>.</div>
<div class="t-li1"><span class="t-li">3)</span> Copy constructor. <span class="t-rev-inl t-since-cxx11"><span>If <code>*this</code> and <code>other</code> both have dynamic type <code>std::domain_error</code> then <span class="t-c"><span class="mw-geshi cpp source-cpp"><a href="http://en.cppreference.com/w/cpp/string/byte/strcmp"><span class="kw1143">std::<span class="me2">strcmp</span></span></a><span class="br0">&#40;</span>what<span class="br0">&#40;</span><span class="br0">&#41;</span>, other.<span class="me1">what</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="sy1">==</span> <span class="nu0">0</span></span></span>.</span> <span><span class="t-mark-rev t-since-cxx11">(since C++11)</span></span></span></div>
<h3> <span class="mw-headline" id="Parameters">Parameters</span></h3>
<table class="t-par-begin">


<tr class="t-par">
<td>  what_arg
</td>
<td> -
</td>
<td>  explanatory string
</td></tr>
<tr class="t-par">
<td>  other
</td>
<td> -
</td>
<td>  another exception object to copy
</td></tr></table>
<h3> <span class="mw-headline" id="Exceptions">Exceptions</span></h3>
<div class="t-li1"><span class="t-li">1-2)</span> May throw <span class="t-lc"><a href="/w/cpp/memory/new/bad_alloc" title="cpp/memory/new/bad alloc">std::bad_alloc</a></span></div>
<h3> <span class="mw-headline" id="Notes">Notes</span></h3>
<p>Because copying <code>std::domain_error</code> is not permitted to throw exceptions, this message is typically stored internally as a separately-allocated reference-counted string. This is also why there is no constructor taking <code>std::string&amp;&amp;</code>: it would have to copy the content anyway.
</p>
</div>
<div class="t-member">
<h2> <span class="mw-headline" id="std::domain_error::operator.3D"> <span style="font-size:0.7em; line-height:130%">std::domain_error::</span>operator= </span></h2>
<table class="t-dcl-begin"><tbody>
</tbody>
<tbody class="t-dcl-rev">
<tr class="t-dcl t-until-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp">domain_error<span class="sy3">&amp;</span> operator<span class="sy1">=</span><span class="br0">&#40;</span> <span class="kw4">const</span> domain_error<span class="sy3">&amp;</span> other <span class="br0">&#41;</span><span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td> <span class="t-mark-rev t-until-cxx11">(until C++11)</span> </td>
</tr>
<tr class="t-dcl t-since-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp">domain_error<span class="sy3">&amp;</span> operator<span class="sy1">=</span><span class="br0">&#40;</span> <span class="kw4">const</span> domain_error<span class="sy3">&amp;</span> other <span class="br0">&#41;</span> <span class="kw1">noexcept</span><span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td> <span class="t-mark-rev t-since-cxx11">(since C++11)</span> </td>
</tr>
</tbody><tbody>
<tr class="t-dcl-sep"><td></td><td></td><td></td></tr>
</tbody></table>
<p>Assigns the contents with those of <code>other</code>. <span class="t-rev-inl t-since-cxx11"><span>If <code>*this</code> and <code>other</code> both have dynamic type <code>std::domain_error</code> then <span class="t-c"><span class="mw-geshi cpp source-cpp"><a href="http://en.cppreference.com/w/cpp/string/byte/strcmp"><span class="kw1143">std::<span class="me2">strcmp</span></span></a><span class="br0">&#40;</span>what<span class="br0">&#40;</span><span class="br0">&#41;</span>, other.<span class="me1">what</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="br0">&#41;</span> <span class="sy1">==</span> <span class="nu0">0</span></span></span> after assignment.</span> <span><span class="t-mark-rev t-since-cxx11">(since C++11)</span></span></span>
</p>
<h3> <span class="mw-headline" id="Parameters_2">Parameters</span></h3>
<table class="t-par-begin">


<tr class="t-par">
<td>  other
</td>
<td> -
</td>
<td>  another exception object to assign with
</td></tr></table>
<h3> <span class="mw-headline" id="Return_value">Return value</span></h3>
<p><span class="t-c"><span class="mw-geshi cpp source-cpp"><span class="sy2">*</span>this</span></span>
</p>
</div>
<div class="t-member">
<h2> <span class="mw-headline" id="std::domain_error::what"> <span style="font-size:0.7em; line-height:130%">std::domain_error::</span>what </span></h2>
<table class="t-dcl-begin"><tbody>
</tbody>
<tbody class="t-dcl-rev">
<tr class="t-dcl t-until-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp"><span class="kw1">virtual</span> <span class="kw4">const</span> <span class="kw4">char</span><span class="sy2">*</span> what<span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="kw4">const</span> <span class="kw1">throw</span><span class="br0">&#40;</span><span class="br0">&#41;</span><span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td> <span class="t-mark-rev t-until-cxx11">(until C++11)</span> </td>
</tr>
<tr class="t-dcl t-since-cxx11">
<td> <div><span class="mw-geshi cpp source-cpp"><span class="kw1">virtual</span> <span class="kw4">const</span> <span class="kw4">char</span><span class="sy2">*</span> what<span class="br0">&#40;</span><span class="br0">&#41;</span> <span class="kw4">const</span> <span class="kw1">noexcept</span><span class="sy4">;</span></span></div></td>
<td class="t-dcl-nopad">  </td>
<td> <span class="t-mark-rev t-since-cxx11">(since C++11)</span> </td>
</tr>
</tbody><tbody>
<tr class="t-dcl-sep"><td></td><td></td><td></td></tr>
</tbody></table>
<p>Returns the explanatory string.
</p>
<h3> <span class="mw-headline" id="Parameters_3">Parameters</span></h3>
<p>(none)
</p>
<h3> <span class="mw-headline" id="Return_value_2">Return value</span></h3>
<p>Pointer to a null-terminated string with explanatory information. The string is suitable for conversion and display as a <span class="t-lc"><a href="/w/cpp/string/basic_string" title="cpp/string/basic string">std::wstring</a></span>. The pointer is guaranteed to be valid at least until the exception object from which it is obtained is destroyed, or until a non-const member function (e.g. copy assignment operator) on the exception object is called.
</p>
<h3> <span class="mw-headline" id="Notes_2">Notes</span></h3>
<p>Implementations are allowed but not required to override <code>what()</code>.
</p>
</div>
<div class="t-inherited">
<h2> <span class="mw-headline" id="Inherited_from_std::logic_error">Inherited from  <a href="/w/cpp/error/logic_error" title="cpp/error/logic error"><span style="font-size:0.7em; line-height:130%">std::</span>logic_error</a> </span></h2>
</div>
<div class="t-inherited">
<h2> <span class="mw-headline" id="Inherited_from_std::exception">Inherited from  <a href="/w/cpp/error/exception" title="cpp/error/exception"> <span style="font-size:0.7em; line-height:130%">std::</span>exception</a> </span></h2>
<h3> <span class="mw-headline" id="Member_functions_2">Member functions</span></h3>
<table class="t-dsc-begin">

<tr class="t-dsc">
<td>  <div class="t-dsc-member-div t-dsc-member-nobold-div"><div><a href="/w/cpp/error/exception/%7Eexception" title="cpp/error/exception/~exception"> <span class="t-lines"><span>(destructor)</span></span></a></div><div><span class="t-lines"><span><span class="t-cmark">[virtual]</span></span></span></div></div>
</td>
<td>  destroys the exception object <br /> <span class="t-mark">(virtual public member function of <code>std::exception</code>)</span> <span class="editsection noprint plainlinks" title="Edit this template"><a rel="nofollow" class="external text" href="https://en.cppreference.com/mwiki/index.php?title=Template:cpp/error/exception/dsc_destructor&amp;action=edit">&#91;edit&#93;</a></span>
</td></tr>

<tr class="t-dsc">
<td>  <div class="t-dsc-member-div"><div><a href="/w/cpp/error/exception/what" title="cpp/error/exception/what"> <span class="t-lines"><span>what</span></span></a></div><div><span class="t-lines"><span><span class="t-cmark">[virtual]</span></span></span></div></div>
</td>
<td>   returns an explanatory string <br /> <span class="t-mark">(virtual public member function of <code>std::exception</code>)</span> <span class="editsection noprint plainlinks" title="Edit this template"><a rel="nofollow" class="external text" href="https://en.cppreference.com/mwiki/index.php?title=Template:cpp/error/exception/dsc_what&amp;action=edit">&#91;edit&#93;</a></span>
</td></tr>
</table>
</div>

<!-- 
NewPP limit report
Preprocessor visited node count: 9198/1000000
Preprocessor generated node count: 13940/1000000
Post‐expand include size: 267023/2097152 bytes
Template argument size: 53776/2097152 bytes
Highest expansion depth: 35/40
Expensive parser function count: 0/100
-->

<!-- Saved in parser cache with key mwiki1-mwiki_en_:pcache:idhash:2086-0!*!0!!en!2!* and timestamp 20200726120951 -->
</div>                    <!-- /bodycontent -->
                                        <!-- printfooter -->
                    <div class="printfooter">
                    Retrieved from "<a href="https://en.cppreference.com/mwiki/index.php?title=cpp/error/domain_error&amp;oldid=118294">https://en.cppreference.com/mwiki/index.php?title=cpp/error/domain_error&amp;oldid=118294</a>"                    </div>
                    <!-- /printfooter -->
                                                            <!-- catlinks -->
                    <div id='catlinks' class='catlinks catlinks-allhidden'></div>                    <!-- /catlinks -->
                                                            <div class="visualClear"></div>
                    <!-- debughtml -->
                                        <!-- /debughtml -->
                </div>
                <!-- /bodyContent -->
            </div>
        </div>
        <!-- /content -->
        <!-- footer -->
        <div id="cpp-footer-base" class="noprint">
            <div id="footer">
                        <div id="cpp-navigation">
            <h5>Navigation</h5>
            <ul>
<li id="n-Support-us"><a href="http://www.cppreference.com/support" rel="nofollow">Support us</a></li><li id="n-recentchanges"><a href="/w/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-FAQ"><a href="/w/Cppreference:FAQ">FAQ</a></li><li id="n-Offline-version"><a href="/w/Cppreference:Archives">Offline version</a></li>            </ul>
        </div>
                        <div id="cpp-toolbox">
            <h5><span>Toolbox</span><a href="#"></a></h5>
            <ul>
<li id="t-whatlinkshere"><a href="/w/Special:WhatLinksHere/cpp/error/domain_error" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/w/Special:RecentChangesLinked/cpp/error/domain_error" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-upload"><a href="http://upload.cppreference.com/w/Special:Upload" title="Upload files [u]" accesskey="u">Upload file</a></li><li id="t-specialpages"><a href="/w/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mwiki/index.php?title=cpp/error/domain_error&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mwiki/index.php?title=cpp/error/domain_error&amp;oldid=118294" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mwiki/index.php?title=cpp/error/domain_error&amp;action=info">Page information</a></li>            </ul>
        </div>
                        <div id="cpp-languages">
            <div><ul><li>In other languages</li></ul></div>
            <div><ul>
<li class="interwiki-de"><a href="http://de.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="de" hreflang="de">Deutsch</a></li><li class="interwiki-es"><a href="http://es.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="es" hreflang="es">Español</a></li><li class="interwiki-fr"><a href="http://fr.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="fr" hreflang="fr">Français</a></li><li class="interwiki-it"><a href="http://it.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="it" hreflang="it">Italiano</a></li><li class="interwiki-ja"><a href="http://ja.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="ja" hreflang="ja">日本語</a></li><li class="interwiki-pt"><a href="http://pt.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="pt" hreflang="pt">Português</a></li><li class="interwiki-ru"><a href="http://ru.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="ru" hreflang="ru">Русский</a></li><li class="interwiki-zh"><a href="http://zh.cppreference.com/w/cpp/error/domain_error" title="cpp/error/domain error" lang="zh" hreflang="zh">中文</a></li>            </ul></div>
        </div>
            <ul id="footer-info">
                                    <li id="footer-info-lastmod"> This page was last modified on 22 April 2020, at 10:00.</li>
                                    <li id="footer-info-viewcount">This page has been accessed 91,255 times.</li>
                            </ul>
                    <ul id="footer-places">
                                    <li id="footer-places-privacy"><a href="/w/Cppreference:Privacy_policy" title="Cppreference:Privacy policy">Privacy policy</a></li>
                                    <li id="footer-places-about"><a href="/w/Cppreference:About" title="Cppreference:About">About cppreference.com</a></li>
                                    <li id="footer-places-disclaimer"><a href="/w/Cppreference:General_disclaimer" title="Cppreference:General disclaimer">Disclaimers</a></li>
                            </ul>
                                    <ul id="footer-icons" class="noprint">
                                    <li id="footer-poweredbyico">
                                            <a href="//www.mediawiki.org/"><img src="/mwiki/skins/common/images/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" width="88" height="31" /></a>                                            <a href="http://qbnz.com/highlighter/"><img src="//upload.cppreference.com/mwiki/images/2/2b/powered_by_geshi_88x31.png" alt="Powered by GeSHi" height="31" width="88" /></a>                                            <a href="http://www.tigertech.net/referral/cppreference.com"><img src="//upload.cppreference.com/mwiki/images/9/94/powered_by_tigertech_88x31.png" alt="Hosted by Tiger Technologies" height="31" width="88" /></a>                                        </li>
                                </ul>
                        <div style="clear:both">
            </div>
            </div>
        </div>
        <!-- /footer -->
        <script>if(window.mw){
mw.loader.state({"site":"loading","user":"missing","user.groups":"ready"});
}</script>
<script src="https://en.cppreference.com/mwiki/load.php?debug=false&amp;lang=en&amp;modules=skins.cppreference2&amp;only=scripts&amp;skin=cppreference2&amp;*"></script>
<script>if(window.mw){
mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.page.ready","mediawiki.searchSuggest","mediawiki.hidpi","ext.gadget.ColiruCompiler","ext.gadget.MathJax"], null, true);
}</script>
<script src="https://en.cppreference.com/mwiki/load.php?debug=false&amp;lang=en&amp;modules=site&amp;only=scripts&amp;skin=cppreference2&amp;*"></script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-2828341-1']);
_gaq.push(['_setDomainName', 'cppreference.com']);
_gaq.push(['_trackPageview']);
</script><!-- Served in 1.350 secs. -->
	</body>
<!-- Cached 20200726120951 -->
</html>
