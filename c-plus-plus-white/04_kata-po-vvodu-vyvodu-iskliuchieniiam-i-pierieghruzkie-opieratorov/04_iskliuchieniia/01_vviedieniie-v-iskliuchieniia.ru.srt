1
00:00:05,260 --> 00:00:09,320
В данном видео мы поговорим об исключениях в языке C++.

2
00:00:09,320 --> 00:00:11,815
Мы разберемся зачем нужны исключения,

3
00:00:11,815 --> 00:00:14,668
как реализованы исключения в языке C++,

4
00:00:14,668 --> 00:00:17,105
как их надо выкидывать и что с ними вообще делать.

5
00:00:17,105 --> 00:00:20,990
А также узнаем как обрабатывать исключения в вашем коде.

6
00:00:20,990 --> 00:00:24,290
Если кратко, то исключение — это нестандартная ситуация,

7
00:00:24,290 --> 00:00:26,150
то есть когда ваш код ожидает,

8
00:00:26,150 --> 00:00:27,860
что вокруг него будет какая-то определенная

9
00:00:27,860 --> 00:00:30,325
среда или определенные инварианты, они не соблюдаются.

10
00:00:30,325 --> 00:00:32,750
Самый банальный пример: у вас есть функция,

11
00:00:32,750 --> 00:00:35,000
которая суммирует две матрицы и, например,

12
00:00:35,000 --> 00:00:37,640
одна из матриц имеет неправильную размерность.

13
00:00:37,640 --> 00:00:39,100
Все. Исключительная ситуация.

14
00:00:39,100 --> 00:00:40,675
Здесь можно бросить исключение.

15
00:00:40,675 --> 00:00:43,130
Как это сделать? Разберемся прямо сейчас.

16
00:00:43,130 --> 00:00:45,765
Давайте рассмотрим следующий пример,

17
00:00:45,765 --> 00:00:50,985
когда у нас есть класс даты и мы ее хотим как-то парсить с входного потока.

18
00:00:50,985 --> 00:00:54,140
ОК. Что у нас есть в дате?

19
00:00:54,140 --> 00:01:00,310
В дате есть год, в дате есть месяц и в дате есть число.

20
00:01:00,310 --> 00:01:05,480
Замечательно! Классик мы создали. Что мы хотим?

21
00:01:05,480 --> 00:01:10,952
Давайте захотим, например, читать дату с входного потока или давайте,

22
00:01:10,952 --> 00:01:13,815
чтобы было совсем просто, будем читать дату из строки.

23
00:01:13,815 --> 00:01:18,640
Заведем переменную "date_str" и запишем

24
00:01:18,640 --> 00:01:24,495
туда следующую дату: "2017/01/25".

25
00:01:24,495 --> 00:01:29,570
Это будет 25 января 2017 года.

26
00:01:29,570 --> 00:01:33,340
Отлично! Давайте заведем функцию "ParseDate",

27
00:01:33,340 --> 00:01:35,450
которая нам будет возвращать дату,

28
00:01:35,450 --> 00:01:38,480
на вход она будет принимать строку.

29
00:01:43,700 --> 00:01:47,070
На вход она будет принимать константную строчку,

30
00:01:47,070 --> 00:01:51,870
в которой будет храниться текст даты.

31
00:01:52,830 --> 00:01:56,330
Значит, что мы здесь дальше будем делать?

32
00:01:56,330 --> 00:01:58,710
Мы объявим строковый поток,

33
00:01:58,710 --> 00:02:01,680
с которым дальше будем работать.

34
00:02:01,680 --> 00:02:05,535
Назовем его "stream" и положим туда входную строчку.

35
00:02:05,535 --> 00:02:09,990
После этого заведем переменную даты

36
00:02:09,990 --> 00:02:17,130
"date" и начнем из этого потока считывать туда всю необходимую информацию,

37
00:02:17,130 --> 00:02:20,337
то есть считаем год.

38
00:02:20,337 --> 00:02:24,645
После этого нам надо пропустить следующий символ,

39
00:02:24,645 --> 00:02:28,427
как мы помним, там идет слеш.

40
00:02:28,427 --> 00:02:31,600
То есть 2017, вот мы его считали в строчке 18,

41
00:02:31,600 --> 00:02:34,750
далее идет слеш, потом ноль один, потом слеш, потом 25.

42
00:02:34,750 --> 00:02:38,010
Слеш мы можем пропустить с помощью функции "ignore".

43
00:02:38,010 --> 00:02:40,300
Сюда надо передать количество символов,

44
00:02:40,300 --> 00:02:42,505
которое мы хотим пропустить.

45
00:02:42,505 --> 00:02:48,710
После этого аналогичным образом мы считаем месяц и день,

46
00:02:50,310 --> 00:02:53,420
то есть еще раз считали год, пропустили слеш,

47
00:02:53,420 --> 00:02:56,790
считали месяц, пропустили слеш, считали день.

48
00:02:56,790 --> 00:02:58,631
Отлично! Наша дата считана,

49
00:02:58,631 --> 00:03:04,035
мы можем вернуть нашу дату.

50
00:03:04,035 --> 00:03:10,080
Давайте выведем то, что у нас получится на экран,

51
00:03:10,080 --> 00:03:14,375
но для начала объявим перемену типа Date,

52
00:03:14,375 --> 00:03:19,840
назовем ее "date" и распарсим строчку выше.

53
00:03:21,340 --> 00:03:30,810
После этого с помощью "cout" мы сможем красиво вывести эту дату.

54
00:03:30,810 --> 00:03:34,835
В данном случае я устанавливаю ширину поля два и заполнитель ноль,

55
00:03:34,835 --> 00:03:36,855
чтобы когда мы вводим числа меньше 10,

56
00:03:36,855 --> 00:03:41,800
у нас был красивый заполнитель в виде нуля и дата смотрелась изящно.

57
00:03:44,090 --> 00:03:53,850
Выведем день, поставим точку,

58
00:03:54,210 --> 00:03:57,105
после этого, на самом деле,

59
00:03:57,105 --> 00:04:04,400
мы можем повторить весь тот код и вывести месяц.

60
00:04:10,870 --> 00:04:19,800
После этого мы опять можем немного повторить наш код,

61
00:04:19,800 --> 00:04:24,120
точнее даже просто вывести год.

62
00:04:25,660 --> 00:04:28,670
И не забываем перенос строки.

63
00:04:29,530 --> 00:04:32,475
Что ж, запустим наш код.

64
00:04:32,475 --> 00:04:33,900
Отлично!

65
00:04:33,900 --> 00:04:42,112
Мы видим, что дата распарсилась правильно: "25.01.2017".

66
00:04:42,112 --> 00:04:47,143
Вроде задача решена и как-то эта функция у нас работает,

67
00:04:47,143 --> 00:04:49,420
но давайте попробуем защититься от ситуации,

68
00:04:49,420 --> 00:04:54,295
когда данные на вход могут прийти не в том формате,

69
00:04:54,295 --> 00:04:56,240
совсем-совсем не в том, который мы ожидаем.

70
00:04:56,240 --> 00:04:58,270
В данном случае мы явно задекларировали,

71
00:04:58,270 --> 00:05:01,580
когда ставили задачу, что разделитель — это слеш.

72
00:05:01,580 --> 00:05:03,340
Там может быть, например,

73
00:05:03,340 --> 00:05:06,190
буква "a", тут может быть буква "b",

74
00:05:06,190 --> 00:05:08,260
и это уже не та дата,

75
00:05:08,260 --> 00:05:10,150
которую нам надо распарсить,

76
00:05:10,150 --> 00:05:11,890
то есть непонятно вообще,

77
00:05:11,890 --> 00:05:14,770
дата ли это или это какой-то специальный код.

78
00:05:14,770 --> 00:05:18,100
Хорошо бы в таком случае донести до того,

79
00:05:18,100 --> 00:05:19,460
кто вызывает эту функцию о том,

80
00:05:19,460 --> 00:05:21,805
что формат входных данных — он неправилен.

81
00:05:21,805 --> 00:05:25,010
Но сейчас функция этого совершенно не делает,

82
00:05:25,010 --> 00:05:27,850
то есть мы запустим данные кода, он скомпилируется,

83
00:05:27,850 --> 00:05:30,820
и из этой какой-то странной строчки,

84
00:05:30,820 --> 00:05:32,380
которая совсем не похожа на дату,

85
00:05:32,380 --> 00:05:35,950
мы извлекли 25 января 2017 года.

86
00:05:35,950 --> 00:05:37,525
Почему так получается?

87
00:05:37,525 --> 00:05:41,800
Потому что мы считаем сначала 2017 в переменную год,

88
00:05:41,800 --> 00:05:44,260
после этого пропустим букву "a",

89
00:05:44,260 --> 00:05:46,090
после этого считаем месяц,

90
00:05:46,090 --> 00:05:48,645
пропустим букву "b" и считаем день.

91
00:05:48,645 --> 00:05:52,265
Как от этой ситуации можно защититься?

92
00:05:52,265 --> 00:05:55,915
У потока есть метод "peek",

93
00:05:55,915 --> 00:06:00,150
он позволяет посмотреть, какой следующий символ идет в потоке.

94
00:06:00,150 --> 00:06:04,130
C одной стороны, мы можем написать много проверок на то,

95
00:06:04,130 --> 00:06:07,915
что если функция "peek" возвращает не слеш,

96
00:06:07,915 --> 00:06:10,515
то все плохо, надо вернуть "false".

97
00:06:10,515 --> 00:06:12,920
Тогда у нас еще и, замечу,

98
00:06:12,920 --> 00:06:18,290
поменяется сразу сигнатура функции: "ParseDate" должна будет возвращать "bool" и, видимо,

99
00:06:18,290 --> 00:06:21,875
отдельным аргументом она должна будет принимать дату,

100
00:06:21,875 --> 00:06:23,450
которую мы будем модифицировать,

101
00:06:23,450 --> 00:06:28,550
и вот этот флажок "bool" — он будет означать: удалось нам дату распарсить или не удалось.

102
00:06:28,550 --> 00:06:30,965
При этом, так надо будет делать вообще во всех местах,

103
00:06:30,965 --> 00:06:32,705
где мы используем эту функцию.

104
00:06:32,705 --> 00:06:36,020
Это немного утомительно, это усложняет наш

105
00:06:36,020 --> 00:06:39,555
код и поэтому я даже писать не буду эту реализацию.

106
00:06:39,555 --> 00:06:45,875
Давайте рассмотрим то, что есть в языке C++ специально для отлова таких ситуаций.

107
00:06:45,875 --> 00:06:48,050
Это исключение.

108
00:06:48,050 --> 00:06:50,630
Исключение — это специальный механизм,

109
00:06:50,630 --> 00:06:54,455
который позволяет сообщить вызывающему коду,

110
00:06:54,455 --> 00:06:58,100
что произошла какая-то ошибка: что-то пошло не так,

111
00:06:58,100 --> 00:06:59,443
что-то пошло не по плану.

112
00:06:59,443 --> 00:07:02,300
Мы ожидали одни данные, а получили другие.

113
00:07:02,300 --> 00:07:04,805
Мы ожидали, что интернет у нас есть,

114
00:07:04,805 --> 00:07:06,165
а интернета у нас нету.

115
00:07:06,165 --> 00:07:08,900
В общем, это непредвиденная ситуация,

116
00:07:08,900 --> 00:07:12,275
о которой мы хотим явно сообщать.

117
00:07:12,275 --> 00:07:17,929
Давайте просто рассмотрим пример и дальше станет,

118
00:07:17,929 --> 00:07:20,292
я надеюсь, все намного понятнее.

119
00:07:20,292 --> 00:07:23,225
Во-первых, давайте.

120
00:07:23,225 --> 00:07:26,840
Давайте сначала просто будем считывать наши символы

121
00:07:26,840 --> 00:07:30,457
из потока и сообщать об этих исключительных ситуациях.

122
00:07:30,457 --> 00:07:41,764
То есть напишем: "if (stream.peek()!= '/')",

123
00:07:41,764 --> 00:07:48,295
то воспользуемся специальным синтаксисом: "throw",

124
00:07:48,295 --> 00:07:52,426
а дальше выбрасываем в специальный класс в языке C++ — "exception".

125
00:07:52,426 --> 00:07:57,040
Это класс-исключение, которое сообщит вызывающему коду,

126
00:07:57,040 --> 00:08:02,427
то есть функции "main" из которой мы и вызываем функцию "ParseDate",

127
00:08:02,427 --> 00:08:04,190
что пошло что-то не так.

128
00:08:04,190 --> 00:08:06,755
Здесь мы делаем две проверки.

129
00:08:06,755 --> 00:08:10,270
Первую, после того как считываем год, проверяем,

130
00:08:10,270 --> 00:08:12,310
что если следующий символ слеш,

131
00:08:12,310 --> 00:08:14,420
то соответственно мы будем его игнорировать.

132
00:08:14,420 --> 00:08:16,150
Если следующий символ не слеш,

133
00:08:16,150 --> 00:08:19,465
то нам нужно кинуть исключение и сообщить вызывающему коду,

134
00:08:19,465 --> 00:08:21,519
то есть функции "main",

135
00:08:21,519 --> 00:08:23,595
из которой мы и вызываем эту функцию,

136
00:08:23,595 --> 00:08:25,910
что что-то пошло не так.

137
00:08:25,910 --> 00:08:28,370
Замечу, что проверку надо сделать и после того,

138
00:08:28,370 --> 00:08:29,630
как мы считаем месяц,

139
00:08:29,630 --> 00:08:31,550
так как там тоже ожидается слеш.

140
00:08:31,550 --> 00:08:34,680
Соответственно, проверяем следующие символы с потока,

141
00:08:34,680 --> 00:08:37,550
если он не слеш, то бросаем исключение.

142
00:08:37,550 --> 00:08:41,990
Давайте сделаем нашу дату снова правильной, вернемся сюда слеши,

143
00:08:41,990 --> 00:08:44,225
сейчас код должен отработать без ошибок.

144
00:08:44,225 --> 00:08:46,640
Да! Он отработал.

145
00:08:46,640 --> 00:08:53,750
Сделаем строчку невалидной. Вот, мы видим,

146
00:08:53,750 --> 00:08:57,290
что что-то пошло не так, программа упала,

147
00:08:57,290 --> 00:09:02,775
но, по крайней мере, код перестал выполняться, и ладно,

148
00:09:02,775 --> 00:09:04,373
это уже лучше, чем то,

149
00:09:04,373 --> 00:09:09,215
что мы получаем какую-то странную дату и можем сделать какие-то странные транзакции.

150
00:09:09,215 --> 00:09:12,535
Давайте для начала уберем дублирование,

151
00:09:12,535 --> 00:09:15,520
потому что здесь явно повторяются одни и те же действия.

152
00:09:15,520 --> 00:09:18,860
Мы проверяем следующий символ в потоке.

153
00:09:18,860 --> 00:09:20,365
Если он не слеш,

154
00:09:20,365 --> 00:09:22,645
то мы просто кидаем исключение,

155
00:09:22,645 --> 00:09:26,475
иначе — игнорируем этот символ.

156
00:09:26,475 --> 00:09:35,145
Создадим функцию: "EnsureNextSymbolAndSkip".

157
00:09:35,145 --> 00:09:40,000
Будем сюда передавать строковый поток.

158
00:09:41,780 --> 00:09:47,620
После этого мы можем прямо вставить туда этот код.

159
00:09:48,760 --> 00:09:51,398
Эта функция просто проверяет,

160
00:09:51,398 --> 00:09:54,309
что следующий символ слеш и если это слеш,

161
00:09:54,309 --> 00:09:58,600
то выкидывает его, иначе бросает исключение.

162
00:09:58,600 --> 00:10:03,215
Соответственно, здесь мы должны ее вызвать на нашем потоке,

163
00:10:03,215 --> 00:10:06,070
на нашем строковом потоке.

164
00:10:07,720 --> 00:10:10,120
И вызвать ее еще раз.

165
00:10:10,120 --> 00:10:13,295
Отлично! Считали год, проверили данные,

166
00:10:13,295 --> 00:10:18,465
считали месяц, проверили данные.

167
00:10:18,465 --> 00:10:21,765
Как правильно обрабатывать исключения?

168
00:10:21,765 --> 00:10:24,990
Всем нам понятно, что вот такое падение программы,

169
00:10:24,990 --> 00:10:27,140
которое сейчас произошло на ваших глазах,

170
00:10:27,140 --> 00:10:28,445
это на самом деле плохо.

171
00:10:28,445 --> 00:10:31,380
Представьте, работаете вы в каком-нибудь приложении,

172
00:10:31,380 --> 00:10:33,690
сидите в интернете, вдруг ваш браузер падает,

173
00:10:33,690 --> 00:10:36,325
все вкладки закрылись, закладки потерлись,

174
00:10:36,325 --> 00:10:37,795
история потерлась, конечно же,

175
00:10:37,795 --> 00:10:39,525
это из рук вон плохо.

176
00:10:39,525 --> 00:10:44,063
Все ошибки надо правильно обрабатывать.

177
00:10:44,063 --> 00:10:49,710
Для того, чтобы обработать ошибку в языке C++ есть специальный синтаксис.

178
00:10:49,710 --> 00:10:53,401
Начинается он с ключевого слова "try",

179
00:10:53,401 --> 00:10:54,975
дальше идут фигурные скобки,

180
00:10:54,975 --> 00:10:59,290
в этих фигурных скобках нужно написать тот код,

181
00:10:59,290 --> 00:11:02,435
который потенциально может кидать исключения.

182
00:11:02,435 --> 00:11:05,280
Пока я поставлю просто многоточие.

183
00:11:05,280 --> 00:11:09,720
После блока "try" идет блок "catch".

184
00:11:11,400 --> 00:11:16,530
При этом в "catch" мы напишем класс,

185
00:11:16,530 --> 00:11:20,915
который мы хотим получить,

186
00:11:20,915 --> 00:11:24,182
то есть "catch" мы говорим: "Поймай следующее исключение".

187
00:11:24,182 --> 00:11:26,645
Все исключения наследуются от класса exception,

188
00:11:26,645 --> 00:11:29,570
поэтому мы можем ловить по exception-ссылкам.

189
00:11:29,570 --> 00:11:35,305
И здесь мы пишем какой-то обработчик нашей ошибки.

190
00:11:35,305 --> 00:11:38,854
Давайте проверим все это на практике,

191
00:11:38,854 --> 00:11:42,980
то есть опасной функцией у нас является функция "ParseDate".

192
00:11:44,210 --> 00:11:50,031
Давайте занесем все это в блок "try",

193
00:11:50,031 --> 00:11:55,605
после этого сделаем блок

194
00:11:55,605 --> 00:12:05,320
"catch" и здесь мы напишем,

195
00:12:05,620 --> 00:12:11,432
что что-то пошло не так: "exception happens".

196
00:12:11,432 --> 00:12:16,805
Попробуем запустить наш код.

197
00:12:16,805 --> 00:12:19,190
Замечательно! Видим строчку "exception happens",

198
00:12:19,190 --> 00:12:23,260
то есть функция проверки данных выбросила исключение,

199
00:12:23,260 --> 00:12:25,478
потому что там не было слеша,

200
00:12:25,478 --> 00:12:28,215
а мы его поймали и написали, что произошла беда.

201
00:12:28,215 --> 00:12:30,565
Программа при этом завершается нормально,

202
00:12:30,565 --> 00:12:33,120
с нулевым кодом возврата.

203
00:12:34,320 --> 00:12:38,595
Давайте заменим обратно на слеши.

204
00:12:38,595 --> 00:12:43,190
Видим, что дата распарсилась и написалась на экран.

205
00:12:43,190 --> 00:12:47,360
Хорошо бы донести до вызывающего кода в чем произошла ошибка.

206
00:12:47,360 --> 00:12:52,685
То есть хорошо бы в exception как-то записать информацию о том, что пошло не так.

207
00:12:52,685 --> 00:12:55,654
Проблема ли в окружении, то есть, там,

208
00:12:55,654 --> 00:12:57,350
отсутствует файлик, например, мы кидаем

209
00:12:57,350 --> 00:12:59,810
исключение и говорим: "Файл не найден по такому-то пути".

210
00:12:59,810 --> 00:13:02,870
Сразу становится понятно, что там надо просто его создать.

211
00:13:02,870 --> 00:13:04,828
Либо какой-то баг в коде,

212
00:13:04,828 --> 00:13:08,780
что функция ожидает на вход какую-нибудь матрицу фиксированной длины или массив,

213
00:13:08,780 --> 00:13:10,970
приходит что-то другое и, таким образом,

214
00:13:10,970 --> 00:13:12,500
она сообщает о том,

215
00:13:12,500 --> 00:13:14,225
что что-то пошло не так.

216
00:13:14,225 --> 00:13:17,367
Для этого есть "runtime_error",

217
00:13:17,367 --> 00:13:20,356
внутрь которой можно передать сообщение об ошибке.

218
00:13:20,356 --> 00:13:22,093
И, собственно, давайте его создадим.

219
00:13:22,093 --> 00:13:26,848
Для этого я заиспользую класс "stringstream",

220
00:13:26,848 --> 00:13:29,960
туда положу следующий текст: "expected / ,

221
00:13:29,960 --> 00:13:39,120
but has: " и запишем собственно символ,

222
00:13:39,340 --> 00:13:42,380
который был в потоке.

223
00:13:42,630 --> 00:13:45,861
После этого вызовем метод "str",

224
00:13:45,861 --> 00:13:47,455
который из потока вернет строчку,

225
00:13:47,455 --> 00:13:49,570
которая в нем записана.

226
00:13:49,570 --> 00:13:53,839
А далее, чтобы получить текст этого сообщения,

227
00:13:53,839 --> 00:13:59,485
мы воспользуемся методом, который есть у класса exception. Метод называется "what".

228
00:13:59,485 --> 00:14:02,315
Если в сообщении есть какой-то текст,

229
00:14:02,315 --> 00:14:07,080
он здесь будет возвращен при вызове метода "what".

230
00:14:07,080 --> 00:14:09,620
Запустим наш код, видим,

231
00:14:09,620 --> 00:14:12,880
что дата сейчас парсится хорошо.

232
00:14:12,880 --> 00:14:16,500
Давайте попробуем сделать ее плохой.

233
00:14:17,070 --> 00:14:21,745
Видим, что случилось: случилось исключение,

234
00:14:21,745 --> 00:14:24,595
мы ожидали слеш, а получили 97.

235
00:14:24,595 --> 00:14:29,520
На самом деле 97 — это код буквы "a".

236
00:14:29,520 --> 00:14:36,720
Давайте приведем к символу то,

237
00:14:36,720 --> 00:14:40,940
что возвращает метод "peek" и запустим еще раз.

238
00:14:40,940 --> 00:14:44,865
Данным кодом я говорю просто: "Преобразуй число,

239
00:14:44,865 --> 00:14:49,255
которое здесь возвращается к char, к символу".

240
00:14:49,255 --> 00:14:51,770
Теперь мы видим, сообщение стало более внятным,

241
00:14:51,770 --> 00:14:56,380
что случилось исключение, мы ожидали слеш, а получили букву "a".

242
00:14:56,380 --> 00:15:01,305
Что самое интересное, в том месте,

243
00:15:01,305 --> 00:15:05,655
где бросается исключение было вызвано уже две функции от,

244
00:15:05,655 --> 00:15:07,950
начиная с того места, где мы исключение только ловим.

245
00:15:07,950 --> 00:15:10,480
Что я имею в виду? Смотрите, вот мы объявляем блок "try",

246
00:15:10,480 --> 00:15:14,805
и говорим: "ParseDate от данной строчки".

247
00:15:14,805 --> 00:15:16,670
После этого запускается функция: "ParseDate".

248
00:15:16,670 --> 00:15:19,510
Пошла первая степень вложенности.

249
00:15:19,510 --> 00:15:22,755
Мы уже внутри следующей функции.

250
00:15:22,755 --> 00:15:25,440
Начинаем работу с потоком, вызываем функцию: "EnsureNextSymbolAndSkip".

251
00:15:25,440 --> 00:15:31,960
Попадаем сюда, уже вторая степень вложенности

252
00:15:31,960 --> 00:15:38,895
от изначальной точки и только здесь мы бросаем исключение с нашим сообщением об ошибке.

253
00:15:38,895 --> 00:15:45,840
При этом ловим его уже в функции самого верхнего уровня,

254
00:15:45,840 --> 00:15:48,921
то есть возвращаемся на нулевой уровень и там логируем,

255
00:15:48,921 --> 00:15:51,360
что что-то пошло не так.

256
00:15:52,320 --> 00:15:54,970
В этой лекции мы узнали о том,

257
00:15:54,970 --> 00:15:57,500
как работать с исключениями в языке C++,

258
00:15:57,500 --> 00:15:59,710
как обрабатывать исключительные ситуации,

259
00:15:59,710 --> 00:16:03,490
которые у вас появляются и что для этого нужно сделать.

260
00:16:03,490 --> 00:16:05,765
Теперь мы знакомы с исключениями в языке C++.

261
00:16:05,765 --> 00:16:08,335
Узнали когда их нужно применять.

262
00:16:08,335 --> 00:16:09,580
Напомню, это те случаи,

263
00:16:09,580 --> 00:16:11,890
когда в вашей программе что-то идет не по плану.

264
00:16:11,890 --> 00:16:14,050
Узнали как их обрабатывать,

265
00:16:14,050 --> 00:16:17,005
познакомились с блоками "try" и "catch".

266
00:16:17,005 --> 00:16:18,790
Напомню, что "try" — это тот блок

267
00:16:18,790 --> 00:16:21,505
в котором может потенциально произойти какая-нибудь ошибка,

268
00:16:21,505 --> 00:16:23,065
потенциально опасный блок кода,

269
00:16:23,065 --> 00:16:25,045
который может выбросить исключение.

270
00:16:25,045 --> 00:16:28,310
А в блоке "catch" вы его ловите и правильно обрабатываете.

271
00:16:28,310 --> 00:16:33,130
Напомню, что некоторые классы-исключения позволяют сохранять в себе ошибку,

272
00:16:33,130 --> 00:16:36,985
в которой содержатся описания этой нестандартной ситуации, которая случилась.

273
00:16:36,985 --> 00:16:40,810
В наших примерах это был неправильный разделитель и мы сообщали о том,

274
00:16:40,810 --> 00:16:45,050
что мы ожидали один разделитель, а получили на самом деле другой.