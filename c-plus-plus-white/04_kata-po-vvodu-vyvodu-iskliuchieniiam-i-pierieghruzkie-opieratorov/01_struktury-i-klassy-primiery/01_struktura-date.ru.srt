1
00:00:00,000 --> 00:00:08,413
Итак, мы подробно изучили классы.

2
00:00:08,413 --> 00:00:12,245
Мы изучили, что в классах бывают поля,
бывают методы, конструкторы и деструкторы.

3
00:00:12,245 --> 00:00:16,490
Давайте наконец рассмотрим какой-нибудь
реальный и полезный пример класса.

4
00:00:16,490 --> 00:00:20,010
Например, часто приходится
иметь дело с датами.

5
00:00:20,010 --> 00:00:21,699
Дата — это по сути что?

6
00:00:21,699 --> 00:00:24,665
Это день, месяц и год — три целых числа.

7
00:00:24,665 --> 00:00:27,821
Логично просто взять и написать структуру.

8
00:00:27,821 --> 00:00:29,320
Напишем структуру.

9
00:00:29,320 --> 00:00:35,423
Структура Date, в ней будет поле day,

10
00:00:35,423 --> 00:00:41,520
поле month и поле year — день,
месяц и год.

11
00:00:41,520 --> 00:00:44,448
Как мы будем эту структуру использовать?

12
00:00:44,448 --> 00:00:49,048
Наверное, очень просто,
вот так вот: создаем объект типа Date,

13
00:00:49,048 --> 00:00:54,931
называем его date и пишем с помощью
фигурных скобок какую-нибудь дату.

14
00:00:54,931 --> 00:00:59,080
Например, 1 января 2017 года.

15
00:00:59,080 --> 00:01:02,830
Пробуем скомпилировать этот код.

16
00:01:02,830 --> 00:01:10,630
Код компилируется, скомпилировался.

17
00:01:10,630 --> 00:01:11,830
Хорошо.

18
00:01:11,830 --> 00:01:15,174
Давайте, наверное,
как-нибудь эту дату выведем.

19
00:01:15,174 --> 00:01:21,360
Напишем функцию PrintDate, которая примет
на вход дату по константной ссылке,

20
00:01:21,360 --> 00:01:26,863
чтобы лишний раз не копировать,
и здесь ее выведет.

21
00:01:26,863 --> 00:01:31,974
date.day — наверное,
логично вывести их через точку, —

22
00:01:31,974 --> 00:01:37,475
date.month, опять же точка, и date.year.

23
00:01:37,475 --> 00:01:44,080
Перевод строки, и здесь эту дату выведем.

24
00:01:44,080 --> 00:01:50,698
[ЗВУК] Давайте этот код

25
00:01:50,698 --> 00:01:55,790
запустим и увидим нашу дату — 1 января.

26
00:01:55,790 --> 00:02:00,643
Казалось бы, все нормально,
но представьте теперь, что вы смотрите

27
00:02:00,643 --> 00:02:05,139
на этот код спустя несколько лет,
или кто-то другой смотрит на этот код,

28
00:02:05,139 --> 00:02:09,750
причем он на него смотрит так, что не
видит само определение структуры Date.

29
00:02:09,750 --> 00:02:11,350
Вот, скажем, вот так вот.

30
00:02:11,350 --> 00:02:17,240
И здесь написано не {1, 1, 2017}, а
что-то вот такое, например, {10, 11, 12}.

31
00:02:17,240 --> 00:02:23,120
Можете ли вы спустя несколько лет
после того, как вы написали этот код,

32
00:02:23,120 --> 00:02:27,090
или смотря на чужой вот такой вот код,
сказать, какая это дата?

33
00:02:27,090 --> 00:02:31,347
Может быть, это код русского человека,
который привык сначала писать день,

34
00:02:31,347 --> 00:02:32,745
потом месяц, потом год.

35
00:02:32,745 --> 00:02:35,367
Тогда это 10-е ноября 12-го года.

36
00:02:35,367 --> 00:02:40,211
Может быть, это код американца,
и тогда это будет 11-е октября 12-го года.

37
00:02:40,211 --> 00:02:42,120
А может быть, этот код писал программист,

38
00:02:42,120 --> 00:02:45,430
и тогда у вас получается
12-е ноября 10-го года.

39
00:02:45,430 --> 00:02:49,832
То есть нельзя вот по такой записи сразу
легко сказать, где здесь день, где месяц,

40
00:02:49,832 --> 00:02:50,390
а где год.

41
00:02:50,390 --> 00:02:52,216
Как же мы решим эту проблему?

42
00:02:52,216 --> 00:02:55,070
Мы напишем для нашей
структуры конструктор.

43
00:02:55,070 --> 00:03:01,775
Но если конструктор будет принимать три
целых числа, то легче нам не станет.

44
00:03:01,775 --> 00:03:05,314
Мы все так же будем видеть вот здесь три
числа и не будем понимать, где день,

45
00:03:05,314 --> 00:03:06,380
где месяц, а где год.

46
00:03:06,380 --> 00:03:12,201
Поэтому конструктор будет принимать не
целые числа, а некоторые обертки над ними.

47
00:03:12,201 --> 00:03:16,750
А именно он будет принимать
объект типа Day, new_day,

48
00:03:16,750 --> 00:03:22,779
объект типа Month,
который мы назовем new_month,

49
00:03:22,779 --> 00:03:27,150
и объект типа Year,
который мы назовем new_year.

50
00:03:27,150 --> 00:03:30,200
Что это за типы?

51
00:03:30,200 --> 00:03:32,727
Это на самом деле простые
структуры с одним полем.

52
00:03:32,727 --> 00:03:35,620
Сейчас мы увидим, зачем это нужно,
как это нам поможет.

53
00:03:35,620 --> 00:03:39,840
Итак, структура Day имеет одно
единственное целочисленное поле,

54
00:03:39,840 --> 00:03:42,183
назовем его value, например.

55
00:03:42,183 --> 00:03:45,140
Аналогично выглядит структура Month,

56
00:03:45,140 --> 00:03:49,947
тоже с единственным полем value,

57
00:03:49,947 --> 00:03:55,870
и структура Year, то же самое, int value.

58
00:03:55,870 --> 00:04:01,570
И конструктор принимает,

59
00:04:01,570 --> 00:04:05,780
собственно, объекты этих типов и
инициализирует поля этой структуры.

60
00:04:05,780 --> 00:04:10,740
day = new_day.value,

61
00:04:10,740 --> 00:04:15,690
month = new_month.value,

62
00:04:15,690 --> 00:04:22,390
year = new_year.value.

63
00:04:22,390 --> 00:04:29,090
Скомпилируется ли наш текущий код теперь?

64
00:04:29,090 --> 00:04:31,849
Давайте посмотрим.

65
00:04:31,849 --> 00:04:34,274
Что-то пошло не так.

66
00:04:34,274 --> 00:04:38,979
Да, компилятор, видя запись {10,
11, 12}, не знает,

67
00:04:38,979 --> 00:04:42,050
как по ним получить объекты day,
month и year.

68
00:04:42,050 --> 00:04:43,102
Хорошо.

69
00:04:43,102 --> 00:04:47,177
Это вынуждает нас написать вот такой

70
00:04:47,177 --> 00:04:51,950
код: Day{10},

71
00:04:51,950 --> 00:04:57,050
Month{11}, Year{12}.

72
00:04:57,050 --> 00:05:00,981
По сути,
я сказал компилятору: «Вот здесь создай

73
00:05:00,981 --> 00:05:05,842
объект типа Day со значением 10, здесь
создай объект типа Month со значением 11,

74
00:05:05,842 --> 00:05:08,560
здесь создай объект типа
Year со значением 12».

75
00:05:08,560 --> 00:05:11,333
Поскольку это структура,
то конструкторы мне не нужны,

76
00:05:11,333 --> 00:05:13,221
я просто использую фигурные скобки.

77
00:05:13,221 --> 00:05:15,530
Давайте попробуем этот код скомпилировать.

78
00:05:15,530 --> 00:05:18,203
Код не компилируется.

79
00:05:18,203 --> 00:05:19,272
Почему?

80
00:05:19,272 --> 00:05:25,246
Потому что компилятор смотрит на
старый файл, я его не сохранил.

81
00:05:25,246 --> 00:05:28,500
Теперь я его сохранил,
и код компилируется.

82
00:05:28,500 --> 00:05:31,448
Отлично, 10.11.12.

83
00:05:31,448 --> 00:05:32,870
Код работает.

84
00:05:32,870 --> 00:05:36,910
Во-первых, у нас получилось,
что мы явно по этому коду видим,

85
00:05:36,910 --> 00:05:39,640
что здесь день, здесь месяц, а здесь год.

86
00:05:39,640 --> 00:05:45,260
Во-вторых, если вдруг я перепутаю,
и у меня сначала будет месяц,

87
00:05:45,260 --> 00:05:50,885
а потом день,
то такой код не скомпилируется.

88
00:05:50,885 --> 00:05:54,865
Ну просто потому, что компилятор
ожидает сначала день, потом месяц,

89
00:05:54,865 --> 00:05:56,900
а у меня сначала месяц, потом день.

90
00:05:56,900 --> 00:06:01,644
Месяц нельзя привести ко дню, и наоборот.

91
00:06:01,644 --> 00:06:02,675
Хорошо.

92
00:06:02,675 --> 00:06:03,667
Все ли сейчас в порядке?

93
00:06:03,667 --> 00:06:05,744
На самом деле, да,
уже можно писать читаемый код.

94
00:06:05,744 --> 00:06:08,450
Но тем не менее, опять же,
представьте себя через несколько лет.

95
00:06:08,450 --> 00:06:11,580
Или представьте,
что кто-то другой пришел менять ваш код.

96
00:06:11,580 --> 00:06:14,904
Точнее, пришел его посмотреть,
заодно решил что-нибудь улучшить.

97
00:06:14,904 --> 00:06:18,730
И вот он видит, что вы явно пишете
Day{10}, Month{11}, Year{12}.

98
00:06:18,730 --> 00:06:21,800
А он знает, ну или вы,
собственно, уже знаете,

99
00:06:21,800 --> 00:06:25,754
что если вы хотите вызвать конструктор
от каких-то вот таких объектов,

100
00:06:25,754 --> 00:06:27,651
и компилятор уже знает, что, например,

101
00:06:27,651 --> 00:06:32,089
первый параметр конструктора имеет тип
Day, вам не нужно явно указывать этот тип.

102
00:06:32,089 --> 00:06:35,778
Вы можете просто оставить
пустые фигурные скобки с 10,

103
00:06:35,778 --> 00:06:40,380
а здесь просто фигурные скобки с 11,
а здесь фигурные скобки с 12.

104
00:06:40,380 --> 00:06:45,352
То есть вы, возможно, забыли о том,
что это все делалось для лучшей

105
00:06:45,352 --> 00:06:49,828
читаемости кода,
и решили код сделать более коротким.

106
00:06:49,828 --> 00:06:53,788
И при всем при этом он
продолжает компилироваться.

107
00:06:53,788 --> 00:06:56,641
Собственно, мы этот вопрос уже изучали.

108
00:06:56,641 --> 00:06:58,630
Если компилятор явно видит,

109
00:06:58,630 --> 00:07:01,518
какую переменную какого типа
принимает функция или конструктор,

110
00:07:01,518 --> 00:07:04,820
можно просто указать фигурные скобки
со списком аргументов конструктора.

111
00:07:04,820 --> 00:07:06,332
Вот, код работает.

112
00:07:06,332 --> 00:07:11,038
Как же нам сделать его более
устойчивым к таким вот оптимизациям,

113
00:07:11,038 --> 00:07:12,810
к таким вот изменениям?

114
00:07:12,810 --> 00:07:14,216
Давайте сделаем следующее.

115
00:07:14,216 --> 00:07:18,440
Давайте все-таки напишем конструкторы
этим структурам, простейшие конструкторы,

116
00:07:18,440 --> 00:07:21,561
от одного целого числа.

117
00:07:21,561 --> 00:07:25,430
Конструктор в структуре Day,

118
00:07:25,430 --> 00:07:30,325
давайте здесь напишем new_value,
value = new_value.

119
00:07:30,325 --> 00:07:32,942
Простейший конструктор.

120
00:07:32,942 --> 00:07:39,590
То же самое для месяца, [ЗВУК]

121
00:07:39,590 --> 00:07:47,652
new_value, value = new_value.

122
00:07:47,652 --> 00:07:51,350
И то же самое для года.

123
00:07:51,350 --> 00:08:00,311
[ЗВУК] На самом деле,

124
00:08:00,311 --> 00:08:02,906
сейчас лучше не стало.

125
00:08:02,906 --> 00:08:07,968
Мы даже можем это увидеть,
снова запустив этот код.

126
00:08:07,968 --> 00:08:09,566
Да, код работает.

127
00:08:09,566 --> 00:08:12,198
На самом деле, стало даже хуже.

128
00:08:12,198 --> 00:08:17,650
Давайте увидим это, вернувшись к
первоначальному варианту создания даты.

129
00:08:17,650 --> 00:08:21,749
Мы снова можем не указывать фигурные
скобки, мы снова можем писать,

130
00:08:21,749 --> 00:08:24,470
как в самом начале: {10, 11, 12}.

131
00:08:24,470 --> 00:08:26,149
Давайте это увидим.

132
00:08:26,149 --> 00:08:29,899
Вот наш код успешно
компилируется и запускается.

133
00:08:29,899 --> 00:08:30,789
В чем же дело?

134
00:08:30,789 --> 00:08:35,328
Мы, написав вот такие вот конструкторы,
разрешили компилятору

135
00:08:35,328 --> 00:08:39,700
неявно преобразовывать целое
число в объект типа Day,

136
00:08:39,700 --> 00:08:43,292
неявно преобразовывать целое число
в объект типа Month и неявно преобразовывать

137
00:08:43,292 --> 00:08:45,985
целое число в объект типа Year,
что компилятор и сделал.

138
00:08:45,985 --> 00:08:51,070
Он смог спокойно по аргументам 10, 11,
12 вызвать, получить Day, Month и Year.

139
00:08:51,070 --> 00:08:56,150
Чтобы такого не происходило, мы должны
компилятору сказать: «Не делай так больше.

140
00:08:56,150 --> 00:09:00,213
Не осуществляй неявное преобразование
целого числа вот к этим моим замечательным

141
00:09:00,213 --> 00:09:01,090
структуркам».

142
00:09:01,090 --> 00:09:02,128
Как мы это сделаем?

143
00:09:02,128 --> 00:09:04,090
Мы напишем ключевое слово explicit.

144
00:09:04,090 --> 00:09:05,986
Explicit означает «явный»,

145
00:09:05,986 --> 00:09:10,630
мы сделаем эти конструкторы явными и
запретим их вызывать без нашего ведома.

146
00:09:10,630 --> 00:09:16,080
Все три конструктора, Day,
Month и Year сделаем explicit.

147
00:09:16,080 --> 00:09:20,810
Теперь попробуем этот код

148
00:09:20,810 --> 00:09:25,625
запустить.

149
00:09:25,625 --> 00:09:30,440
Видим, что у нас есть какие-то ошибки.

150
00:09:30,440 --> 00:09:34,542
Собственно, компилятор снова не может
по числам 10, 11 и 12 получить день,

151
00:09:34,542 --> 00:09:35,190
месяц и год.

152
00:09:35,190 --> 00:09:40,930
Более того, мы не можем не указывать типы,
даже если мы используем фигурные скобки.

153
00:09:40,930 --> 00:09:46,110
Мы все еще не сказали компилятору
явно вызвать нужные конструкторы,

154
00:09:46,110 --> 00:09:48,314
поэтому код не компилируется.

155
00:09:48,314 --> 00:09:55,191
Вот компилятор пишет, что он не может по
десятке в фигурных скобках получить Day,

156
00:09:55,191 --> 00:09:59,681
потому что будет использован explicit
constructor, явный конструктор.

157
00:09:59,681 --> 00:10:04,112
Та же проблема с месяцем и с годом, везде
мы пытаемся вызвать явный конструктор вот

158
00:10:04,112 --> 00:10:05,718
с помощью такого синтаксиса.

159
00:10:05,718 --> 00:10:10,000
Так делать нельзя,
и поэтому нам придется написать вот так,

160
00:10:10,000 --> 00:10:12,120
как мы и написали вначале.

161
00:10:12,120 --> 00:10:19,213
Day{10}, Month{11} и Year{12}.

162
00:10:19,213 --> 00:10:24,158
Компилируем этот код, запускаем,
видим, что все хорошо.

163
00:10:24,158 --> 00:10:29,060
Более того, теперь, поскольку у нас явно
написаны конструкторы для структур Day,

164
00:10:29,060 --> 00:10:32,437
Month и Year, мы можем вместо
фигурных скобок использовать круглые,

165
00:10:32,437 --> 00:10:34,090
если вдруг кому-то так привычнее.

166
00:10:34,090 --> 00:10:38,800
Здесь круглая скобка,
здесь круглая скобка,

167
00:10:38,800 --> 00:10:42,204
и здесь тоже круглые скобки.

168
00:10:42,204 --> 00:10:46,511
Код успешно компилируется и запускается.

169
00:10:46,511 --> 00:10:48,686
Итак, что мы сейчас сделали?

170
00:10:48,686 --> 00:10:53,176
По сути, мы с помощью механизма
конструкторов, с помощью ключевого слова

171
00:10:53,176 --> 00:10:58,402
explicit научились писать
более читаемые структуры.

172
00:10:58,402 --> 00:11:02,490
Точнее, структуры,
у которых более читаемый способ создания.

173
00:11:02,490 --> 00:11:04,153
Мы явно должны указать...

174
00:11:04,153 --> 00:11:07,550
По сути, мы повторяем синтаксис
именованных полей,

175
00:11:07,550 --> 00:11:09,994
которые есть в некоторых
языках программирования.

176
00:11:09,994 --> 00:11:12,610
В C++ этого нет,
поэтому можем сделать вот такой вот,

177
00:11:12,610 --> 00:11:16,053
такой вот способ использовать с
помощью трех вспомогательных структур.

178
00:11:16,053 --> 00:11:19,835
Делаем три вспомогательные структуры,
пишем для них explicit конструкторы,

179
00:11:19,835 --> 00:11:23,763
явные конструкторы, и после этого можем
использовать вот такой вот синтаксис

180
00:11:23,763 --> 00:11:27,060
создания даты, явно указывая,
где день, где месяц, а где год.