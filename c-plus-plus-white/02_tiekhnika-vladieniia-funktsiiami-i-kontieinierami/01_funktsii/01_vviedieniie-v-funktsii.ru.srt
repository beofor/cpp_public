1
00:00:00,000 --> 00:00:09,424
В этом видео мы с вами узнаем,
как писать функции в C++.

2
00:00:09,424 --> 00:00:12,289
Прежде, весь код,
который мы с вами рассматривали,

3
00:00:12,289 --> 00:00:13,990
мы писали внутри функции Main.

4
00:00:13,990 --> 00:00:17,753
Конечно же,
в C++ можно писать и другие функции.

5
00:00:17,753 --> 00:00:22,742
Давайте для начала посмотрим, зачем,
вообще, разбивать код на функции.

6
00:00:22,742 --> 00:00:27,330
Ну, во-первых, разделение программы
на функции упрощает ее понимание.

7
00:00:27,330 --> 00:00:32,018
Вы берете кусок кода, даете ему
какое-то имя, которое помогает понять,

8
00:00:32,018 --> 00:00:34,590
объясняет, что этот кусок кода делает.

9
00:00:34,590 --> 00:00:40,020
Это, безусловно,
позволяет проще разобраться в программе.

10
00:00:40,020 --> 00:00:46,230
Кроме того, выделение кода функции
упрощает его повторное использование.

11
00:00:46,230 --> 00:00:47,545
Вы взяли какой-то код,

12
00:00:47,545 --> 00:00:50,620
вынесли его в функцию и вызвали
эту функцию в одном месте.

13
00:00:50,620 --> 00:00:52,527
Вы теперь можете взять эту функцию,

14
00:00:52,527 --> 00:00:56,970
вызвать ее в другом месте и использовать
для решения какой-то другой вашей задачи,

15
00:00:56,970 --> 00:01:01,805
что тоже существенно
ускоряет написание программ.

16
00:01:01,805 --> 00:01:06,640
Наконец, функции – это единственный
способ реализации рекурсивных алгоритмов.

17
00:01:06,640 --> 00:01:10,810
Итак, давайте посмотрим,
как же писать функции в C++.

18
00:01:10,810 --> 00:01:18,174
Переключимся в eclipse и вспомним пример,
который мы уже рассматривали.

19
00:01:18,174 --> 00:01:21,020
Это программа,
которая считает сумму двух чисел.

20
00:01:21,020 --> 00:01:25,795
Напишем ее: объявим переменные x,
y, считаем

21
00:01:25,795 --> 00:01:30,942
их из консоли и выведем x + y.

22
00:01:30,942 --> 00:01:36,967
Скомпилируем, запустим,
убедимся, что все работаем,

23
00:01:36,967 --> 00:01:41,985
введем 2 и 3, получили 5,
– все, мы написали программу.

24
00:01:41,985 --> 00:01:47,910
Давайте теперь вычисление суммы
вынесем в отдельную функцию.

25
00:01:47,910 --> 00:01:50,738
Что нам для этого надо сделать?

26
00:01:50,738 --> 00:01:55,869
Для этого мы напишем функцию Sum,
которая принимает два

27
00:01:55,869 --> 00:02:01,250
параметра и возвращает их сумму.

28
00:02:01,250 --> 00:02:06,641
Как нам теперь этой
функцией воспользоваться?

29
00:02:06,641 --> 00:02:10,773
Ну мы просто в месте,
где она нам необходима,

30
00:02:10,773 --> 00:02:13,932
вызовем Sum и передадим туда x и y.

31
00:02:13,932 --> 00:02:18,330
Скомпилируем, запустим,

32
00:02:18,330 --> 00:02:21,750
введем, например, 3 и 4 и вот получили 7.

33
00:02:21,750 --> 00:02:23,657
Программа работает корректно.

34
00:02:23,657 --> 00:02:26,244
Значит, на что вам надо обратить внимание?

35
00:02:26,244 --> 00:02:27,817
Ну, во-первых, на то,

36
00:02:27,817 --> 00:02:32,930
что объявление функции в C++ начинается
с типа возвращаемого значения.

37
00:02:32,930 --> 00:02:37,390
Мы уже говорили, что C++ – это
язык со статической типизацией,

38
00:02:37,390 --> 00:02:44,276
и поэтому для функций тоже надо указывать,
значение какого типа они возвращают.

39
00:02:44,276 --> 00:02:46,390
За типом идет имя функции.

40
00:02:46,390 --> 00:02:49,979
После имени в круглых скобках
мы перечисляем параметры.

41
00:02:49,979 --> 00:02:52,110
Разделяем мы параметры запятой.

42
00:02:52,110 --> 00:02:57,320
И здесь важно отметить, что для каждого
параметра функции нужно указать его тип.

43
00:02:57,320 --> 00:03:02,260
После описания параметров в
фигурных скобках идет тело функции,

44
00:03:02,260 --> 00:03:07,200
в котором можно выполнять
любые команды языка C++.

45
00:03:07,200 --> 00:03:08,824
Важно здесь отметить,

46
00:03:08,824 --> 00:03:13,970
что для возврата значений из функции
используется оператор return.

47
00:03:13,970 --> 00:03:18,726
Собственно, вот,
мы считаем сумму x + y, и,

48
00:03:18,726 --> 00:03:22,730
чтобы вернуть ее из функции,
вызываем return.

49
00:03:22,730 --> 00:03:26,014
У return есть одна особенность важная,

50
00:03:26,014 --> 00:03:30,295
которую надо помнить: он
завершает выполнение функции.

51
00:03:30,295 --> 00:03:34,210
Давайте рассмотрим пример,
который это демонстрирует.

52
00:03:34,210 --> 00:03:40,010
Допустим, у нас есть такая задача,
у нас есть набор слов,

53
00:03:40,010 --> 00:03:44,110
и нужно проверить,
входит ли данное слово в этот набор.

54
00:03:44,110 --> 00:03:47,598
Давайте оформим это в виде функции.

55
00:03:47,598 --> 00:03:50,530
Мы напишем функцию Contains,

56
00:03:50,530 --> 00:03:55,573
которая принимает вектор строк

57
00:03:55,573 --> 00:04:01,010
(это наш набор слов) и одну строку w,

58
00:04:01,010 --> 00:04:06,410
для которой мы и хотим проверить:
входит она в набор слов words или нет.

59
00:04:06,410 --> 00:04:11,181
Чтобы у нас это компилировалось,
нам надо добавить заголовочный

60
00:04:11,181 --> 00:04:15,215
файл vector и заголовочный файл string.

61
00:04:15,215 --> 00:04:18,517
Значит, обратите внимание,

62
00:04:18,517 --> 00:04:24,260
что для функции Contains я использую, в
качестве возвращаемого значения, тип bool,

63
00:04:24,260 --> 00:04:30,123
потому что она должна только сообщить:
есть слово в наборе, или нет его в наборе.

64
00:04:30,123 --> 00:04:33,247
Тут достаточно логического типа.

65
00:04:33,247 --> 00:04:36,568
Итак, реализуем нашу функцию.

66
00:04:36,568 --> 00:04:41,573
Напишем: for
(auto s : words) нам надо перебрать все

67
00:04:41,573 --> 00:04:46,930
элементы вектора words,
поэтому мы используем цикл for.

68
00:04:46,930 --> 00:04:48,375
Проверим.

69
00:04:48,375 --> 00:04:54,900
Если очередной элемент
вектора words равен w,

70
00:04:54,900 --> 00:04:59,580
то вернем true.

71
00:04:59,580 --> 00:05:05,468
Если мы так

72
00:05:05,468 --> 00:05:10,640
и не смогли найти слово, которое равно w,
то мы возвращаемся к нашей функции false.

73
00:05:10,640 --> 00:05:16,370
Давайте убедимся, что она работает.

74
00:05:16,370 --> 00:05:21,133
Допустим, выведем результат

75
00:05:21,133 --> 00:05:26,430
запуска функции Contains от
набора из слов, например,

76
00:05:26,430 --> 00:05:32,008
«воздух», «вода», «огонь»,

77
00:05:32,008 --> 00:05:37,287
и поищем в этом наборе слово,
например, «огонь».

78
00:05:37,287 --> 00:05:41,669
Скомпилируем, видим,
что программа компилируется,

79
00:05:41,669 --> 00:05:46,750
запустим, и мы видим,
что в консоль было выведено 1.

80
00:05:46,750 --> 00:05:50,900
Мы знаем, что слово «огонь» есть
в нашем наборе, поэтому мы знаем,

81
00:05:50,900 --> 00:05:52,920
что функция должна вернуть true.

82
00:05:52,920 --> 00:05:58,169
И у нас появилась 1,
потому что по умолчанию

83
00:05:58,169 --> 00:06:03,005
в C++ значения логического
типа выводятся как 0 и 1.

84
00:06:03,005 --> 00:06:09,009
Собственно, если в качестве
слова искомого мы возьмем milk,

85
00:06:09,009 --> 00:06:13,198
например, опять же скомпилируем,
запустим и мы видим,

86
00:06:13,198 --> 00:06:16,910
что вывелось 0,
потому что слова milk в нашем наборе нет.

87
00:06:16,910 --> 00:06:21,942
Итак, давайте теперь поищем в

88
00:06:21,942 --> 00:06:28,096
нашем наборе слово water, «вода».

89
00:06:28,096 --> 00:06:36,080
И давайте мы выполним нашу
программу в отладчике.

90
00:06:36,080 --> 00:06:38,345
Запускаем отладчик.

91
00:06:38,345 --> 00:06:43,564
Для этого я нажимаю на иконку «жучка́»

92
00:06:43,564 --> 00:06:49,925
в верхней части экрана.

93
00:06:49,925 --> 00:06:54,210
У меня запускается отладчик.

94
00:06:54,210 --> 00:06:59,125
Все, мы видим, что отладчик запустился и

95
00:06:59,125 --> 00:07:02,650
остановился на первой
строке нашей программы.

96
00:07:02,650 --> 00:07:08,060
Давайте выполним программу до
входа внутрь функции Contains.

97
00:07:08,060 --> 00:07:13,403
Для этого мы установим
курсор на ее первой строке

98
00:07:13,403 --> 00:07:18,520
и в меню Clips выберем пункт Run to Line.

99
00:07:18,520 --> 00:07:23,035
Все, значит, у нас она выполнилась,

100
00:07:23,035 --> 00:07:27,138
мы видим, что седьмая строчка нашей
программы подсветилась зеленым,

101
00:07:27,138 --> 00:07:32,250
то есть исполнение нашей программы
дошло до тела функции Contains.

102
00:07:32,250 --> 00:07:36,490
И давайте выполним нашу функцию по шагам.

103
00:07:36,490 --> 00:07:40,760
Зайдем в «Цикл».

104
00:07:40,760 --> 00:07:43,991
Сейчас у нас будет выполняться сравнение.

105
00:07:43,991 --> 00:07:51,106
Мы знаем, что мы ищем строчку water и
она не равна air, поэтому мы ожидаем,

106
00:07:51,106 --> 00:07:56,139
что при выполнении сравнения, мы не
зайдем внутрь тела условного оператора,

107
00:07:56,139 --> 00:08:00,300
а перейдем к следующей итерации цикла.

108
00:08:00,300 --> 00:08:04,922
Нажмем команду «Сделать шаг» в отладчике
и видим, что да, действительно,

109
00:08:04,922 --> 00:08:08,496
мы перешли в следующую итерацию цикла.

110
00:08:08,496 --> 00:08:15,303
Снова зайдем в тело цикла и теперь мы
ожидаем, что сравнение будет успешным,

111
00:08:15,303 --> 00:08:20,670
потому что water это второй
элемент нашего набора.

112
00:08:20,670 --> 00:08:22,240
Выполним.

113
00:08:22,240 --> 00:08:25,794
И мы видим, да,
мы действительно вошли внутрь цикла.

114
00:08:25,794 --> 00:08:27,760
И теперь какая у нас ситуация?

115
00:08:27,760 --> 00:08:31,485
У нас цикл наш for должен,
вообще, выполнить три итерации,

116
00:08:31,485 --> 00:08:33,910
потому что у нас три элемента в векторе.

117
00:08:33,910 --> 00:08:38,004
Но на втором мы нашли совпадение,

118
00:08:38,004 --> 00:08:41,782
поэтому мы выполняем return и мы видим,

119
00:08:41,782 --> 00:08:46,807
что исполнение переместилось в
закрывающую скобку тело функции.

120
00:08:46,807 --> 00:08:50,520
Собственно, выполнение
функции на этом закончилось.

121
00:08:50,520 --> 00:08:55,390
И когда мы делаем следующий шаг,
то оказываемся уже внутри функции main.

122
00:08:55,390 --> 00:08:59,044
Собственно, этот пример вам демонстрирует,

123
00:08:59,044 --> 00:09:03,782
что оператор return завершает
выполнение функции, независимо от того,

124
00:09:03,782 --> 00:09:08,480
какие действия там еще осталось выполнить.

125
00:09:08,480 --> 00:09:11,660
Подведем итоги.

126
00:09:11,660 --> 00:09:18,014
В этом видео мы узнали,
как писать функции в C++.

127
00:09:18,014 --> 00:09:23,910
Еще раз поговорили о том, какова
польза разбиения программы на функции.

128
00:09:23,910 --> 00:09:27,180
Теперь вы знаете синтаксис
описания функции.

129
00:09:27,180 --> 00:09:31,956
Знаете, что для возврата значения с
функции нужно использовать return,

130
00:09:31,956 --> 00:09:35,870
и знаете его важную особенность,
которая заключается в том,

131
00:09:35,870 --> 00:09:39,270
что оператор return завершает
выполнение функции.