1
00:00:00,000 --> 00:00:05,982
[БЕЗ_ЗВУКА] Давайте

2
00:00:05,982 --> 00:00:10,980
продолжим тему конструкторов и изучим так
называемые конструкторы по умолчанию.

3
00:00:10,980 --> 00:00:17,444
Итак, если мы написали
для нашего класса параметризованный

4
00:00:17,444 --> 00:00:22,199
конструктор, который принимает на вход две
строки, то теперь у меня не будет работать

5
00:00:22,199 --> 00:00:27,110
привычный мне синтаксис создания
переменной с какими-то значениями полей.

6
00:00:27,110 --> 00:00:31,725
Я не смогу написать «Route route;»,

7
00:00:31,725 --> 00:00:36,619
я не смогу создать просто какой-то маршрут,
не указывая конкретные названия городов.

8
00:00:36,619 --> 00:00:37,150
Почему?

9
00:00:37,150 --> 00:00:40,720
Потому что я уже сказал
компилятору: все отлично,

10
00:00:40,720 --> 00:00:43,068
я понимаю,
как я хочу создавать свои маршруты,

11
00:00:43,068 --> 00:00:45,072
я не хочу никакие маршруты по умолчанию,

12
00:00:45,072 --> 00:00:47,840
а хочу создавать маршруты
для двух конкретных городов.

13
00:00:47,840 --> 00:00:51,025
Компилятор говорит: окей,
я не дам тебе создать маршрут,

14
00:00:51,025 --> 00:00:53,340
не указывая конкретные названия городов.

15
00:00:53,340 --> 00:00:58,848
На самом деле компилятор это мог делать,
только помогая тем, по сути, кто

16
00:00:58,848 --> 00:01:03,590
еще не умеет работать с конструкторами или
кому просто не нужен конструктор в классе.

17
00:01:03,590 --> 00:01:05,741
Но теперь-то мы умеем писать конструкторы.

18
00:01:05,741 --> 00:01:08,939
Если мы все еще хотим
уметь создавать маршрут,

19
00:01:08,939 --> 00:01:10,990
не указывая конкретные названия городов,

20
00:01:10,990 --> 00:01:14,811
мы можем дописать так называемый
конструктор по умолчанию.

21
00:01:14,811 --> 00:01:18,743
Это будет, по сути, такой же метод,
у него будет такое же название,

22
00:01:18,743 --> 00:01:24,160
совпадающее с названием класса, у него не
будет параметров, ну и тело будет пустым.

23
00:01:24,160 --> 00:01:28,920
Если мы не хотим ничего делать по
умолчанию, не хотим никак инициализировать

24
00:01:28,920 --> 00:01:32,570
поля, мы, собственно, ничего писать
и не будем в теле этого конструктора.

25
00:01:32,570 --> 00:01:34,730
Такой конструктор ничего не делает.

26
00:01:34,730 --> 00:01:39,530
Раньше компилятор писал его за нас, когда
у нас еще не было никаких конструкторов,

27
00:01:39,530 --> 00:01:42,875
теперь же, когда мы дописали какой-то
конструктор, компилятор не стал

28
00:01:42,875 --> 00:01:45,860
сам дописывать этот код,
мы вполне себе сами можем его написать.

29
00:01:45,860 --> 00:01:50,245
Ну у нас может возникнуть
необходимость по умолчанию

30
00:01:50,245 --> 00:01:52,461
все-таки заполнять поля
чем-то осмысленным.

31
00:01:52,461 --> 00:01:55,421
Например, мы хотим чтобы по умолчанию,
когда я пишу название типа,

32
00:01:55,421 --> 00:01:58,526
название переменной и никак
не инициализирую, чтобы у меня

33
00:01:58,526 --> 00:02:02,625
по умолчанию был маршрут от Москвы до
Санкт-Петербурга — нет никаких проблем:

34
00:02:02,625 --> 00:02:05,088
я в конструкторе по умолчанию, в его теле,

35
00:02:05,088 --> 00:02:08,906
в фигурных скобках проинициализирую поля
source и destination так, как мне нужно.

36
00:02:08,906 --> 00:02:13,874
source = "Moscow", destination = "Saint
Petersburg", и вызову метод UpdateLength.

37
00:02:13,874 --> 00:02:17,480
Всё, чудесно,
у меня есть теперь маршрут по умолчанию.

38
00:02:17,480 --> 00:02:21,080
Я могу не думать, и у меня будет
какой-то валидный маршрут от Москвы до

39
00:02:21,080 --> 00:02:25,621
Санкт-Петербурга, никак даже
не надо инициализировать.

40
00:02:25,621 --> 00:02:26,450
Хорошо.

41
00:02:26,450 --> 00:02:29,832
Мы научились писать конструкторы,
давайте подробно разберемся,

42
00:02:29,832 --> 00:02:32,350
как же мы теперь можем эти
конструкторы вызывать.

43
00:02:32,350 --> 00:02:36,573
Какие нам будут плюшки от
этого удобного синтаксиса?

44
00:02:36,573 --> 00:02:39,800
Как мы сможем удобно создавать объекты?

45
00:02:39,800 --> 00:02:44,310
Итак, опять же, если я пишу название типа
и название переменной, точка с запятой,

46
00:02:44,310 --> 00:02:47,925
по сути, ничего не говорю о том,
как я хочу инициализировать переменную —

47
00:02:47,925 --> 00:02:50,080
у меня вызовется конструктор по умолчанию.

48
00:02:50,080 --> 00:02:54,109
В данном случае получится маршрут
от Москвы до Санкт-Петербурга.

49
00:02:54,109 --> 00:02:58,700
Если я после названия переменной поставлю
круглые скобки и в них перечислю что-то,

50
00:02:58,700 --> 00:03:02,579
то у меня вызовется параметризованный
конструктор и вот это вот что-то в круглых

51
00:03:02,579 --> 00:03:06,013
скобках передастся как
параметры в этот конструктор.

52
00:03:06,013 --> 00:03:09,890
А что если у меня есть функция,
которая принимает маршрут?

53
00:03:09,890 --> 00:03:13,613
Например, функция PrintRoute, которая
принимает маршрут по константной ссылке.

54
00:03:13,613 --> 00:03:15,860
Как мне передать конкретный
маршрут в эту функцию?

55
00:03:15,860 --> 00:03:19,145
Нужно ли мне действительно создавать
временную переменную маршрута,

56
00:03:19,145 --> 00:03:22,860
как-то ее инициализировать и потом эту
переменную подставлять в круглые скобки

57
00:03:22,860 --> 00:03:23,850
при вызове функции?

58
00:03:23,850 --> 00:03:25,049
Конечно, нет.

59
00:03:25,049 --> 00:03:29,770
Если я хочу передать в функцию
PrintRoute маршрут по умолчанию,

60
00:03:29,770 --> 00:03:35,714
я в списке аргументов функции пишу
Route и пустые круглые скобки сразу же.

61
00:03:35,714 --> 00:03:39,935
У меня, по сути, создался временный
маршрут по умолчанию и сразу передался

62
00:03:39,935 --> 00:03:42,590
в функцию, никакую переменную
заводить не понадобилось.

63
00:03:42,590 --> 00:03:46,838
Или даже можно короче: компилятор
все-таки не глупый, он видит,

64
00:03:46,838 --> 00:03:53,102
что функция PrintRoute принимает на вход
маршрут — объект типа Route, а я только

65
00:03:53,102 --> 00:03:57,240
что при вызове функции PrintRoute написал
явно Route и пустые круглые скобки.

66
00:03:57,240 --> 00:04:00,787
То есть компилятор и так знает,
что функция PrintRoute принимает на вход

67
00:04:00,787 --> 00:04:05,380
маршрут, а я все равно указал: «создай мне
маршрут по умолчанию, без параметров».

68
00:04:05,380 --> 00:04:07,030
Наверное, можно короче.

69
00:04:07,030 --> 00:04:11,563
Можно при вызове функции PrintRoute
написать пустые фигурные скобки.

70
00:04:11,563 --> 00:04:16,740
И это будет означать, что вы хотите при
вызове функции создать маршрут с помощью

71
00:04:16,740 --> 00:04:21,723
конструктора по умолчанию и этот
маршрут передать в эту функцию.

72
00:04:21,723 --> 00:04:24,705
А что если вы хотите использовать
параметризованный конструктор?

73
00:04:24,705 --> 00:04:26,550
Что если у вас есть
конкретные названия городов,

74
00:04:26,550 --> 00:04:29,810
вы хотите создать маршрут между
ними и передать его в функцию?

75
00:04:29,810 --> 00:04:35,190
В этом случае опять же вы можете
в списке аргументов функции написать

76
00:04:35,190 --> 00:04:40,020
название типа Route и в круглых скобках
сразу, не создавая никакой переменной,

77
00:04:40,020 --> 00:04:45,230
перечислить названия городов, перечислить
аргументы параметризованного конструктора.

78
00:04:45,230 --> 00:04:49,279
Или использовать уже известный
синтаксис с фигурными скобками:

79
00:04:49,279 --> 00:04:53,982
не указывая названия типа Route, просто
в списке аргументов функции поставить

80
00:04:53,982 --> 00:04:58,730
фигурные скобки и в них перечислить
аргументы параметризованного конструктора,

81
00:04:58,730 --> 00:05:00,965
просто перечислить названия городов.

82
00:05:00,965 --> 00:05:05,600
То есть, по сути, читая эту строчку,
любой программист поймет: ага, тот,

83
00:05:05,600 --> 00:05:11,070
кто писал этот код, хочет распечатать
маршрут от Звенигорода до Истры.

84
00:05:11,070 --> 00:05:16,549
Все очень понятно, код легко читается,
особенно с учетом того,

85
00:05:16,549 --> 00:05:22,230
что у меня название функции PrintRoute
отражает то, что она печатает маршрут.

86
00:05:22,230 --> 00:05:26,280
На самом деле,
вот так вот легко передавать конкретный

87
00:05:26,280 --> 00:05:29,038
маршрут можно не только в нашу функцию,
которую мы написали сами,

88
00:05:29,038 --> 00:05:33,105
например функцию PrintRoute, но и в
функции, которые уже встроены в язык.

89
00:05:33,105 --> 00:05:37,649
Например, если вы создадите вектор
маршрутов, вектор от Route — никаких

90
00:05:37,649 --> 00:05:42,056
проблем нет, — и захотите добавить в
этот вектор с помощью метода push_back

91
00:05:42,056 --> 00:05:46,172
конкретный маршрут, то вам не нужно будет
создавать переменную маршрута для этого.

92
00:05:46,172 --> 00:05:49,290
Вы опять же сможете использовать
синтаксис с фигурными скобками.

93
00:05:49,290 --> 00:05:53,430
Вы сможете в списке аргументов метода
push_back написать фигурные скобки

94
00:05:53,430 --> 00:05:55,250
и конкретные названия городов.

95
00:05:55,250 --> 00:06:00,420
Компилятор сам создаст за вас объект
и передаст его в метод push_back.

96
00:06:00,420 --> 00:06:03,806
Тот же самый удобный синтаксис с
фигурными скобками можно использовать,

97
00:06:03,806 --> 00:06:07,150
если вы хотите вернуть конкретный
маршрут из какой-то функции.

98
00:06:07,150 --> 00:06:10,650
Например, мы пишем простейшую
глупую функцию GetRoute,

99
00:06:10,650 --> 00:06:14,815
которая принимает флажочек is_empty,
и если этот флажочек — true,

100
00:06:14,815 --> 00:06:18,300
то мы хотим вернуть пустой маршрут,
маршрут по умолчанию.

101
00:06:18,300 --> 00:06:19,611
Как мы это сделаем?

102
00:06:19,611 --> 00:06:24,020
Если флажочек — true, напишем if,
и в этой ветке напишем return,

103
00:06:24,020 --> 00:06:27,985
и в return'е явно напишем, что мы хотим
вызвать конструктор по умолчанию,

104
00:06:27,985 --> 00:06:30,250
что мы хотим вернуть маршрут по умолчанию.

105
00:06:30,250 --> 00:06:34,390
Компилятор уже видит, что эта функция
возвращает объект типа Route,

106
00:06:34,390 --> 00:06:36,240
поэтому я не должен явно указывать,

107
00:06:36,240 --> 00:06:40,190
что я создаю маршрут — я в этом return'е
просто пишу пустые фигурные скобки.

108
00:06:40,190 --> 00:06:42,919
А если флажочек is_empty — false,
то я хочу вернуть

109
00:06:42,919 --> 00:06:47,420
какой-то непустой маршрут — опять же,
например, маршрут от Звенигорода до Истры.

110
00:06:47,420 --> 00:06:53,025
Я пишу return, и в фигурных скобках список
аргументов конструктора — две строки,

111
00:06:53,025 --> 00:06:55,497
которые передадутся в
параметризованный конструктор,

112
00:06:55,497 --> 00:06:57,520
опять же не надо явно
указывать название типа.

113
00:06:57,520 --> 00:07:02,292
На самом деле, конечно же,
конструкторы есть не только у ваших типов,

114
00:07:02,292 --> 00:07:05,590
которые вы написали, но и у
встроенных типов, например у вектора.

115
00:07:05,590 --> 00:07:08,840
Если вы пишете какую-то функцию,
которая хочет вернуть вектор,

116
00:07:08,840 --> 00:07:12,980
и в этой функции хотите вдруг
где-то вернуть пустой вектор,

117
00:07:12,980 --> 00:07:17,875
например где-то в условном операторе,
то вам не нужно писать vector&lt;int&gt;() —

118
00:07:17,875 --> 00:07:21,840
нет, вы можете просто написать
пустые фигурные скобки в return'е.

119
00:07:21,840 --> 00:07:27,430
Вы пишете «return {}» — компилятор видит,
что эта функция должна вернуть вектор,

120
00:07:27,430 --> 00:07:31,100
и сам создает за вас пустой
вектор и возвращает из функции.

121
00:07:31,100 --> 00:07:35,120
Или, может быть, вы хотите вернуть
вектор из конкретных чисел.

122
00:07:35,120 --> 00:07:38,486
Вам не нужно как-то специально его
создавать, заполнять — вы просто пишете

123
00:07:38,486 --> 00:07:42,110
return, и в фигурных скобках — список
чисел, которые должны быть в этом векторе.

124
00:07:42,110 --> 00:07:45,550
Опять же везде работает удобный
синтаксис — компилятор за вас понимает,

125
00:07:45,550 --> 00:07:48,070
какой тип должен иметь объект,
который вы создаете.