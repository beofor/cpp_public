1
00:00:00,000 --> 00:00:07,768
[БЕЗ СЛОВ] Обсудим такую
сущность как деструкторы.

2
00:00:07,768 --> 00:00:09,460
Что такое деструктор?

3
00:00:09,460 --> 00:00:11,592
Деструктор вызывается
при уничтожении объекта.

4
00:00:11,592 --> 00:00:13,523
Вы уже знаете,
что есть специальные методы,

5
00:00:13,523 --> 00:00:16,560
которые вызываются при создании объекта, —
называются конструкторы.

6
00:00:16,560 --> 00:00:19,523
Логично было бы предположить, что при
уничтожении объектов также вызываются

7
00:00:19,523 --> 00:00:22,766
некоторые специальные методы,
они называются деструкторами.

8
00:00:22,766 --> 00:00:24,980
Что принято делать в деструкторах?

9
00:00:24,980 --> 00:00:28,726
В деструкторах, как правило,
откатывают, отменяют те действия,

10
00:00:28,726 --> 00:00:31,910
которые были сделаны в
конструкторе или в других методах.

11
00:00:31,910 --> 00:00:35,042
Например, если вы почему-то вручную
выделяли память в конструкторе

12
00:00:35,042 --> 00:00:37,808
или других методах,
вы должны ее освободить в деструкторе.

13
00:00:37,808 --> 00:00:40,514
Если вы открыли файл в конструкторе
или в каком-то методе,

14
00:00:40,514 --> 00:00:42,220
вы в деструкторе должны его закрыть.

15
00:00:42,220 --> 00:00:46,222
Или, на самом деле, в деструкторе можно
делать всё, что вам нравится, всё,

16
00:00:46,222 --> 00:00:47,015
что вам нужно.

17
00:00:47,015 --> 00:00:50,302
Например, вывести какую-то
отладочную информацию.

18
00:00:50,302 --> 00:00:51,810
Давайте этим и займемся.

19
00:00:51,810 --> 00:00:55,590
Рассмотрим снова наш класс маршрута.

20
00:00:55,590 --> 00:01:00,003
В нём уже есть конструкторы: конструктор
без параметров, конструктор с параметрами,

21
00:01:00,003 --> 00:01:03,469
и методы GetSource, GetDestination,
GetLength и так далее.

22
00:01:03,469 --> 00:01:05,389
Давайте вот что сделаем.

23
00:01:05,389 --> 00:01:09,558
Мы в нашем классе вызываем метод
UpdateLength периодически,

24
00:01:09,558 --> 00:01:11,831
и как будто бы в реальной
жизни он может быть долгим —

25
00:01:11,831 --> 00:01:15,090
это сейчас у меня написано как
заглушка для функции ComputeDistance

26
00:01:15,090 --> 00:01:17,669
какое-то странное вычисление расстояния
между городами — на самом деле нет,

27
00:01:17,669 --> 00:01:19,830
в реальности это может быть
вхождение в какую-то базу данных.

28
00:01:19,830 --> 00:01:24,119
Поэтому этот метод может быть долгим
и поэтому в какой-то момент может

29
00:01:24,119 --> 00:01:27,060
получиться так,
что наш класс работает долго.

30
00:01:27,060 --> 00:01:32,475
Мы хотим понять почему, подозреваем в
замедлении работы метод UpdateLength, даже

31
00:01:32,475 --> 00:01:38,265
функцию ComputeDistance, и хотим как-то
замерить для конкретного объекта маршрута,

32
00:01:38,265 --> 00:01:43,903
для каких городов мы вызывали на протяжении
всей жизни этого объекта метод UpdateLength.

33
00:01:43,903 --> 00:01:46,236
Как мы это будем делать?

34
00:01:46,236 --> 00:01:52,250
Давайте создадим так называемый лог
вызовов функции ComputeDistance.

35
00:01:52,250 --> 00:01:56,830
То есть мы сделаем вектор в нашем классе —
вектор строк —

36
00:01:56,830 --> 00:02:00,110
который мы назовём compute_distance_log.

37
00:02:00,110 --> 00:02:02,360
В него будем писать информацию о том,

38
00:02:02,360 --> 00:02:05,820
для каких городов вызывалась
функция ComputeDistance.

39
00:02:05,820 --> 00:02:10,336
Раз мы используем вектор,
мы должны добавить

40
00:02:10,336 --> 00:02:15,778
заголовочный файл vector,
и теперь нужно этот вектор

41
00:02:15,778 --> 00:02:20,340
заполнять в методе UpdateLength.

42
00:02:20,340 --> 00:02:27,884
Пишем: compute_distance_log.push_back
от чего?

43
00:02:27,884 --> 00:02:36,016
От тех городов, для которых мы
вызываем функцию ComputeDistance.

44
00:02:36,016 --> 00:02:43,875
То есть source —
давайте разделим дефисом — и destination.

45
00:02:43,875 --> 00:02:44,629
Отлично.

46
00:02:44,629 --> 00:02:47,990
Давайте проверим, что класс компилируется.

47
00:02:47,990 --> 00:02:51,150
Кажется, всё хорошо.

48
00:02:51,150 --> 00:02:56,062
Теперь давайте найдем место,
где можно вывести этот лог.

49
00:02:56,062 --> 00:02:59,939
Можно было бы сделать какой-то метод,
который этот лог просто выведет,

50
00:02:59,939 --> 00:03:03,822
а можно сказать компилятору: как только
объект уничтожится, выведи, пожалуйста,

51
00:03:03,822 --> 00:03:05,940
весь этот лог вызова
функции ComputeDistance.

52
00:03:05,940 --> 00:03:08,521
Для этого мы напишем деструктор.

53
00:03:08,521 --> 00:03:10,660
Как мы напишем деструктор?

54
00:03:10,660 --> 00:03:13,665
У него нет возвращаемого значения,
как и у конструктора,

55
00:03:13,665 --> 00:03:16,130
потому что он просто
уничтожает объекты и всё,

56
00:03:16,130 --> 00:03:19,404
и у него название выглядит вот
так: тильда и название класса,

57
00:03:19,404 --> 00:03:20,877
в данном случае — ˜ Route.

58
00:03:20,877 --> 00:03:24,310
В этом деструкторе мы можем
написать какой угодно код.

59
00:03:24,310 --> 00:03:31,443
Например, мы можем пройтись
циклом по compute_distance_log.

60
00:03:31,443 --> 00:03:38,410
Давайте переберем с переменной
entry по compute_distance_log,

61
00:03:38,410 --> 00:03:44,209
обычный цикл по этому

62
00:03:44,209 --> 00:03:49,555
самому вектору, и здесь мы просто
в cout выводим эту строчку лога.

63
00:03:49,555 --> 00:03:51,680
В конце пишем перевод строки,

64
00:03:51,680 --> 00:03:56,980
чтобы каждая строчка лога была на
отдельной строке в файле, в выводе нашем.

65
00:03:56,980 --> 00:03:59,403
Казалось бы, можно компилировать код.

66
00:03:59,403 --> 00:04:01,273
Отлично, код компилируется.

67
00:04:01,273 --> 00:04:07,326
Давайте теперь, чтобы посмотреть, как он
работает, создадим переменную маршрута.

68
00:04:07,326 --> 00:04:14,770
Давайте изначально у нас будет
маршрут от Москвы до Санкт-Петербурга,

69
00:04:14,770 --> 00:04:19,870
потом мы вызовем метод SetSource,

70
00:04:19,870 --> 00:04:25,855
[БЕЗ СЛОВ] поменяем

71
00:04:25,855 --> 00:04:30,935
начало маршрута, например, на Выборг,

72
00:04:30,935 --> 00:04:37,870
а конец маршрута поменяем на Вологду.

73
00:04:37,870 --> 00:04:43,030
Что же выведет этот код?

74
00:04:43,030 --> 00:04:47,070
Компилируем, всё хорошо,

75
00:04:47,070 --> 00:04:50,689
запускаем и видим тот самый
compute_distance_log.

76
00:04:50,689 --> 00:04:55,672
Изначально мы создали маршрут
от Москвы до Санкт-Петербурга.

77
00:04:55,672 --> 00:04:56,500
Мы видим,

78
00:04:56,500 --> 00:05:02,050
что вызвалась функция ComputeDistance
от Москвы и Санкт-Петербурга.

79
00:05:02,050 --> 00:05:06,046
Затем мы поменяли начало маршрута на
Выборг, маршрут стал от Выборга до

80
00:05:06,046 --> 00:05:09,453
Санкт-Петербурга, что мы и видим
в нашем отладочном выводе.

81
00:05:09,453 --> 00:05:12,182
Наконец, мы поменяли конец
маршрута на Вологду,

82
00:05:12,182 --> 00:05:15,490
и вызвалась функция ComputeDistance
от Выборга и Вологды.

83
00:05:15,490 --> 00:05:17,745
Давайте посмотрим в отладчике,

84
00:05:17,745 --> 00:05:22,887
как же у нас происходили вызовы
конструкторов и деструктора.

85
00:05:22,887 --> 00:05:24,460
Запускаем.

86
00:05:24,460 --> 00:05:28,852
Итак, вот мы находимся в первой строчке,

87
00:05:28,852 --> 00:05:32,270
хотим создать объект маршрута.

88
00:05:32,270 --> 00:05:35,529
Понятно, что мы сразу
попадаем в конструктор —

89
00:05:35,529 --> 00:05:37,840
конструктор от двух параметров.

90
00:05:37,840 --> 00:05:42,160
В этом конструкторе мы инициализируем поле
source, инициализируем поле destination

91
00:05:42,160 --> 00:05:43,805
и вызываем метод UpdateLength.

92
00:05:43,805 --> 00:05:45,802
Можем посмотреть, как он вызывается.

93
00:05:45,802 --> 00:05:49,654
Сначала мы вызываем функцию
ComputeDistance, попадаем в неё,

94
00:05:49,654 --> 00:05:52,530
затем эта функция завершилась,

95
00:05:52,530 --> 00:05:58,560
и здесь мы добавляем новую запись в
compute_distance_log, как и ожидалось.

96
00:05:58,560 --> 00:06:03,060
Отлично, всё, конструктор закончился,

97
00:06:03,060 --> 00:06:07,280
переходим на следующую строчку функции
main, вызываем метод SetSource от Выборга.

98
00:06:07,280 --> 00:06:11,450
В этом методе мы меняем значение
поля source и опять вызываем метод

99
00:06:11,450 --> 00:06:15,275
UpdateLength, в котором, как и ожидалось,
вызываем функцию ComputeDistance,

100
00:06:15,275 --> 00:06:17,720
затем добавляем новую запись
в compute_distance_log.

101
00:06:17,720 --> 00:06:21,580
Отсюда мы тоже выходим.

102
00:06:21,580 --> 00:06:25,664
И, наконец, вызываем метод SetDestination,
в котором происходит ровно то же самое.

103
00:06:25,664 --> 00:06:28,340
Мы меняем destination,
попадаем в метод UpdateLength,

104
00:06:28,340 --> 00:06:30,107
здесь вызываем функцию ComputeDistance

105
00:06:30,107 --> 00:06:33,750
и затем добавляем
новую запись в compute_distance_log.

106
00:06:33,750 --> 00:06:40,230
Отлично, теперь нам пора
выходить из функции main,

107
00:06:40,230 --> 00:06:44,357
объект должен перестать существовать,
он больше не сможет быть использован,

108
00:06:44,357 --> 00:06:48,010
поскольку мы выйдем из функции main,
объект route нужно уничтожить.

109
00:06:48,010 --> 00:06:52,170
Когда объект нужно уничтожить,
компилятор вызывает деструктор,

110
00:06:52,170 --> 00:06:56,640
вот тот самый метод ˜ Route вызывается,
и здесь тот самый цикл for.

111
00:06:56,640 --> 00:07:03,880
В этом цикле for мы выводим
по одному элементу нашего вектора.

112
00:07:03,880 --> 00:07:08,570
Вот и всё, мы их все вывели,
объект уничтожился.